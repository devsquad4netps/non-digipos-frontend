const customTheme = {
  customTheme: {
    color: {
      primary: "#29647B",
      reject: "#ff0404"
    },
  },
  typography: {
    fontFamily: `Inter, sans-serif`,
    fontSize: 14,
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
  },
};

export default customTheme;
