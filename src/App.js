import React from "react";
import { Routes, Route } from "react-router-dom";
import Layout from "./components/Layout";

import Login from "./pages/Login";
import Dashboard from "./pages/Dashboard";

// SETTINGS PAGES
import MasterMerchant from "./pages/Settings/MasterMerchant";
import MasterMerchantAdd from "./pages/Settings/MasterMerchantAdd";
import MasterProduct from "./pages/Settings/MasterProduct";
import MasterUserManagement from "./pages/Settings/MasterUserManagement";
import MasterAttention from "./pages/Settings/MasterAttention";
import MasterInvoice from "./pages/Settings/MasterInvoice";
import MasterProductGroup from "./pages/Settings/MasterProductGroup";
import MasterMerchantEdit from "./pages/Settings/MasterMerchantEdit";
import MasterMenu from "./pages/Settings/MasterMenu";

//Receivable Pages
import MappingFakturPajak from "./pages/NonDigipos/Receivable/MappingFakturPajak";
import DistributeInvoiceBeforeSign from "./pages/NonDigipos/Receivable/DistributeInvoiceBeforeSign";
import UploadDistribusiAfterSign from "./pages/NonDigipos/Receivable/UploadDistribusiAfterSign";
import GenerateNoInvoice from "./pages/NonDigipos/Receivable/GenerateNoInvoice";
import InquiryReviewSummaryBar from "./pages/NonDigipos/Receivable/InquiryReviewSummaryBar";
import CreateTransaksiExcludeBA from "./pages/NonDigipos/Receivable/CreateTransaksiExcludeBA";
import UploadTransaksiExcludeBA from "./pages/NonDigipos/Receivable/UploadTransksiExcludeBA";

//Transaaction AR P2P
import CreateCustomerP2P from "./pages/NonDigipos/TransactionArP2P/CreateCustomerP2P";
import CreateTransaksiP2P from "./pages/NonDigipos/TransactionArP2P/CreateTransaksiP2P";
import UploadTransaksiP2P from "./pages/NonDigipos/TransactionArP2P/UploadTransaksiP2P";

//Transaction BA Pages
import GenerateBARNumber from "./pages/NonDigipos/TransactionBA/GenerateBARNumber";
import InqueryReviewSummary from "./pages/NonDigipos/TransactionBA/InqueryReviewSummary";
import SyncBigQuery from "./pages/NonDigipos/TransactionBA/SyncBigQuery";
import DistribusiBar from "./pages/NonDigipos/TransactionBA/DistribusiBar";
import ReviewSummaryMerchant from "./pages/NonDigipos/TransactionBA/ReviewSummaryMerchant";

//Transaction Tax Pages
import DistribusiBuktiPotong from "./pages/NonDigipos/TransactionTax/DistribusiBuktiPotong";
import DistribusiFakturPajak from "./pages/NonDigipos/TransactionTax/DistribusiFakturPajak";
import RegisterTaxNumber from "./pages/NonDigipos/TransactionTax/RegisterTaxNumber";
import InquiryGenerateCsvFakturPajak from "./pages/NonDigipos/TransactionTax/InquiryGenerateCsvFakturPajak";
import MasterBank from "./pages/Settings/MasterBank";
import MigrasiDataCsv from "./pages/NonDigipos/TransactionBA/MigrasiDataCsv";

function App() {
  return (
    <Routes>
      <Route path="/login" element={<Login />} />
      <Route path="/" element={<Layout />}>
        <Route path="/dashboard" element={<Dashboard />} />
        <Route
          path="/non-digipos/recievable-revenue-tools/upload-distribusi-after-sign"
          element={<UploadDistribusiAfterSign />}
        />
        <Route path="/settings/master-merchant" element={<MasterMerchant />} />
        <Route
          path="/settings/master-merchant/edit/:id"
          element={<MasterMerchantEdit />}
        />
        <Route
          path="/settings/master-merchant/add"
          element={<MasterMerchantAdd />}
        />
        <Route path="/settings/master-product" element={<MasterProduct />} />
        <Route
          path="/settings/user-management"
          element={<MasterUserManagement />}
        />
        <Route
          path="/settings/master-attention"
          element={<MasterAttention />}
        />
        <Route path="/settings/master-invoice" element={<MasterInvoice />} />
        <Route path="/settings/master-product" element={<MasterProduct />} />
        <Route
          path="/settings/master-attention"
          element={<MasterAttention />}
        />
        <Route
          path="/settings/master-product-group"
          element={<MasterProductGroup />}
        />
        <Route path="/settings/master-bank" element={<MasterBank />} />
        <Route path="/settings/master-menu" element={<MasterMenu />} />

        {/* RECEIVABLE PAGES */}
        <Route
          path="/non-digipos/receivable/mapping-faktur-pajak"
          element={<MappingFakturPajak />}
        />
        <Route
          path="/non-digipos/receivable/inquiry-review-summary-bar"
          element={<InquiryReviewSummaryBar />}
        />
        <Route
          path="/non-digipos/receivable/distribute-invoice-before-sign"
          element={<DistributeInvoiceBeforeSign />}
        />
        <Route
          path="/non-digipos/receivable/upload-distribusi-after-sign"
          element={<UploadDistribusiAfterSign />}
        />
        <Route
          path="/non-digipos/receivable/generate-no-invoice"
          element={<GenerateNoInvoice />}
        />

        {/* TRANSACTION BA PAGES */}
        <Route
          path="/non-digipos/transaction-ba/generate-bar-number"
          element={<GenerateBARNumber />}
        />
        <Route
          path="/non-digipos/transaction-ba/inquery-review-summary"
          element={<InqueryReviewSummary />}
        />
        <Route
          path="/non-digipos/import-data/migrasi-data-csv"
          element={<MigrasiDataCsv />}
        />
        <Route
          path="/non-digipos/transaction-ba/sync-big-query"
          element={<SyncBigQuery />}
        />
        <Route
          path="/non-digipos/transaction-ba/distribusi-bar"
          element={<DistribusiBar />}
        />
        <Route
          path="/non-digipos/transaction-ba/review-summary-merchant"
          element={<ReviewSummaryMerchant />}
        />

        {/* TRANSACTION TAX PAGES */}
        <Route
          path="/non-digipos/transaksi-tax/inquery-generate-csv-faktur-pajak"
          element={<InquiryGenerateCsvFakturPajak />}
        />
        <Route
          path="/non-digipos/transaksi-tax/register-tax-number"
          element={<RegisterTaxNumber />}
        />
        <Route
          path="/non-digipos/transaksi-tax/distribusi-bukti-potong"
          element={<DistribusiBuktiPotong />}
        />
        <Route
          path="/non-digipos/transaksi-tax/distribusi-faktur-pajak"
          element={<DistribusiFakturPajak />}
        />

        {/* TRANSACTION EXCLUDE BA PAGES */}
        <Route
          path="/non-digipos/recievable-exclude-ba/create-transaksi-exclude-ba"
          element={<CreateTransaksiExcludeBA />}
        />
        <Route
          path="/non-digipos/recievable-exclude-ba/upload-transaksi-exclude-ba"
          element={<UploadTransaksiExcludeBA />}
        />

        {/* TRANSACTION AR P2P PAGES */}
        <Route
          path="/non-digipos/transaksi-ar-p2p/create-customer-p2p"
          element={<CreateCustomerP2P />}
        />
        <Route
          path="/non-digipos/transaksi-ar-p2p/create-transaksi-p2p"
          element={<CreateTransaksiP2P />}
        />
        <Route
          path="/non-digipos/transaksi-ar-p2p/upload-transaksi-p2p"
          element={<UploadTransaksiP2P />}
        />

        <Route path="*" element={<h1>TIDAK ADA HALAMAN</h1>} />
      </Route>
    </Routes>
  );
}

export default App;
