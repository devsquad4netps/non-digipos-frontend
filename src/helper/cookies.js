import Cookie from "js-cookie";

export const getToken = () => {
  const tokens = Cookie.get("token");
  return tokens;
};

export const getUserId = () => {
  let user = Cookie.get("data-user-id");
  // user = JSON.parse("[" + user + "]");
  return user;
};

export const setToken = (data) => {
  Cookie.set("token", data.token);
  Cookie.set("token-notif", data.token_notif);
};

export const setUserId = (data) => {
  Cookie.set("data-user-id", data);
};

export const removeCookie = () => {
  Cookie.remove("data-user-id");
  Cookie.remove("token");
  Cookie.remove("token-notif");
};
