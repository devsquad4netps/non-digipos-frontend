import { Axios } from "../http";

export const SyncNewViews = async () => {
  try {
    const response = await Axios.get(
      "/merchant-sychronize-datasource/big-query/new-avaliable"
    );
    return response;
  } catch (error) {
    console.log(error);
  }
};

export const SyncLogViews = async () => {
  try {
    const response = await Axios.get(
      "merchant-sychronize-datasource/big-query/views"
    );
    return response;
  } catch (error) {
    console.log(error);
  }
};
