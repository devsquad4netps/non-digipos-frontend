import { Axios } from "../http";
import { getUserId } from "../cookies";
// merchant
const Login = async (body) => {
  let url = `/users/login`;
  let res = await Axios.post(url, body);
  return res;
};

const DataUser = async () => {
  let url = `/users/${getUserId()}`;
  let res = await Axios.get(url);
  return res;
};

const getMenu = async () => {
  let url = `/Menu`;
  let res = await Axios.get(url);
  return res;
};

const merchantList = () => {
  let url = `/merchant-action/views`;
  let res = Axios.get(url);
  return res;
};
const merchantGet = async (id) => {
  let url = `/merchant-action/view/${id}`;
  let res = await Axios.get(url);
  return res;
};
const merchantCreate = async (data) => {
  let url = `/merchant-action/create`;
  let res = await Axios.post(url, data);
  return res;
};
const merchantUpdate = async (id, data) => {
  let url = `/merchant-action/update/${id}`;
  let res = await Axios.post(url, data);
  return res;
};
const merchantDelete = async (id) => {
  let url = `/merchant-action/delete/${id}`;
  let res = await Axios.post(url);
  return res;
};

// merchant class
const merchantClassList = async () => {
  let url = `/merchant-action/merchant-class/views`;
  let res = await Axios.get(url);
  return res;
};
const merchantClassGet = async (id) => {
  let url = `/merchant-action/merchant-class/view/${id}`;
  let res = await Axios.get(url);
  return res;
};
const merchantClassCreate = async (data) => {
  let url = `/merchant-action/merchant-class/create`;
  let res = await Axios.post(url, data);
  return res;
};
const merchantClassUpdate = async (id, data) => {
  let url = `/merchant-action/merchant-class/update/${id}`;
  let res = await Axios.post(url, data);
  return res;
};
const merchantClassDelete = async (id) => {
  let url = `/merchant-action/merchant-class/delete/${id}`;
  let res = await Axios.get(url);
  return res;
};

//MERCHANT REKENING
const merchantRekeningCreate = async (id, body) => {
  let url = `/merchant-action/merchant-rekening/${id}/create`;
  let res = await Axios.post(url, body);
  return res;
};

const merchantRekeningUpdate = async (id, idRek, body) => {
  let url = `/merchant-action/merchant-rekening/${id}/update/${idRek}`;
  let res = await Axios.post(url, body);
  return res;
};

const merchantRekeningDelete = async (id, idRek) => {
  let url = `/merchant-action/merchant-rekening/${id}/delete/${idRek}`;
  let res = await Axios.post(url);
  return res;
};

const merchantRekeningActive = async (id, idRek) => {
  let url = `/merchant-action/merchant-rekening/${id}/actived/${idRek}`;
  let res = await Axios.post(url);
  return res;
};

const merchantRekeningDetail = async (id, idRek) => {
  let res = await Axios(
    `/merchant-action/merchant-rekening/${id}/view/${idRek}`
  );
  return res;
};

const merchantRekeningList = async (id) => {
  let url = `/merchant-action/merchant-rekening/${id}/views`;
  let res = await Axios.get(url);
  return res;
};

//MERCHANT PIC
const merchantPicCreate = async (id, body) => {
  let url = `/merchant-action/merchant-pic/${id}/create`;
  let res = await Axios.post(url, body);
  return res;
};

const merchantPicList = async (id) => {
  let url = `/merchant-action/merchant-pic/${id}/views`;
  let res = await Axios.get(url);
  return res;
};

const merchantPicDetail = async (id, idPic) => {
  let url = `/merchant-action/merchant-pic/${id}/view/${idPic}`;
  let res = await Axios.get(url);
  return res;
};

const merchantPicUpdate = async (id, idPic, body) => {
  let url = `/merchant-action/merchant-pic/${id}/update/${idPic}`;
  let res = await Axios.post(url, body);
  return res;
};

const merchantPicDelete = async (id, idPic) => {
  let url = `/merchant-action/merchant-pic/${id}/delete/${idPic}`;
  let res = await Axios.post(url);
  return res;
};

const merchantPicActive = async (id, idRek) => {
  let url = `/merchant-action/merchant-pic/${id}/actived/${idRek}`;
  let res = await Axios.post(url);
  return res;
};

// merchant product
const merchantProductList = async () => {
  let url = `/merchant-product-action/views`;
  let res = await Axios.get(url);
  return res;
};
const merchantProductGet = async (id) => {
  let url = `/merchant-product-action/view/${id}`;
  let res = await Axios.get(url);
  return res;
};
const merchantProductCreate = async (data) => {
  let url = `/merchant-product-action/create`;
  let res = await Axios.post(url, data);
  return res;
};
const merchantProductUpdate = async (id, data) => {
  let url = `/merchant-product-action/update/${id}`;
  let res = await Axios.post(url, data);
  return res;
};
const merchantProductDelete = async (id) => {
  let url = `/merchant-product-action/delete/${id}`;
  let res = await Axios.post(url);
  return res;
};
// merchant product tribe
const merchantProductTribeList = () => {
  let url = `/merchant-product-action/tribe/views`;
  let res = Axios.get(url);
  return res;
};
const merchantProductTribeGet = async (id) => {
  let url = `/merchant-product-action/tribe/view/${id}`;
  let res = await Axios.get(url);
  return res;
};
const merchantProductTribeCreate = async (data) => {
  let url = `/merchant-product-action/tribe/create`;
  let res = await Axios.post(url, data);
  return res;
};
const merchantProductTribeUpdate = async (id, data) => {
  let url = `/merchant-product-action/tribe/update/${id}`;
  let res = await Axios.post(url, data);
  return res;
};
const merchantProductTribeDelete = async (id) => {
  let url = `/merchant-product-action/tribe/delete/${id}`;
  let res = await Axios.get(url);
  return res;
};
// merchant product skema
const merchantProductSkemaList = () => {
  let url = `/merchant-product-action/skema/views`;
  let res = Axios.get(url);
  return res;
};
const merchantProductSkemaGet = async (id) => {
  let url = `/merchant-product-action/skema/view/${id}`;
  let res = await Axios.get(url);
  return res;
};
const merchantProductSkemaCreate = async (data) => {
  let url = `/merchant-product-action/skema/create`;
  let res = await Axios.post(url, data);
  return res;
};
const merchantProductSkemaUpdate = async (id, data) => {
  let url = `/merchant-product-action/skema/update/${id}`;
  let res = await Axios.post(url, data);
  return res;
};
const merchantProductSkemaDelete = async (id) => {
  let url = `/merchant-product-action/skema/delete/${id}`;
  let res = await Axios.get(url);
  return res;
};
// merchant product group
const merchantProductGroupList = async (MP_ID) => {
  let url = `/merchant-product-group-action/views?filter={"where" : {"and":[{"MP_ID" : ${MP_ID} }] } }`;
  let res = await Axios.get(url);
  return res;
};
const merchantProductGroupGet = async (id) => {
  let url = `/merchant-product-group-action/view/${id}`;
  let res = await Axios.get(url);
  return res;
};
const merchantProductGroupCreate = async (data) => {
  let url = `/merchant-product-group-action/create`;
  let res = await Axios.post(url, data);
  return res;
};
const merchantProductGroupUpdate = async (id, data) => {
  let url = `/merchant-product-group-action/update/${id}`;
  let res = await Axios.post(url, data);
  return res;
};
const merchantProductGroupDelete = async (id) => {
  let url = `/merchant-product-group-action/delete/${id}`;
  let res = await Axios.post(url);
  return res;
};
const merchantProductGroupRunQuery = async (data) => {
  let url = `/merchant-product-group-action/run-query`;
  let res = await Axios.post(url, data);
  return res;
};
// merchant product variable
const merchantProductVariableList = async (MPG_ID) => {
  let url = `/merchant-product-variable-action/${MPG_ID}/views`;
  let res = await Axios.get(url);
  return res;
};
const merchantProductVariableGet = async (id) => {
  let url = `/merchant-product-variable-action/view/${id}`;
  let res = await Axios.get(url);
  return res;
};
const merchantProductVariableCreate = async (data) => {
  let url = `/merchant-product-variable-action/create`;
  let res = await Axios.post(url, data);
  return res;
};
const merchantProductVariableUpdate = async (id, data) => {
  let url = `/merchant-product-variable-action/update/${id}`;
  let res = await Axios.post(url, data);
  return res;
};
const merchantProductVariableDelete = async (id) => {
  let url = `/merchant-product-variable-action/delete/${id}`;
  let res = await Axios.post(url);
  return res;
};
// merchant query
const merchantProductQueryGet = async (id) => {
  let url = `/merchant-product-query-action/${id}/views`;
  let res = await Axios.get(url);
  return res;
};
const merchantProductQueryUpdate = async (id, data) => {
  let url = `/merchant-product-query-action/update/${id}`;
  let res = await Axios.post(url, data);
  return res;
};
// merchant product condition
const merchantProductConditionGet = async (id) => {
  let url = `/merchant-condition-action/${id}/views/`;
  let res = await Axios.get(url);
  return res;
};
const merchantProductConditionCreate = async (data) => {
  let url = `/merchant-condition-action/create`;
  let res = await Axios.post(url, data);
  return res;
};
const merchantProductConditionGetById = async (MPC_ID) => {
  let url = `/merchant-condition-action/view/${MPC_ID}`;
  let res = await Axios.get(url);
  return res;
};
const merchantProductConditionUpdate = async (id, data)  => {
  let url = `/merchant-condition-action/update/${id}`;
  let res = await Axios.post(url, data);
  return res;
};

// MPV_TYPE_KEY
const ddlMPV_TYPE_KEY = async () => {
  let url = `/merchant-product-group-action/mvp-type-key/views`;
  let res = await Axios.get(url);
  return res;
};
// MPV_TYPE
const ddlMPV_TYPE = async () => {
  let url = `/merchant-product-group-action/mvp-type/views`;
  let res = await Axios.get(url);
  return res;
};

//Attention
const masterAttentionList = async (pic_type) => {
  let url = `/finarya-attention/non-digipos/pic/views?filter={"where" : {"and":[{"PIC_TYPE" : ${pic_type} }] } }`;
  let res = await Axios.get(url);
  return res;
};

const masterAttentionCreate = async (data) => {
  let url = `/finarya-attention/non-digipos/pic/create`;
  let res = await Axios.post(url, data, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  });
  return res;
};
const masterAttentionUpdate = async (id, data) => {
  let url = `/finarya-attention/non-digipos/pic/update/${id}`;
  let res = await Axios.post(url, data);
  return res;
};
const masterAttentionGet = (id) => {
  let url = `/finarya-attention/non-digipos/pic/view/${id}`;
  let res = Axios.get(url);
  return res;
};
const masterAttentionDelete = async (id) => {
  let url = `/finarya-attention/non-digipos/pic/delete/${id}`;
  let res = await Axios.post(url);
  return res;
};
const MasterView = async () => {
  let url = `/merchant-transaksi-master-action/views`;
  let res = await Axios.get(url);
  return res;
};

//Master Bank
const masterBankList = async () => {
  let url = `/master-bank/views`;
  let res = await Axios.get(url);
  return res;
};
const masterBankGet = (id) => {
  let url = `/master-bank/view/${id}`;
  let res = Axios.get(url);
  return res;
};
const masterBankCreate = async (data) => {
  let url = `/master-bank/create`;
  let res = await Axios.post(url, data);
  return res;
};
const masterBankUpdate = async (id, data) => {
  let url = `/master-bank/update/${id}`;
  let res = await Axios.post(url, data);
  return res;
};
const masterBankDelete = async (id) => {
  let url = `/master-bank/delete/${id}`;
  let res = await Axios.post(url);
  return res;
};

//users
const masterUserList = async () => {
  let url = `/users`;
  let res = await Axios.get(url);
  return res;
};
const masterUserGet = (id) => {
  let url = `/users/${id}`;
  let res = Axios.get(url);
  return res;
};
const masterUserCreate = async (data) => {
  let url = `/signup`;
  let res = await Axios.post(url, data);
  return res;
};
const masterUserUpdate = async (id, data) => {
  let url = `/users/${id}`;
  let res = await Axios.put(url, data);
  return res;
};
const masterUserDelete = async (id) => {
  let url = `/users/${id}`;
  let res = await Axios.delete(url);
  return res;
};

//role
const masterRoleList = async () => {
  let url = `/Roles`;
  let res = await Axios.get(url);
  return res;
};
//upload bar sign
const uploadBarSign = async (data) => {
  let url = `/distribusi-upload/merchant/upload/bbfaa827-83d9-4d03-8dcf-8fdb95ed801c/bar-sign`;
  let res = await Axios.post(url, data);
  return res;
};

//upload bukti potong
const uploadBuktiPotong = async (data) => {
  let url = `/distribusi-upload/merchant/upload/bbfaa827-83d9-4d03-8dcf-8fdb95ed801c/bukti-potong`;
  let res = await Axios.post(url, data);
  return res;
};
//transaction tax
const registerNoFakturPajak = async (data) => {
  let url = `/merchant-faktur-pajak/register-nomor`;
  let res = await Axios.post(url, data);
  return res;
};
const getFakturPajakSummaryRequest = async () => {
  let url = `/faktur-pajak/summary/request`;
  let res = await Axios.get(url);
  return res;
};
const getFakturPajakSummaryTotal= async () => {
  let url = `/faktur-pajak/summary/total`;
  let res = await Axios.get(url);
  return res;
};
const cancelFakturPajakSummaryResult = async (data) => {
  let url = `/merchant-faktur-pajak/register-nomor/cancel-no-faktur`;
  let res = await Axios.post(url, data);
  return res;
};
//master transaction
const getMasterTransaction = async () => {
  let url = `/merchant-transaksi-master-action/views`;
  let res = await Axios.get(url);
  return res;
};


//upload faktur pajak
const uploadFakturPajak = async (data) => {
  let url = `/distribusi-upload/merchant/upload/bbfaa827-83d9-4d03-8dcf-8fdb95ed801c/faktur-pajak`;
  let res = await Axios.post(url, data);
  return res;
};

//merchant transaksi
const merchantTransaksiList = async () => {
  let url = `/merchant-transaksi-master-action/views`;
  let res = await Axios.get(url);
  return res;
};

//upload migrasi data csv
const migrasiDataCsv = async () => {
  let url = `/import-excel-datasource/views`;
  let res = await Axios.get(url);
  return res;
};
const uploadMigrasiDataCsv = async (data) => {
  let url = `/import-excel-datasource/upload`;
  let res = await Axios.post(url, data);
  return res;
};


export default {
  getMenu,
  MasterView,
  Login,
  DataUser,
  merchantList,
  merchantGet,
  merchantCreate,
  merchantUpdate,
  merchantDelete,
  merchantRekeningCreate,
  merchantRekeningDetail,
  merchantRekeningList,
  merchantRekeningUpdate,
  merchantRekeningDelete,
  merchantRekeningActive,
  merchantPicList,
  merchantPicCreate,
  merchantPicDetail,
  merchantPicUpdate,
  merchantPicDelete,
  merchantPicActive,
  merchantClassList,
  merchantClassGet,
  merchantClassCreate,
  merchantClassUpdate,
  merchantClassDelete,
  merchantProductList,
  merchantProductGet,
  merchantProductCreate,
  merchantProductUpdate,
  merchantProductDelete,
  merchantProductTribeList,
  merchantProductTribeGet,
  merchantProductTribeCreate,
  merchantProductTribeUpdate,
  merchantProductTribeDelete,
  merchantProductSkemaList,
  merchantProductSkemaGet,
  merchantProductSkemaCreate,
  merchantProductSkemaUpdate,
  merchantProductSkemaDelete,
  merchantProductGroupList,
  merchantProductGroupGet,
  merchantProductGroupCreate,
  merchantProductGroupUpdate,
  merchantProductGroupDelete,
  merchantProductGroupRunQuery,
  merchantProductVariableList,
  merchantProductVariableGet,
  merchantProductVariableCreate,
  merchantProductVariableUpdate,
  merchantProductVariableDelete,
  masterAttentionList,
  masterAttentionGet,
  masterAttentionDelete,
  masterAttentionCreate,
  masterAttentionUpdate,
  ddlMPV_TYPE_KEY,
  ddlMPV_TYPE,
  masterBankList,
  masterBankGet,
  masterBankDelete,
  masterBankCreate,
  masterBankUpdate,
  masterUserList,
  masterUserGet,
  masterUserCreate,
  masterUserUpdate,
  masterUserDelete,
  masterRoleList,
  uploadBuktiPotong,
  uploadFakturPajak,
  uploadBarSign,
  merchantTransaksiList,
  registerNoFakturPajak,
  getFakturPajakSummaryRequest,
  getFakturPajakSummaryTotal,
  cancelFakturPajakSummaryResult,
  getMasterTransaction,
  merchantProductQueryGet,
  merchantProductQueryUpdate,
  migrasiDataCsv,
  merchantProductConditionGet,
  merchantProductConditionCreate,
  merchantProductConditionGetById,
  merchantProductConditionUpdate,
  uploadMigrasiDataCsv,
};
