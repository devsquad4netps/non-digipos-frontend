import axios from "axios";
import { getUserId, getToken } from "./cookies";
import { Snackbar, Alert } from "@mui/material";
import Swal from "sweetalert2";
import { removeCookie } from "./cookies";
// import mHelper from "../config/Helper"

const URL_API = "http://financed.4netps.co.id/finance-endpoint/";

export const Axios = axios.create({
  baseURL: URL_API,
  headers: {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
  },
});

Axios.interceptors.request.use(
  (config) => {
    const tokens = getToken();
    if (tokens) {
      config.headers["Authorization"] = `Bearer ${tokens}`;
    }
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

Axios.interceptors.response.use(
  function (response) {
    // Do something with response data
    return response;
  },
  function (error) {
    // Do something with response error
    errorHandler(error);
    return Promise.reject(error);
  }
);

export const errorHandler = (error) => {
  if (error.response && error.response.data.error) {
    // if (error.response.data.error.name === "UnauthorizedError") {
    //   Swal.fire({
    //     icon: "error",
    //     title: "Error",
    //     text: error,
    //     showConfirmButton: false,
    //     timer: 3000,
    //   })
    //     .then(() => removeCookie())
    //     .finally(() => (window.location.href = "/login"));
    // }
  } else {
    return error;
  }
};

// eslint-disable-next-line import/no-anonymous-default-export
export default { Axios, errorHandler };
