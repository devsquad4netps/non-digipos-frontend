import { Axios } from "./http";
import Swal from "sweetalert2";

export const CONVERT_MONTH = (val) => {
  const date = new Date(val);
  const monthList = [
    "Januari",
    "Febuari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember",
  ];
  return val ? monthList[date.getMonth()] : "No Data";
};

export const GENERATE_PULUHAN = (nominal) => {
  var angka = Number(nominal);
  var rounded = Number.isInteger(angka) ? angka : angka.toFixed(2);
  var number_string = rounded.toString(),
    separator = "",
    split = number_string.split("."),
    sisa = split[0].length % 3,
    rupiah = split[0].substr(0, sisa),
    ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);

  if (ribuan) {
    separator = sisa ? "," : "";
    rupiah += separator + ribuan.join(",");
  }
  rupiah = split[1] !== undefined ? rupiah + "." + split[1] : rupiah;
  return rupiah;
};

export const VIEW_FILE_BASE64 = (res) => {
  var byteCharacters = atob(res);
  var byteNumbers = new Array(byteCharacters.length);
  for (var i = 0; i < byteCharacters.length; i++) {
    byteNumbers[i] = byteCharacters.charCodeAt(i);
  }
  var byteArray = new Uint8Array(byteNumbers);
  var file = new Blob([byteArray], { type: "application/pdf;base64" });
  var fileURL = URL.createObjectURL(file);
  window.open(fileURL);
};

export const VIEW_FILE_PDF = (val, id) => {
  const { MPDOC_NAME, MPDOC_TYPE } = val;
  Axios.post(
    `/document-view/file/${
      MPDOC_TYPE === 1
        ? "bar"
        : MPDOC_TYPE === 2
        ? "bar/sign"
        : MPDOC_TYPE === 3
        ? "invoice"
        : MPDOC_TYPE === 4
        ? "invoice/sign"
        : MPDOC_TYPE === 5
        ? "faktur-pajak"
        : MPDOC_TYPE === 6
        ? "bukti-potong"
        : ""
    }/${id}/`,
    {
      names: MPDOC_NAME,
    }
  ).then(({ data }) => {
    if (data.success) {
      VIEW_FILE_BASE64(data.data);
    } else {
      Swal.fire({
        icon: data.success ? "success" : "error",
        title: data.success ? "Success" : "Error",
        text: data.message,
      });
    }
  });
};

export const SEARCH_DATA_TABLE = (datas, searchText) => {
  var searchInfo = searchText.trim().toUpperCase();
  var dataSearch = [];
  if (searchInfo != "") {
    for (var i in datas) {
      var textFound = [];
      var info = datas[i];
      for (var j in info) {
        var thisText = info[j];
        if (j == "id" || j == "ID") {
          continue;
        }
        if (thisText != null || thisText instanceof Array) {
          if (thisText.toString().toUpperCase().includes(searchInfo) == false) {
            if (thisText.length > 0) {
              for (var k in thisText) {
                var newInfo = thisText[k];
                for (var l in newInfo) {
                  var newText = newInfo[l];
                  if (newText !== null) {
                    if (
                      newText.toString().toUpperCase().includes(searchInfo) ==
                      false
                    ) {
                      textFound.push("0");
                    } else {
                      textFound.push("1");
                    }
                  }
                }
              }
            }
          } else {
            textFound.push("1");
          }
        } else {
          textFound.push("0");
        }
      }
      if (textFound.indexOf("1") == -1) {
        continue;
      }
      dataSearch.push(info);
    }
  } else {
    dataSearch = datas;
  }
  return dataSearch;
};
