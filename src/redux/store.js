import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import globalReducer from "./globalReducer";

const reducers = combineReducers({
  globalReducer,
});

const store = createStore(reducers);
export default store;
