import moment from "moment";
let d = new Date();
d.setMonth(d.getMonth() - 1);

const initialState = {
  period: moment(d).format("YYYY-MM"),
  dataUser: {},
  refresh: false,
  dataSearch: {
    id: "",
    value: "",
  },
};

const globalReducer = (state = initialState, action) => {
  switch (action.type) {
    case "PERIOD":
      return { ...state, period: action.payload };
    case "DATA_USER":
      return { ...state, dataUser: action.payload };
    case "REFRESH":
      return { ...state, refresh: action.payload };
    case "DATA_SEARCH":
      return { ...state, dataSearch: action.payload };
    default:
      return state;
  }
};

export default globalReducer;
