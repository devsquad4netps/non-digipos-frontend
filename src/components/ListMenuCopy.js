import React, { useEffect, useState } from "react";
import { List, Grid } from "@mui/material";
import ListItemButton from "@mui/material/ListItemButton";
import Collapse from "@mui/material/Collapse";
import { makeStyles } from "@mui/styles";
import { Link } from "react-router-dom";
import master from "../helper/services/master";

import {
  DashboardIcon,
  PaperIcon,
  ChildMenuIconFirst,
  ChildMenuIconSecond,
  ChildMenuPaperIcon,
  ArrowIcon,
  SettingIcons,
} from "../assets/icons/customIcon";

const useStyles = makeStyles({
  root: {
    width: "100%",
    paddingRight: "24px",
    "& .MuiListItemText-root": {
      margin: 0,
      paddingTop: "6px",
      paddingBottom: "6px",
    },
    "& .MuiListItemButton-root": {
      padding: 0,
      // border: "1px solid",
    },
    "& span": {
      maxWidth: "180px",
    },
  },
});

export default function ListMenuCopy({ menus, setChoseId }) {
  const classes = useStyles();

  const getMenu = async () => {
    try {
      const menus = await master.getMenu();
      console.log(menus);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    // getMenu();
  }, []);

  return (
    <Grid
      sx={{ paddingBottom: "100px", width: "100%" }}
      className="ndgp-sidebar-menu"
    >
      <List
        component="nav"
        className={classes.root}
        aria-labelledby="nested-list-subheader"
        sx={{ width: "auto" }}
      >
        <MenusFunction res={menus} setChoseId={setChoseId} />
      </List>
    </Grid>
  );
}

const LoopFunction = ({ res, url, setChoseId }) => {
  return (
    <MenusFunction
      res={res}
      child={true}
      urlChild={url}
      setChoseId={setChoseId}
    />
  );
};

const MenusFunction = ({ res, child, urlChild, setChoseId }) => {
  const [open, setOpen] = useState(null);

  // console.log(setChoseId);

  const handleClick = (val) => {
    setOpen(val.child.length > 0 && open === val.id ? null : val.id);
    setChoseId(val.child.length > 0 && open === val.id ? null : val);
  };

  return res.map((val, i) => {
    return (
      <Grid
        key={i}
        sx={{
          "& .link-route": {
            textDecoration: "none",
          },
        }}
      >
        <Link to="#" className="link-route">
          <ListItemButton
            onClick={() => {
              handleClick(val);
            }}
            sx={{
              marginBottom: "12px",
              minWidth: "180px",
              backgroundColor: open === val.id && !child && "#29647B",
            }}
          >
            <Grid
              sx={{
                display: "flex",
                alignItems: "center",
                paddingLeft: "24px",
                paddingTop: "6px",
                paddingBottom: "6px",
                justifyContent: "space-between",
                borderLeft:
                  open === val.id &&
                  val.child.length === 0 &&
                  child &&
                  "6px solid #29647B",
                width: "100%",
                backgroundColor:
                  open === val.id &&
                  val.child.length === 0 &&
                  child &&
                  "whiteSmoke",
                "& svg": {
                  marginRight: "8px",
                  color:
                    open === val.id && !child
                      ? "white"
                      : open === val.id && child
                      ? "#29647B"
                      : "rgba(0, 0, 0, 0.5)",
                },
                "& h4": {
                  fontWeight: 600,
                  fontSize: "14px",
                  lineHeight: "16px",
                  margin: 0,
                  color:
                    open === val.id && !child
                      ? "white"
                      : open === val.id && child
                      ? "#29647B"
                      : "rgba(0, 0, 0, 0.5)",
                  maxWidth: "180px",
                  textDecoration: "none",
                },
                "&:hover": {
                  "& h4, svg": {
                    color: "#29647B",
                  },
                },
                "& .arrow-icon": {
                  transform: open === val.id ? "" : "rotate(-90deg)",
                  transition: "0.3s",
                },
              }}
            >
              <Grid
                sx={{
                  display: "flex",
                  alignItems: "center",
                }}
              >
                {val.icon || ""}
                <h4 data={val.child}>{val.M_Name}</h4>
              </Grid>
              {val.child.length > 0 && <ArrowIcon className="arrow-icon" />}
            </Grid>
          </ListItemButton>
        </Link>
        {val.child && (
          <Collapse
            in={open === val.id}
            timeout="auto"
            unmountOnExit
            sx={{ marginLeft: "10px" }}
          >
            <LoopFunction
              res={val.child}
              url={urlChild ? urlChild + val.M_Link : val.M_Link}
              setChoseId={setChoseId}
            />
          </Collapse>
        )}
      </Grid>
    );
  });
};
