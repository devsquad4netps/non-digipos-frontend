import React, { useEffect, useState } from "react";
import CustomModal from "./CustomModal";
import { Grid, CircularProgress } from "@mui/material";
import { Axios } from "../helper/http";
import { CheckIcon } from "../assets/icons/customIcon";
import moment from "moment";
import EmptyData from "./EmptyData";

export default function ModalLogDistribute({ open, onClose }) {
  const [dataLog, setDataLog] = useState([]);

  useEffect(() => {
    console.log("HALO");
    Axios.get(
      `/distribusi-upload/merchant/distribute-log/${open.MPM_ID}/views`
    ).then((res) => {
      console.log(res.data.datas);
      setDataLog(res.data.datas);
    });
  }, []);

  return (
    <CustomModal width="80%" open={Boolean(open)} onClose={onClose}>
      <Grid
        sx={({ customTheme }) => ({
          "& .content": {
            flexDirection: "row",
            display: "flex",
            "& .content-left": {
              textAlign: "center",
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              justifyContent: "center",
              "& .line": {
                width: "6px",
                height: "100%",
                flex: 1,
              },
              "& .circle": {
                width: "42px",
                height: "42px",
                padding: "6px",
              },
            },
            "& .content-right": {
              width: "100%",
              padding: "12px",
              "& .label": {
                minWidth: "140px",
              },
              "& p": {
                margin: 0,
                fontSize: "14px",
              },
              "& span": {
                marginRight: "6px",
              },
              "& .item": {
                marginBottom: "6px",
              },
            },
          },
        })}
      >
        {dataLog.length > 0 ? (
          dataLog.map((res, i) => {
            return (
              <Grid className="content" display="flex" position="relative">
                <Grid className="content-left">
                  <Grid
                    className="line"
                    sx={({ customTheme }) => ({
                      backgroundColor: customTheme.color.primary,
                    })}
                  />
                  <Grid className="circle">
                    {res.SEND_FLAG === 2 ? <CheckIcon /> : <CircularProgress />}
                  </Grid>
                  <Grid
                    className="line"
                    sx={({ customTheme }) => ({
                      backgroundColor: customTheme.color.primary,
                    })}
                  />
                </Grid>
                <Grid className="content-right" container>
                  <Grid item md={6}>
                    <Grid display="flex" className="item">
                      <p className="label">Send To</p>
                      <span>:</span>
                      <p>{res.SEND_CC}</p>
                    </Grid>
                    <Grid display="flex" className="item">
                      <p className="label">Email account</p>
                      <span>:</span>
                      <p>{res.MR_ACOUNT_NUMBER}</p>
                    </Grid>
                    <Grid display="flex" className="item">
                      <p className="label">Send At</p>
                      <span>:</span>
                      <p>
                        {moment(Number(res.CREATE_SEND)).format(
                          "YYYY-MM-DD HH:mm"
                        )}
                      </p>
                    </Grid>
                    <Grid display="flex" className="item">
                      <p className="label">Message</p>
                      <span>:</span>
                      <p>{res.MPDIST_MESSAGE}</p>
                    </Grid>
                  </Grid>
                  <Grid item md={6} container>
                    <Grid item md={6}>
                      <Grid display="flex" className="item">
                        <p className="label">Send Bar</p>
                        <span>:</span>
                        <p>{res.SEND_BAR ? "True" : "False"}</p>
                      </Grid>
                      <Grid display="flex" className="item">
                        <p className="label">Send Bar Sign</p>
                        <span>:</span>
                        <p>{res.SEND_BAR_SIGN ? "True" : "False"}</p>
                      </Grid>
                      <Grid display="flex" className="item">
                        <p className="label">Send Bukpot</p>
                        <span>:</span>
                        <p>{res.SEND_BUKPOT ? "True" : "False"}</p>
                      </Grid>
                      <Grid display="flex" className="item">
                        <p className="label">Send Invoice Sign</p>
                        <span>:</span>
                        <p>{res.SEND_INVOICE_SIGN ? "True" : "False"}</p>
                      </Grid>
                    </Grid>
                    <Grid item md={6}>
                      <Grid display="flex" className="item">
                        <p className="label">Send Faktur</p>
                        <span>:</span>
                        <p>{res.SEND_FAKTUR ? "True" : "False"}</p>
                      </Grid>
                      <Grid display="flex" className="item">
                        <p className="label">Send Invoice</p>
                        <span>:</span>
                        <p>{res.SEND_INVOICE ? "True" : "False"}</p>
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
                {dataLog.length - 1 !== i && (
                  <Grid
                    className=""
                    sx={({ customTheme }) => ({
                      border: `1px solid #BDBDBD`,
                      position: "absolute",
                      width: "93.5%",
                      bottom: "0px",
                      display: "flex",
                      flex: 1,
                      left: "4.5%",
                    })}
                  />
                )}
              </Grid>
            );
          })
        ) : (
          <EmptyData />
        )}
      </Grid>
    </CustomModal>
  );
}
