import React, { useState, useEffect } from "react";
import TableComponent from "../../TableComponent";
import { Checkbox, Grid, Menu, MenuItem } from "@mui/material";
import { CrossIcon, CheckIcon } from "../../../assets/icons/customIcon";
import CustomButon from "../../CustomButon";
import { Axios } from "../../../helper/http";
import Swal from "sweetalert2";
import CustomChip from "../../CustomChip";
import { CONVERT_MONTH } from "../../../helper/globalFuncion";
import { VIEW_FILE_BASE64 } from "../../../helper/globalFuncion";
import ModalManage from "../../ModalManage";
import ModalUploadBulk from "../../ModalUploadBulk";
import { GENERATE_PULUHAN, VIEW_FILE_PDF } from "../../../helper/globalFuncion";
import ModalDistribute from "../../ModalDistribute";
import { useSelector } from "react-redux";
import ModalLogDistribute from "../../ModalLogDistribute";
import moment from "moment/moment";

export default function DistribusiBarTable() {
  const { period, refresh } = useSelector(({ globalReducer }) => globalReducer);
  const [open, setOpen] = useState(false);
  const [rows, setRows] = useState([]);
  const [loading, setLoading] = useState([]);
  const [selected, setSelected] = useState([]);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [modalManage, setModalManage] = useState();
  const [modalBulk, setModalBulk] = useState(false);
  const [modalLogDistribute, setModalLogDistribute] = useState(false);

  const openMenu = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleOpen = () => {
    if (selected.length > 0) {
      setOpen(true);
    } else {
      Swal.fire({
        icon: "warning",
        title: "tidak ada data yang dipilih",
        text: "mohon pilih data yang akan di distribusi",
      });
    }
  };

  console.log();

  const columns = [
    [
      {
        name: (
          <Checkbox
            checked={rows.length === selected.length}
            indeterminate={
              selected.length > 0 && selected.length !== rows.length
            }
            onClick={() => {
              selected.length > 0
                ? setSelected([])
                : setSelected(rows.map((val) => val.MPM_ID));
            }}
            sx={{
              color: "white",
              "&.Mui-checked": {
                color: "white",
              },
              "&.MuiCheckbox-indeterminate": {
                color: "white",
              },
            }}
          />
        ),
        rowSpan: 2,
        renderCell: ({ MPM_ID }, i) => (
          <Checkbox
            checked={selected.includes(MPM_ID)}
            onClick={(e) => {
              e.stopPropagation();
              if (e.target.checked) {
                setSelected([...selected, MPM_ID]);
              } else {
                let data = [...selected];
                setSelected(data.filter((val) => val !== MPM_ID));
              }
            }}
          />
        ),
        width: 10,
      },
      {
        name: "Number BAR",
        rowSpan: 2,
        renderCell: ({ MPM_NO_BAR }, i) => MPM_NO_BAR,
      },
      {
        name: (
          <div>
            ID <br /> Merchant
          </div>
        ),
        rowSpan: 2,
        renderCell: ({ M_ID }) => M_ID,
        width: 70,
      },
      {
        name: "Nama Merchant",
        rowSpan: 2,
        renderCell: ({ M_LEGAL_NAME }) => M_LEGAL_NAME,
        width: 200,
      },
      {
        name: "Transaksi Merchant",
        colSpan: 8,
        heading: true,
      },
    ],
    [
      {
        name: "Tahun",
        renderCell: ({ PERIODE }) => new Date(PERIODE).getFullYear(),
      },
      {
        name: "Bulan",
        renderCell: ({ PERIODE }) => CONVERT_MONTH(PERIODE),
      },
      {
        name: "MDR",
        renderCell: ({ MPM_MDR }) =>
          MPM_MDR ? GENERATE_PULUHAN(MPM_MDR) : "No Data",
      },
      {
        name: "TRX",
        renderCell: ({ MPM_AMOUNT }) =>
          MPM_AMOUNT ? GENERATE_PULUHAN(MPM_AMOUNT) : "No Data",
      },
      // {
      //   name: "Total Amount",
      //   renderCell: ({ MPM_AMOUNT }) =>
      //     MPM_AMOUNT ? GENERATE_PULUHAN(MPM_AMOUNT) : "No Data",
      // },
      {
        name: "Total",
        renderCell: ({ MPM_TOTAL }) =>
          MPM_TOTAL ? GENERATE_PULUHAN(MPM_TOTAL) : "No Data",
      },
      {
        name: "Status",
        renderCell: (data) => <CustomChip.Status data={data} />,
      },
      {
        name: "View File PDF",
        renderCell: ({ DOCUMENT, MPM_ID }) => (
          <Grid>
            <CustomChip MPM_ID={DOCUMENT} onClick={handleClick} label="View" />
            <Menu
              id="basic-menu"
              anchorEl={anchorEl}
              open={openMenu}
              onClose={handleClose}
              MenuListProps={{
                "aria-labelledby": "basic-button",
              }}
            >
              {DOCUMENT.map((val) => {
                return (
                  <MenuItem onClick={() => VIEW_FILE_PDF(val, MPM_ID)}>
                    {val?.MPDOC_NAME}
                  </MenuItem>
                );
              })}
            </Menu>
          </Grid>
        ),
      },
      {
        name: "Action",
        renderCell: (res) => (
          <Grid display="flex" gap="12px">
            <CustomChip
              onClick={() => setModalManage(res)}
              label="Manage Doc"
            />
            <CustomChip
              onClick={() => setModalLogDistribute(res)}
              label="Log"
            />
          </Grid>
        ),
      },
    ],
  ];

  useEffect(() => {
    getDataTable();
  }, [refresh]);

  const getDataTable = async () => {
    setLoading(true);
    Axios.get(
      "/merchant-transaksi-master-action/views?"
      // , {
      //   params: {
      //     filter: { where: { and: [{ AT_POSISI: 3 }, { PERIODE: period }] } },
      //   },
      // }
    )
      .then((response) => {
        if (response.data.success) {
          console.log(response.data.datas);
          setRows(response.data.datas);
          setTimeout(() => {
            setLoading(false);
          }, 1000);
        }
      })
      .catch((err) => console.log(err));
  };

  return (
    <Grid mt="36px">
      <CustomButon.Primary onClick={handleOpen} width={195}>
        Distribusi
      </CustomButon.Primary>
      <CustomButon.Primary
        sx={{ marginLeft: "32px" }}
        // onClick={() => setOpenBulk(true) }
        // width={195}
        onClick={() => setModalBulk(true)}
      >
        Upload Bulk
      </CustomButon.Primary>
      <Grid mt="36px">
        <TableComponent
          idSearch="distribusi-bar"
          loading={loading}
          columns={columns}
          rows={rows}
        />
      </Grid>
      {modalBulk && (
        <ModalUploadBulk open={modalBulk} onClose={() => setModalBulk(false)} />
      )}

      {modalManage && (
        <ModalManage
          open={modalManage}
          onClose={() => {
            setModalManage(false);
            getDataTable();
          }}
        />
      )}
      {open && (
        <ModalDistribute
          open={open}
          onClose={() => {
            setOpen(false);
            getDataTable();
          }}
          selected={selected}
        />
      )}
      {modalLogDistribute && (
        <ModalLogDistribute
          open={modalLogDistribute}
          onClose={() => setModalLogDistribute(false)}
        />
      )}
    </Grid>
  );
}
