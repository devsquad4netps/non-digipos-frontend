import React, { useEffect, useState } from "react";
import { faker } from "@faker-js/faker";
import { Grid, Icon, IconButton, Checkbox } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
import Swal from "sweetalert2";

import TableComponent from "../../TableComponent";
import { CheckIcon, EyeIcon } from "../../../assets/icons/customIcon";
import CustomChip from "../../CustomChip";
import CustomButon from "../../CustomButon";
import CustomModal from "../../CustomModal";
import { Axios } from "../../../helper/http";

import master from "../../../helper/services/master";
import { CONVERT_MONTH, GENERATE_PULUHAN } from "../../../helper/globalFuncion";
import { WarningIcon } from "../../../assets/icons/customIcon";
import ModalReject from "../../ModalReject";

export default function TableInqueryReviewSummary() {
  const { period, refresh } = useSelector((state) => state.globalReducer);
  const [rows, setRows] = useState([]);
  const [loading, setLoading] = useState(false);
  const [selected, setSelected] = useState([]);
  const [modalReject, setModalReject] = useState(false);

  const columns = [
    [
      {
        name: (
          <Checkbox
            // indeterminate={true}
            sx={{
              color: "white",
              "&.Mui-checked": {
                color: "white",
              },
            }}
          />
        ),
        rowSpan: 2,
        renderCell: ({ MPM_ID }, i) => (
          <Checkbox
            checked={selected.includes(MPM_ID)}
            onClick={({ target }) => {
              if (target.checked) {
                setSelected([...selected, MPM_ID]);
              } else {
                let data = [...selected];
                setSelected(data.filter((val) => val !== MPM_ID));
              }
            }}
          />
        ),
        width: 10,
      },
      {
        name: "Detail",
        rowSpan: 2,
        width: 24,
        renderCell: ({ status }, i) => (
          <IconButton>
            <EyeIcon />
          </IconButton>
        ),
      },
      {
        name: (
          <div>
            ID <br /> Merchant
          </div>
        ),
        rowSpan: 2,
        renderCell: ({ M_ID }) => M_ID || "No Data",
        width: 70,
      },
      {
        name: "Nama Merchant",
        rowSpan: 2,
        renderCell: ({ M_LEGAL_NAME }) => M_LEGAL_NAME || "No Data",
        width: 200,
      },
      {
        name: "Transaksi Merchant",
        colSpan: 9,
        heading: true,
      },
    ],
    [
      {
        name: "Tahun",
        renderCell: ({ PERIODE }) => new Date(PERIODE).getFullYear(),
      },
      {
        name: "Bulan",
        renderCell: ({ PERIODE }) => CONVERT_MONTH(PERIODE),
      },
      {
        name: "MDR",
        renderCell: ({ MPM_MDR }) =>
          MPM_MDR ? GENERATE_PULUHAN(MPM_MDR) : "No Data",
      },
      {
        name: "TRX",
        renderCell: ({ MPM_AMOUNT }) =>
          MPM_AMOUNT ? GENERATE_PULUHAN(MPM_AMOUNT) : "No Data",
      },
      // {
      //   name: "Total Amount",
      //   renderCell: ({ MPM_AMOUNT }) =>
      //     MPM_AMOUNT ? GENERATE_PULUHAN(MPM_AMOUNT) : "No Data",
      // },
      {
        name: "Total",
        renderCell: ({ MPM_TOTAL }) =>
          MPM_TOTAL ? GENERATE_PULUHAN(MPM_TOTAL) : "No Data",
      },
      {
        name: "Status",
        renderCell: (data) => <CustomChip.Status data={data} />,
      },
    ],
  ];

  useEffect(() => {
    getDataTable();
  }, [refresh]);

  const getDataTable = async () => {
    setLoading(true);
    Axios.get("/merchant-transaksi-master-action/views?", {
      params: {
        filter: { where: { and: [{ AT_POSISI: 1 }, { PERIODE: period }] } },
      },
    })
      .then((response) => {
        if (response.data.success) {
          console.log(response.data.datas);
          setRows(response.data.datas);
          setTimeout(() => {
            setLoading(false);
          }, 500);
        }
      })
      .catch((err) => console.log(err));
  };

  const handleValidate = () => {
    if (selected.length > 0) {
      Swal.fire({
        title: "Apakah anda yakin?",
        text: `Validasi Generate Bar Number Periode ${period}`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Submit",
      }).then((result) => {
        if (result.isConfirmed) {
          Axios.post(
            "/merchant-transaksi-master-action/validation/data-master/bussines-assurace/approved",
            {
              MPM_ID: selected,
            }
          ).then(({ data }) => {
            Swal.fire({
              icon: data.success ? "success" : "error",
              title: data.success ? "Success" : "Error",
              text: data.success && "Validasi data berhasil!",
            });
            getDataTable();
          });
        }
      });
    } else {
      noDataSelect();
    }
  };

  const noDataSelect = (label) => {
    return Swal.fire({
      icon: "warning",
      title: "tidak ada data yang dipilih",
      text: `mohon pilih data yang akan di ${label ? label : "validasi"}`,
    });
  };

  return (
    <Grid>
      <Grid
        mt="36px"
        sx={{ "& a": { textDecoration: "none" } }}
        container
        gap="24px"
      >
        <CustomButon.Primary onClick={handleValidate}>
          Validasi
        </CustomButon.Primary>
        <CustomButon.Error
          onClick={() =>
            selected.length > 0 ? setModalReject(true) : noDataSelect("reject")
          }
        >
          Reject
        </CustomButon.Error>
      </Grid>
      <Grid mt="36px">
        <TableComponent loading={loading} columns={columns} rows={rows} />
      </Grid>
      {modalReject && (
        <ModalReject
          open={modalReject}
          onClose={() => setModalReject(false)}
          urlApi={
            "/merchant-transaksi-master-action/validation/data-master/bussines-assurace/reject"
          }
          selected={selected}
          getDataTable={() => getDataTable()}
        />
      )}
    </Grid>
  );
}
