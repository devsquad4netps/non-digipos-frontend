import React, { useState, useEffect, useCallback, useMemo } from "react";
import { faker } from "@faker-js/faker";
import { Grid, Icon, IconButton, Checkbox, Chip } from "@mui/material";

import TableComponent from "../../TableComponent";
import { CheckIcon, EyeIcon, SyncIcon } from "../../../assets/icons/customIcon";
import CustomButon from "../../CustomButon";
import ModalNewBigQuery from "./ModalNewBigQuery";
import CustomModal from "../../CustomModal";
import DateComp from "../../DatePicker";
import {
  SyncNewViews,
  SyncLogViews,
} from "../../../helper/services/transactionBa";
import ProgressOnTable from "../../ProgressOnTable";
import moment from "moment";

export default function TableSyncBigQuery() {
  const [open, setOpen] = useState(false);
  const [openEx, setOpenEx] = useState(false);
  const [loading, setLoading] = useState(false);
  const [rows, setRows] = useState([]);
  const [status, setStatus] = useState({
    panding: [],
    running: [],
  });

  const columns = [
    [
      {
        name: "Periode",
        renderCell: ({ PERIODE }) => (PERIODE ? PERIODE : "No Data"),
      },
      {
        name: "Synchronize",
        renderCell: ({ nameMerchant }) =>
          nameMerchant ? nameMerchant : "No Data",
      },
      {
        name: "Product",
        renderCell: ({ MERCHANT_PRODUCT }) =>
          MERCHANT_PRODUCT?.MP_BRAND ? MERCHANT_PRODUCT?.MP_BRAND : "No Data",
      },
      {
        name: "Count",
        renderCell: ({ SPL_COUNT_TRX }) =>
          SPL_COUNT_TRX ? SPL_COUNT_TRX : "No Data",
      },
      {
        name: "MDR",
        renderCell: ({ SPL_MDR }) => (SPL_MDR ? SPL_MDR : "No Data"),
      },
      {
        name: "Amount",
        renderCell: ({ SPL_TRX_AMOUNT }) =>
          SPL_TRX_AMOUNT ? SPL_TRX_AMOUNT : "No Data",
      },
      {
        name: "Process",
        renderCell: ({ PROGRES }) => <ProgressOnTable value={PROGRES} />,
      },
      // {
      //   name: "User",
      //   renderCell: ({ nameMerchant }) =>
      //     nameMerchant ? nameMerchant : "No Data",
      // },
      {
        name: "At Create",
        renderCell: ({ AT_CREATE }) =>
          AT_CREATE ? moment(Number(AT_CREATE)).format("YYYY-MM-DD HH:mm") : "",
      },
      {
        name: "At Finish",
        renderCell: ({ AT_UPDATE }) =>
          AT_UPDATE ? moment(`${AT_UPDATE}`).format("YYYY-MM-DD HH:mm") : "",
      },
      {
        name: "Status",
        renderCell: ({ AT_FLAG }) =>
          AT_FLAG === 0
            ? "Pending"
            : AT_FLAG === 1
            ? "On Progress"
            : AT_FLAG === 2
            ? "Success"
            : "Failure",
      },
      {
        name: "Action",
        renderCell: ({ nameMerchant }) => (
          <Chip
            label="View"
            variant="outlined"
            sx={{ color: "blue", borderColor: "blue" }}
          />
        ),
      },
    ],
  ];

  const hanldeOpen = useCallback(() => {
    setOpen(true);
  }, [open]);

  useEffect(() => {
    getSyncData();
  }, []);

  const getSyncData = async () => {
    setLoading(true);
    try {
      const response = await SyncLogViews();
      if (response.data.success) {
        console.log(response.data.datas);
        const { datas } = response.data;
        setRows(datas);
        const pending = datas.filter((val) => val.AT_FLAG === 0);
        const running = datas.filter((val) => val.AT_FLAG === 1);
        setStatus({ pending: pending, running: running });
      }
    } catch (error) {
      console.log(error);
    }
    setTimeout(() => {
      setLoading(false);
    }, 1000);
  };

  // const hanldeOpen = () => setOpen(true);

  return (
    <Grid>
      <Grid>
        <CustomButon.Primary
          onClick={hanldeOpen}
          sx={{
            "& svg": {
              marginRight: "10px",
            },
          }}
        >
          <SyncIcon />
          NEW SYNC BIG QUERY
        </CustomButon.Primary>
        <CustomButon.Primary
          onClick={() => setOpenEx(true)}
          sx={{
            "& svg": {
              marginRight: "10px",
            },
            marginLeft: "24px",
          }}
        >
          <SyncIcon />
          EXISTING SYNC BIG QUERY
        </CustomButon.Primary>
      </Grid>
      <Grid mt="24px">
        <p>
          PENDING : ( {status?.pending?.length} PROCESS), RUNNING : ({" "}
          {status?.running?.length} PROCESS)
        </p>
      </Grid>
      <Grid mt="12px">
        <TableComponent columns={columns} rows={rows} loading={loading} />
      </Grid>
      {open && (
        <ModalNewBigQuery open={open} handleClose={() => setOpen(false)} />
      )}
      {openEx && (
        <ModalExisting open={openEx} onClose={() => setOpenEx(false)} />
      )}
    </Grid>
  );
}

const ModalExisting = ({ open, onClose }) => {
  return (
    <CustomModal open={open} onClose={onClose} width="360px">
      <Grid display="flex" alignItems="center" gap="16px">
        Periode <DateComp.Periode />
      </Grid>
      <Grid
        mt="8px"
        sx={{
          display: "flex",
          flexDirection: "column",
          textAlign: "left",
          alignItems: "flex-start",
        }}
      >
        <Grid>
          <Checkbox />
          <label for="">ITEM KU</label>
        </Grid>
        <Grid>
          <Checkbox />
          <label for="">UNIPIN</label>
        </Grid>
        <Grid>
          <Checkbox />
          <label for="">SYARIAH</label>
        </Grid>
      </Grid>
      <Grid mt="36px" gap="32px" display="flex">
        <CustomButon.Error>Cancel</CustomButon.Error>
        <CustomButon.Primary>Submit</CustomButon.Primary>
      </Grid>
    </CustomModal>
  );
};
