import React from "react";
import TableComponent from "../../TableComponent";

export default function InqueryReviewSummaryTable() {
  const columns = [
    [
      {
        name: "status",
        rowSpan: 2,
        renderCell: ({ status }) => (status ? "Active" : "GA AKTIVE"),
      },
      {
        name: "ID Merchant",
        rowSpan: 2,
        renderCell: ({ IdMerchant }) => (IdMerchant ? IdMerchant : "No Data"),
      },
      {
        name: "Nama Merchant",
        rowSpan: 2,
        renderCell: ({ IdMerchant }) =>
          IdMerchant ? "2020/06/14 08:43:14" : "No Data",
      },
      {
        name: "Transaksi Merchant",
        colSpan: 8,
        heading: true,
        renderCell: ({ IdMerchant }) => (IdMerchant ? IdMerchant : "No Data"),
      },
    ],
    [
      {
        name: "Tahun",
        renderCell: ({ IdMerchant }) => (IdMerchant ? "2020" : "No Data"),
      },
      {
        name: "Bulan",
        renderCell: ({ IdMerchant }) => (IdMerchant ? "April" : "No Data"),
      },
      {
        name: "MDR",
        renderCell: ({ mdr }) => (mdr ? mdr : "No Data"),
      },
      {
        name: "TRX",
        renderCell: ({ trx }) => (trx ? trx : "No Data"),
      },
      {
        name: "Total Amount",
        renderCell: ({ totalAmount }) =>
          totalAmount ? totalAmount : "No Data",
      },
      {
        name: "Total MDR",
        renderCell: ({ totalMdr }) => (totalMdr ? totalMdr : "No Data"),
      },
      {
        name: "Status",
        renderCell: ({ IdMerchant }) => (IdMerchant ? IdMerchant : "No Data"),
      },
    ],
  ];

  const rows = [
    {
      status: 1,
      IdMerchant: 159,
      nameMerchant: "farhan",
      tahun: 1998,
      bulan: 26,
      mdr: 1000230,
      trx: 12930092,
      totalAmount: 12999,
      totalMdr: 1995499,
      statusActive: true,
    },
    {
      status: 1,
      IdMerchant: 159,
      nameMerchant: "farhan",
      tahun: 1998,
      bulan: 26,
      mdr: 1000230,
      trx: 12930092,
      totalAmount: 12999,
      totalMdr: 1995499,
      statusActive: true,
    },
    {
      status: 1,
      IdMerchant: 159,
      nameMerchant: "farhan",
      tahun: 1998,
      bulan: 26,
      mdr: 1000230,
      trx: 12930092,
      totalAmount: 12999,
      totalMdr: 1995499,
      statusActive: true,
    },
    {
      status: 0,
      IdMerchant: 159,
      nameMerchant: "farhan",
      tahun: 1998,
      bulan: 26,
      mdr: 1000230,
      trx: 12930092,
      totalAmount: 12999,
      totalMdr: 1995499,
      statusActive: true,
    },
  ];
  return <TableComponent columns={columns} rows={rows} />;
}
