import React, { useState, useEffect } from "react";
import TableComponent from "../../TableComponent";
import { Checkbox, Grid, Menu, MenuItem, IconButton } from "@mui/material";
import { CrossIcon, CheckIcon, DownloadIcon, PlusButtonIcon, TrashIcon } from "../../../assets/icons/customIcon";
import CustomButon from "../../CustomButon";
import { Axios } from "../../../helper/http";
import Swal from "sweetalert2";
import CustomChip from "../../CustomChip";
import { CONVERT_MONTH } from "../../../helper/globalFuncion";
import { VIEW_FILE_BASE64 } from "../../../helper/globalFuncion";
import ModalManage from "../../ModalManage";
import ModalUploadBulk from "../../ModalUploadBulk";
import { GENERATE_PULUHAN, VIEW_FILE_PDF } from "../../../helper/globalFuncion";
import ModalDistribute from "../../ModalDistribute";
import { useSelector } from "react-redux";
import master from "../../../helper/services/master";
import CustomInputTab from "../../../components//InputComponentTab";
import CustomModal from "../../../components/CustomModal";
import CustomButonFile from "../../../components/CustomButtonFile";
import { useForm } from "react-hook-form";
import { CSVLink, CSVDownload } from "react-csv";
import CustomSelectMr from "../../../components/SelectComponentMr";
import CustomSelectProd from "../../../components/SelectComponentProd";
import CustomSelect from "../../../components/SelectComponent";
import { validateDateTime } from "@mui/x-date-pickers/internals/hooks/validation/useDateTimeValidation";
import { Mp } from "@mui/icons-material";

export default function TableMigrasiDataCsv() {
  const [dataMerchant, setDataMerchant] = useState([]);
  const [dataProduct, setDataProduct] = useState([]);
  const [M_ID, setM_ID] = useState([]);
  const [MP_ID, setMP_ID] = useState([]);
  const [IMPORT_UPLOAD, setIMPORT_UPLOAD] = useState();
  const [periodeTahun, setperiodeTahun] = useState("");
  const [periodeBulan, setperiodeBulan] = useState("");
  
  const handleClose = () => setState({ ...state, loading: false });
  
  const [state, setState] = useState({
    open: false,
    loading: false,
    refresh: false,
    files: null,
  });

  const onFileChange = (e) => {
    console.log(e.target.files[0])
    setIMPORT_UPLOAD(e.target.files[0]);
  }

  const onRemoveImage = () => {
    setState({...state, files: null});
  }

  const {open, files, loading, refresh} = state;

  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
    control,
    setValue,
  } = useForm();

  const setLoading = () => setState({ ...state, loading: true });
  const setOpen = () => setState({ ...state, open: !open });

  
  const [datamigrasiDataCsv, setMigrasiDataCsv] = useState([]);

  const getMigrasiDataCsv = async () => {
    try {
      const response = await master.migrasiDataCsv();
      if (response.data.success) {
        console.log(response.data.datas);
        setMigrasiDataCsv(response.data.datas);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const getDataMerchant = async () => {
    try {
      const response = await master.merchantList();
      console.log(response.data.datas);
      if (response.data.success) {
        setDataMerchant(response.data.datas);
      }
    } catch (error) {
      console.log(error);
    }
  };
  const onSelectMerchant = (val) => {
    console.log(val);
    setM_ID(val)
    getDataProduct(val);
  }

  const getDataProduct = async (val) => {
    try {
      const response = await master.merchantProductList(val);
      console.log(response.data.datas);
      if (response.data.success) {
        setDataProduct(response.data.datas);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const onSubmit = async () => {
    const formData = new FormData();
    formData.append('IMPORT_UPLOAD',IMPORT_UPLOAD);
    formData.append('IM_PERIODE',periodeTahun+'-'+periodeBulan);
    formData.append('M_ID',M_ID);
    formData.append('MP_ID',MP_ID);
    setLoading();
    try {
      const request = await master.uploadMigrasiDataCsv(formData);
      // console.log(request);
      if (request.data.success) {
        setState({ ...state, loading: false, open: false});
        Swal.fire({
          icon: "success",
          title: "Success",
          text: "Create data success",
        });
      } else {
        setState({...state, loading: false, open: false});
        Swal.fire({
          icon: "error",
          title: "Error",
          text: "Error on submit"
        });
      }
    } catch ({ response: { data } }) {
      console.log(data);
      setState({ ...state, loading: false, open: false });
      Swal.fire({
        icon: "error",
        title: "Error",
        text: data.error.message,
      });
    }
  }


  useEffect(() => {
    getDataMerchant();
    getMigrasiDataCsv();
  }, []);

//   const handleOpen = () => {
//     if (selected.length > 0) {
//       setOpen(true);
//     } else {
//       Swal.fire({
//         icon: "warning",
//         title: "tidak ada data yang dipilih",
//         text: "mohon pilih data yang akan di distribusi",
//       });
//     }
//   };



  const columns = [
    [
      {
            rowspan: 2,
            name: "Status",
            renderCell: ({ AT_FLAG=1 }) => (AT_FLAG ? <CheckIcon/> : <CrossIcon/>),
            width: 10
      },
      {
        name: "Periode",
        rowSpan: 2,
        renderCell: ({ IM_PERIODE }, i) => IM_PERIODE,
      },
      {
        name: "File Name",
        rowSpan: 2,
        renderCell: ({ IM_NAME}) => IM_NAME,
        width: 200,
      },
      {
        name: "Count Transaksi",
        rowSpan: 2,
        renderCell: ({ IM_COUNT_TRX}) => IM_COUNT_TRX
,
        width: 200,
      },
      {
        name: "Amount",
        rowSpan: 2,
        renderCell: ({ IM_TRX_AMOUNT}) => IM_TRX_AMOUNT
,
        width: 200,
      },
      {
        name: "MDR",
        rowSpan: 2,
        renderCell: ({ IM_MDR}) => IM_MDR,
        width: 200,
      },
      {
        name: "Progress",
        rowSpan: 2,
        renderCell: ({ IM_PROCESS_NOW,IM_PROCESS_COUNT}) => IM_PROCESS_NOW/IM_PROCESS_COUNT*100
        ,
        width: 200,
      },
      {
        name: "Created At",
        rowSpan: 2,
        renderCell: ({ AT_CREATE }) => AT_CREATE,
        width: 200,
      },
      {
        name: "End At",
        rowSpan: 2,
        renderCell: ({ M_LEGAL_NAME }) => M_LEGAL_NAME,
        width: 200,
      }
    ]
  ];

  return (
    <Grid mt="36px">
      <CustomButon.Primary onClick={(e) => setOpen(true)} width={195}>
        Click To Upload
      </CustomButon.Primary>
      <CustomButon.Primary
        sx={{ marginLeft: "32px" }}>
        Import Excel
      </CustomButon.Primary>
      {/* <CustomButon.Primary
        sx={{ marginLeft: "650px" }}>
        Submit
      </CustomButon.Primary> */}
      <Grid mt="36px">
        <TableComponent loading={loading} columns={columns} rows={datamigrasiDataCsv} />
      </Grid>

      <CustomModal width="800px" open={open} onClose={handleClose} title="Migrasi Data CSV">
          <form onSubmit={handleSubmit(onSubmit)} id="onSubmit">
          <Grid container spacing={10} rowSpacing={12}>
            <Grid item xs={6} rowSpacing="12px">
              <div style={{display: 'flex', flexDirection: "row", justifyContent: "flex-start", alignItems: "center", marginBottom: 10}}>
                  <div style={{marginRight: 10}}>
                    <span>Nama File: </span>
                  </div>
                  { IMPORT_UPLOAD 
                    ? 
                    <div>
                        <IconButton style={{position: "absolute", top: -15, right: -15, color: 'red'}} onClick={onRemoveImage}>
                            <TrashIcon />
                        </IconButton>
                      {IMPORT_UPLOAD.name}
                    </div>
                    :
                    <div style={{width: 100, border: "1px solid #ccc", borderRadius: 12, display: 'flex', justifyContent: "center", alignItems: "center"}}>
                      <p style={{color: "#ccc", fontSize:10}}>Select File</p>
                    </div>
                  }
              </div>
              <input id="file-image" style={{display: 'none'}} type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" onChange={onFileChange} multiple={true} />
              
              {!IMPORT_UPLOAD && <CustomButonFile.Primary label="Upload File :" onClick={() => document.getElementById("file-image").click()}>Upload File</CustomButonFile.Primary>}
              <CustomSelectMr.SelectLabel
                  item
                  label="Merchant" labelWidth="110px"
                  required
                  value={M_ID}
                  options={dataMerchant}
                  onChange={(e)=>onSelectMerchant(e.target.value)}
                  />
              <CustomSelectProd.SelectLabel
                  item
                  label="Product" labelWidth="110px"
                  required
                  value={MP_ID}
                  options={dataProduct}
                  onChange={(e)=>setMP_ID(e.target.value)}
                  />
              <CustomSelect.SelectLabel
                    item
                    label="Tahun" labelWidth="110px"
                    required
                    value={(periodeTahun)}
                    options={[{label: '2022', value:2022},{label: '2021', value:2021},{label: '2020', value:2020}]}
                    onChange={(e)=>setperiodeTahun(e.target.value)}
                    />
              <CustomSelect.SelectLabel
                    item
                    label="Bulan" labelWidth="110px"
                    required
                    value={(periodeBulan)}
                    options={[{label: 'Januari', value: '01'},
                    {label: 'Febuari', value: '02'},
                    {label: 'Maret', value: '03'},
                    {label: 'April', value: '04'},
                    {label: 'Mei', value: '05'},
                    {label: 'Juni', value: '06'},
                    {label: 'Juli', value: '07'},
                    {label: 'Agustus', value: '08'},
                    {label: 'September', value: '09'},
                    {label: 'Oktober', value: '10'},
                    {label: 'November', value: '11'},
                    {label: 'Desember', value: '12'},
                  ]}
                    onChange={(e)=>setperiodeBulan(e.target.value)}
                    />
            </Grid>
            <Grid item justifyContent="flex-end" container gap="24px">
              <CustomButon.Primary
                size="small"
                onClick={setOpen}
                sx={{ "& span": { marginLeft: "8px" }, backgroundColor: "red" }}
              >
                Cancel{" "}
              </CustomButon.Primary>
              <CustomButon.Primary
                id="submit-csv"
                type="submit"
                size="small"
                // onClick={(e) => setOpen(true)}
                sx={{ "& span": { marginLeft: "8px" } }}
              >
                Save
              </CustomButon.Primary>
            </Grid>
          </Grid>
          </form>
        </CustomModal>

    </Grid>

      


  );
}
