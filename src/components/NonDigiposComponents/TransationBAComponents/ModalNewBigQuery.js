import React, { useState, useEffect, useMemo } from "react";
import CustomModal from "../../CustomModal";
import { Grid, Radio, CircularProgress } from "@mui/material";
import { makeStyles } from "@mui/styles";
import DateComp from "../../DatePicker";
import CustomButon from "../../CustomButon";
import TableComponent from "../../TableComponent";
import { SyncNewViews } from "../../../helper/services/transactionBa";
import { Axios } from "../../../helper/http";
import Swal from "sweetalert2";
import { useSelector } from "react-redux";

const useStyles = makeStyles((theme) => {
  const { customTheme } = theme;
  return {
    root: {
      "& .header": {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        "& h3": {
          display: "flex",
          width: "225px",
          margin: 0,
        },
        "& div": {
          width: "100%",
          height: "2px",
          backgroundColor: "#D9D9D9",
        },
      },
      "& .status": {
        marginTop: "32px",
        "& .status-field": {
          padding: "4px",
          display: "flex",
          alignItems: "center",
          gap: "16px",
          "& h3": {
            margin: 0,
            wdth: "100%",
            display: "inline-block",
          },
          "& .avatar": {
            minWidth: "36px",
            minHeight: "36px",
            borderRadius: "50%",
            border: "2px solid black",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            fontWeight: "800",
            fontSize: "16px",
          },
          "& .line": {
            border: "1px solid",
            width: "auto",
            flex: 1,
          },
        },
      },
    },
    treeRoot: {},
  };
});

export default function ModalNewBigQuery({ open, handleClose }) {
  const nowPeriode = useSelector((state) => state.globalReducer.period);
  const classes = useStyles();
  const [active, setActive] = useState(0);
  const [dataTreeTable, setDataTreeTable] = useState([]);
  const [periode, setPeriod] = useState(nowPeriode);
  const [dataSelect, setDataSelect] = useState({
    product: {},
  });

  const getNewViews = async () => {
    const response = await SyncNewViews();
    const { datas } = response.data;
    let data = [];
    Object.keys(datas).map((val) => {
      data.push(datas[val]);
    });
    console.log(datas);
    let newArray = [];
    data.map((val) => {
      Object.keys(val?.DATA).map((res) => {
        newArray.push({
          id: val.ID,
          company: val.LABEL,
          product: val?.DATA[res]?.DATA,
          target: val?.DATA[res].TARGET,
          headTable: val?.DATA[res]?.LABEL,
        });
      });
    });
    setDataTreeTable(newArray);
  };

  const columns = [
    [
      {
        name: "Action",
        renderCell: (val) => {
          return (
            <Radio
              onClick={() => {
                console.log(val, dataSelect);
                setDataSelect(val);
              }}
              checked={
                (val.company && val.headTable) ===
                (dataSelect?.company && dataSelect.headTable)
              }
            />
          );
        },
      },
      {
        name: "Company",
        renderCell: (val) => val?.company,
        align: "left",
      },
      {
        name: "Product",
        renderCell: (val) => val?.headTable,
        align: "left",
      },
      {
        name: "Target",
        renderCell: (val) =>
          Object?.keys(val?.product).map((res) => {
            return val?.product[res].LABEL + ", ";
          }),
        align: "left",
      },
    ],
  ];

  useMemo(() => {
    getNewViews();
  }, []);

  useEffect(() => {}, [open]);

  const handleNext = () => {
    if (active === dataSelect?.target?.length) {
      console.log("IM here");
      Axios.post(
        "/merchant-sychronize-datasource/big-query/merhant-group/create",
        {
          MPG_ID: dataSelect.target,
          PERIODE: periode,
        }
      )
        .then(({ data }) => {
          handleClose();
          Swal.fire({
            icon: data.success ? "success" : "error",
            title: data.success ? "Success" : "Error",
            text: data.message,
          });
        })
        .catch((err) => console.log(err));
    } else if (dataSelect.company) {
      setActive(active + 1);
    }
  };

  const handleBack = () => {
    if (active > 0) {
      setActive(active - 1);
    }
  };

  const data = [];
  Object.keys(dataSelect?.product).map((val) => {
    data.push(dataSelect?.product[val]);
  });

  return (
    <CustomModal open={open} onClose={handleClose} width="60%">
      <>
        <Grid className={classes.root}>
          <Grid className="header">
            <h3>SYNCHRONIZE TRANSAKSI</h3>
            <div></div>
          </Grid>
          <Grid
            display="flex"
            justifyContent="flex-end"
            mt="24px"
            alignItems="center"
            sx={({ customTheme }) => ({
              "& h2": {
                marginRight: "18px",
                fontWeight: 700,
                fontSize: "18px",
                lineHeight: "20px",
                color: customTheme.color.primary,
              },
            })}
          >
            <h2>Perode :</h2>
            <DateComp.Periode
              withoutSubmit
              onChange={(val) => setPeriod(val)}
              disabled={active > 0}
            />
          </Grid>
          <Grid className="status">
            <Grid className="status-field">
              {active > 0 &&
                data.map((val, i) => {
                  return (
                    <React.Fragment key={1}>
                      <Grid
                        className="avatar"
                        sx={{
                          backgroundColor: active === i + 1 && "#9747FF",
                          color: active === i + 1 && "white",
                        }}
                      >
                        {i + 1}
                      </Grid>
                      <h3>{val.LABEL}</h3>
                      {i !== dataSelect.target.length - 1 && (
                        <div className="line" />
                      )}
                    </React.Fragment>
                  );
                })}
            </Grid>
          </Grid>
          <Grid className="body">
            {active === 0 ? (
              <Grid mt="24px">
                <TableComponent
                  columns={columns}
                  rows={dataTreeTable}
                  noPaging
                />
              </Grid>
            ) : (
              <TableSync
                periode={periode}
                active={active}
                dataSelect={dataSelect}
              />
            )}
          </Grid>
          <Grid container justifyContent="flex-end" mt="32px" gap="24px">
            <CustomButon.Primary disabled={active === 0} onClick={handleBack}>
              Back
            </CustomButon.Primary>
            <CustomButon.Primary onClick={handleNext}>
              {active === 0
                ? "Create"
                : active === dataSelect?.target?.length
                ? "Submit"
                : "Next"}
            </CustomButon.Primary>
          </Grid>
        </Grid>
      </>
    </CustomModal>
  );
}

const TableSync = ({ active, dataSelect, periode }) => {
  const [columns, setColumns] = useState([]);
  const [rows, setRows] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState();
  let datas = [];
  Object.keys(dataSelect.product).map((val) =>
    datas.push(dataSelect.product[val])
  );
  let data = datas[active - 1];

  useEffect(() => {
    setError();
    getDataTable();
  }, [active]);

  const getDataTable = () => {
    setLoading(true);
    Axios.get(
      `merchant-sychronize-datasource/big-query/merhant-group/views?MPG_ID=${data?.ID}&PERIODE=${periode}`
    )
      .then((res) => {
        setRows(res.data.datas.RAW);
        let newColumns = [];
        newColumns.push(
          Object.entries(res.data.datas.FIELD).map((val) => {
            // columns.push(
            return {
              name: res.data.datas.FIELD[val[0]].LABEL,
              renderCell: (res) => res[val[0]],
            };
          })
        );
        setColumns(newColumns);
        setIsLoading();
      })
      .catch((err) => {
        if (err) {
          setError(err);
          setRows([]);
          setColumns([]);
          setIsLoading();
        }
      });
  };

  const setIsLoading = () => {
    setTimeout(() => {
      setLoading(false);
    }, 1000);
  };

  return (
    <Grid
      mt="32px"
      sx={{
        "& p": {
          color: "red",
        },
      }}
    >
      <Grid
        textAlign="center"
        sx={{
          "& h2": {
            fontWeight: 700,
            fontSize: "16px",
            lineHeight: "16px",
          },
        }}
      >
        <h2>BIG QUERY TRANSAKSI</h2>
      </Grid>
      <Grid justifyContent="space-between" mt="32px" mb="18px">
        <CustomButon.Primary>Inquery Big Query</CustomButon.Primary>
      </Grid>
      {error && <p>{error?.message}</p>}
      {loading ? (
        <CircularProgress />
      ) : (
        <TableComponent columns={columns} rows={rows} noPaging />
      )}
    </Grid>
  );
};
