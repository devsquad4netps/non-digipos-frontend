import React, { useEffect, useState } from "react";
import {
  Checkbox,
  Grid,
  Button,
  ButtonGroup,
  TextField,
  IconButton,
  Menu,
  MenuItem,
} from "@mui/material";
import Swal from "sweetalert2";

import TableComponent from "../../TableComponent";
import master from "../../../helper/services/master";
import CustomButon from "../../CustomButon";
import { CONVERT_MONTH } from "../../../helper/globalFuncion";
import CustomChip from "../../CustomChip";
import CustomModal from "../../CustomModal";
import CustomInput from "../../InputComponent";
import { Axios } from "../../../helper/http";
import { CheckIcon, CrossIcon } from "../../../assets/icons/customIcon";
import ModalManage from "../../ModalManage";
import ModalUploadBulk from "../../ModalUploadBulk";
import { GENERATE_PULUHAN } from "../../../helper/globalFuncion";

import { VIEW_FILE_PDF } from "../../../helper/globalFuncion";
import { useSelector } from "react-redux";
import ModalReject from "../../ModalReject";
import ModalLogDistribute from "../../ModalLogDistribute";

export default function ReviewSummaryMerchantTable() {
  const { period, refresh } = useSelector(({ globalReducer }) => globalReducer);
  const [rows, setRows] = useState([]);
  const [openGenerate, setOpenGenerate] = useState(false);
  const [loading, setLoading] = useState([]);
  const [selected, setSelected] = useState([]);
  const [body, setBody] = useState({});
  const [modalManage, setModalManage] = useState();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [modalBulk, setModalBulk] = useState(false);
  const [modalReject, setModalReject] = useState(false);
  const [modalLogDistribute, setModalLogDistribute] = useState(false);

  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const columns = [
    [
      {
        name: (
          <Checkbox
            sx={{
              color: "white",
              "&.Mui-checked": {
                color: "white",
              },
            }}
          />
        ),
        rowSpan: 2,
        renderCell: ({ MPM_ID }, i) => (
          <Checkbox
            checked={selected.includes(MPM_ID)}
            onClick={({ target, GENERATE_BAR_SIGN }) => {
              handleChecked(target, MPM_ID, GENERATE_BAR_SIGN);
            }}
          />
        ),
        width: 10,
      },
      {
        name: "Number BAR",
        rowSpan: 2,
        renderCell: ({ MPM_NO_BAR }, i) => MPM_NO_BAR,
      },
      {
        name: (
          <div>
            ID <br /> Merchant
          </div>
        ),
        rowSpan: 2,
        renderCell: ({ M_ID }) => M_ID,
        width: 70,
      },
      {
        name: "Nama Merchant",
        rowSpan: 2,
        renderCell: ({ M_LEGAL_NAME }) => M_LEGAL_NAME,
        width: 200,
      },
      {
        name: "Transaksi Merchant",
        colSpan: 10,
        heading: true,
      },
    ],
    [
      {
        name: "Tahun",
        renderCell: ({ PERIODE }) => new Date(PERIODE).getFullYear(),
      },
      {
        name: "Bulan",
        renderCell: ({ PERIODE }) => CONVERT_MONTH(PERIODE),
      },
      {
        name: "MDR",
        renderCell: ({ MPM_MDR }) =>
          MPM_MDR ? GENERATE_PULUHAN(MPM_MDR) : "No Data",
      },
      {
        name: "TRX",
        renderCell: ({ MPM_AMOUNT }) =>
          MPM_AMOUNT ? GENERATE_PULUHAN(MPM_AMOUNT) : "No Data",
      },
      // {
      //   name: "Total Amount",
      //   renderCell: ({ MPM_AMOUNT }) =>
      //     MPM_AMOUNT ? GENERATE_PULUHAN(MPM_AMOUNT) : "No Data",
      // },
      {
        name: "Total",
        renderCell: ({ MPM_TOTAL }) =>
          MPM_TOTAL ? GENERATE_PULUHAN(MPM_TOTAL) : "No Data",
      },
      {
        name: "Status",
        renderCell: (data) => <CustomChip.Status data={data} />,
      },
      {
        name: "Status Doc",
        renderCell: ({ GENERATE_BAR_SIGN }) =>
          GENERATE_BAR_SIGN === 1 ? <CheckIcon /> : <CrossIcon />,
      },
      {
        name: "View File PDF",
        renderCell: ({ DOCUMENT, MPM_ID }) => (
          <Grid>
            <CustomChip MPM_ID={DOCUMENT} onClick={handleClick} label="View" />
            <Menu
              id="basic-menu"
              anchorEl={anchorEl}
              open={open}
              onClose={handleClose}
              MenuListProps={{
                "aria-labelledby": "basic-button",
              }}
            >
              {DOCUMENT.map((val) => {
                return (
                  <MenuItem onClick={() => VIEW_FILE_PDF(val, MPM_ID)}>
                    {val?.MPDOC_NAME}
                  </MenuItem>
                );
              })}
            </Menu>
          </Grid>
        ),
      },
      {
        name: "Action",
        renderCell: (res) => (
          <Grid display="flex" gap="12px">
            <CustomChip
              onClick={() => setModalManage(res)}
              label="Manage Doc"
            />
            <CustomChip
              onClick={() => setModalLogDistribute(res)}
              label="Log"
            />
          </Grid>
        ),
      },
    ],
  ];

  const handleChecked = (target, MPM_ID) => {
    if (target.checked) {
      setSelected([...selected, MPM_ID]);
    } else {
      let data = [...selected];
      setSelected(data.filter((val) => val !== MPM_ID));
    }
  };

  const getDataTable = async () => {
    setLoading(true);
    Axios.get("/merchant-transaksi-master-action/views?", {
      params: {
        filter: { where: { and: [{ AT_POSISI: 4 }, { PERIODE: period }] } },
      },
    })
      .then((response) => {
        if (response.data.success) {
          console.log(response.data.datas);
          setRows(response.data.datas);
          setTimeout(() => {
            setLoading(false);
          }, 1000);
        }
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    getDataTable();
  }, [refresh]);

  const handleGenerate = () => {
    setOpenGenerate(false);
    if (selected.length > 0) {
      Axios.post("/merchant-transaksi-master-action/generate/number-bar", {
        ...body,
        MPM_ID: selected,
      }).then(({ data }) => {
        Swal.fire({
          icon: data.success ? "success" : "error",
          title: data.success ? "Success" : "Error",
          text: data.success && "Generate data berhasil!",
        });
        getDataTable();
      });
    } else {
      Swal.fire({
        icon: "warning",
        title: "tidak ada data yang dipilih",
        text: "mohon pilih data yang akan di generate",
      });
    }
  };

  const handleValidate = () => {
    if (selected.length > 0) {
      Swal.fire({
        title: "Apakah anda yakin?",
        text: `Validasi Generate Bar Number Periode ${period}`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Submit",
      }).then((result) => {
        if (result.isConfirmed) {
          Axios.post(
            "/merchant-transaksi-master-action/validation/data-master/merchant/approved",
            {
              MPM_ID: selected,
            }
          ).then(({ data }) => {
            Swal.fire({
              icon: data.success ? "success" : "error",
              title: data.success ? "Success" : "Error",
              text: data.success && "Validasi data berhasil!",
            });
            getDataTable();
          });
        }
      });
    } else {
      noDataSelect();
    }
  };

  const noDataSelect = (label) => {
    return Swal.fire({
      icon: "warning",
      title: "tidak ada data yang dipilih",
      text: `mohon pilih data yang akan di ${label ? label : "validasi"}`,
    });
  };

  return (
    <Grid>
      <Grid
        mt="36px"
        sx={{ "& a": { textDecoration: "none" } }}
        container
        gap="24px"
      >
        <CustomButon.Primary onClick={() => setModalBulk(true)}>
          Upload Bulk
        </CustomButon.Primary>
        <CustomButon.Primary width={195} onClick={handleValidate}>
          Validasi
        </CustomButon.Primary>
        <CustomButon.Error
          onClick={() =>
            selected.length > 0 ? setModalReject(true) : noDataSelect("reject")
          }
        >
          Reject
        </CustomButon.Error>
      </Grid>
      <Grid mt="36px">
        <TableComponent loading={loading} columns={columns} rows={rows} />
      </Grid>
      {modalBulk && (
        <ModalUploadBulk open={modalBulk} onClose={() => setModalBulk(false)} />
      )}
      {openGenerate && (
        <ModalGenerate
          open={openGenerate}
          onClose={() => {
            setOpenGenerate(false);
          }}
          handleGenerate={handleGenerate}
          noLastChange={(val) =>
            setBody({ ...body, LAST_NUMBER: Number(val.target.value) })
          }
          FknChange={(val) => setBody({ ...body, LABEL: val.target.value })}
        />
      )}
      {modalManage && (
        <ModalManage open={modalManage} onClose={() => setModalManage()} />
      )}
      {modalReject && (
        <ModalReject
          open={modalReject}
          onClose={() => setModalReject(false)}
          urlApi={
            "/merchant-transaksi-master-action/validation/data-master/merchant/rejected"
          }
          selected={selected}
          getDataTable={() => getDataTable()}
        />
      )}
      {modalLogDistribute && (
        <ModalLogDistribute
          open={modalLogDistribute}
          onClose={() => setModalLogDistribute(false)}
        />
      )}
    </Grid>
  );
}

const ModalGenerate = ({
  open,
  onClose,
  handleGenerate,
  noLastChange,
  FknChange,
}) => {
  return (
    <CustomModal height={"auto"} width={360} open={open} onClose={onClose}>
      <Grid
        sx={{
          "& h3": {
            fontWeight: 700,
            fontSize: "12px",
            lineHeight: "16px",
            margin: 0,
            marginBottom: "12px",
          },
        }}
      >
        <h3>Generate Bar</h3>
      </Grid>
      <Grid container spacing={"12px"} mt="0px">
        <Grid item md={12}>
          <label for="">Masukkan Nomor Terakhir BAR</label>
          <CustomInput
            onChange={noLastChange}
            id="noLastBar"
            sx={{ marginTop: "8px" }}
          />
        </Grid>
        <Grid item md={12}>
          <label for="">FKN/FINOPT</label>
          <CustomInput
            onChange={FknChange}
            id="FKN/FINOPT"
            sx={{ marginTop: "8px" }}
          />
        </Grid>
        <Grid container justifyContent="flex-end" spacing="24px" mt="0px">
          <Grid item>
            <CustomButon.Error width={92} onClick={onClose}>
              Cancel
            </CustomButon.Error>
          </Grid>
          <Grid item>
            <CustomButon.Primary width={92} onClick={handleGenerate}>
              Process
            </CustomButon.Primary>
          </Grid>
        </Grid>
      </Grid>
    </CustomModal>
  );
};
