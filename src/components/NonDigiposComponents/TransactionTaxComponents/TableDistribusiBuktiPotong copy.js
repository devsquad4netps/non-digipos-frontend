import React, { useEffect, useState } from "react";
import {Checkbox, Grid} from "@mui/material";
import TableComponent from "../../TableComponent";
import { CrossIcon, CheckIcon } from "../../../assets/icons/customIcon";
import master from "../../../helper/services/master.js";
import Swal from "sweetalert2";
import CustomChip from "../../CustomChip";
import ModalManage from "../../ModalManage";
import ModalUploadBulk from "../../ModalUploadBulk";

function TableDistribusiBuktiPotong({onSelect}) {
  const [loading, setLoading] = useState(true);
  const [dataMerchant, setDataMerchant] = useState([]);
  const [refresh, setRefresh] = useState(false);
  const [modalManage, setModalManage] = useState();

  const getDataMerchant = async () => {
    setLoading(true);
    try {
      const response = await master.merchantTransaksiList();
      if (response.data.success) {
        setDataMerchant(response.data.datas);
        setTimeout(() => {
          setLoading(false);
        }, 1000);
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getDataMerchant();
  }, [refresh]);

  const columns = [
    [
      {
        name: (
          <Checkbox
            sx={{
              color: "white",
              "&.Mui-checked": {
                color: "white",
              },
            }}
           
          />
        ),
        rowSpan: 2,
        renderCell: ({ M_CODE }, i) => <Checkbox  onChange={(e) => onSelect(e, M_CODE)} />,
        width: 10,
      },
      {
        name: "Kode Merchant",
        renderCell: ({ M_CODE }) => M_CODE,
      },
      {
        name: "Nama Merchant",
        renderCell: ({ M_NAME }) => M_NAME,
      },
      {
        name: "Nama File",
        renderCell: ({ DOCUMENT }) => {
          let arr = []
          DOCUMENT.forEach(file =>{
            console.log(file.MPDOC_NAME)
            arr.push(file.MPDOC_NAME)
          })
          return arr.join(' , ','\n')
        }
        
      },
      {
        name: "Status",
        renderCell: ({ AT_FLAG=1 }) => (AT_FLAG ? <CheckIcon/> : <CrossIcon/>),
      },
      {
        name: "View File PDF",
        renderCell: ({ DOCUMENT, MPM_ID }) => (
          <Grid>
            <CustomChip MPM_ID={DOCUMENT} label="View" />
            
          </Grid>
        ),
      },
      {
        name: "Action",
        renderCell: ({ MPM_ID }) => (
          <CustomChip
            MPM_ID={MPM_ID}
            onClick={() => setModalManage(MPM_ID)}
            label="Manage Doc"
          />
        ),
      },
    ],
  ];



  return <TableComponent columns={columns} rows={dataMerchant} />;
}

export default TableDistribusiBuktiPotong;
