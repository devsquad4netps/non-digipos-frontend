import React, { useEffect, useState } from "react";
import TableComponent from "../../TableComponent";
import {Checkbox, Grid} from "@mui/material";
import { CrossIcon, CheckIcon } from "../../../assets/icons/customIcon";
import CustomChip from "../../CustomChip";
import master from "../../../helper/services/master.js";
import Swal from "sweetalert2";


function TableDistribusiFakturPajak({onSelect}) {
  const [loading, setLoading] = useState(true);
  const [dataMerchantTrans, setDataMerchantTrans] = useState([]);
  const [refresh, setRefresh] = useState(false);
  const [modalManage, setModalManage] = useState();

  const getDataMerchantTrans = async () => {
    setLoading(true);
    try {
      const response = await master.merchantTransaksiList();
      if (response.data.success) {
        setDataMerchantTrans(response.data.datas);
        setTimeout(() => {
          setLoading(false);
        }, 1000);
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getDataMerchantTrans();
  }, [refresh]);

  const columns = [
    [
      {
        name: (
          <Checkbox
            sx={{
              color: "white",
              "&.Mui-checked": {
                color: "white",
              },
            }}
          />
        ),
        rowSpan: 2,
        renderCell: ({ MPM_NO_BAR }, i) => <Checkbox  onChange={(e) => onSelect(e, MPM_NO_BAR)} />,
        width: 10,
      },
      {
        name: "Number BAR",
        rowSpan: 2,
        renderCell: ({ MPM_NO_BAR}) => MPM_NO_BAR,
      },
      {
        name: "Invoice Number",
        rowSpan: 2,
        renderCell: ({ MPM_NO_INVOICE }) => MPM_NO_INVOICE,
      },
      {
        name: "No Faktur Pajak",
        rowSpan: 2,
        renderCell: ({ MPM_NO_NUMBER }) => MPM_NO_NUMBER,
      },
      {
        name: "Nama Merchant",
        rowSpan: 2,
        renderCell: ({ M_NAME }) => M_NAME,
      },
      {
        name: "Transaksi Merchant",
        colSpan: 8,
        heading: true,
        renderCell: ({ transaksiMerchant }) => transaksiMerchant,
      },
    ],[
      {
        name: "Tahun",
        renderCell: ({ PERIODE }) => (PERIODE ? PERIODE.split("-")[0] : "no data"),
      },
      {
        name: "Bulan",
        renderCell: ({ PERIODE }) => (PERIODE ? PERIODE.split("-")[1] : "no data"),
      },
      {
        name: "MDR",
        renderCell: ({ MPM_MDR }) => MPM_MDR,
      },
      {
        name: "TRX",
        renderCell: ({ MPM_COUNT_TRX }) => MPM_COUNT_TRX,
      },
      {
        name: "Total Amount",
        renderCell: ({ MPM_AMOUNT }) => MPM_AMOUNT,
      },
      {
        name: "Distribusi",
        renderCell: ({ AT_FLAG=1 }) => (AT_FLAG ? <CheckIcon/> : <CrossIcon/>),
      },
      {
        name: "View File PDF",
        renderCell: ({ DOCUMENT, MPM_ID }) => (
          <Grid>
            <CustomChip MPM_ID={DOCUMENT} label="View" />
            
          </Grid>
        ),
      },
      {
        name: "Action",
        renderCell: ({ MPM_ID }) => (
          <CustomChip
            MPM_ID={MPM_ID}
            onClick={() => setModalManage(MPM_ID)}
            label="Manage Doc"
          />
        ),
      },
    ],
  ];


  return <TableComponent columns={columns} rows={dataMerchantTrans} />;
}

export default TableDistribusiFakturPajak;
