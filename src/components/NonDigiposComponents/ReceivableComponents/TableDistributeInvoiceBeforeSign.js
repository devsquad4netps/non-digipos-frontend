import React, { useEffect, useState } from "react";
import { Checkbox, Grid, Menu, MenuItem } from "@mui/material";
import Swal from "sweetalert2";

import TableComponent from "../../TableComponent";
import master from "../../../helper/services/master";
import CustomButon from "../../CustomButon";
import { CONVERT_MONTH } from "../../../helper/globalFuncion";
import CustomChip from "../../CustomChip";
import CustomModal from "../../CustomModal";
import CustomInput from "../../InputComponent";
import { Axios } from "../../../helper/http";
import { CheckIcon, CrossIcon } from "../../../assets/icons/customIcon";
import ModalManage from "../../ModalManage";
import ModalUploadBulk from "../../ModalUploadBulk";
import { GENERATE_PULUHAN } from "../../../helper/globalFuncion";

import { VIEW_FILE_PDF } from "../../../helper/globalFuncion";
import ModalDistribute from "../../ModalDistribute";
import { useSelector } from "react-redux";

export default function TableDistributeInvoiceBeforeSign() {
  const { period, refresh } = useSelector(({ globalReducer }) => globalReducer);
  const [openDistribute, setOpenDistribute] = useState(false);
  const [rows, setRows] = useState([]);
  const [loading, setLoading] = useState([]);
  const [selected, setSelected] = useState([]);
  const [modalManage, setModalManage] = useState();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const columns = [
    [
      {
        name: (
          <Checkbox
            sx={{
              color: "white",
              "&.Mui-checked": {
                color: "white",
              },
            }}
          />
        ),
        rowSpan: 2,
        renderCell: ({ MPM_ID }, i) => (
          <Checkbox
            checked={selected.includes(MPM_ID)}
            onClick={({ target }) => {
              if (target.checked) {
                setSelected([...selected, MPM_ID]);
              } else {
                let data = [...selected];
                setSelected(data.filter((val) => val !== MPM_ID));
              }
            }}
          />
        ),
        width: 10,
      },
      {
        name: "Number BAR",
        rowSpan: 2,
        renderCell: ({ MPM_NO_BAR }, i) => MPM_NO_BAR,
      },
      {
        name: "Invoice Number",
        rowSpan: 2,
        renderCell: ({ MPM_NO_INVOICE }, i) => MPM_NO_INVOICE,
      },
      {
        name: "Nama Merchant",
        rowSpan: 2,
        renderCell: ({ M_LEGAL_NAME }) => M_LEGAL_NAME,
        width: 200,
      },
      {
        name: "Transaksi Merchant",
        colSpan: 10,
        heading: true,
      },
    ],
    [
      {
        name: "Tahun",
        renderCell: ({ PERIODE }) => new Date(PERIODE).getFullYear(),
      },
      {
        name: "Bulan",
        renderCell: ({ PERIODE }) => CONVERT_MONTH(PERIODE),
      },
      {
        name: "MDR",
        renderCell: ({ MPM_MDR }) =>
          MPM_MDR ? GENERATE_PULUHAN(MPM_MDR) : "No Data",
      },
      {
        name: "TRX",
        renderCell: ({ MPM_AMOUNT }) =>
          MPM_AMOUNT ? GENERATE_PULUHAN(MPM_AMOUNT) : "No Data",
      },
      // {
      //   name: "Total Amount",
      //   renderCell: ({ MPM_AMOUNT }) =>
      //     MPM_AMOUNT ? GENERATE_PULUHAN(MPM_AMOUNT) : "No Data",
      // },
      {
        name: "Total",
        renderCell: ({ MPM_TOTAL }) =>
          MPM_TOTAL ? GENERATE_PULUHAN(MPM_TOTAL) : "No Data",
      },
      {
        name: "Status",
        renderCell: (data) => <CustomChip.Status data={data} />,
      },
      {
        name: "View File PDF",
        renderCell: ({ DOCUMENT, MPM_ID }) => (
          <Grid>
            <CustomChip MPM_ID={DOCUMENT} onClick={handleClick} label="View" />
            <Menu
              id="basic-menu"
              anchorEl={anchorEl}
              open={open}
              onClose={handleClose}
              MenuListProps={{
                "aria-labelledby": "basic-button",
              }}
            >
              {DOCUMENT.map((val) => {
                return (
                  <MenuItem onClick={() => VIEW_FILE_PDF(val, MPM_ID)}>
                    {val?.MPDOC_NAME}
                  </MenuItem>
                );
              })}
            </Menu>
          </Grid>
        ),
      },
      {
        name: "Action",
        renderCell: (res) => (
          <CustomChip onClick={() => setModalManage(res)} label="Manage Doc" />
        ),
      },
    ],
  ];

  const getDataTable = async () => {
    setLoading(true);
    Axios.get("/merchant-transaksi-master-action/views?", {
      params: {
        filter: {
          where: {
            and: [{ AT_POSISI: 6 }, { AT_FLAG: 1 }, { PERIODE: period }],
          },
        },
      },
    })
      .then((response) => {
        if (response.data.success) {
          console.log(response.data.datas);
          setRows(response.data.datas);
          setTimeout(() => {
            setLoading(false);
          }, 1000);
        }
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    getDataTable();
  }, [refresh]);

  const handleOpen = () => {
    if (selected.length > 0) {
      setOpenDistribute(true);
    } else {
      Swal.fire({
        icon: "warning",
        title: "tidak ada data yang dipilih",
        text: "mohon pilih data yang akan di distribusi",
      });
    }
  };

  return (
    <Grid>
      <Grid
        mt="36px"
        sx={{ "& a": { textDecoration: "none" } }}
        container
        gap="24px"
      >
        <CustomButon.Primary onClick={handleOpen} width={195}>
          Distribusi
        </CustomButon.Primary>
      </Grid>
      <Grid mt="36px">
        <TableComponent loading={loading} columns={columns} rows={rows} />
      </Grid>
      {modalManage && (
        <ModalManage open={modalManage} onClose={() => setModalManage()} />
      )}
      {openDistribute && (
        <ModalDistribute
          open={openDistribute}
          onClose={() => {
            setOpenDistribute(false);
            getDataTable();
          }}
          selected={selected}
        />
      )}
    </Grid>
  );
}
