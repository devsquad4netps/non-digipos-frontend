import React, { useEffect, useState } from "react";
import { Checkbox, Grid, Menu, MenuItem } from "@mui/material";
import Swal from "sweetalert2";

import TableComponent from "../../TableComponent";
import CustomButon from "../../CustomButon";
import { CONVERT_MONTH } from "../../../helper/globalFuncion";
import CustomChip from "../../CustomChip";
import { Axios } from "../../../helper/http";
import ModalManage from "../../ModalManage";
import { useSelector } from "react-redux";

import { VIEW_FILE_PDF } from "../../../helper/globalFuncion";
import ModalReject from "../../ModalReject";

export default function InquiryReviewSummaryBarTable() {
  const { period, refresh } = useSelector(({ globalReducer }) => globalReducer);
  const [rows, setRows] = useState([]);
  const [openGenerate, setOpenGenerate] = useState(false);
  const [loading, setLoading] = useState([]);
  const [selected, setSelected] = useState([]);
  const [body, setBody] = useState({});
  const [modalManage, setModalManage] = useState();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [modalReject, setModalReject] = useState(false);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const columns = [
    [
      {
        name: (
          <Checkbox
            sx={{
              color: "white",
              "&.Mui-checked": {
                color: "white",
              },
            }}
          />
        ),
        rowSpan: 2,
        renderCell: ({ MPM_ID }, i) => (
          <Checkbox
            onClick={({ target }) => {
              if (target.checked) {
                setSelected([...selected, MPM_ID]);
              } else {
                let data = [...selected];
                setSelected(data.filter((val) => val !== MPM_ID));
              }
            }}
          />
        ),
        width: 10,
      },
      {
        name: "Number BAR",
        rowSpan: 2,
        renderCell: ({ MPM_NO_BAR }, i) => MPM_NO_BAR,
      },
      {
        name: (
          <div>
            ID <br /> Merchant
          </div>
        ),
        rowSpan: 2,
        renderCell: ({ M_ID }) => M_ID,
        width: 70,
      },
      {
        name: "Nama Merchant",
        rowSpan: 2,
        renderCell: ({ M_LEGAL_NAME }) => M_LEGAL_NAME,
        width: 200,
      },
      {
        name: "Transaksi Merchant",
        colSpan: 9,
        heading: true,
      },
    ],
    [
      {
        name: "Tahun",
        renderCell: ({ PERIODE }) => new Date(PERIODE).getFullYear(),
      },
      {
        name: "Bulan",
        renderCell: ({ PERIODE }) => CONVERT_MONTH(PERIODE),
      },
      {
        name: "MDR",
        renderCell: ({ MPM_MDR }) => MPM_MDR,
      },
      {
        name: "TRX",
        renderCell: ({ MPM_COUNT_TRX }) => MPM_COUNT_TRX,
      },
      {
        name: "Total",
        renderCell: ({ MPM_TOTAL }) => MPM_TOTAL,
      },
      {
        name: "Status",
        renderCell: (data) => <CustomChip.Status data={data} />,
      },
      {
        name: "View File PDF",
        renderCell: ({ DOCUMENT, MPM_ID }) => (
          <Grid>
            <CustomChip MPM_ID={DOCUMENT} onClick={handleClick} label="View" />
            <Menu
              id="basic-menu"
              anchorEl={anchorEl}
              open={open}
              onClose={handleClose}
              MenuListProps={{
                "aria-labelledby": "basic-button",
              }}
            >
              {DOCUMENT.map((val) => {
                return (
                  <MenuItem onClick={() => VIEW_FILE_PDF(val, MPM_ID)}>
                    {val?.MPDOC_NAME}
                  </MenuItem>
                );
              })}
            </Menu>
          </Grid>
        ),
      },
      {
        name: "Action",
        renderCell: (res) => (
          <CustomChip onClick={() => setModalManage(res)} label="Manage Doc" />
        ),
      },
    ],
  ];

  const getDataTable = async () => {
    setLoading(true);
    Axios.get("/merchant-transaksi-master-action/views?", {
      params: {
        filter: { where: { and: [{ AT_POSISI: 5 }, { PERIODE: period }] } },
      },
    })
      .then((response) => {
        if (response.data.success) {
          console.log(response.data.datas);
          setRows(response.data.datas);
          setTimeout(() => {
            setLoading(false);
          }, 1000);
        }
      })
      .catch((err) => console.log(err));
  };

  useEffect(() => {
    getDataTable();
  }, [refresh]);

  const handleValidate = () => {
    if (selected.length > 0) {
      Swal.fire({
        title: "Apakah anda yakin?",
        text: `Validasi Generate Bar Number Periode ${period}`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Submit",
      }).then((result) => {
        if (result.isConfirmed) {
          Axios.post(
            "/merchant-transaksi-master-action/validation/data-master/account-recv/validate",
            {
              MPM_ID: selected,
            }
          ).then(({ data }) => {
            Swal.fire({
              icon: data.success ? "success" : "error",
              title: data.success ? "Success" : "Error",
              text: data.success && "Validasi data berhasil!",
            });
            getDataTable();
          });
        }
      });
    } else {
      noDataSelect();
    }
  };

  const noDataSelect = (label) => {
    return Swal.fire({
      icon: "warning",
      title: "tidak ada data yang dipilih",
      text: `mohon pilih data yang akan di ${label ? label : "validasi"}`,
    });
  };

  return (
    <Grid>
      <Grid
        mt="36px"
        sx={{ "& a": { textDecoration: "none" } }}
        container
        gap="24px"
      >
        <CustomButon.Primary width={195}>Export to Excel</CustomButon.Primary>
        <CustomButon.Primary width={195} onClick={handleValidate}>
          Validasi
        </CustomButon.Primary>
        <CustomButon.Error
          onClick={() =>
            selected.length > 0 ? setModalReject(true) : noDataSelect("reject")
          }
        >
          Reject
        </CustomButon.Error>
      </Grid>
      <Grid mt="36px">
        <TableComponent loading={loading} columns={columns} rows={rows} />
      </Grid>
      {modalManage && (
        <ModalManage open={modalManage} onClose={() => setModalManage()} />
      )}
      {modalReject && (
        <ModalReject
          open={modalReject}
          onClose={() => setModalReject(false)}
          urlApi={
            "/merchant-transaksi-master-action/validation/data-master/account-recv/reject-ba"
          }
          selected={selected}
          getDataTable={() => getDataTable()}
        />
      )}
    </Grid>
  );
}
