import React from "react";
import Checkbox from "@mui/material/Checkbox";
import TableComponentSticky from "../../TableComponentSticky";
import { CrossIcon, CheckIcon } from "../../../assets/icons/customIcon";

function TableUploadTransaksiExcludeBa() {
  const columns = [
    [
      {
        name: (
          <Checkbox
            sx={{
              color: "white",
              "&.Mui-checked": {
                color: "white",
              },
            }}
          />
        ),
        rowSpan: 2,
        renderCell: ({ status }, i) => <Checkbox />,
        sticky: true
      },
      {
        name: "Customer Name",
        renderCell: ({ kodeMerchant }) => kodeMerchant,
        textAlign: "left",
        sticky: true
      },
      {
        name: "Customer Code",
        renderCell: ({ namaMerchant }) =>
          namaMerchant ? namaMerchant : "No Data",
        textAlign: "left",
        sticky: true
      },
      {
        name: "Invoice Type",
        renderCell: ({ namaFile }) => namaFile,
        textAlign: "left",
        sticky: true
      },
      {
        name: "Month",
        renderCell: ({ namaFile }) => namaFile,
        textAlign: "left",
        sticky: true
      },
      {
        name: "Transaksi Source",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Transaksi Type",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Terms",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Invoice Date",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Due Date",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "DPP",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "PPN",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "PPH",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Amount",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Description",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Account Name",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "No Rek",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "No BAST",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "PIC",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "No Kontrak",
        renderCell: ({ namaFile }) => namaFile,
      },
    ],
  ];

  const rows = [
    {
      code: 6690,
      kodeMerchant: "PT. Pegadaian (Persero)",
      namaMerchant: "",
      namaFile: "-",
      status: false,
    },
    {
      code: 6690,
      kodeMerchant: "PT. Bank Rakyat Indonesia",
      namaMerchant: "",
      namaFile: "-",
      status: true,
    },
    {
        code: 6690,
        kodeMerchant: "PT. Bank Rakyat Indonesia",
        namaMerchant: "",
        namaFile: "-",
        status: true,
    }
  ];
  return <TableComponentSticky columns={columns} rows={rows} />;
}

export default TableUploadTransaksiExcludeBa;
