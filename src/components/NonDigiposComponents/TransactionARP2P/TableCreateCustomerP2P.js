import React from "react";
import Checkbox from "@mui/material/Checkbox";
import TableComponent from "../../TableComponent";
import { CrossIcon, CheckIcon } from "../../../assets/icons/customIcon";

function TableCreateCustomerP2P() {
  const columns = [
    [
      {
        name: (
          <Checkbox
            sx={{
              color: "white",
              "&.Mui-checked": {
                color: "white",
              },
            }}
          />
        ),
        rowSpan: 2,
        renderCell: ({ status }, i) => <Checkbox />,
        sticky: true
      },
      {
        name: "ID",
        renderCell: ({ kodeMerchant }) => kodeMerchant,
        textAlign: "left",
        sticky: true
      },
      {
        name: "Name",
        renderCell: ({ namaMerchant }) =>
          namaMerchant ? namaMerchant : "No Data",
          textAlign: "left",
          sticky: true
      },
      {
        name: "NPWP",
        renderCell: ({ namaFile }) => namaFile,
        textAlign: "left",
        sticky: true
      },
      {
        name: "Account Desc",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Legal Name",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Customer Class",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Customer Source",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Prefix",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Expired Date",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Site Name",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Address NPWP",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "City",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Province",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Postal Code",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Country",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Business",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Bill To",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "First Name",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Last Name",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Phone",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Phone Area",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Mobile Phone Code",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Mobile Phone Number",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Email",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Interface Status",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Organization ID",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Error Message",
        renderCell: ({ namaFile }) => namaFile,
      },
    ],
  ];

  const rows = [
    {
      code: 6690,
      kodeMerchant: "Manual_P2P",
      namaMerchant: "",
      namaFile: "-",
      status: false,
    },
    {
      code: 6690,
      kodeMerchant: "Manual_P2P",
      namaMerchant: "",
      namaFile: "-",
      status: true,
    }
  ];
  return <TableComponent columns={columns} rows={rows}/>;
}

export default TableCreateCustomerP2P;
