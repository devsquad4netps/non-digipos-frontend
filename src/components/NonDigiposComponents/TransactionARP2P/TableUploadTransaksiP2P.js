import React from "react";
import Checkbox from "@mui/material/Checkbox";
import TableComponentSticky from "../../TableComponentSticky";
import { CrossIcon, CheckIcon } from "../../../assets/icons/customIcon";

function TableUploadTransaksiP2P() {
  const columns = [
    [
      {
        name: (
          <Checkbox
            sx={{
              color: "white",
              "&.Mui-checked": {
                color: "white",
              },
            }}
          />
        ),
        rowSpan: 2,
        renderCell: ({ status }, i) => <Checkbox />,
        sticky: true
      },
      {
        name: "Transaction Source",
        renderCell: ({ kodeMerchant }) => kodeMerchant,
        textAlign: "left",
        sticky: true
      },
      {
        name: "Transaction Type",
        renderCell: ({ namaMerchant }) =>
          namaMerchant ? namaMerchant : "No Data",
        textAlign: "left",
        sticky: true
      },
      {
        name: "Cust Name",
        renderCell: ({ namaFile }) => namaFile,
        textAlign: "left",
        sticky: true
      },
      {
        name: "Inv Number",
        renderCell: ({ namaFile }) => namaFile,
        textAlign: "left",
        sticky: true
      },
      {
        name: "Month",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Tenor",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Tanggal Pendanaan",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Due Date",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Disbursment Amount",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Description",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Contract Number",
        renderCell: ({ namaFile }) => namaFile,
      },
      {
        name: "Loan Type",
        renderCell: ({ namaFile }) => namaFile,
      }
    ],
  ];

  const rows = [
    {
      code: 6690,
      kodeMerchant: "Manual_P2P",
      namaMerchant: "",
      namaFile: "-",
      status: false,
    },
    {
      code: 6690,
      kodeMerchant: "Manual_P2P",
      namaMerchant: "",
      namaFile: "-",
      status: true,
    }
  ];
  return <TableComponentSticky columns={columns} rows={rows} />;
}

export default TableUploadTransaksiP2P;
