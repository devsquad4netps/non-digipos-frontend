import React, { useState, useEffect } from "react";
import {
  Checkbox,
  Grid,
  RadioGroup,
  FormControlLabel,
  Radio,
} from "@mui/material";
import CustomModal from "./CustomModal";
import { Axios } from "../helper/http";
import { makeStyles } from "@mui/styles";
import CustomInput from "./InputComponent";
import Swal from "sweetalert2";
import { useForm } from "react-hook-form";
import CustomButon from "./CustomButon";

const useStyles = makeStyles((theme) => ({
  Modal: {
    "& .title": {
      fontWeight: 700,
      fontSize: "16px",
      lineHeight: "15px",
    },
    "& .items": {
      "& label": {
        display: "flex",
        justifyContent: "space-between",
      },
    },
  },
}));

export default function ModalDistribute({ open, onClose, selected }) {
  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
    setValue,
  } = useForm();
  const classes = useStyles();
  const [datas, setDatas] = useState({
    AVALUABLE_ID: {},
  });
  const [dataDistribute, setDataDistribute] = useState({
    DISTRIBUTE_MERCHANT: 0,
    FEEDBACK_MERCHANT: 0,
  });
  const [body, setBody] = useState({
    MPM_ID: selected,
    DOC_DISTRIBUTE: [0, 0, 0, 0, 0, 0, 0],
    NOTE: " ",
    SEND_CC: "",
  });

  const checkDistribute = () => {
    Axios.post("/merchant-transaksi-master-action/distribusi/checking", {
      ...dataDistribute,
      MPM_ID: selected,
    }).then((res) => {
      setDatas(res.data.datas);
      const { AVALIABLE_DOC } = res.data.datas;
      const clone = [0, 0, 0, 0, 0, 0, 0];
      Object.keys(AVALIABLE_DOC).map((val) => {
        return (
          AVALIABLE_DOC[val] === 1 &&
          (val === "BAR"
            ? (clone[0] = 1)
            : val === "BAR_SIGN"
            ? (clone[1] = 1)
            : val === "INVOICE"
            ? (clone[2] = 1)
            : val === "INVOICE_SIGN"
            ? (clone[3] = 1)
            : val === "FAKTUR_PAJAK"
            ? (clone[4] = 1)
            : val === "BUKPOT"
            ? (clone[5] = 1)
            : val === "OTHERS"
            ? (clone[6] = 1)
            : "")
        );
      });
      setBody({ ...body, DOC_DISTRIBUTE: clone });
      // datas["BAR"] === 1 ? setBody({...body, DOC_DISTRIBUTE :})
    });
  };

  useEffect(() => {
    checkDistribute();
  }, [dataDistribute]);

  const handleCheck = (index) => {
    let clone = [...body?.DOC_DISTRIBUTE];
    clone[index] === 1 ? (clone[index] = 0) : (clone[index] = 1);
    setBody({ ...body, DOC_DISTRIBUTE: clone });
  };

  var isDisable = true;
  Object.keys(datas?.AVALUABLE_ID).map((val, i) => {
    if (datas?.AVALUABLE_ID[val].length > 0) {
      if (
        body?.DOC_DISTRIBUTE[i] === 1 &&
        (dataDistribute.DISTRIBUTE_MERCHANT === 1 ||
          dataDistribute.FEEDBACK_MERCHANT === 1)
      ) {
        isDisable = false;
      }
    }
  });

  const onSubmit = (val) => {
    onClose();
    let result = { ...body, ...dataDistribute, ...val };
    Axios.post(
      "merchant-transaksi-master-action/distribusi/process-distribute",
      result
    )
      .then(({ data }) =>
        Swal.fire({
          icon: data.success ? "success" : "error",
          title: data.success ? "Success" : "Error",
          text: data.message,
        })
      )
      .catch((error) => console.log(error));
  };

  return (
    <CustomModal
      open={open}
      className={classes.Modal}
      onClose={onClose}
      width={800}
    >
      <h3 className="title">Form Distribusi Document</h3>
      <Grid container alignItems="center" className="items" spacing="24px">
        <Grid item md={5}>
          <label for="">
            Distribute to BU <span>:</span>{" "}
          </label>
        </Grid>
        <Grid item md={7}>
          <RadioGroup
            row
            aria-labelledby="demo-row-radio-buttons-group-label"
            name="row-radio-buttons-group"
            onChange={(e, v) => {
              setBody({ ...body, DOC_DISTRIBUTE: [0, 0, 0, 0, 0, 0, 0] });
              setDataDistribute({
                ...dataDistribute,
                DISTRIBUTE_MERCHANT: Number(v),
              });
            }}
            value={dataDistribute?.DISTRIBUTE_MERCHANT}
          >
            <FormControlLabel value={1} control={<Radio />} label="Yes" />
            <FormControlLabel value={0} control={<Radio />} label="No" />
          </RadioGroup>
        </Grid>
      </Grid>
      <Grid container alignItems="center" className="items" spacing="24px">
        <Grid item md={5}>
          <label for="">
            Feedback Merchant <span>:</span>{" "}
          </label>
        </Grid>
        <Grid item md={7}>
          <RadioGroup
            row
            aria-labelledby="demo-row-radio-buttons-group-label"
            name="row-radio-buttons-group"
            onChange={(e, v) => {
              setBody({ ...body, DOC_DISTRIBUTE: [0, 0, 0, 0, 0, 0, 0] });
              setDataDistribute({
                ...dataDistribute,
                FEEDBACK_MERCHANT: Number(v),
              });
            }}
            value={dataDistribute?.FEEDBACK_MERCHANT}
          >
            <FormControlLabel value={1} control={<Radio />} label="Yes" />
            <FormControlLabel value={0} control={<Radio />} label="No" />
          </RadioGroup>
        </Grid>
      </Grid>
      <Grid container className="items" spacing="24px">
        <Grid item md={5}>
          <label for="">
            Document Distribusi <span>:</span>{" "}
          </label>
        </Grid>
        <Grid item md={7} container>
          <Grid item md={7}>
            <FormControlLabel
              sx={{ width: "fit-content" }}
              label={`Berita Acara Recon (${datas?.AVALIABLE_DOC?.BAR})`}
              control={
                <Checkbox
                  checked={body.DOC_DISTRIBUTE[0] === 1}
                  onClick={() => handleCheck(0)}
                  disabled={datas?.AVALIABLE_DOC?.BAR === 0}
                />
              }
            />
          </Grid>
          <Grid item md={5}>
            <FormControlLabel
              sx={{ width: "fit-content" }}
              label={`Faktur Pajak (${datas?.AVALIABLE_DOC?.FAKTUR_PAJAK})`}
              control={
                <Checkbox
                  checked={body.DOC_DISTRIBUTE[4] === 1}
                  onClick={() => handleCheck(4)}
                  disabled={datas?.AVALIABLE_DOC?.FAKTUR_PAJAK === 0}
                />
              }
            />
          </Grid>
          <Grid item md={7}>
            <FormControlLabel
              sx={{ width: "fit-content" }}
              label={`Berita Acara Sign (${datas?.AVALIABLE_DOC?.BAR_SIGN})`}
              control={
                <Checkbox
                  checked={body.DOC_DISTRIBUTE[1] === 1}
                  onClick={() => handleCheck(1)}
                  disabled={datas?.AVALIABLE_DOC?.BAR_SIGN === 0}
                />
              }
            />
          </Grid>
          <Grid item md={5}>
            <FormControlLabel
              sx={{ width: "fit-content" }}
              label={`Bukti Potong (${datas?.AVALIABLE_DOC?.BUKPOT})`}
              control={
                <Checkbox
                  checked={body.DOC_DISTRIBUTE[5] === 1}
                  onClick={() => handleCheck(5)}
                  disabled={datas?.AVALIABLE_DOC?.BUKPOT === 0}
                />
              }
            />
          </Grid>
          <Grid item md={7}>
            <FormControlLabel
              sx={{ width: "fit-content" }}
              label={`Invoice (${datas?.AVALIABLE_DOC?.INVOICE})`}
              control={
                <Checkbox
                  checked={body.DOC_DISTRIBUTE[2] === 1}
                  onClick={() => handleCheck(2)}
                  disabled={datas?.AVALIABLE_DOC?.INVOICE === 0}
                />
              }
            />
          </Grid>
          <Grid item md={5}>
            <FormControlLabel
              sx={{ width: "fit-content" }}
              label={`Others (${datas?.AVALIABLE_DOC?.OTHERS})`}
              control={
                <Checkbox
                  checked={body.DOC_DISTRIBUTE[6] === 1}
                  onClick={() => handleCheck(6)}
                  disabled={datas?.AVALIABLE_DOC?.OTHERS === 0}
                />
              }
            />
          </Grid>
          <Grid item md={7}>
            <FormControlLabel
              sx={{ width: "fit-content" }}
              label={`Invoice Sign (${datas?.AVALIABLE_DOC?.INVOICE_SIGN})`}
              control={
                <Checkbox
                  checked={body.DOC_DISTRIBUTE[3] === 1}
                  onClick={() => handleCheck(3)}
                  disabled={datas?.AVALIABLE_DOC?.INVOICE_SIGN === 0}
                />
              }
            />
          </Grid>
        </Grid>
      </Grid>
      <form onSubmit={handleSubmit(onSubmit)} id="submit-form">
        <Grid
          container
          className="items"
          spacing="24px"
          sx={{
            "& label": {
              color: errors["SEND_CC"] && "red",
            },
            "& .required": {
              flex: 1,
              color: "red",
              marginLeft: "4px",
            },
          }}
        >
          <Grid item md={5}>
            <label for="">
              Send CC <span className="required">*</span> <span>:</span>{" "}
            </label>
          </Grid>
          <Grid item md={7} container>
            <CustomInput
              id="SEND_CC"
              errors={errors}
              register={{
                ...register("SEND_CC", {
                  required: true,
                }),
              }}
            />
          </Grid>
        </Grid>
        <Grid
          container
          className="items"
          spacing="24px"
          mt="-16px"
          sx={{
            "& label": {
              color: errors["SEND_CC"] && "red",
            },
            "& .required": {
              flex: 1,
              color: "red",
              marginLeft: "4px",
            },
          }}
        >
          <Grid item md={5}>
            <label for="">
              Note <span className="required">*</span> <span>:</span>{" "}
            </label>
          </Grid>
          <Grid item md={7} container>
            <CustomInput
              errors={errors}
              id="NOTE"
              register={{ ...register("NOTE", { required: true }) }}
              multiline
              minRows={3}
            />
          </Grid>
        </Grid>
        <Grid container className="items" spacing="24px" mt="-16px">
          <Grid item md={5}>
            <label for="">
              Transaksi Selected Distribusi <span>:</span>{" "}
            </label>
          </Grid>
          <Grid item md={7} container>
            <strong>{selected.length}</strong>
          </Grid>
        </Grid>
        <Grid container className="items" spacing="24px" mt="-16px">
          <Grid item md={5}>
            <label for="">
              Transaksi Available Distribusi <span>:</span>{" "}
            </label>
          </Grid>
          <Grid item md={7} container>
            <strong>
              {Object.keys(datas?.AVALUABLE_ID).length > 0 &&
                Object.values(datas?.AVALUABLE_ID).reduce((a, b) =>
                  a.length > b.length ? a : b
                ).length}{" "}
              (Show)
            </strong>
          </Grid>
        </Grid>
        <Grid container className="items" mt="10px" justifyContent="flex-end">
          <CustomButon.Error onClick={onClose}>Cancel</CustomButon.Error>
          <CustomButon.Primary
            sx={{ marginLeft: "32px" }}
            type="submit"
            id="submit-form"
            disabled={isDisable}
          >
            Submit
          </CustomButon.Primary>
        </Grid>
      </form>
    </CustomModal>
  );
}
