import React, { useState, useEffect } from "react";
import { Outlet } from "react-router-dom";
import { makeStyles } from "@mui/styles";

import { Grid, IconButton, Avatar, SvgIcon, Skeleton } from "@mui/material";

import { BellIcon, NavbarIcon, LogoutIcon } from "../assets/icons/customIcon";

import ListMenu from "./ListMenu";
import BAToolsLogo from "../assets/images/logo.svg";
import { getUserId } from "../helper/cookies";
import master from "../helper/services/master";
import { useSelector, useDispatch } from "react-redux";
import { removeCookie } from "../helper/cookies";

const useStyles = makeStyles((theme) => {
  const { customTheme } = theme;
  return {
    root: {
      "& h3": {
        margin: 0,
      },
    },
    navbar: {
      backgroundColor: customTheme ? customTheme.color.primary : "",
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      padding: "24px",
      "& svg": {
        color: "white",
      },
    },
    content: {
      width: "100%",
      backgroundColor: "#E1E9FF",
      overflow: "auto",
      marginRight: "5px",
      marginTop: "26px",
      marginLeft: "14px",
      paddingRight: "18px",
      "&::-webkit-scrollbar": {
        backgroundColor: "#aebde5",
        width: "10px",
      },
      "&::-webkit-scrollbar-thumb": {
        background: customTheme ? customTheme.color.primary : "",
        borderRadius: "10px",
      },
      paddingBottom: "150px",
      paddingTop: "10px",
      paddingLeft: "10px",
    },
    sidebar: {
      transition: "0.3s",
      backgroundColor: " #FFFFFF",
      boxShadow: "0px 0px 20px rgba(0, 0, 0, 0.15)",
      minWidth: "270px",
    },
    contentRight: {
      display: "flex",
      alignItems: "center",
      color: "white",
    },
  };
});

export default function Layout(params) {
  const idUser = getUserId();
  const classes = useStyles();
  const [sidebar, setSidebar] = useState(true);
  const [loading, setLoading] = useState(true);
  const dataUser = useSelector((state) => state.globalReducer.dataUser);
  const dispatch = useDispatch();

  const getDataUser = async () => {
    const response = await master.DataUser();
    dispatch({ type: "DATA_USER", payload: response.data });
    setTimeout(() => {
      setLoading(false);
    }, 1000);
  };

  const doLogout = () => {
    removeCookie();
    window.location.href = window.location.origin + "/login";
  }

  useEffect(() => {
    getDataUser();
    if (!idUser) {
      window.location.href = window.location.origin + "/login";
    }
  }, []);

  return (
    <Grid
      sx={{
        height: "100vh",
        display: "flex",
        flexDirection: "column",
        overflow: "hidden",
        backgroundColor: "#E1E9FF",
      }}
      className={classes.root}
    >
      <Grid className={classes.navbar}>
        <Grid>
          <IconButton onClick={() => setSidebar(!sidebar)}>
            <SvgIcon component="object">
              <NavbarIcon />
            </SvgIcon>
          </IconButton>
        </Grid>
        <Grid className={classes.contentRight}>
          <BellIcon />
          <Avatar sx={{ ml: "36px", mr: "16px" }}>SA</Avatar>
          {loading ? (
            <Skeleton variant="text" width={"200px"} />
          ) : (
            <h3>Halo, {dataUser?.username}</h3>
          )}
          <IconButton onClick={() => doLogout()}>
            <SvgIcon component="object">
              <LogoutIcon />
            </SvgIcon>
          </IconButton>
        </Grid>
      </Grid>
      <Grid
        height="100%"
        display="flex"
        className={sidebar ? "sidebar-open" : "sidebar-closed"}
      >
        <Grid
          className={[classes.sidebar, "ndgp-sidebar"]}
          sx={{
            overflow: "auto",
            height: "100%",
          }}
        >
          <Grid sx={{ height: "100%" }}>
            <Grid
              sx={{
                "& img": {
                  margin: "24px",
                  width: "100%",
                  height: "auto",
                },
              }}
              container
              className="ndgp-sidebar-logo"
            >
              <img src={BAToolsLogo} alt="" />
            </Grid>
            <ListMenu />
          </Grid>
        </Grid>
        <Grid className={classes.content}>
          <Outlet />
        </Grid>
      </Grid>
    </Grid>
  );
}
