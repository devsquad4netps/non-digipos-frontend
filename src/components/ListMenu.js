import React, { useEffect, useState } from "react";
import { List, Grid } from "@mui/material";
import ListItemButton from "@mui/material/ListItemButton";
import Collapse from "@mui/material/Collapse";
import { makeStyles } from "@mui/styles";
import { Link } from "react-router-dom";
import master from "../helper/services/master";
import { Axios } from "../helper/http";
import { getUserId } from "../helper/cookies";
import { ListMenuSidebar } from "../assets/icons/customIcon";
import { useLocation } from "react-router-dom";

import {
  DashboardIcon,
  PaperIcon,
  ChildMenuIconFirst,
  ChildMenuIconSecond,
  ChildMenuPaperIcon,
  ArrowIcon,
  SettingIcons,
} from "../assets/icons/customIcon";
import id from "date-fns/esm/locale/id/index";

const useStyles = makeStyles({
  root: {
    width: "100%",
    paddingRight: "24px",
    "& .MuiListItemText-root": {
      margin: 0,
      paddingTop: "6px",
      paddingBottom: "6px",
    },
    "& .MuiListItemButton-root": {
      padding: 0,
      // border: "1px solid",
    },
    "& span": {
      maxWidth: "180px",
    },
  },
});

function list_to_tree(list) {
  var map = {},
    node,
    roots = [],
    i;

  for (i = 0; i < list.length; i += 1) {
    map[list[i].id] = i; // initialize the map
    list[i].child = []; // initialize the children
  }

  for (i = 0; i < list.length; i += 1) {
    node = list[i];
    if (node.M_Parent !== 0) {
      // if you have dangling branches check that map[node.parentId] exists
      list[map[node.M_Parent]].child.push(node);
    } else {
      roots.push(node);
    }
  }
  return roots;
}

export default function ListMenu() {
  const classes = useStyles();
  const [dataMenu, setDataMenu] = useState([]);
  const [allMenu, setAllMenu] = useState([]);

  const link = useLocation()
    .pathname.split("/")
    .filter((val) => val !== "");

  const getMenu = async () => {
    Axios.get(`/users/${getUserId()}`).then((res) => {
      Axios.post("/Menu/getMenuRole", {
        role: res.data.id,
      }).then((result) => {
        const data = result.data.info;
        setAllMenu(data);
        const newArray = [];
        data.map((val) => newArray.push({ ...val, child: [] }));

        setDataMenu(
          list_to_tree(newArray.sort((a, b) => a.M_Index - b.M_Index))
        );
      });
    });
  };

  useEffect(() => {
    getMenu();
  }, []);

  return (
    <Grid
      sx={{ paddingBottom: "100px", width: "100%" }}
      className="ndgp-sidebar-menu"
    >
      <List
        component="nav"
        className={classes.root}
        aria-labelledby="nested-list-subheader"
        sx={{ width: "auto" }}
      >
        <MenusFunction res={dataMenu} allMenu={allMenu} link={link} />
      </List>
    </Grid>
  );
}

const LoopFunction = ({ res, url, allMenu, link }) => {
  return (
    <MenusFunction
      res={res}
      child={true}
      urlChild={url}
      link={link}
      allMenu={allMenu}
    />
  );
};

const MenusFunction = ({ res, child, urlChild, allMenu, link }) => {
  const [open, setOpen] = useState();

  useEffect(() => {
    if (link.length > 0) {
      let menuOpen = allMenu
        .filter((val) => val.M_Link === `/${link[0]}`)
        .pop();
      setOpen(menuOpen);
    }
  }, [allMenu]);

  const handleClick = (val) => {
    setOpen(val.child.length > 0 && open === val ? 0 : val);
    if (val.child.length > 0) {
    }
  };

  return res.map((val, i) => {
    return (
      <Grid
        key={i}
        sx={{
          "& .link-route": {
            textDecoration: "none",
          },
        }}
      >
        <Link
          to={
            val?.M_Link
              ? val?.child?.length > 0
                ? "#"
                : urlChild
                ? urlChild + val?.M_Link
                : val?.M_Link
              : "none-pages"
          }
          className="link-route"
        >
          <ListItemButton
            onClick={() => {
              handleClick(val);
            }}
            sx={{
              marginBottom: "12px",
              minWidth: "180px",
              backgroundColor: open?.id === val.id && !child && "#29647B",
            }}
          >
            <Grid
              sx={{
                display: "flex",
                alignItems: "center",
                paddingLeft: "24px",
                paddingTop: "6px",
                paddingBottom: "6px",
                justifyContent: "space-between",
                borderLeft:
                  open?.id === val.id &&
                  val.child.length === 0 &&
                  child &&
                  "6px solid #29647B",
                width: "100%",
                backgroundColor:
                  open?.id === val.id &&
                  val.child.length === 0 &&
                  child &&
                  "whiteSmoke",
                "& svg": {
                  marginRight: "8px",
                  color:
                    open?.id === val.id && !child
                      ? "white"
                      : open?.id === val.id && child
                      ? "#29647B"
                      : "rgba(0, 0, 0, 0.5)",
                },
                "& h4": {
                  fontWeight: 600,
                  fontSize: "14px",
                  lineHeight: "16px",
                  margin: 0,
                  color:
                    open?.id === val.id && !child
                      ? "white"
                      : open?.id === val.id && child
                      ? "#29647B"
                      : "rgba(0, 0, 0, 0.5)",
                  maxWidth: "180px",
                  textDecoration: "none",
                },
                "&:hover": {
                  "& h4, svg": {
                    color: "#29647B",
                  },
                },
                "& .arrow-icon": {
                  transform: open?.id === val.id ? "" : "rotate(-90deg)",
                  transition: "0.3s",
                },
              }}
            >
              <Grid
                sx={{
                  display: "flex",
                  alignItems: "center",
                }}
              >
                {/* {val?.M_Icon || ""} */}
                {
                  ListMenuSidebar.filter((res) => res.id === val.M_Icon)[0]
                    ?.icon
                }
                <h4 data={val.child}>{val.M_Name}</h4>
              </Grid>
              {val.child.length > 0 && <ArrowIcon className="arrow-icon" />}
            </Grid>
          </ListItemButton>
        </Link>
        {val.child && (
          <Collapse
            in={open?.id === val.id}
            timeout="auto"
            unmountOnExit
            sx={{ marginLeft: "10px" }}
          >
            <LoopFunction
              res={val.child}
              url={urlChild ? urlChild + val.M_Link : val.M_Link}
              allMenu={allMenu}
              link={link.filter((a, i) => i !== 0)}
            />
          </Collapse>
        )}
      </Grid>
    );
  });
};
