import React from "react";
import { Grid } from "@mui/material";

export default function ProgressOnTable({ value = 0 }) {
  return (
    <Grid
      sx={({ customTheme }) => ({
        position: "relative",
        background: `linear-gradient(90deg, ${customTheme.color.primary} ${value}%, transparent ${value}%)`,
        // border: "1px solid gray",
        color: value > 55 && "white",
      })}
    >
      <strong>{value}%</strong>
    </Grid>
  );
}
