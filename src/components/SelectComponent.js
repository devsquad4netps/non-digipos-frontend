import React from "react";
import { Button, MenuItem, Grid, Select } from "@mui/material";

const CustomSelect = (props) => {
  return <Button variant="contained">Contained</Button>;
};
const SelectLabel = ({
  label = "",
  required = false,
  labelStyle = {},
  labelWidth,
  register,
  value= '',
  onChange,
  options= [{label: 'Option 1', id:1 ,value:1},{label: 'Option 2', id:2, value:2},{label: 'Option 3', id:3, value:3}]
}) => {
  return (
    <Grid
      item
      sx={{
        display: "flex",
        alignItems: "center",
        width: "100%",
        "& label": {
          minWidth: labelWidth || "180px",
          fontWeight: 600,
          fontSize: "12px",
          borderRadius: "10px",
          lineHeight: "16px",
          ...labelStyle,
          "& span": {
            color: "red",
            marginLeft: "5px",
          },
        },
      }}
    >
      <label htmlFor="">
        {label || "Undifend"}
        {required && <span>*</span>}
      </label>
      <Select
        disableUnderline
        value={value}
        variant="standard"
        fullWidth
        onChange={onChange}
        inputProps={{
          ...register,
          sx: {
            padding: 0,
            paddingLeft: "10px",
            paddingRight: "10px",
            backgroundColor: "white",
            borderRadius: "10px",
            minHeight: "25px",
            height: "25px",
            fontSize: "12px",
            border: "1px solid #000000",
          },
        }}
        >
          {options.map((option)=> {
            return (<MenuItem key={option.value} value={option.value}>{option.label}</MenuItem>)
          })}
        
        </Select>
    </Grid>
  );
};

const SelectLabelAPIGet = ({
  label = "",
  required = false,
  labelStyle = {},
  labelWidth,
  register,
  value= '',
  onChange,
  width,
  options= [{label: 'Option 1', id:1 ,value:1},{label: 'Option 2', id:2, value:2},{label: 'Option 3', id:3, value:3}]
}) => {
  return (
    <Grid
      item
      sx={{
        display: "flex",
        alignItems: "center",
        width: "100%",
        "& label": {
          minWidth: labelWidth || "180px",
          fontWeight: 600,
          fontSize: "14px",
          borderRadius: "10px",
          lineHeight: "16px",
          ...labelStyle,
          "& span": {
            color: "red",
            marginLeft: "5px",
          },
        },
      }}
    >
      <label htmlFor="">
        {label || "Undifend"}
        {required && <span>*</span>}
      </label>
      <Select
        disableUnderline
        value={value}
        variant="standard"
        fullWidth
        onChange={onChange}
        inputProps={{
          ...register,
          sx: {
            padding: 0,
            paddingLeft: "10px",
            paddingRight: "10px",
            backgroundColor: "white",
            borderRadius: "10px",
            minHeight: "25px",
            height: "25px",
            fontSize: "12px",
            border: "1px solid #000000",
          },
        }}
        >
          {options.map((option)=> {
            return (<MenuItem key={option.ID} value={option.ID}>{option.LABEL}</MenuItem>)
          })}
        
        </Select>
    </Grid>
  );
};

CustomSelect.SelectLabel = SelectLabel;
CustomSelect.SelectLabelAPIGet = SelectLabelAPIGet;

export default CustomSelect;
