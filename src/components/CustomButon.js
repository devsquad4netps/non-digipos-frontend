import React from "react";
import { Button } from "@mui/material";

const CustomButon = (props) => {
  return <Button variant="contained">Contained</Button>;
};

const ButtonPrimary = ({
  sx,
  children,
  onClick,
  size = "",
  type,
  disabled,
  id,
  minWidth,
  component,
}) => {
  return (
    <Button
      id={id}
      onClick={onClick}
      variant="contained"
      type={type}
      component={component}
      disabled={disabled}
      sx={(theme) => ({
        textTransform: "none",
        backgroundColor: theme.customTheme.color.primary,
        padding: size === "small" ? "5px" : "8px",
        paddingLeft: "26px",
        paddingRight: "26px",
        fontWeight: 700,
        fontSize: "12px",
        lineHeight: "14px",
        minWidth: minWidth,
        ...sx,
      })}
    >
      {children}
    </Button>
  );
};

const ButtonError = ({
  sx,
  children,
  onClick,
  size = "",
  disabled,
  minWidth,
}) => {
  return (
    <Button
      color="error"
      onClick={onClick}
      variant="contained"
      disabled={disabled}
      sx={(theme) => ({
        textTransform: "none",
        backgroundColor: "#FF0000",
        padding: size === "small" ? "5px" : "8px",
        paddingLeft: "26px",
        paddingRight: "26px",
        fontWeight: 700,
        fontSize: "12px",
        lineHeight: "14px",
        minWidth: minWidth,
        ...sx,
      })}
    >
      {children}
    </Button>
  );
};

CustomButon.Primary = ButtonPrimary;
CustomButon.Error = ButtonError;

export default CustomButon;
