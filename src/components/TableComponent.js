import React, { useState, useEffect, Fragment } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Grid,
  Pagination,
  CircularProgress,
  Collapse,
  Select,
  MenuItem,
} from "@mui/material";
import EmptyTable from "../assets/images/empty-table.png";
import { useSelector, useDispatch } from "react-redux";
import { SEARCH_DATA_TABLE } from "../helper/globalFuncion";

export default function TableComponent({
  columns,
  rows,
  noPaging,
  loading,
  onClick,
  hover,
  selected,
  isCollapse,
  dataCollapse,
  noImage,
  idSearch,
}) {
  const { dataSearch } = useSelector(({ globalReducer }) => globalReducer);
  const [openCollapse, setOpenCollapse] = useState(false);
  const [page, setPage] = useState(1);
  const [rowsPage, setRowsPage] = useState(10);
  const [isLoading, setIsLoading] = useState(loading);
  const [newRows, setNewRows] = useState([]);

  useEffect(() => {
    setIsLoading(loading);
  }, [loading]);

  useEffect(() => {
    setNewRows(rows);
  }, [rows]);

  useEffect(() => {
    if (idSearch === dataSearch.id && dataSearch?.id !== "") {
      setIsLoading(true);
      setNewRows(SEARCH_DATA_TABLE(rows, dataSearch?.value));
      setTimeout(() => {
        setIsLoading(false);
      }, 500);
    }
  }, [dataSearch]);

  return (
    <Grid>
      <TableContainer
        component={Paper}
        sx={{
          borderRadius: "10px",
          padding: "0px",
          overflow: "auto",
          paddingBottom: "12px",
        }}
        df
      >
        <Table sx={{ width: "100%" }} aria-label="simple table">
          <TableHead
            sx={(theme) => ({
              backgroundColor: theme.customTheme.color.primary,
            })}
          >
            {columns.map((res, i) => {
              return (
                <TableRow key={i}>
                  {res.map(
                    (
                      { name, rowSpan, colSpan, align, heading, width, sticky },
                      index
                    ) => {
                      let sxProp;

                      if (!sticky) {
                        sxProp = {
                          border: "1px solid white",
                          color: "white",
                          padding: 0,
                          paddingTop: "14px",
                          paddingBottom: "14px",
                          fontWeight: 700,
                          fontSize: "14px",
                          lineHeight: "16px",
                          minWidth: width ? (isLoading ? 120 : width) : 120,
                          // minWidth: width,
                          paddingLeft: "8px",
                          paddingRight: "8px",
                          transition: "0.3s",
                        };
                      } else {
                        sxProp = (theme) => ({
                          backgroundColor: theme.customTheme.color.primary,
                          position: "sticky",
                          left: !width ? index * 120 : index * width - 12,
                          zIndex: 2,
                          border: "1px solid white",
                          color: "white",
                          padding: 0,
                          paddingTop: "14px",
                          paddingBottom: "14px",
                          fontWeight: 700,
                          fontSize: "14px",
                          lineHeight: "16px",
                          minWidth: width ? (isLoading ? 120 : width) : 120,
                          // minWidth: width,
                          paddingLeft: "8px",
                          paddingRight: "8px",
                          transition: "0.3s",
                        });
                      }
                      return (
                        <TableCell
                          align={align || "center"}
                          key={index}
                          rowSpan={rowSpan}
                          colSpan={colSpan}
                          sx={sxProp}
                        >
                          {name}
                        </TableCell>
                      );
                    }
                  )}
                </TableRow>
              );
            })}
          </TableHead>
          <TableBody>
            {isLoading ? (
              <TableRow
                sx={{
                  width: "100%",
                  position: "relative",
                  height: "300px",
                }}
              >
                <Grid
                  component="td"
                  sx={({ customTheme }) => ({
                    width: "100%",
                    position: "absolute",
                    left: "0px",
                    height: "100%",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    // height: "200px",
                    "& h3": {
                      paddingLeft: "24px",
                      color: customTheme.color.primary,
                    },
                  })}
                >
                  <CircularProgress
                    size={60}
                    sx={({ customTheme }) => ({
                      color: customTheme.color.primary,
                    })}
                  />
                  <h3>Loading...</h3>
                </Grid>
              </TableRow>
            ) : newRows.length > 0 ? (
              newRows
                .slice((page - 1) * rowsPage, page * rowsPage)
                .map((result, index) => {
                  let sxProp;
                  if (selected) {
                    sxProp = {
                      "&:nth-of-type(even)": {
                        backgroundColor: "white",
                      },
                      "&:nth-of-type(odd)": {
                        backgroundColor: "#EDEDED",
                      },
                      "&:last-child td, &:last-child th": {
                        border: 0,
                      },
                      cursor: "pointer",
                    };
                  } else {
                    if (isCollapse) {
                      sxProp = {
                        backgroundColor: index % 2 === 0 ? "#EDEDED" : "white",
                        cursor: "pointer",
                      };
                    } else {
                      sxProp = {
                        "&:nth-of-type(even)": {
                          backgroundColor: "white",
                        },
                        "&:nth-of-type(odd)": {
                          backgroundColor: "#EDEDED",
                        },
                        "&:last-child td, &:last-child th": {
                          border: 0,
                        },
                      };
                    }
                  }

                  let columLength = [];
                  columns.map((val) => val.map((res) => columLength.push(res)));
                  columLength = columLength.filter((val) => !val.colSpan);

                  return (
                    <Fragment>
                      <TableRow
                        onClick={() => {
                          if (onClick) {
                            onClick(result);
                            setOpenCollapse(
                              openCollapse === index ? false : index
                            );
                          }
                        }}
                        key={index}
                        hover={hover}
                        selected={selected}
                        sx={sxProp}
                      >
                        {columns.map((res) =>
                          res.map(
                            (
                              {
                                heading,
                                align,
                                renderCell,
                                width,
                                textAlign,
                                sticky,
                              },
                              i
                            ) => {
                              if (!heading) {
                                let sxProp;
                                if (!sticky) {
                                  sxProp = {
                                    fontWeight: 400,
                                    fontSize: "14px",
                                    lineHeight: "16px",
                                    padding: "12px",
                                    paddingTop: "8px",
                                    paddingBottom: "8px",
                                  };
                                } else {
                                  sxProp = (theme) => ({
                                    backgroundColor: "inherit",
                                    position: "sticky",
                                    left: !width ? i * 120 : i * width - 12,
                                    zIndex: 1,
                                    fontWeight: 400,
                                    fontSize: "14px",
                                    lineHeight: "16px",
                                    padding: "12px",
                                    paddingTop: "8px",
                                    paddingBottom: "8px",
                                  });
                                }
                                return (
                                  <TableCell
                                    key={i}
                                    align={align || textAlign || "center"}
                                    sx={sxProp}
                                  >
                                    {renderCell
                                      ? renderCell(
                                          result,
                                          index,
                                          rowsPage * (page - 1) + index + 1
                                        )
                                      : "GA ADA"}
                                  </TableCell>
                                );
                              }
                            }
                          )
                        )}
                      </TableRow>
                      {isCollapse && (
                        <TableRow>
                          <TableCell
                            colSpan={columLength.length}
                            sx={{
                              paddingTop: 0,
                              paddingBottom: 0,
                            }}
                          >
                            <Collapse
                              in={openCollapse === index}
                              timeout="auto"
                              unmountOnExit
                              sx={{
                                width: "100%",
                              }}
                            >
                              <Grid container colSpan={10}>
                                {dataCollapse && dataCollapse()}
                              </Grid>
                            </Collapse>
                          </TableCell>
                        </TableRow>
                      )}
                    </Fragment>
                  );
                })
            ) : (
              <TableRow
                sx={{
                  width: "100%",
                  position: "relative",
                  height: noImage ? "100%" : "300px",
                }}
              >
                <Grid
                  sx={{
                    width: "100%",
                    position: "absolute",
                    left: "0px",
                    height: "100%",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    // height: "200px",
                  }}
                  component="td"
                >
                  {!noImage && (
                    <Grid
                      sx={{
                        "& img": {
                          height: "240px",
                        },
                        height: "100%",
                        textAlign: "center",
                        "& h2": {
                          margin: 0,
                          marginTop: "-24px",
                          color: "gray",
                        },
                      }}
                    >
                      <img src={EmptyTable} alt="" />
                      <h2>No Data Availabe</h2>
                    </Grid>
                  )}
                </Grid>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <Grid mt="36px" container justifyContent="flex-end">
        {!noPaging && (
          <Grid style={{ display: "flex", alignItems: "center" }}>
            Show
            <Select
              labelId="demo-select-small"
              id="demo-select-small"
              size="small"
              sx={{
                backgroundColor: "white",
                padding: 0,
                height: "32px",
                marginRight: "12px",
                marginLeft: "12px",
                width: "80px",
              }}
              value={rowsPage}
              onChange={(e) => {
                setRowsPage(Number(e.target.value));
                setPage(1);
              }}
            >
              <MenuItem value={5}>5</MenuItem>
              <MenuItem value={10}>10</MenuItem>
              <MenuItem value={25}>25</MenuItem>
              <MenuItem value={50}>50</MenuItem>
              <MenuItem value={100}>100</MenuItem>
            </Select>
            <Pagination
              page={page}
              onChange={(a, b) => setPage(b)}
              sx={(theme) => ({
                "& .MuiPagination-ul": {
                  "& button": {
                    margin: 0,
                    borderRadius: 0,
                    border: "none",
                    backgroundColor: "white",
                  },
                },
                "& .MuiPaginationItem-previousNext": {
                  // border: "2px solid  blue!important",
                  marginRight: "8px!important",
                  marginLeft: "8px!important",
                },
                "& .Mui-selected": {
                  border: "2px solid",
                  "&[aria-current=true]": {
                    backgroundColor: theme.customTheme.color.primary,
                    color: "white",
                  },
                },
                marginRight: "-8px!important",
              })}
              count={Math.round(newRows.length / rowsPage) || 1}
              variant="outlined"
              shape="rounded"
            />
          </Grid>
        )}
      </Grid>
    </Grid>
  );
}
