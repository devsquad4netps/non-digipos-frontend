import React, { useEffect, useState, useCallback, useMemo } from "react";
import {
  Button,
  TextField,
  Grid,
  Input,
  InputAdornment,
  Select,
  OutlinedInput,
  IconButton,
} from "@mui/material";

import { SearchIcon } from "../assets/icons/customIcon";
import { EyeSolidIcon, EyeSolidSlahIcon } from "../assets/icons/customIcon";

const CustomInput = ({
  label = "",
  type = "text",
  multiline,
  minRows,
  required = false,
  labelStyle = {},
  labelWidth,
  register,
  id = "",
  errors = {},
  errorMessage,
  disabled,
  value,
  placeholder,
  compo,
  sx,
  onChange,
}) => {
  const isError = errors[id] ? true : false;
  errorMessage = errors[id]?.message || "";
  return (
    <Grid
      container
      sx={{
        "& p": {
          margin: 0,
          color: "red",
        },
      }}
    >
      <Input
        autoComplete={true}
        id={id}
        type={type}
        disableUnderline
        size="small"
        fullWidth
        value={value}
        variant="contained"
        multiline={multiline}
        minRows={minRows}
        disabled={disabled}
        onChange={onChange}
        placeholder={placeholder ? placeholder : label}
        sx={{
          "& .Mui-disabled": {
            border: "1px solid #C5C5C5",
            backgroundColor: "#C5C5C5",
          },
          ...sx,
        }}
        inputProps={{
          ...register,
          sx: {
            padding: 0,
            paddingLeft: "10px",
            paddingRight: "10px",
            backgroundColor: "white",
            borderRadius: "10px",
            minHeight: "25px",
            height: "25px",
            fontSize: "12px",
            border: isError ? "1.2px solid red" : "1.2px solid #000000",
          },
        }}
      />
      {isError && (
        <p>{errorMessage ? errorMessage : `${label} cannot be empety`}</p>
      )}
    </Grid>
  );
};

const InputLabel = ({
  label = "",
  type = "text",
  multiline,
  minRows,
  required = false,
  labelStyle = {},
  labelWidth,
  register,
  id = "",
  errors = {},
  errorMessage,
  disabled,
  value,
  placeholder,
  compo,
  width,
}) => {
  const isError = errors[id] ? true : false;
  return (
    <Grid
      item
      sx={{
        m: 0.2,
        display: "flex",
        alignItems: !multiline && "center",
        width: "100%",
        "& label": {
          minWidth: labelWidth || "180px",
          fontWeight: 600,
          fontSize: "14px",
          lineHeight: "16px",
          color: isError && "red",
          ...labelStyle,
          "& span": {
            color: "red",
            marginLeft: "5px",
          },
        },
      }}
    >
      <label htmlFor="">
        {label || "Undifend"}
        {required && <span>*</span>}
      </label>
      <Grid
        container
        sx={{
          "& p": {
            margin: 0,
            color: "red",
          },
        }}
      >
        {!compo ? (
          <Input
            id={id}
            type={type}
            disableUnderline
            size="small"
            fullWidth
            value={value}
            variant="contained"
            multiline={multiline}
            minRows={minRows}
            disabled={disabled}
            placeholder={placeholder ? placeholder : label}
            sx={{
              "& .Mui-disabled": {
                border: "1px solid #C5C5C5",
                backgroundColor: "#C5C5C5",
              },
            }}
            inputProps={{
              ...register,
              sx: {
                padding: 0,
                paddingLeft: "10px",
                paddingRight: "10px",
                backgroundColor: "white",
                borderRadius: "10px",
                minHeight: "25px",
                height: "25px",
                fontSize: "12px",
                border: isError ? "1.2px solid red" : "1.2px solid #000000",
              },
            }}
          />
        ) : (
          <Grid>{compo}</Grid>
        )}
        {isError && (
          <p>{errorMessage ? errorMessage : `${label} cannot be empety`}</p>
        )}
      </Grid>
    </Grid>
  );
};

CustomInput.InputLabel = InputLabel;

const LoginInput = ({
  icon,
  position = "start",
  placeholder,
  register,
  id = "",
  errors = {},
  errorMessage,
  type,
  disabled,
}) => {
  const [show, setShow] = useState(false);
  const isError = errors[id] ? true : false;

  return (
    <Grid sx={{ textAlign: "left", "& span": { color: "red" } }}>
      <TextField
        id={id}
        variant="outlined"
        size="small"
        placeholder={placeholder}
        fullWidth
        error={isError}
        type={type === "password" && !show ? "password" : ""}
        disabled={disabled}
        InputProps={{
          startAdornment: (
            <InputAdornment position={position}>{icon}</InputAdornment>
          ),
          endAdornment: type === "password" && (
            <InputAdornment position={position}>
              <IconButton
                onClick={() => setShow(!show)}
                sx={{
                  padding: 0,
                  "& svg": {
                    width: "20px",
                  },
                }}
              >
                {!show ? <EyeSolidSlahIcon /> : <EyeSolidIcon />}
              </IconButton>
            </InputAdornment>
          ),
          ...register,
        }}
      />
      {isError && (
        <span>{errorMessage ? errorMessage : `${id} cannot be empety`}</span>
      )}
    </Grid>
  );
};

CustomInput.LoginInput = LoginInput;

const FileInput = ({
  register,
  errors,
  id,
  values,
  errorMessage,
  onChange,
}) => {
  const [val, setVal] = useState();

  const handleView = () => {
    if (val) {
      let reader = new FileReader();
      reader.readAsDataURL(val);
      reader.onload = () => {
        console.log(reader);
        // window.location.href = reader.result;
        window.open(reader.result, "_blank");
        // return reader.result;
      };
      reader.onerror = function (error) {
        console.log("Error: ", error);
      };
    }
  };

  const isError = errors && errors[id] ? true : false;

  return (
    <Grid
      sx={{
        "& .error": {
          color: "red",
        },
      }}
    >
      <Grid
        sx={{
          display: "flex",
          justifyContent: "space-between",
          "& span": {
            width: "80%",
            color: "blue",
            textDecoration: "underline",
            "&: hover": {
              cursor: "pointer",
            },
            textAlign: "right",
          },
        }}
      >
        <Button
          sx={({ customTheme }) => ({
            textTransform: "none",
            fontWeight: 700,
            fontSize: "12px",
            lineHeight: "15px",
            margin: "0px",
            height: "100%",
            border: `1.2px solid ${isError ? "red" : "#000000"}`,
            borderRadius: "10px 0px 0px 10px",
            backgroundColor: isError ? "red" : customTheme.color.primary,
          })}
          padding={0}
          variant="contained"
          component="label"
          size="small"
        >
          Browse
          <input
            id={id}
            {...register}
            onChange={onChange ? onChange : register.onChange}
            type="file"
            hidden
            accept="application/pdf"
          />
        </Button>
        <Grid
          sx={{
            border: `1.2px solid ${isError ? "red" : "#000000"}`,
            width: "100%",
            borderLeft: "none",
            borderRadius: "0px 10px 10px 0px",
            paddingLeft: "8px",
          }}
        >
          {(values?.length > 0 && values[0].name) || values || ""}
        </Grid>
        {/* <span onClick={handleView}>View Attach</span> */}
      </Grid>
      {isError && (
        <span className="error">
          {errorMessage ? errorMessage : "File cannot be empety"}
        </span>
      )}
    </Grid>
  );
};

CustomInput.File = FileInput;

const SelectInput = ({
  children,
  multiline,
  labelWidth,
  labelStyle,
  label,
  required,
  errorMessage,
  defaultValue,
  register,
  value,
  errors = {},
  id = "",
  onChange,
  labelSize,
}) => {
  const [defVal, setDefVal] = useState();

  useMemo(() => {
    setDefVal(defaultValue);
  }, [defaultValue]);

  const isError = errors[id] || false;

  return (
    <Grid
      item
      sx={{
        display: "flex",
        alignItems: !multiline && "center",
        width: "100%",
        "& label": {
          minWidth: labelWidth || "180px",
          fontWeight: 600,
          fontSize: labelSize || "12px",
          lineHeight: "16px",
          color: isError && "red",
          ...labelStyle,
          "& span": {
            color: "red",
            marginLeft: "5px",
          },
        },
      }}
    >
      <label htmlFor="">
        {label || "Undifend"}
        {required && <span>*</span>}
      </label>
      <Grid
        container
        sx={{
          "& p": {
            margin: 0,
            color: "red",
          },
        }}
      >
        <Select
          // value={age}
          // onChange={handleChange}
          value={value || defVal}
          onChange={onChange}
          autoWidth
          fullWidth
          size="small"
          sx={{
            "& .MuiOutlinedInput-notchedOutline": {
              border: "none!important",
            },
          }}
          inputProps={{
            ...register,
            sx: {
              padding: 0,
              paddingLeft: "10px",
              paddingRight: "10px",
              backgroundColor: "white",
              borderRadius: "10px",
              minHeight: "25px",
              height: "25px",
              fontSize: "12px",
              border: isError ? "1.2px solid red" : "1.2px solid #000000",
            },
          }}
        >
          {children}
        </Select>
        {isError && (
          <p>{errorMessage ? errorMessage : `${label} cannot be empety`}</p>
        )}
      </Grid>
    </Grid>
  );
};

CustomInput.Select = SelectInput;

const SearchInput = ({ onChange }) => {
  return (
    <OutlinedInput
      id="outlined-adornment-weight"
      onChange={onChange}
      endAdornment={
        <InputAdornment position="end">
          <SearchIcon />
        </InputAdornment>
      }
      sx={{ borderRadius: "8px" }}
      aria-describedby="outlined-weight-helper-text"
      inputProps={{
        "aria-label": "weight",
        sx: {
          padding: "9px",
          paddingTop: "4.5px",
          paddingBottom: "4.5px",
        },
      }}
    />
  );
};

CustomInput.Search = SearchInput;

export default CustomInput;
