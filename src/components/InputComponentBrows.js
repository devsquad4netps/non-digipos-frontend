import React from "react";
import { Button } from "@mui/material";

const InputComponentBrows = (props) => {
  return <Button variant="contained">Contained</Button>;
};
 
const ButtonPrimary = ({ sx, children, onClick, size = "" }) => {
  return (
    <Button
      onClick={onClick}
      variant="contained"
      sx={(theme) => ({
        textTransform: "none",
        backgroundColor: theme.customTheme.color.primary,
        padding: size === "small" ? "5px" : "8px",
        paddingLeft: "26px",
        paddingRight: "26px",
        fontWeight: 700,
        type: "file",
        fontSize: "12px",
        lineHeight: "14px",
        ...sx,
      })}
    >
      {children}
    </Button>
  );
};

InputComponentBrows.Primary = ButtonPrimary;

export default InputComponentBrows;
