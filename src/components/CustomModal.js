import React from "react";
import { Modal, Grid, IconButton } from "@mui/material";
import { XMarksIcon } from "../assets/icons/customIcon";

export default function CustomModal({
  open,
  onClose,
  children,
  title,
  width,
  height,
  className,
}) {
  return (
    <Modal
      open={open}
      onClose={onClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Grid
        sx={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          height: height ? height : "auto",
          width: width ? width : "auto",
          bgcolor: "#FFFFFF",
          p: "32px",
          borderRadius: "10px",
          "& h1, h2, h3": {
            margin: 0,
          },
          "& h3": {
            marginBottom: "12px",
            fontWeight: 700,
            fontSize: "12px",
            lineHeight: "16px",
          },
        }}
      >
        <Grid sx={{ position: "relative", paddingTop: "12px" }}>
          <IconButton
            onClick={onClose}
            sx={{
              position: "absolute",
              right: -24,
              top: -24,
              "& svg": {
                height: "18px",
                width: "auto",
              },
            }}
          >
            <XMarksIcon />
          </IconButton>
          {title && <h3>{title || ""}</h3>}
          <Grid className={className}>{children}</Grid>
        </Grid>
      </Grid>
    </Modal>
  );
}
