import React from "react";
import { Button, MenuItem, Grid, Select } from "@mui/material";

const CustomSelectBrand = (props) => {
  return <Button variant="contained">Contained</Button>;
};
const SelectLabel = ({
  label = "",
  required = false,
  labelStyle = {},
  labelWidth,
  options= [{label: 'ITEM KU', value:'ITEM KU'},{label: 'UNIPIN', value:'UNIPIN'},{label: 'SYARIAH', value:'SYARIAH'}],
  register
}) => {
  return (
    <Grid
      item
      sx={{
        display: "flex",
        alignItems: "center",
        width: "100%",
        "& label": {
          minWidth: labelWidth || "180px",
          fontWeight: 600,
          fontSize: "12px",
          borderRadius: "10px",
          lineHeight: "16px",
          ...labelStyle,
          "& span": {
            color: "red",
            marginLeft: "5px",
          },
        },
      }}
    >
      <label htmlFor="">
        {label || "Undifend"}
        {required && <span>*</span>}
      </label>
      <Select
        disableUnderline
        variant="standard"
        fullWidth
        {...register}
        inputProps={{
          sx: {
            padding: 0,
            paddingLeft: "10px",
            paddingRight: "10px",
            backgroundColor: "white",
            borderRadius: "10px",
            minHeight: "25px",
            height: "25px",
            fontSize: "12px",
            border: "1px solid #000000",
          },
        }}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {options.map((option, i)=> {
            return (<MenuItem key={i} value={option.value}>{option.label}</MenuItem>)
          })}
        
        </Select>
      {/* <Input
        disableUnderline
        size="small"
        fullWidth
        variant="contained"
        multiline={multiline}
        minRows={minRows}
        inputProps={{
          sx: {
            padding: 0,
            paddingLeft: "10px",
            paddingRight: "10px",
            backgroundColor: "white",
            borderRadius: "10px",
            minHeight: "25px",
            height: "25px",
            fontSize: "12px",
            border: "1px solid #000000",
          },
        }}
      /> */}
    </Grid>
  );
};

CustomSelectBrand.SelectLabel = SelectLabel;

export default CustomSelectBrand;
