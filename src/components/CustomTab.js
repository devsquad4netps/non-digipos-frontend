import React from "react";
import { Button } from "@mui/material";

const CustomButon = (props) => {
  return <Button variant="contained">Contained</Button>;
};
 
const ButtonPrimary = (props) => {
  return (
    <Button
      variant="contained"
      sx={(theme) => ({
        ...props.sx,
        textTransform: "none",
        backgroundColor: theme.customTheme.color.primary,
        padding: "20px",
        paddingLeft: "26px",
        paddingRight: "26px",
        fontWeight: 700,
        fontSize: "12px",
        lineHeight: "14px",
        marginTop: "10px",
        width: '515px'
      })}
    >
      {props.children}
    </Button>
  );
};

CustomButon.Primary = ButtonPrimary;

export default CustomButon;
