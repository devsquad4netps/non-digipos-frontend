import React from "react";
import { Button, TextField, Grid, Input, Checkbox } from "@mui/material";

const CustomInputCek = (props) => {
  return <Button variant="contained">Contained</Button>;
};
 
const InputLabel = ({
  label = "",
  multiline,
  minRows,
  required = false,
  labelStyle = {},
  labelWidth,
  register,
  checked,
  id = "",
}) => {
  return (
    <Grid
      item
      sx={{
        display: "flex",
        alignItems: !multiline && "center",
        width: "100%",
        "& label": {
          minWidth: labelWidth || "180px",
          fontWeight: 600,
          fontSize: "12px",
          lineHeight: "16px",
          ...labelStyle,
          "& span": {
            color: "red",
            marginLeft: "5px",
          },
        },
      }}
    >
      <label htmlFor="">
        {label || "Undifend"}
        {required && <span>*</span>}
      </label>
      <Checkbox
         
        disableUnderline
        size="small"
        fullWidth
        variant="contained"
        multiline={multiline}
        minRows={minRows}
        inputProps={{
          ...register,
          sx: {
            padding: 0,
            paddingLeft: "10px",
            paddingRight: "10px",
            backgroundColor: "white",
            borderRadius: "10px",
            minHeight: "50px",
            height: "25px",
            fontSize: "12px",
            border: "1px solid #000000",
          },
        }}
        defaultChecked={checked ? checked : false}
      />
    </Grid>
  );
};

CustomInputCek.InputLabel = InputLabel;

export default CustomInputCek;
