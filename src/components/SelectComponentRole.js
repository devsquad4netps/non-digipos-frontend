import React from "react";
import { Button, MenuItem, Grid, Select } from "@mui/material";

const CustomSelectRole = (props) => {
  return <Button variant="contained">Contained</Button>;
};
const SelectLabel = ({
  label = "",
  required = false,
  labelStyle = {},
  labelWidth,
  value= '',
  onChange,
  options= [{label: 'SUPERADMIN', value:1},{label: 'BUSINESS ASSURANCE', value:2},{label: 'RECEIVABLE', value:3},{label: 'TAX', value:4}],
  register
}) => {
  return (
    <Grid
      item
      sx={{
        display: "flex",
        alignItems: "center",
        width: "100%",
        "& label": {
          minWidth: labelWidth || "180px",
          fontWeight: 600,
          fontSize: "12px",
          borderRadius: "10px",
          lineHeight: "16px",
          ...labelStyle,
          "& span": {
            color: "red",
            marginLeft: "5px",
          },
        },
      }}
    >
      <label htmlFor="">
        {label || "Undifend"}
        {required && <span>*</span>}
      </label>
      <Select
        disableUnderline
        value={value}
        variant="standard"
        fullWidth
        onChange={onChange}
        {...register}
        inputProps={{
          sx: {
            padding: 0,
            paddingLeft: "10px",
            paddingRight: "10px",
            backgroundColor: "white",
            borderRadius: "10px",
            minHeight: "25px",
            height: "25px",
            fontSize: "12px",
            border: "1px solid #000000",
          },
        }}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          {options.map((option, i)=> {
            return (<MenuItem key={i} value={option.value}>{option.label}</MenuItem>)
          })}
        
        </Select>
      {/* <Input
        disableUnderline
        size="small"
        fullWidth
        variant="contained"
        multiline={multiline}
        minRows={minRows}
        inputProps={{
          sx: {
            padding: 0,
            paddingLeft: "10px",
            paddingRight: "10px",
            backgroundColor: "white",
            borderRadius: "10px",
            minHeight: "25px",
            height: "25px",
            fontSize: "12px",
            border: "1px solid #000000",
          },
        }}
      /> */}
    </Grid>
  );
};

CustomSelectRole.SelectLabel = SelectLabel;

export default CustomSelectRole;
