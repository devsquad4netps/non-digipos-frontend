import React from "react";
import EmptyDataImage from "../assets/images/empty-data.png";
import { Grid, CircularProgress } from "@mui/material";
import { color } from "@mui/system";

export default function EmptyData({ loading }) {
  return (
    <Grid
      sx={{
        backgroundColor: "white",
        boxShadow: "0px 0px 8px 2px rgba(0, 0, 0, 0.25)",
        borderRadius: "20px",
        textAlign: "center",
        "& h4": {
          margin: 0,
          color: "#7F7F7F",
        },
        paddingTop: "24px",
        paddingBottom: "24px",
      }}
    >
      {loading ? (
        <CircularProgress
          sx={({ customTheme }) => ({ color: customTheme.color.primary })}
        />
      ) : (
        <>
          <img src={EmptyDataImage} alt="" />
          <h3>OOOPS!</h3>
          <h4>Sorry~ No data found</h4>
        </>
      )}
    </Grid>
  );
}
