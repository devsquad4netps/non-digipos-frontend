import React, { useState, useEffect } from "react";
import { Stack, TextField, IconButton, Grid, Button } from "@mui/material";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import { useForm, Controller } from "react-hook-form";
import { useSelector, useDispatch } from "react-redux";
import moment from "moment";

import { SubmitIcon } from "../assets/icons/customIcon";

const DateComp = ({ register, value, controler, setValue, id }) => {
  const [values, setVal] = useState(moment().format("YYYY-MM-DD"));
  const { handleSubmit, control } = useForm();

  const handleChange = (val) => {
    const date = moment(val).format("YYYY-MM-DD");
    console.log(date);
    setVal(date);
    // let date = val ? new Date(val) : values;
    // let newDate = `${date.getFullYear()}/${
    //   date.getMonth() + 1
    // }/${date.getDate()}`;
  };

  useEffect(() => {
    setValue(id, values);
  }, [values]);

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <Stack spacing={3}>
        <DesktopDatePicker
          inputFormat="yyyy/MM/dd"
          value={values}
          onChange={handleChange}
          renderInput={(params) => {
            return (
              <TextField
                sx={{ width: "180px" }}
                {...register}
                size="small"
                {...params}
                value={values}
                disabled
              />
            );
          }}
        />
      </Stack>
    </LocalizationProvider>
  );
};

const Periode = ({ onChange, withoutSubmit, disabled }) => {
  const periode = useSelector((state) => state.globalReducer.period);
  const refresh = useSelector((state) => state.globalReducer.refresh);
  const dispatch = useDispatch();

  const handleChange = (newValue) => {
    const date = moment(newValue).format("YYYY-MM");
    dispatch({ type: "PERIOD", payload: date });
    onChange(date);
  };

  useEffect(() => {}, []);

  const handleSubmit = () => {
    dispatch({ type: "REFRESH", payload: !refresh });
  };

  return (
    <Grid
      sx={{
        display: "inline-flex",
        alignItems: "center",
      }}
    >
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <Stack spacing={3}>
          <DesktopDatePicker
            format="YYYY-MM"
            openTo="month"
            views={["year", "month"]}
            inputFormat="yyyy-MM"
            value={new Date(periode)}
            onChange={handleChange}
            InputProps={{ readOnly: true }}
            disabled={disabled}
            renderInput={(params) => {
              return (
                <TextField
                  sx={{ width: "180px" }}
                  disabled={true}
                  size="small"
                  {...params}
                />
              );
            }}
          />
        </Stack>
      </LocalizationProvider>
      {!withoutSubmit && (
        <IconButton
          type="submit"
          onClick={handleSubmit}
          sx={({ customTheme }) => ({
            marginLeft: "12px",
            backgroundColor: customTheme.color.primary,
            color: "white",
            width: "40px",
            borderRadius: "6px",
            height: "32px",
          })}
        >
          <SubmitIcon />
        </IconButton>
      )}
    </Grid>
  );
};

DateComp.Periode = Periode;

export default DateComp;
