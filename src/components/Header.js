import React, { Fragment, useState, useEffect } from "react";
import {
  Paper,
  Grid,
  IconButton,
  TextField,
  InputAdornment,
} from "@mui/material";
import {
  HomeIcon,
  ArrowRightIcon,
  ArrowIcon,
  SearchIcon,
} from "../assets/icons/customIcon";
import { useLocation } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import DateComp from "./DatePicker";

export default function Header({ ...props }) {
  const dispatch = useDispatch();
  const { titles, idSearch } = props;
  const url = useLocation().pathname.split("/");
  const [dataSearch, setDataSearch] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    dispatch({
      type: "DATA_SEARCH",
      payload: { id: idSearch, value: dataSearch },
    });
  };

  useEffect(() => {
    dispatch({ type: "DATA_SEARCH", payload: { id: "", value: "" } });
  }, []);

  let datas = titles ? titles : url;
  return (
    <Paper
      sx={{
        boxShadow: "0px 0px 8px 2px rgba(0, 0, 0, 0.25)",
        borderRadius: "20px",
        padding: "24px",
        paddingTop: "20px",
        paddingBottom: "20px",
        "& .content-left": {
          display: "flex",
          alignItems: "center",
          gap: "12px",
          color: "#29647B",
          "& .arrow": {
            transform: "rotate(-90deg)",
          },
          "& h4": {
            margin: 0,
            fontWeight: 400,
            fontSize: "14px",
            lineHeight: "16px",
          },
          "& .active": {
            fontWeight: 700,
          },
        },
        "& .content-right": {
          display: "flex",
          justifyContent: "flex-end",
        },
      }}
    >
      <Grid
        display="flex"
        sx={({ customTheme }) => ({
          paddingLeft: "12px",
          paddingTop: "4px",
          paddingBottom: "4px",
        })}
        container
      >
        <Grid className="content-left" item md={8}>
          <IconButton href="/dashboard">
            <HomeIcon className="test" />
          </IconButton>
          {datas.map((val, index) => {
            if (val) {
              return (
                <Fragment>
                  <ArrowIcon className="arrow" />
                  <h4 className={index === url.length - 1 ? "active" : ""}>
                    {val}
                  </h4>
                </Fragment>
              );
            }
          })}
        </Grid>
        <Grid className="content-right" item md={4}>
          <DateComp.Periode />
          <form action="" onSubmit={handleSubmit}>
            <TextField
              id="outlined-start-adornment"
              sx={{ m: 1, width: "240px", marginLeft: "24px" }}
              size="small"
              onChange={(e) => setDataSearch(e.target.value)}
              onBlur={(e) => handleSubmit()}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="start">
                    <IconButton type="submit">
                      <SearchIcon />
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
          </form>
        </Grid>
      </Grid>
    </Paper>
  );
}
