import React, { useState, useEffect } from "react";
import { Axios } from "../helper/http";
import {
  Grid,
  IconButton,
  TextField,
  ButtonGroup,
  Button,
  Switch,
} from "@mui/material";

import Swal from "sweetalert2";

import { TrashIcon } from "../assets/icons/customIcon";
import CustomModal from "./CustomModal";
import CustomInput from "./InputComponent";
import TableComponent from "./TableComponent";
import CustomButon from "./CustomButon";

const ModalManage = ({ open, onClose }) => {
  const [active, setActive] = useState(0);
  const [rows, setRows] = useState([]);
  const [loading, setLoading] = useState(false);
  const [file, setFile] = useState();
  const columns = [
    [
      {
        name: "No",
        renderCell: ({ MPM_NO_BAR }, i) => i + 1,
        width: 20,
      },
      {
        name: "Document",
        renderCell: ({ MPDOC_NAME }, i) => MPDOC_NAME,
      },
      {
        name: "At Create",
        renderCell: ({ AT_CREATE }, i) => AT_CREATE,
      },
      {
        name: "User Create",
        renderCell: ({ MPM_NO_BAR }, i) => "",
      },
      {
        name: "Status",
        renderCell: ({ AT_FLAG, MPDOC_ID }, i) => (
          <Switch
            onClick={() => {
              if (AT_FLAG !== 1) handleSwitch(MPDOC_ID);
            }}
            checked={AT_FLAG === 1}
          />
        ),
      },
      {
        name: "Action",
        renderCell: ({ MPDOC_ID }, i) => (
          <Grid>
            <IconButton onClick={() => handleDelete(MPDOC_ID)}>
              <TrashIcon />
            </IconButton>
          </Grid>
        ),
        width: 80,
      },
    ],
  ];

  const handleSwitch = (idDoc) => {
    Axios.post("/merchant-manage-doc/active/" + idDoc)
      .then((res) => {
        console.log(res);
        if (res.data.success) {
          getDataTable();
        }
      })
      .catch((err) => console.log(err));
  };

  const handleDelete = (MPDOC_ID) => {
    Axios.post(`/merchant-manage-doc/delete/${MPDOC_ID}`)
      .then(({ data }) => {
        if (data.success) {
          onClose();
          handleAlert(data);
        }
      })
      .catch((err) => console.log(err));
  };

  const getDataTable = () => {
    setLoading(true);
    Axios.get(
      `/merchant-manage-doc/views/${open.MPM_ID}/${
        active === 0
          ? "bar"
          : active === 1
          ? "bar-sign"
          : active === 2
          ? "invoice"
          : active === 3
          ? "invoice-sign"
          : active === 4
          ? "faktur-pajak"
          : active === 5
          ? "bukti-potong"
          : "others"
      }`
    ).then(({ data }) => {
      setRows(data.datas);
    });
    setTimeout(() => {
      setLoading(false);
    }, 1000);
  };

  useEffect(() => {
    getDataTable();
  }, [active]);

  const handleUpload = () => {
    const formData = new FormData();
    formData.append("document", file[0]);
    Axios.post(
      `/distribusi-upload/merchant/upload/${open.MPM_ID}/${
        active === 0
          ? "bar"
          : active === 1
          ? "bar-sign"
          : active === 2
          ? "invoice"
          : active === 3
          ? "invoice-sign"
          : active === 4
          ? "faktur-pajak"
          : active === 5
          ? "bukti-potong"
          : "others"
      }`,
      formData
    )
      .then(({ data }) => {
        onClose();
        if (data.success) {
          setFile([]);
        }
        handleAlert(data);
        console.log(data);
      })
      .catch((err) => console.log(err));
  };

  const handleAlert = (data) => {
    Swal.fire({
      icon: data.success ? "success" : "error",
      title: data.success ? "Success" : "Error",
      text: data.message.sqlMessage || data.message || "error",
    });
  };

  return (
    <CustomModal height={"auto"} open={open} onClose={onClose} width="auto">
      <ButtonGroup
        variant="contained"
        aria-label="outlined primary button group"
        sx={{ width: "100%" }}
      >
        {[
          "BAR",
          "BAR Sign",
          "Invoice",
          "Invoice Sign",
          "Faktur Pajak",
          "Bukpot",
          "Others",
        ].map((val, i) => {
          return (
            <Button
              onClick={() => setActive(i)}
              sx={({ customTheme }) => ({
                backgroundColor:
                  active === i
                    ? customTheme.color.primary
                    : "rgba(41, 100, 123, 0.5)",
                width: "100%",
                textTransform: "none",
              })}
            >
              {val}
            </Button>
          );
        })}
      </ButtonGroup>
      <TextField
        multiline
        fullWidth
        disabled
        minRows={3}
        sx={{ mt: "12px", mb: "12px", maxHeight: "150px", overflow: "auto" }}
        value={JSON.stringify(open)}
      />
      <Grid container mb="12px" spacing={"18px"}>
        <Grid item md={4}>
          <CustomInput.File
            values={file || []}
            onChange={(e) => setFile(e.target.files)}
          />
        </Grid>
        <Grid item md={4}>
          <Button
            onClick={handleUpload}
            sx={({ customTheme }) => ({
              backgroundColor: customTheme.color.primary,
              textTransform: "none",
              color: "white",
              fontWeight: 700,
              fontSize: "14px",
              lineHeight: "17px",
            })}
          >
            Uppload Doc
          </Button>
        </Grid>
        <Grid item md={4}>
          <CustomInput.Search />
        </Grid>
      </Grid>
      <TableComponent
        rows={rows}
        columns={columns}
        noPaging
        loading={loading}
      />
      <Grid container justifyContent="flex-end">
        <CustomButon.Error onClick={onClose}>Cancel</CustomButon.Error>
      </Grid>
    </CustomModal>
  );
};

export default ModalManage;
