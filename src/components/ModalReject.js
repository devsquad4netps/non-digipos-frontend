import React, { useState } from "react";
import CustomModal from "./CustomModal";
import { TextField, Grid } from "@mui/material";
import CustomButon from "./CustomButon";
import { Axios } from "../helper/http";
import Swal from "sweetalert2";

export default function ModalReject({
  open,
  urlApi,
  onClose,
  selected,
  getDataTable,
}) {
  const [note, setNote] = useState("");
  const [error, setError] = useState(false);

  console.log(urlApi);

  const handleSubmit = () => {
    if (note === "") {
      setError(true);
    } else {
      Axios.post(urlApi, {
        MPM_ID: selected,
        NOTE: note,
      }).then(({ data }) => {
        onClose();
        Swal.fire({
          icon: data.success ? "success" : "error",
          title: data.success ? "Success" : "Error",
          text: data.success && "Reject data berhasil!",
        }).then(() => getDataTable());
      });
    }
  };

  return (
    <CustomModal open={open} onClose={onClose} width={500}>
      <Grid>
        <h4>Form Penolakan</h4>
        <Grid
          sx={{
            "& h3": {
              color: error && "red",
            },
          }}
        >
          <h3>Note</h3>
          <TextField
            error={error}
            onChange={(e) => {
              setNote(e.target.value);
              setError(false);
            }}
            fullWidth
            multiline
            minRows={3}
          />
        </Grid>
      </Grid>
      <Grid container justifyContent="flex-end" mt="24px" gap="24px">
        <CustomButon.Error onClick={onClose}>Cancel</CustomButon.Error>
        <CustomButon.Primary onClick={handleSubmit}>Submit</CustomButon.Primary>
      </Grid>
    </CustomModal>
  );
}
