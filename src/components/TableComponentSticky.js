import React, { useState, useEffect } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Grid,
  Pagination,
  CircularProgress,
} from "@mui/material";
import EmptyTable from "../assets/images/empty-table.png";

export default function TableComponentSticky({ columns, rows, noPaging, loading, onClick, hover, selected }) {
  return (
    <Grid>
      <TableContainer
        component={Paper}
        sx={{
          borderRadius: "10px",
          padding: "0px",
          overflow: "auto",
          paddingBottom: "12px",
        }}
      >
        <Table sx={{ width: "100%" }} aria-label="simple table">
          <TableHead
            sx={(theme) => ({
              backgroundColor: theme.customTheme.color.primary,
            })}
          >
            {columns.map((res, i) => {
              return (
                <TableRow 
                key={i}>
                  {res.map(
                    (
                      { name, rowSpan, colSpan, align, heading, width },
                      index
                    ) => {
                      return (
                        <TableCell
                          align={align || "center"}
                          key={index}
                          rowSpan={rowSpan}
                          colSpan={colSpan}
                          sx={{
                            border: "1px solid white",
                            color: "white",
                            padding: 0,
                            paddingTop: "14px",
                            paddingBottom: "14px",
                            fontWeight: 700,
                            fontSize: "14px",
                            lineHeight: "16px",
                            minWidth: width ? (loading ? 120 : width) : 120,
                            // minWidth: width,
                            paddingLeft: "8px",
                            paddingRight: "8px",
                            transition: "0.3s",
                          }}
                        >
                          {name}
                        </TableCell>
                      );
                    }
                  )}
                </TableRow>
              );
            })}
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow
                sx={{
                  width: "100%",
                  position: "relative",
                  height: "300px",
                }}
              >
                <Grid
                  component="td"
                  sx={({ customTheme }) => ({
                    width: "100%",
                    position: "absolute",
                    left: "0px",
                    height: "100%",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    // height: "200px",
                    "& h3": {
                      paddingLeft: "24px",
                      color: customTheme.color.primary,
                    },
                  })}
                >
                  <CircularProgress
                    size={60}
                    sx={({ customTheme }) => ({
                      color: customTheme.color.primary,
                    })}
                  />
                  <h3>Loading...</h3>
                </Grid>
              </TableRow>
            ) : rows.length > 0 ? (
              rows.map((result, index) => {
                let sxProp
                if(selected) {
                  sxProp = {
                    "&:nth-of-type(odd)": {
                      backgroundColor: "#EDEDED",
                    },
                    "&:last-child td, &:last-child th": {
                      border: 0,
                    },
                    cursor: 'pointer'
                  } 
                }else{
                  sxProp = {
                    "&:nth-of-type(odd)": {
                      backgroundColor: "#EDEDED",
                    },
                    "&:last-child td, &:last-child th": {
                      border: 0,
                    },
                  }
                } 
                return (
                  <TableRow
                    onClick = {onClick}
                    key={index}
                    hover={hover}
                    selected={selected}

                    sx={sxProp}
                  >
                    {columns.map((res) =>
                      res.map(
                        (
                          { heading, align, renderCell, width, textAlign },
                          i
                        ) => {
                          if (!heading) {
                            return (
                              <TableCell
                                key={i}
                                align={textAlign || "center"}
                                sx={{
                                  fontWeight: 400,
                                  fontSize: "14px",
                                  lineHeight: "16px",
                                  padding: "12px",
                                  paddingTop: "8px",
                                  paddingBottom: "8px",
                                }}
                              >
                                {renderCell
                                  ? renderCell(result, index)
                                  : "GA ADA"}
                              </TableCell>
                            );
                          }
                        }
                      )
                    )}
                  </TableRow>
                );
              })
            ) : (
              <TableRow
                sx={{
                  width: "100%",
                  position: "relative",
                  height: "300px",
                }}
              >
                <Grid
                  sx={{
                    width: "100%",
                    position: "absolute",
                    left: "0px",
                    height: "100%",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    // height: "200px",
                  }}
                  component="td"
                >
                  <Grid
                    sx={{
                      "& img": {
                        height: "240px",
                      },
                      height: "100%",
                      textAlign: "center",
                      "& h2": {
                        margin: 0,
                        marginTop: "-24px",
                        color: "gray",
                      },
                    }}
                  >
                    <img src={EmptyTable} alt="" />
                    <h2>No Data Availabe</h2>
                  </Grid>
                </Grid>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <Grid mt="36px" container justifyContent="flex-end">
        {!noPaging && (
          <Pagination
            sx={(theme) => ({
              "& .MuiPagination-ul": {
                "& button": {
                  margin: 0,
                  borderRadius: 0,
                  border: "none",
                  backgroundColor: "white",
                },
              },
              "& .MuiPaginationItem-previousNext": {
                // border: "2px solid  blue!important",
                marginRight: "8px!important",
                marginLeft: "8px!important",
              },
              "& .Mui-selected": {
                border: "2px solid",
                "&[aria-current=true]": {
                  backgroundColor: theme.customTheme.color.primary,
                  color: "white",
                },
              },
              marginRight: "-8px!important",
            })}
            count={10}
            variant="outlined"
            shape="rounded"
          />
        )}
      </Grid>
    </Grid>
  );
}
