import React, { useState, useEffect, useCallback } from "react";
import { Grid, Paper, MenuItem } from "@mui/material";
import { useForm } from "react-hook-form";
import { useSelector, useDispatch } from "react-redux";
import Swal from "sweetalert2";
import { Link } from "react-router-dom";
import CustomInput from "../InputComponent";
import CustomButon from "../CustomButon";
import master from "../../helper/services/master";
import DateComp from "../DatePicker";

export default function FormAddMerchant() {
  const data = useSelector((state) => state);
  const [dataClass, setDataClass] = useState([]);
  const [isSubmit, setIsSubmit] = useState(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
    setValue,
  } = useForm();

  const gertMerchantList = async () => {
    try {
      const response = await master.merchantClassList();
      if (response.data.success) {
        console.log(response.data);
        setDataClass(response.data.datas);
        setValue("M_CLASS", response.data.datas[0].M_ID);
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    gertMerchantList();
  }, []);

  const onSubmit = async (result) => {
    setIsSubmit(true);
    result = {
      ...result,
      M_NPWP_ATTACH: result.M_NPWP_ATTACH[0],
      M_PKS_ATTACH: result.M_PKS_ATTACH[0],
      AT_FLAG: 1,
    };

    const formData = new FormData();
    Object.keys(result).forEach((key) => {
      formData.append(key, result[key]);
    });
    try {
      const response = await master.merchantCreate(formData);
      if (response.data.success) {
        console.log(response.data.datas);
        Swal.fire({
          icon: "success",
          title: "succes",
          text: response?.data?.message,
        }).then(
          () =>
            (window.location.href = `/settings/master-merchant/edit/${response.data.datas.M_ID}`)
        );
      } else {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: response?.data?.message?.sqlMessage,
        });
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Error",
        text: error.message,
      });
    }
  };

  console.log(watch());

  return (
    <Grid>
      <form onSubmit={handleSubmit(onSubmit)} id="submit-form">
        <Paper>
          <Grid
            container
            sx={{
              "& h3, h1": {
                margin: 0,
              },
            }}
            // spacing="52px"
            pb="300px"
            padding="24px"
            columnSpacing="56px"
          >
            <Grid item md={6}>
              <h3>Merchant Information</h3>
              <Grid mt="4px" container item xs={12} spacing="12px">
                <CustomInput.InputLabel
                  disabled={isSubmit}
                  label="Name"
                  register={{ ...register("M_NAME", { required: true }) }}
                  id="M_NAME"
                  errors={errors}
                  required
                />
                <CustomInput.InputLabel
                  label="Legal Name"
                  register={{ ...register("M_LEGAL_NAME", { required: true }) }}
                  id="M_LEGAL_NAME"
                  errors={errors}
                  required
                />
                <CustomInput.InputLabel
                  label="Alamat"
                  register={{
                    ...register("M_ALAMAT_KANTOR", { required: true }),
                  }}
                  id="M_ALAMAT_KANTOR"
                  errors={errors}
                  required
                  multiline
                  minRows={2}
                />
                <CustomInput.InputLabel
                  label="City"
                  register={{ ...register("M_CITY", { required: true }) }}
                  id="M_CITY"
                  errors={errors}
                  required
                />
                <CustomInput.InputLabel
                  label="No Perfix"
                  register={{ ...register("M_NO_PERFIX", { required: true }) }}
                  id="M_NO_PERFIX"
                  errors={errors}
                  required
                />
              </Grid>
            </Grid>
            <Grid item md={6}>
              <Grid mt="24px" container item xs={12} spacing="12px">
                <Grid item md={12}>
                  <CustomInput.Select
                    label="Merchant Class"
                    required
                    id="M_CLASS"
                    value={
                      watch("M_CLASS")
                        ? watch("M_CLASS")
                        : dataClass.length > 0 && dataClass[0].MC_ID
                    }
                    register={{ ...register("M_CLASS", { required: true }) }}
                    errors={errors}
                  >
                    {dataClass.map((val) => {
                      return (
                        <MenuItem key={val.MC_ID} value={val.MC_ID}>
                          {val.MC_LABEL}
                        </MenuItem>
                      );
                    })}
                  </CustomInput.Select>
                </Grid>
                <Grid item xs={7}>
                  <CustomInput.InputLabel
                    label="NPWP"
                    register={{ ...register("M_NPWP", { required: true }) }}
                    id="M_NPWP"
                    errors={errors}
                    required
                  />
                </Grid>
                <Grid item xs={5}>
                  <CustomInput.File
                    values={watch("M_NPWP_ATTACH")}
                    errors={errors}
                    id="M_NPWP_ATTACH"
                    label="M_NPWP_ATTACH"
                    register={{
                      ...register("M_NPWP_ATTACH", { required: true }),
                    }}
                  />
                </Grid>
                <CustomInput.InputLabel
                  label="Alamat NPWP"
                  multiline
                  minRows={2}
                  required
                  register={{
                    ...register("M_ALAMAT_NPWP", { required: true }),
                  }}
                  id="M_ALAMAT_NPWP"
                  errors={errors}
                />
                <Grid item xs={7}>
                  <CustomInput.InputLabel
                    label="PKS NO / CONTRACT NO"
                    register={{
                      ...register("M_NO_CONTRACT", { required: true }),
                    }}
                    id="M_NO_CONTRACT"
                    errors={errors}
                  />
                </Grid>
                <Grid item xs={5}>
                  <CustomInput.File
                    values={watch("M_PKS_ATTACH")}
                    errors={errors}
                    id="M_PKS_ATTACH"
                    label="M_PKS_ATTACH"
                    register={{
                      ...register("M_PKS_ATTACH", { required: true }),
                    }}
                  />
                </Grid>
                <CustomInput.InputLabel
                  label="Tgl Gabung"
                  register={{ ...register("M_TGL_GABUNG") }}
                  id="M_TGL_GABUNG"
                  errors={errors}
                  required
                  compo={
                    <DateComp
                      id="M_TGL_GABUNG"
                      setValue={setValue}
                      register={{ ...register("M_TGL_GABUNG") }}
                      value={watch("M_TGL_GABUNG")}
                    />
                  }
                />
              </Grid>
            </Grid>
          </Grid>
        </Paper>
        <Grid item md={12} mt="36px">
          <Grid container justifyContent="flex-end">
            <Link to="/settings/master-merchant/">
              <CustomButon.Error sx={{ width: "92px" }}>
                Cancel
              </CustomButon.Error>
            </Link>
            <CustomButon.Primary
              id="submit-form"
              type="submit"
              sx={{ width: "92px", marginLeft: "32px" }}
            >
              Save
            </CustomButon.Primary>
          </Grid>
        </Grid>
      </form>
    </Grid>
  );
}
