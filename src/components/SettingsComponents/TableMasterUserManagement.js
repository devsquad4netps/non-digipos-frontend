import React, { useEffect, useState } from "react";
import TableComponent from "../TableComponent";
import { Grid, IconButton } from "@mui/material";
import { EditIcon, TrashIcon } from "../../assets/icons/customIcon";
import master from "../../helper/services/master";
import Swal from "sweetalert2";

function TableMasterUserManagement({editFunct, data, deleteFunct }) {
 
  const columns = [
    [
      {
        name: "No",
        renderCell: ({}, index) => index + 1,
      },
      {
        name: "NIK",
        renderCell: ({ nik }) => nik,
      },
      {
        name: "Nama",
        renderCell: ({ fullname }) => fullname,
      },
      {
        name: "Jabatan",
        renderCell: ({ jabatan }) => jabatan,
      },
      {
        name: "Divisi",
        renderCell: ({ divisi }) => divisi,
      },
      {
        name: "Jenis Kelamin",
        renderCell: ({ gender }) => gender,
      },
      {
        name: "Alamat",
        renderCell: ({ address }) => address,
      },
      {
        name: "Phone",
        renderCell: ({ phone }) => phone,
      },
      {
        name: "Email",
        renderCell: ({ email }) => email,
      },
      {
        name: "Action",
        renderCell: ({ id }) => (
          <Grid>
            <IconButton onClick={()=>editFunct(id)}>
              <EditIcon />
            </IconButton>
            <IconButton onClick={() => deleteFunct(id)}>
              <TrashIcon />
            </IconButton>
          </Grid>
        ),
      },
    ],
  ];

  return <TableComponent columns={columns} rows={data} />;
}

export default TableMasterUserManagement;
