import React, { useState, useEffect, useCallback } from "react";
import { Grid, Paper, MenuItem, Switch, IconButton } from "@mui/material";
import { useForm } from "react-hook-form";
import { useSelector, useDispatch } from "react-redux";
import Swal from "sweetalert2";
import { Link } from "react-router-dom";

import { PlusButtonIcon, DownloadIcon } from "../../assets/icons/customIcon";
import CustomInput from "../InputComponent";
import CustomChip from "../CustomChip";
import CustomButon from "../CustomButon";
import CustomModal from "../CustomModal";
import CustomSelectBank from "../../components/SelectComponentBank";
import master from "../../helper/services/master";
import DateComp from "../DatePicker";
import { Axios } from "../../helper/http";
import { VIEW_FILE_BASE64 } from "../../helper/globalFuncion";

import { EditIcon, TrashIcon } from "../../assets/icons/customIcon";

import TableComponent from "../TableComponent";

export default function FormEditMerchant({ initialValues }) {
  const data = useSelector((state) => state);
  const [dataBank, setDataBank] = useState([]);
  const [dataPic, setDataPic] = useState([]);
  const [dataClass, setDataClass] = useState([]);
  const [loadingRek, setLoadingRek] = useState(false);

  const { M_ID } = initialValues;
  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
    setValue,
    getValues,
  } = useForm({
    defaultValues: { ...initialValues, M_CLASS: initialValues.M_CLASS.MC_ID },
  });

  const gertMerchantList = async () => {
    try {
      const response = await master.merchantClassList();
      if (response.data.success) {
        setDataClass(response.data.datas);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleViewFileNPWP = async (doc_name) => {
    try {
      const response = await Axios.post(`/document-view/file/npwp`, {
        names: doc_name,
      });
      if (response.data.success) {
        VIEW_FILE_BASE64(response.data.data);
      } else {
        Swal.fire({
          icon: data.success ? "success" : "error",
          title: data.success ? "Success" : "Error",
          text: data.message,
        });
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Error",
        text: error,
      });
    }
  };
  const handleViewFilePKS = async (doc_name) => {
    try {
      const response = await Axios.post(`/document-view/file/pks`, {
        names: doc_name,
      });
      if (response.data.success) {
        VIEW_FILE_BASE64(response.data.data);
      } else {
        Swal.fire({
          icon: data.success ? "success" : "error",
          title: data.success ? "Success" : "Error",
          text: data.message,
        });
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Error",
        text: error,
      });
    }
  };

  useEffect(() => {
    gertMerchantList();
  }, []);

  const onSubmit = async (result) => {
    result = {
      ...result,
      M_NPWP_ATTACH: result.M_NPWP_ATTACH[0],
      M_PKS_ATTACH: result.M_PKS_ATTACH[0],
      AT_FLAG: 1,
    };

    const formData = new FormData();
    Object.keys(result).forEach((key) => {
      formData.append(key, result[key]);
    });

    if (dataBank.length > 0 && dataPic.length > 0) {
      try {
        const response = await master.merchantUpdate(result.M_ID, formData);
        if (response.data.success) {
        } else {
        }
        Swal.fire({
          icon: response?.data?.success ? "success" : "error",
          title: response?.data?.success ? "Success" : "Error",
          text: response?.data?.message,
        }).then(
          () =>
            response?.data?.success &&
            (window.location.href = "/settings/master-merchant")
        );
      } catch (error) {
        console.log(error);
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error.message,
        });
      }
    } else {
      Swal.fire({
        icon: "warning",
        title: "Warning",
        text: "Minimal tambahkan 1 data rekening dan 1 Pic!",
        showConfirmButton: false,
        timer: 3000,
      });
    }
  };

  console.log(watch("M_CLASS"));

  return (
    <Grid>
      <form onSubmit={handleSubmit(onSubmit)} id="submit-form">
        <Paper>
          <Grid
            container
            sx={{
              "& h3, h1": {
                margin: 0,
              },
            }}
            // spacing="52px"
            pb="300px"
            padding="24px"
            columnSpacing="56px"
          >
            <Grid item md={6}>
              <h3>Merchant Information</h3>
              <Grid mt="4px" container item xs={12} spacing="12px">
                <CustomInput.InputLabel
                  disabled
                  label="Code"
                  register={{ ...register("M_CODE", { required: true }) }}
                  id="code"
                  errors={errors}
                  required
                />
                <CustomInput.InputLabel
                  label="Name"
                  register={{ ...register("M_NAME", { required: true }) }}
                  id="M_NAME"
                  errors={errors}
                  required
                />
                <CustomInput.InputLabel
                  label="Legal Name"
                  register={{ ...register("M_LEGAL_NAME", { required: true }) }}
                  id="M_LEGAL_NAME"
                  errors={errors}
                  required
                />
                <CustomInput.InputLabel
                  label="Alamat"
                  register={{
                    ...register("M_ALAMAT_KANTOR", { required: true }),
                  }}
                  id="M_ALAMAT_KANTOR"
                  errors={errors}
                  required
                  multiline
                  minRows={2}
                />
                <CustomInput.InputLabel
                  label="City"
                  register={{ ...register("M_CITY", { required: true }) }}
                  id="M_CITY"
                  errors={errors}
                  required
                />
                <CustomInput.InputLabel
                  label="No Perfix"
                  register={{ ...register("M_NO_PERFIX", { required: true }) }}
                  id="M_NO_PERFIX"
                  errors={errors}
                  required
                />
              </Grid>
            </Grid>
            <Grid item md={6}>
              <Grid mt="24px" container item xs={12} spacing="12px">
                <Grid item xs={6}>
                  <CustomInput.InputLabel
                    label="NPWP"
                    register={{ ...register("M_NPWP", { required: true }) }}
                    id="M_NPWP"
                    errors={errors}
                    required
                  />
                </Grid>
                <Grid item xs={5}>
                  <CustomInput.File
                    values={watch("M_NPWP_ATTACH")}
                    errors={errors}
                    id="M_NPWP_ATTACH"
                    label="M_NPWP_ATTACH"
                    register={{
                      ...register("M_NPWP_ATTACH", {
                        required: initialValues ? false : true,
                      }),
                    }}
                  />
                </Grid>
                <Grid item xs={1}>
                  <IconButton
                    onClick={() => {
                      handleViewFileNPWP(initialValues.M_NPWP_ATTACH);
                    }}
                    size={"small"}
                    color={"inherit"}
                  >
                    <DownloadIcon />
                  </IconButton>
                </Grid>
                <CustomInput.InputLabel
                  label="Alamat NPWP"
                  multiline
                  minRows={2}
                  required
                  register={{
                    ...register("M_ALAMAT_NPWP", { required: true }),
                  }}
                  id="M_ALAMAT_NPWP"
                  errors={errors}
                />
                <Grid item xs={6}>
                  <CustomInput.InputLabel
                    label="PKS NO / CONTRACT NO"
                    register={{
                      ...register("M_NO_CONTRACT", { required: true }),
                    }}
                    id="M_NO_CONTRACT"
                    errors={errors}
                  />
                </Grid>
                <Grid item xs={5}>
                  <CustomInput.File
                    values={watch("M_PKS_ATTACH")}
                    errors={errors}
                    id="M_PKS_ATTACH"
                    label="M_PKS_ATTACH"
                    register={{
                      ...register("M_PKS_ATTACH", {
                        required: initialValues ? false : true,
                      }),
                    }}
                  />
                </Grid>
                <Grid item xs={1}>
                  <IconButton
                    onClick={() => {
                      handleViewFilePKS(initialValues.M_PKS_ATTACH);
                    }}
                    size={"small"}
                    color={"inherit"}
                  >
                    <DownloadIcon />
                  </IconButton>
                </Grid>
                <Grid item md={7}>
                  <CustomInput.Select
                    label="Merchant Class"
                    required
                    id="M_CLASS"
                    value={
                      watch("M_CLASS")
                        ? watch("M_CLASS")
                        : dataClass.length > 0 && dataClass[0].MC_ID
                    }
                    register={{ ...register("M_CLASS", { required: true }) }}
                    errors={errors}
                  >
                    {dataClass.map((val) => {
                      return (
                        <MenuItem key={val.MC_ID} value={val.MC_ID}>
                          {val.MC_LABEL}
                        </MenuItem>
                      );
                    })}
                  </CustomInput.Select>
                </Grid>
                <CustomInput.InputLabel
                  label="Tgl Gabung"
                  register={{ ...register("M_TGL_GABUNG") }}
                  id="M_TGL_GABUNG"
                  errors={errors}
                  required
                  compo={
                    <DateComp
                      id="M_TGL_GABUNG"
                      setValue={setValue}
                      register={{ ...register("M_TGL_GABUNG") }}
                      value={watch("M_TGL_GABUNG")}
                    />
                  }
                />
              </Grid>
            </Grid>
          </Grid>
        </Paper>
        <Grid container justifyContent="flex-end" mt="36px">
          <Link to="/settings/master-merchant/">
            <CustomButon.Error sx={{ width: "92px" }}>Cancel</CustomButon.Error>
          </Link>
          <CustomButon.Primary
            id="submit-form"
            type="submit"
            sx={{ width: "92px", marginLeft: "32px" }}
          >
            Save
          </CustomButon.Primary>
        </Grid>
        {/* <CustomButon.Primary onClick={postRekening}>test</CustomButon.Primary> */}
      </form>
      <RekeningInformation
        M_ID={M_ID}
        setDataBankParent={(val) => setDataBank(val)}
      />
      <PicInformation M_ID={M_ID} setDataPicParent={(val) => setDataPic(val)} />
    </Grid>
  );
}

const RekeningInformation = ({ M_ID, setDataBankParent }) => {
  const [open, setOpen] = useState(false);
  const [dataBank, setDataBank] = useState([]);
  const [loading, setLoading] = useState(false);
  const [edit, setEdit] = useState(false);
  const {
    register: register2,
    handleSubmit: handleSubmit2,
    setValue,
    resetField,
  } = useForm({});
  const columns = [
    [
      {
        name: "Bank",
        renderCell: ({ MR_BANK_NAME }) => MR_BANK_NAME,
      },
      {
        name: "No Rekening",
        renderCell: ({ MR_ACOUNT_NUMBER }) => MR_ACOUNT_NUMBER,
      },
      {
        name: "Nama Rekening",
        renderCell: ({ MR_REKENING_NAME }) => MR_REKENING_NAME,
      },
      {
        name: "Status",
        renderCell: ({ AT_FLAG, MR_ID }) => (
          <Switch
            onClick={() => setActiveMerchant(MR_ID, AT_FLAG)}
            checked={AT_FLAG === 1}
          />
        ),
      },
      {
        name: "Action",
        renderCell: ({ MR_ID }, index) => (
          <Grid>
            <IconButton onClick={() => handleEdit(MR_ID)}>
              <EditIcon />
            </IconButton>

            <IconButton onClick={() => deteleteMerchantRekening(MR_ID)}>
              <TrashIcon />
            </IconButton>
          </Grid>
        ),
      },
    ],
  ];

  const handleEdit = async (val) => {
    setEdit(val);
    setOpen(true);
    try {
      const response = await master.merchantRekeningDetail(M_ID, val);
      if (response.data.success) {
        const result = response.data.datas;
        const {
          MR_ACOUNT_NUMBER,
          MR_BANK_NAME,
          MR_BANK_BRANCH,
          MR_REKENING_NAME,
        } = result;
        setValue("MR_ACOUNT_NUMBER", MR_ACOUNT_NUMBER);
        setValue("MR_REKENING_NAME", MR_REKENING_NAME);
        setValue("MR_BANK_BRANCH", MR_BANK_BRANCH);
        // setValue("MR_BANK_NAME", MR_BANK_NAME);
        setMPB_ID(MR_BANK_NAME);
        console.log(MR_BANK_NAME);
      }
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };

  const setActiveMerchant = async (val, AT_FLAG) => {
    if (AT_FLAG !== 1) {
      try {
        const response = await master.merchantRekeningActive(M_ID, val);
        Swal.fire({
          icon: response?.data?.success ? "success" : "error",
          title: response?.data?.success ? "Success" : "Error",
          text: response?.data?.message || response?.data?.sqlMessage,
        }).then(() => getRekeningLIst());
      } catch (error) {}
    }
  };

  const [MPB_ID, setMPB_ID] = useState("");
  const [dataGetBank, setDataGetBank] = useState([]);

  const getDataBank = async () => {
    try {
      const request = await master.masterBankList()
      if (request.data.success) {
        console.log(request.data.datas)
        setDataGetBank(request.data.datas)
      }
    } catch ({response: {data}}) {
      // setState({...state, loading: false, open: false, selectedId: null});
      Swal.fire({
        icon: "error",
        title: "Error",
        text: data.error.message,
      });
    }
  }

  const getRekeningLIst = async () => {
    setLoading(true);
    try {
      const response = await master.merchantRekeningList(M_ID);
      if (response.data.success) {
        setDataBank(response.data.datas);
        setDataBankParent(response.data.datas);
      }
      console.log(response);
    } catch (error) {
      console.log(error);
    }
    setTimeout(() => {
      setLoading(false);
    }, 500);
  };

  useEffect(() => {
    getRekeningLIst();
    getDataBank();
  }, []);

  const onSubmitRekening = async (val) => {
    val.MR_BANK_NAME  = MPB_ID
    console.log(val)
    setOpen(false);
    try {
      const response = edit
        ? await master.merchantRekeningUpdate(M_ID, edit, val)
        : await master.merchantRekeningCreate(M_ID, val);

      console.log(response);
      if (response.data.success) {
        edit ? getRekeningLIst() : setDataBank(response.data.datas);
      }
      Swal.fire({
        icon: response?.data?.success ? "success" : "error",
        title: response?.data?.success ? "Success" : "Error",
        text: response?.data?.message?.sqlMessage || response?.data?.message,
      }).then(() => getRekeningLIst());
    } catch (error) {
      console.log(error);
    }
  };

  const handleClose = () => {
    setEdit();
    setOpen(false);
  };

  const deteleteMerchantRekening = async (val) => {
    console.log(val);
    Swal.fire({
      title: "Apakah anda yakin?",
      text: "Akan menghapus data ini?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Confirm",
    }).then(async (result) => {
      if (result.isConfirmed) {
        const response = await master.merchantRekeningDelete(M_ID, val);
        console.log(response);
        Swal.fire({
          icon: response?.data?.success ? "success" : "error",
          title: response?.data?.success ? "Success" : "Error",
          text: response?.data?.message || response?.data?.sqlMessage,
        }).then(() => {
          resetFieldForm();
          getRekeningLIst();
        });
      }
    });
  };

  const resetFieldForm = () => {
    resetField("MR_ACOUNT_NUMBER");
    resetField("MR_REKENING_NAME");
    resetField("MR_BANK_BRANCH");
    resetField("MR_BANK_NAME");
  };

  return (
    <Grid item md={12} mt="36px">
      <h3>Rekening Information</h3>
      <Grid mt="36px" sx={{ "& a": { textDecoration: "none" } }}>
        <CustomButon.Primary
          onClick={(e) => {
            setEdit();
            setOpen(true);
            resetFieldForm();
          }}
          sx={{ "& span": { marginLeft: "8px" } }}
        >
          <PlusButtonIcon />
          <span>Add</span>
        </CustomButon.Primary>
      </Grid>
      <Grid mt="16px">
        <TableComponent rows={dataBank} columns={columns} noPaging />
      </Grid>
      <CustomModal
        open={open}
        onClose={() => setOpen(false)}
        title="Form Rekening"
      >
        <form onSubmit={handleSubmit2(onSubmitRekening)} id="submit-rekening">
          <Grid container item xs={12} rowSpacing="12px">
            {/* <CustomInput.InputLabel
              id="MR_BANK_NAME"
              required
              register={{ ...register2("MR_BANK_NAME", { required: true }) }}
              label="Bank Name :"
              labelWidth="110px"
              placeholder="Bank Name"
            /> */}
            <CustomSelectBank.SelectLabel
                  item
                  label="Bank Name" labelWidth="110px"
                  required
                  value={MPB_ID}
                  options={dataGetBank}
                  onChange={(e)=>{
                    setMPB_ID(e.target.value)
                  console.log(e.target.value)
                  }}
                  />
            <CustomInput.InputLabel
              id="MR_BANK_BRANCH"
              required
              register={{
                ...register2("MR_BANK_BRANCH", { required: true }),
              }}
              label="Branch Name :"
              labelWidth="110px"
              placeholder="Branch Name"
            />
            <CustomInput.InputLabel
              id="MR_ACOUNT_NUMBER"
              required
              register={{
                ...register2("MR_ACOUNT_NUMBER", { required: true }),
              }}
              label="No Rekening :"
              labelWidth="110px"
              placeholder="No Rekening"
            />
            <CustomInput.InputLabel
              id="MR_REKENING_NAME"
              required
              register={{
                ...register2("MR_REKENING_NAME", { required: true }),
              }}
              label="Rekening Name :"
              labelWidth="110px"
              placeholder="Rekening Name"
            />
            <Grid item justifyContent="flex-end" container gap="24px">
              <CustomButon.Primary
                size="small"
                onClick={handleClose}
                sx={{
                  "& span": { marginLeft: "8px" },
                  backgroundColor: "red",
                }}
              >
                Cancel{" "}
              </CustomButon.Primary>
              <CustomButon.Primary
                type="submit"
                id="submit-rekening"
                size="small"
                sx={{ "& span": { marginLeft: "8px" } }}
              >
                Save{" "}
              </CustomButon.Primary>
            </Grid>
          </Grid>
        </form>
      </CustomModal>
    </Grid>
  );
};

const PicInformation = ({ M_ID, setDataPicParent }) => {
  const [open, setOpen] = useState(false);
  const [dataBank, setDataBank] = useState([]);
  const [loading, setLoading] = useState(false);
  const [edit, setEdit] = useState(false);
  const {
    register: register2,
    handleSubmit: handleSubmit2,
    setValue,
    resetField,
  } = useForm({});
  const columns = [
    [
      {
        name: "PIC Name",
        renderCell: ({ PIC_NAME }) => PIC_NAME,
      },
      {
        name: "PIC Phone",
        renderCell: ({ PIC_PHONE }) => PIC_PHONE,
      },
      {
        name: "PIC Jabatan",
        renderCell: ({ PIC_JABATAN }) => PIC_JABATAN,
      },
      {
        name: "PIC Email",
        renderCell: ({ PIC_EMAIL }) => PIC_EMAIL,
      },
      {
        name: "Status",
        renderCell: ({ AT_FLAG, MR_ID }) => (
          <Switch
            onClick={() => setActivePic(MR_ID, AT_FLAG)}
            checked={AT_FLAG === 1}
          />
        ),
      },
      {
        name: "Action",
        renderCell: ({ MR_ID }, index) => {
          return (
            <Grid>
              <IconButton onClick={() => handleEdit(MR_ID)}>
                <EditIcon />
              </IconButton>

              <IconButton onClick={() => deteleteMerchantPic(MR_ID)}>
                <TrashIcon />
              </IconButton>
            </Grid>
          );
        },
      },
    ],
  ];

  const setActivePic = async (val, AT_FLAG) => {
    if (AT_FLAG !== 1) {
      try {
        const response = await master.merchantPicActive(M_ID, val);
        Swal.fire({
          icon: response?.data?.success ? "success" : "error",
          title: response?.data?.success ? "Success" : "Error",
          text: response?.data?.message || response?.data?.sqlMessage,
        }).then(() => getPicLIst());
      } catch (error) {}
    }
  };

  const handleEdit = async (val) => {
    setEdit(val);
    setOpen(true);
    try {
      const response = await master.merchantPicDetail(M_ID, val);
      if (response.data.success) {
        const result = response.data.datas;
        const { PIC_EMAIL, PIC_JABATAN, PIC_NAME, PIC_PHONE } = result;
        setValue("PIC_EMAIL", PIC_EMAIL);
        setValue("PIC_JABATAN", PIC_JABATAN);
        setValue("PIC_NAME", PIC_NAME);
        setValue("PIC_PHONE", PIC_PHONE);
      }
      console.log(response);
    } catch (error) {
      console.log(error);
    }
  };

  const getPicLIst = async () => {
    setLoading(true);
    try {
      const response = await master.merchantPicList(M_ID);
      console.log(response);
      if (response.data.success) {
        setDataBank(response.data.datas);
        setDataPicParent(response.data.datas);
      }
      console.log(response);
    } catch (error) {
      console.log(error);
    }
    setTimeout(() => {
      setLoading(false);
    }, 500);
  };

  useEffect(() => {
    getPicLIst();
  }, []);

  const onSubmitPic = async (val) => {
    console.log(val);
    setOpen(false);
    try {
      const response = edit
        ? await master.merchantPicUpdate(M_ID, edit, val)
        : await master.merchantPicCreate(M_ID, val);

      console.log(response);
      if (response.data.success) {
        edit ? getPicLIst() : setDataBank(response.data.datas);
      }
      Swal.fire({
        icon: response?.data?.success ? "success" : "error",
        title: response?.data?.success ? "Success" : "Error",
        text: response?.data?.message || response?.data?.sqlMessage,
      }).then(() => getPicLIst());
    } catch (error) {
      console.log(error);
    }
  };

  const handleClose = () => {
    setEdit();
    setOpen(false);
  };

  const deteleteMerchantPic = async (val) => {
    console.log(val, M_ID);
    Swal.fire({
      title: "Apakah anda yakin?",
      text: "Akan menghapus data ini?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Confirm",
    }).then(async (result) => {
      if (result.isConfirmed) {
        const response = await master.merchantPicDelete(M_ID, val);
        Swal.fire({
          icon: response?.data?.success ? "success" : "error",
          title: response?.data?.success ? "Success" : "Error",
          text: response?.data?.message || response?.data?.sqlMessage,
        }).then(() => {
          getPicLIst();
          resetFieldForm();
        });
      }
    });
  };

  const resetFieldForm = () => {
    resetField("PIC_EMAIL");
    resetField("PIC_JABATAN");
    resetField("PIC_NAME");
    resetField("PIC_PHONE");
  };

  return (
    <Grid item md={12} mt="36px">
      <h3>PIC Information</h3>
      <Grid mt="36px" sx={{ "& a": { textDecoration: "none" } }}>
        <CustomButon.Primary
          onClick={(e) => {
            setEdit();
            setOpen(true);
            resetFieldForm();
          }}
          sx={{ "& span": { marginLeft: "8px" } }}
        >
          <PlusButtonIcon />
          <span>Add</span>
        </CustomButon.Primary>
      </Grid>
      <Grid mt="16px">
        <TableComponent rows={dataBank} columns={columns} noPaging />
      </Grid>
      <CustomModal
        open={open}
        onClose={() => setOpen(false)}
        title="Form Rekening"
      >
        <form onSubmit={handleSubmit2(onSubmitPic)} id="submit-rekening">
          <Grid container item xs={12} rowSpacing="12px">
            <CustomInput.InputLabel
              id="PIC_NAME"
              required
              register={{ ...register2("PIC_NAME", { required: true }) }}
              label="PIC Name :"
              labelWidth="110px"
              placeholder="PIC Name"
            />
            <CustomInput.InputLabel
              id="PIC_PHONE"
              required
              register={{
                ...register2("PIC_PHONE", { required: true }),
              }}
              label="PIC_PHONE :"
              labelWidth="110px"
              placeholder="PIC Phnoe"
            />
            <CustomInput.InputLabel
              id="PIC_JABATAN"
              required
              register={{
                ...register2("PIC_JABATAN", { required: true }),
              }}
              label="PIC Jabatan :"
              labelWidth="110px"
              placeholder="PIC_Jabatan"
            />
            <CustomInput.InputLabel
              id="PIC_EMAIL"
              required
              register={{
                ...register2("PIC_EMAIL", { required: true }),
              }}
              label="PIC Email :"
              labelWidth="110px"
              placeholder="PIC Email"
            />
            <Grid item justifyContent="flex-end" container gap="24px">
              <CustomButon.Primary
                size="small"
                onClick={handleClose}
                sx={{
                  "& span": { marginLeft: "8px" },
                  backgroundColor: "red",
                }}
              >
                Cancel{" "}
              </CustomButon.Primary>
              <CustomButon.Primary
                type="submit"
                id="submit-rekening"
                size="small"
                sx={{ "& span": { marginLeft: "8px" } }}
              >
                Save{" "}
              </CustomButon.Primary>
            </Grid>
          </Grid>
        </form>
      </CustomModal>
    </Grid>
  );
};
