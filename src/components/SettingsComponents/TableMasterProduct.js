import React, { useEffect, useState } from "react";
import TableComponent from "../TableComponent";
import { Grid, Switch, IconButton } from "@mui/material";
import master from "../../helper/services/master";
import Swal from "sweetalert2";
// import { faker } from "@faker-js/faker";

import { EditIcon, TrashIcon } from "../../assets/icons/customIcon";

function TableMasterProduct({editFunct, data, deleteFunct}) {
  // const [loading, setLoading] = useState(false);
  // const [dataProduct, setDataProduct] = useState([]);

  // const getDataProduct = async () => {
  //   setLoading(true);
  //   try {
  //     const response = await master.merchantProductList();
  //     if (response.data.success) {
  //       setDataProduct(response.data.datas);
  //       setTimeout(() => {
  //         setLoading(false);
  //       }, 1000);
  //     }
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };

  // const deleteProduct = async (val) => {
  //   console.log(val);
  //   Swal.fire({
  //     title: "Apakah anda yakin?",
  //     text: "Akan menghapus data ini?",
  //     icon: "warning",
  //     showCancelButton: true,
  //     confirmButtonColor: "#3085d6",
  //     cancelButtonColor: "#d33",
  //     confirmButtonText: "Confirm",
  //   }).then(async (result) => {
  //     if (result.isConfirmed) {
  //       const response = await master.merchantProductDelete(val);
  //       console.log(response);
  //       if (response) {
  //         Swal.fire("Deleted!", "Your data has been deleted.", "success")
  //       }
  //     }
  //   });
  // };

  const invoiceType =[{label: 'PAID INVOICE', value:1},{label: 'REGULER INVOICE', value:2},{label: 'REIMBURSEMENT', value:3}]

  const columns = [
    [
      {
        name: "No",
        renderCell: ({}, index) => index + 1,
      },
      {
        name: "Code",
        renderCell: ({ MP_CODE }) => MP_CODE,
        textAlign: "left",
        width: 240,
      },
      {
        name: "Brand",
        renderCell: ({ MP_BRAND }) => MP_BRAND,
        textAlign: "left",
        width: 240,
      },
      {
        name: "Brand Description",
        renderCell: ({ MP_DESCRIPTION }) => MP_DESCRIPTION,
        textAlign: "left",
        width: 240,
      },
      {
        name: "Invoice Type",
        renderCell: ({ MP_TYPE_INVOICE }) => invoiceType.map((type)=>(type.value === MP_TYPE_INVOICE)? type.label:''),
        textAlign: "left",
        width: 240,
      },
      {
        name: "Skema",
        renderCell: ({ PRODUCT_SKEMA }) => PRODUCT_SKEMA.MPS_LABEL,
        textAlign: "left",
        width: 240,
      },
      {
        name: "Tribe",
        renderCell: ({ PRODUCT_TRIBE }) => PRODUCT_TRIBE.MPT_LABEL,
      },
      {
        name: "Merchant",
        renderCell: ({ MERCHANT }) => MERCHANT.M_NAME,
      },
      {
        name: "Action",
        renderCell: ({ MP_ID }) => (<Grid>

          <IconButton onClick={()=>editFunct(MP_ID)}>

            <EditIcon />

          </IconButton>

          <IconButton onClick={() => deleteFunct(MP_ID)}>

            <TrashIcon />

          </IconButton>

        </Grid>),
      },
    ],
  ];
 
  return <TableComponent columns={columns} rows={data} />;
}

export default TableMasterProduct;
