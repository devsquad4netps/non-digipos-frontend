import React, { useEffect, useState } from "react";
import TableComponentSticky from "../TableComponentSticky";
import { Grid, Switch, IconButton } from "@mui/material";
// import { faker } from "@faker-js/faker";

import { EditIcon, TrashIcon, CheckIcon } from "../../assets/icons/customIcon";


function TableMasterProductGroup({ data, onClick, deleteFunct, editFunct, selectedID }) {

  const columns = [
    [
      // {
      //   name: "Selection",
      //   renderCell: (MPG_ID) => MPG_ID == selectedID.M ? <CheckIcon/>  : ""
      // },
      {
        name: "No",
        renderCell: ({}, index) => index + 1,
      },
      {
        name: "Label",
        renderCell: ({ MPG_LABEL }) => MPG_LABEL,
        textAlign: "left",
      },
      {
        name: "Type MDR",
        renderCell: ({ MPG_MDR_TYPE }) => MPG_MDR_TYPE == 2 ? "Amount" : "Percent",
        textAlign: "left",

      },
      {
        name: "Amount MDR",
        renderCell: ({ MPG_MDR_AMOUNT }) => MPG_MDR_AMOUNT,
        textAlign: "right",

      },
      {
        name: "Bobot DPP",
        renderCell: ({ MPG_DPP }) => MPG_DPP,
        textAlign: "right",

      },
      {
        name: "Bobot PPN (%)",
        renderCell: ({ MPG_PPN }) => MPG_PPN,
        textAlign: "right",

      },
      {
        name: "Bobot PPH (%)",
        renderCell: ({ MPG_PPH }) => MPG_PPH,
        textAlign: "right",

      },
      // {
      //   name: "Configate",
      //   renderCell: ({ configate }) => configate,
      //   textAlign: "right",
      // },
      {
        name: "Action",
        renderCell: ({ MPG_ID }) => (<Grid>

          <IconButton onClick={()=>{editFunct(MPG_ID)}}>

            <EditIcon />

          </IconButton>

          <IconButton onClick={()=>{deleteFunct(MPG_ID)}}>

            <TrashIcon />

          </IconButton>

        </Grid>),
      },
    ],
  ];
 
  return <TableComponentSticky columns={columns} rows={data} hover={true} selected={true} onClick={onClick}/>;
}

export default TableMasterProductGroup;
