import React from "react";
import TableComponent from "../TableComponent";
import { Grid, Switch, IconButton } from "@mui/material";
// import { faker } from "@faker-js/faker";

import { EditIcon, TrashIcon } from "../../assets/icons/customIcon";

function TableMasterField({ data, deleteFunct, editFunct }) {
  const columns = [
    [
      {
        name: "No",
        renderCell: ({}, index) => index + 1,
      },
      {
        name: "Key Field",
        renderCell: ({ MPV_KEY }) => MPV_KEY,
        textAlign: "left",
        
      },
      {
        name: "Label Field",
        renderCell: ({ MPV_LABEL }) =>
          MPV_LABEL ? MPV_LABEL : "No Data",
        textAlign: "left",
        
        },
      {
        name: "Type Field",
        renderCell: ({ MPV_TYPE }) => MPV_TYPE ? MPV_TYPE.MPV_TYPE_LABEL : "No Data",
        textAlign: "left",
        
      },
      {
        name: "Category Field",
        renderCell: ({ MPV_TYPE_KEY }) => MPV_TYPE_KEY ? MPV_TYPE_KEY.MPV_TYPE_KEY_LABEL : "No Data",
        textAlign: "left",
        
      },
      {
        name: "Action",
        renderCell: ({ MPV_ID }) => (<Grid>

          <IconButton onClick={()=>{editFunct(MPV_ID)}}>

            <EditIcon />

          </IconButton>

          <IconButton onClick={()=>{deleteFunct(MPV_ID)}}>

            <TrashIcon />

          </IconButton>

        </Grid>),
      },
    ],
  ];
 
  return <TableComponent columns={columns} rows={data} />;
}

export default TableMasterField;
