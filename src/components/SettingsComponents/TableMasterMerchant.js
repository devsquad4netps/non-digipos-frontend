import React, { useEffect, useState } from "react";
import TableComponent from "../TableComponent";
import { Grid, Switch, IconButton, Skeleton } from "@mui/material";
import master from "../../helper/services/master";
import Swal from "sweetalert2";
// import { faker } from "@faker-js/faker";

import { EditIcon, TrashIcon } from "../../assets/icons/customIcon";

function TableMasterMerchant() {
  const [loading, setLoading] = useState(true);
  const [dataMerchant, setDataMerchant] = useState([1, 2, 3, 4, 5]);
  const [refresh, setRefresh] = useState(false);

  const getDataMerchant = async () => {
    setLoading(true);
    try {
      const response = await master.merchantList();
      if (response.data.success) {
        setDataMerchant(response.data.datas);
        setTimeout(() => {
          setLoading(false);
        }, 1000);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const deteleteMerchant = async (val) => {
    console.log(val);
    Swal.fire({
      title: "Apakah anda yakin?",
      text: "Akan menghapus data ini?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Confirm",
    }).then(async (result) => {
      if (result.isConfirmed) {
        const response = await master.merchantDelete(val);
        console.log(response);
        if (response) {
          Swal.fire("Deleted!", "Your data has been deleted.", "success").then(
            () => setRefresh(true)
          );
        }
      }
    });
  };

  useEffect(() => {
    getDataMerchant();
  }, [refresh]);

  const columns = [
    [
      {
        name: "No",
        renderCell: ({}, index, No) => No,
        width: 60,
      },
      {
        name: "Code",
        renderCell: ({ M_CODE }) => M_CODE,
        textAlign: "left",
        width: 120,
      },
      {
        name: "Name Merchant",
        renderCell: ({ M_NAME }) => M_NAME,
        width: 240,
        textAlign: "left",
      },
      {
        name: "Legal Name Merchant",
        renderCell: ({ M_LEGAL_NAME }) => M_LEGAL_NAME,
        width: 240,
        textAlign: "left",
      },
      {
        name: "Alamat",
        renderCell: ({ M_ALAMAT_KANTOR }) => M_ALAMAT_KANTOR,
        width: 240,
        textAlign: "left",
      },
      {
        name: "No Prefix",
        renderCell: ({ M_NO_PERFIX }) => M_NO_PERFIX,
        width: 120,
        textAlign: "left",
      },
      {
        name: "NPWP",
        renderCell: ({ M_NPWP }) => M_NPWP,
        width: 240,
        textAlign: "left",
      },
      {
        name: "Alamat NPWP",
        renderCell: ({ M_ALAMAT_NPWP }) => M_ALAMAT_NPWP,
        width: 240,
        textAlign: "left",
      },
      {
        name: "No Contract",
        renderCell: ({ M_NO_CONTRACT }) => M_NO_CONTRACT,
        textAlign: "left",
        width: 120,
      },
      {
        name: "Action",
        renderCell: ({ M_ID }) => (
          <Grid>
            <IconButton href={`/settings/master-merchant/edit/${M_ID}`}>
              <EditIcon />
            </IconButton>

            <IconButton onClick={() => deteleteMerchant(M_ID)}>
              <TrashIcon />
            </IconButton>
          </Grid>
        ),
        width: 120,
      },
    ],
  ];

  return (
    <TableComponent columns={columns} rows={dataMerchant} loading={loading} />
  );
}

export default TableMasterMerchant;
