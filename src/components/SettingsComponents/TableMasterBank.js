import React, { useEffect, useState } from "react";
import TableComponent from "../TableComponent";
import { Grid, Switch, IconButton } from "@mui/material";
import master from "../../helper/services/master";
import Swal from "sweetalert2";
// import { faker } from "@faker-js/faker";

import { EditIcon, TrashIcon } from "../../assets/icons/customIcon";

function TableMasterBank({editFunct, data, deleteFunct}) {

  const columns = [
    [
      {
        name: "No",
        renderCell: ({}, index) => index + 1,
      },
      {
        name: "Code Bank",
        renderCell: ({ CODE_BANK }) => CODE_BANK,
        textAlign: "left",
        width: 240,
      },
      {
        name: "Bank Name",
        renderCell: ({ NAME_BANK }) => NAME_BANK,
        textAlign: "left",
        width: 240,
      },
      {
        name: "Action",
        renderCell: ({ ID }) => (<Grid>

          <IconButton onClick={()=>editFunct(ID)}>

            <EditIcon />

          </IconButton>

          <IconButton onClick={() => deleteFunct(ID)}>

            <TrashIcon />

          </IconButton>

        </Grid>),
      },
    ],
  ];
 
  return <TableComponent columns={columns} rows={data} />;
}

export default TableMasterBank;
