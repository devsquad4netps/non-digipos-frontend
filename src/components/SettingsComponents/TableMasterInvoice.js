import React, { useEffect, useState } from "react";
import TableComponent from "../TableComponent";
import { Grid, Switch, IconButton } from "@mui/material";
import master from "../../helper/services/master";
import Swal from "sweetalert2";
// import { faker } from "@faker-js/faker";

import { EditIcon, TrashIcon, CheckIcon, CrossIcon } from "../../assets/icons/customIcon";

function TableMasterInvoice({editFunct, data, deleteFunct}) {

  const columns = [
    [ 
      {
        name: "No",
        renderCell: ({}, index) => index + 1,
      },
      {
        name: "Name Invoice",
        renderCell: ({ PIC_NAME }) => PIC_NAME,
      },
      {
        name: "Position",
        renderCell: ({ PIC_JABATAN }) => PIC_JABATAN,
      },
      {
        name: "Phone Number",
        renderCell: ({ PIC_NOTELP }) => PIC_NOTELP,
      },
      {
        name: "Email",
        renderCell: ({ PIC_EMAIL }) => PIC_EMAIL,
      },
      {
        name: "Address",
        renderCell: ({ PIC_ALAMAT }) => PIC_ALAMAT,
      },
      {
        name: "NPWP",
        renderCell: ({ PT_NPWP }) => PT_NPWP,
      },
      {
        name: "Account Number",
        renderCell: ({ PT_ACOUNT_NUMBER }) => PT_ACOUNT_NUMBER,
      },
      {
        name: "Bank Name",
        renderCell: ({ PT_BANK_NAME }) => PT_BANK_NAME,
      },
      {
        name: "Branch",
        renderCell: ({ PT_BRANCH }) => PT_BRANCH,
      },
      {
        name: "Set Attention",
        renderCell: ({ AT_FLAG=1 }) => (AT_FLAG ? <CheckIcon/> : <CrossIcon/>),
      },
      {
        name: "Set Sign",
        renderCell: ({ AT_FLAG=1 }) => (AT_FLAG ? <CheckIcon/> : <CrossIcon/>),
      },
      {
        name: "Action",
        renderCell: ({ ID }) => (<Grid>

          <IconButton onClick={()=>editFunct(ID)}>

            <EditIcon />

          </IconButton>

          <IconButton onClick={() => deleteFunct(ID)}>

            <TrashIcon />

          </IconButton>

        </Grid>),
      },
    ],
  ];

  
  return <TableComponent columns={columns} rows={data} />;
}

export default TableMasterInvoice;
