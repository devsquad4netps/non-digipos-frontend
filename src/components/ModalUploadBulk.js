import React, { useCallback, useState, useEffect, useMemo } from "react";
import CustomModal from "./CustomModal";
import { ButtonGroup, Button, Grid } from "@mui/material";
import CustomButon from "./CustomButon";
import { Axios } from "../helper/http";

export default function ModalUploadBulk({ open, onClose }) {
  const [submiting, setSubmiting] = useState(false);
  const [active, setActive] = useState("BAR");
  const [listFile, setListFile] = useState([]);
  const [statusUpload, setStatusUpload] = useState([]);

  const data = [
    "BAR",
    "BAR Sign",
    "Invoice",
    "Invoice Sign",
    "Faktur Pajak",
    "Bukpot",
  ];

  const handleFiles = ({ target }) => {
    setStatusUpload([]);
    const files = target.files;
    let newFiles = [];
    for (var i = 0; i < files.length; i++) {
      newFiles.push(files[i]);
    }
    setListFile(newFiles);
  };

  const handleSubmit = (files) => {
    setSubmiting(true);
    if (files.length > 0) {
      const formData = new FormData();
      formData.append(
        active === "BAR"
          ? "bar_file"
          : active === "BAR Sign"
          ? "bar_sign_file"
          : active === "Invoice"
          ? "invoice_file"
          : active === "Invoice Sign"
          ? "invoice_sign_file"
          : active === "Faktur Pajak"
          ? "faktur_pajak_file"
          : "bukpot_file",
        files[0]
      );
      Axios.post(
        `/distribusi-upload/merchant/upload-bulk/${
          active === "BAR"
            ? "bar"
            : active === "BAR Sign"
            ? "bar-sign"
            : active === "Invoice"
            ? "invoice"
            : active === "Invoice Sign"
            ? "invoice-sign"
            : active === "Faktur Pajak"
            ? "faktur-pajak"
            : "bukti-potong"
        }`,
        formData
      )
        .then((res) => {
          setTimeout(() => {
            setStatusUpload((prev) => [...prev, res.data]);
            files = files.filter((a, i) => i !== 0);
            handleSubmit(files);
          }, 500);
        })
        .catch((err) => console.log("ERR", err));
    } else {
      setSubmiting(false);
    }
  };

  return (
    <CustomModal open={open} onClose={onClose} width="auto">
      <Grid
        sx={({ customTheme }) => ({
          border: `1px solid ${customTheme.color.primary}`,
          borderRadius: "6px",
        })}
      >
        <ButtonGroup
          variant="contained"
          aria-label="outlined primary button group"
        >
          {data.map((val) => {
            return (
              <Button
                sx={({ customTheme }) => ({
                  backgroundColor:
                    val === active
                      ? customTheme.color.primary
                      : "rgba(41, 100, 123, 0.5)",
                  textTransform: "none",
                  width: "120px",
                })}
                onClick={() => setActive(val)}
              >
                {val}
              </Button>
            );
          })}
        </ButtonGroup>
        <Grid
          container
          sx={{
            padding: "28px",
            justifyContent: "space-between",
          }}
        >
          <Grid
            alignItems="center"
            sx={{
              "& h3": {
                fontWeight: 700,
                fontSize: "16px",
                marginRight: "16px",
              },
              display: "flex",
            }}
          >
            <h3>Upload File :</h3>
            <CustomButon.Primary disabled={submiting} component="label">
              Choose File
              <input
                type="file"
                accept="application/pdf"
                hidden
                multiple
                onChange={handleFiles}
              />
            </CustomButon.Primary>
          </Grid>
          <CustomButon.Primary
            disabled={submiting}
            onClick={() => handleSubmit(listFile)}
          >
            Upload
          </CustomButon.Primary>
          <Grid container sx={{ border: "1px solid #29647B" }} mt="24px"></Grid>
          <Grid container sx={{ paddingTop: "24px", paddingBottom: "24px" }}>
            {listFile.map((val, i) => {
              return (
                <Grid
                  container
                  alignItems="center"
                  sx={{
                    "& label": {
                      minWidth: "400px",
                    },
                    "& p": {
                      color:
                        submiting && !statusUpload[i]
                          ? "black"
                          : statusUpload[i]?.success
                          ? "green"
                          : "red",
                    },
                  }}
                >
                  <label for="">{val.name}</label>
                  <p>
                    {statusUpload[i]?.message
                      ? statusUpload[i]?.message
                      : submiting
                      ? "Uploading ..."
                      : ""}
                  </p>
                </Grid>
              );
            })}
          </Grid>
        </Grid>
      </Grid>
      <Grid container justifyContent="flex-end" mt="24px">
        <CustomButon.Error onClick={() => onClose()}>Cancel</CustomButon.Error>
      </Grid>
    </CustomModal>
  );
}
