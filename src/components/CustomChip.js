import React from "react";
import { Button, Chip } from "@mui/material";
import { ChipSuccess, CrossIconCricle } from "../assets/icons/customIcon";

const CustomChip = ({ label, success, onClick }) => {
  return (
    <Chip
      onClick={onClick}
      size="small"
      icon={success && <ChipSuccess />}
      label={label ? label : success ? "Validasi" : "undefined"}
      variant="outlined"
      sx={({ customTheme }) => ({
        padding: "0px",
        paddingRight: "12px",
        paddingLeft: !success && "12px",
        "& svg": {
          marginLeft: "12px",
        },
        color: customTheme.color.primary,
        borderColor: customTheme.color.primary,
      })}
    />
  );
};

const Status = ({ label, success, onClick, data }) => {
  let { AT_FLAG } = data;
  return (
    <Chip
      onClick={onClick}
      size="small"
      icon={
        AT_FLAG === 1 ? (
          <ChipSuccess />
        ) : AT_FLAG === 2 ? (
          <CrossIconCricle className="cross" />
        ) : (
          ""
        )
      }
      label={
        AT_FLAG === 1
          ? "Validate"
          : AT_FLAG === 2
          ? "Rejected"
          : "Waiting Validation"
      }
      variant="outlined"
      sx={({ customTheme }) => ({
        padding: "0px",
        paddingRight: "12px",
        paddingLeft: AT_FLAG === 0 ? "12px" : "",
        "& svg": {
          marginLeft: "12px",
        },
        "& .cross": {
          height: "16px",
          marginLeft: "12px",
          color: "red",
        },
        color:
          AT_FLAG === 1
            ? customTheme.color.primary
            : AT_FLAG === 2
            ? "red"
            : "",
        borderColor:
          AT_FLAG === 1
            ? customTheme.color.primary
            : AT_FLAG === 2
            ? "red"
            : "",
      })}
    />
  );
};

CustomChip.Status = Status;

const RedChip = ({ label, reject }) => {
  return (
    <Chip
      size="small"
      icon={<ChipSuccess />}
      label={label ? label : reject ? "Rejected" : "undefined"}
      variant="outlined"
      sx={({ customTheme }) => ({
        padding: "0px",
        paddingRight: "12px",
        "& svg": {
          marginLeft: "12px",
        },
        color: customTheme.color.reject,
        borderColor: customTheme.color.reject,
      })}
    />
  );
};

CustomChip.Reject = RedChip;

export default CustomChip;
