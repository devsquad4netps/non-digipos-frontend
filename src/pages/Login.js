import React, { useState } from "react";
import { Grid, Alert, AlertTitle } from "@mui/material";

import { makeStyles } from "@mui/styles";

import CustomInput from "../components/InputComponent";

import ImageLogin from "../assets/images/image-login.png";
import LinkAjaLogo from "../assets/images/linkaja-logo.png";

import { UserIcon, LockIcon } from "../assets/icons/customIcon";
import CustomButon from "../components/CustomButon";

import { useForm } from "react-hook-form";
import master from "../helper/services/master";
import { setToken, setUserId } from "../helper/cookies";

const useStyles = makeStyles((theme) => {
  return {
    root: {
      height: "100vh",
      "& .content-left": {
        height: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        [theme.breakpoints.down("md")]: {
          display: "none",
        },
        "& img": {
          width: "100%",
          marginRight: "108px",
          marginLeft: "108px",
        },
      },
      "& .content-right": {
        height: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        "& .content": {
          textAlign: "center",
          width: "100%",
          marginLeft: "108px",
          marginRight: "108px",
          [theme.breakpoints.down("md")]: {
            marginLeft: "24px",
            marginRight: "24px",
          },
          "& p": {
            fontWeight: "400",
            fontSize: "14px",
            lineHeight: "16px",
            marginTop: "32px",
            marginBottom: "32px",
          },
        },
      },
    },
  };
});

export default function Login() {
  const classes = useStyles();
  const [isSubmit, setIsSubmit] = useState(false);
  const [error, setError] = useState(null);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data) => {
    setIsSubmit(true);
    try {
      const response = await master.Login(data);
      if (response.status === 200) {
        setUserId(response.data.data.user.id);
        setToken(response.data.data.user);
        window.location.href = window.location.origin + "/dashboard";
      }
      setIsSubmit(false);
    } catch (error) {
      setIsSubmit(false);
      setError(error.response.data.error);
      console.log(error);
    }
  };
  return (
    <Grid className={classes.root}>
      <Grid container height="100%">
        <Grid
          item
          md={6}
          xs={12}
          sx={{
            display: { xs: "none!important", md: "flex!important" },
            backgroundColor: "#E1EAFF",
          }}
          className="content-left"
        >
          <img src={ImageLogin} alt="" />
        </Grid>
        <Grid item md={6} xs={12} className="content-right">
          <Grid className="content">
            <img src={LinkAjaLogo} alt="" />
            <p>Welcome back, please login to your account.</p>
            {error && (
              <Alert
                severity="error"
                mb={"32px"}
                sx={{ textAlign: "left", marginBottom: "12px" }}
              >
                <AlertTitle>Error</AlertTitle>
                <strong>{error.code} </strong>
                {error.message || ""}
                {error?.details && (
                  <ul>
                    Details
                    {error.details.map((val) => {
                      return (
                        <li>
                          {val.path} : {val.message}
                        </li>
                      );
                    })}
                  </ul>
                )}
              </Alert>
            )}
            <Grid
              component="form"
              rowSpacing="12px"
              display="flex"
              flexDirection="column"
              gap="20px"
              onSubmit={handleSubmit(onSubmit)}
            >
              <CustomInput.LoginInput
                id="email"
                icon={<UserIcon />}
                placeholder="Email"
                register={{ ...register("email", { required: true }) }}
                errors={errors}
                disabled={isSubmit}
              />
              <CustomInput.LoginInput
                icon={<LockIcon />}
                placeholder="Password"
                id="password"
                type="password"
                register={{ ...register("password", { required: true }) }}
                errors={errors}
                disabled={isSubmit}
              />
              <Grid container justifyContent="flex-end">
                <CustomButon.Primary disabled={isSubmit} type="submit">
                  Login
                </CustomButon.Primary>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
