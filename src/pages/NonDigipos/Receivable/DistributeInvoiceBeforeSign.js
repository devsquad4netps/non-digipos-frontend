import React from "react";
import { Grid } from "@mui/material";
import { PlusButtonIcon } from "../../../assets/icons/customIcon";
import CustomButon from "../../../components/CustomButon";
import Header from "../../../components/Header";
import TableDistributeInvoiceBeforeSign from "../../../components/NonDigiposComponents/ReceivableComponents/TableDistributeInvoiceBeforeSign";

export default function DistributeInvoiceBeforeSign() {
  return (
    <Grid>
      <Header
        titles={[
          "Non Digipost",
          "Recieveable Revenue Tools",
          "Distribusi Invoice Before Sign",
        ]}
      />
      <Grid mt="36px" gap="36px">
        <TableDistributeInvoiceBeforeSign />
      </Grid>
    </Grid>
  );
}
