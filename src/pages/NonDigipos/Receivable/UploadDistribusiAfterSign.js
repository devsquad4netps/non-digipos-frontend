import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { Grid, Modal, Button, IconButton } from "@mui/material";
import Header from "../../../components/Header";
import CustomButon from "../../../components/CustomButon";
import CustomModal from "../../../components/CustomModal";
import CustomButonFile from "../../../components/CustomButtonFile";
import { PlusButtonIcon, TrashIcon } from "../../../assets/icons/customIcon";
import UploadDistribusiAfterSignTable from "../../../components/NonDigiposComponents/ReceivableComponents/UploadDistribusiAfterSignTable";

export default function UploadDistribusiAfterSign() {
  return (
    <Grid>
      <Header
        titles={[
          "Non Digipost",
          "Recieveable Revenue Tools",
          "Upload & Distribusi After Sign",
        ]}
      />
      <Grid mt={3}>
        <UploadDistribusiAfterSignTable />
      </Grid>
    </Grid>
  );
}
