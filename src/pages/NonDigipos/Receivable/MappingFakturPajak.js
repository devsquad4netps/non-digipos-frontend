import React from "react";
import { Grid } from "@mui/material";

import Header from "../../../components/Header";
import CustomButon from "../../../components/CustomButon";

import TableMappingFakturPajak from "../../../components/NonDigiposComponents/ReceivableComponents/TableMappingFakturPajak";

export default function MappingFakturPajak() {
  return (
    <Grid>
      <Header
        titles={[
          "Non Digipost",
          "Recieveable Revenue Tools",
          "Mapping Faktur Pajak Regular & Pajak Invoice",
        ]}
      />
      <Grid mt="36px" gap="36px">
        <TableMappingFakturPajak />
      </Grid>
    </Grid>
  );
}
