import React from "react";
import { Grid } from "@mui/material";

import Header from "../../../components/Header";
import CustomButon from "../../../components/CustomButon";
import InqueryReviewSummaryBarTable from "../../../components/NonDigiposComponents/ReceivableComponents/InquiryReviewSummaryBarTable";

export default function InqueryReviewSummaryBar() {
  return (
    <Grid>
      <Header
        titles={[
          "Non Digipost",
          "Recieveable Revenue Tools",
          "Inquiry & Reviews Summary BAR",
        ]}
      />
      <Grid mt="36px">
        <InqueryReviewSummaryBarTable />
      </Grid>
    </Grid>
  );
}
