import React, { useState, useEffect } from "react";
import { Grid, Modal, Button } from "@mui/material";

import Header from "../../../components/Header";
import TableUploadTransaksiExcludeBa from "../../../components/NonDigiposComponents/ReceivableComponents/TableUploadTransaksiExcludeBa";
import CustomButon from "../../../components/CustomButon";
import CustomInput from "../../../components/InputComponent";
import CustomTab from "../../../components/CustomTab";
import CustomInputTab from "../../../components/InputComponentTab";
import CustomInputText from "../../../components/InputComponentText";
import CustomSelect from "../../../components/SelectComponent";
import CustomModal from "../../../components/CustomModal";
import { PlusButtonIcon } from "../../../assets/icons/customIcon";
import { Link } from "react-router-dom";

export default function UploadTransaksiExcludeBA() {
    const [open, setOpen] = useState();
    const handleClose = () => setOpen(false);
   
    return (
      <Grid>
      <Header titles={['Non Digipost', 'Transaksi Tax','Upload Transaksi Exclude BA']}/>
        <Grid mt="36px" sx={{ "& a": { textDecoration: "none" } }}>
              <CustomButon.Primary
                onClick={(e) => setOpen(true)}
                sx={{ "& span": { marginLeft: "8px" } }}
              >
                <PlusButtonIcon />
                <span>Upload Transaksi Exclude BA</span>
              </CustomButon.Primary>
            </Grid>
        <Grid mt="38px">
          <TableUploadTransaksiExcludeBa />
        </Grid>
        {/* {open && (
        <CustomModal width="800px" open={open} onClose={handleClose} title="Form Upload Transaksi P2P">
          <Grid container spacing={10} rowSpacing={12}>
          <Grid item xs={6} rowSpacing="12px">
            <CustomInputTab.InputLabel label="Customer Name :" labelWidth="110px" rowSpacing={12} />
            <CustomInputTab.InputLabel label="Customer Code :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Invoice Type :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Month :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Transaction Source :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Transaction Type :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Terms :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Invoice Date :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Due Date :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="DPP :" labelWidth="110px" />
            </Grid>
            <Grid item xs={6} rowSpacing="12px">
            <CustomInputTab.InputLabel label="PPN :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="PPH :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Amount :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Description :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Account Name :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="No Rek :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Branch :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="PIC :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="No Kontrak :" labelWidth="110px" />
            </Grid>
            
            <Grid item justifyContent="flex-end" container gap="24px">
              <CustomButon.Primary
                size="small"
                onClick={handleClose}
                sx={{ "& span": { marginLeft: "8px" }, backgroundColor: "red" }}
              >
                Cancel{" "}
              </CustomButon.Primary>
              <CustomButon.Primary
                size="small"
                onClick={(e) => setOpen(true)}
                sx={{ "& span": { marginLeft: "8px" } }}
              >
                Save{" "}
              </CustomButon.Primary>
            </Grid>
          </Grid>
        </CustomModal>
      )} */}
      </Grid>
  );
}
