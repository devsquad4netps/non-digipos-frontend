import React from "react";
import { Grid } from "@mui/material";
import Header from "../../../components/Header";
import CustomButon from "../../../components/CustomButon";
import { PlusButtonIcon } from "../../../assets/icons/customIcon";
import GenerateNoInvoiceTable from "../../../components/NonDigiposComponents/ReceivableComponents/GenerateNoInvoiceTable";

export default function GenerateNoInvoice() {
  return (
    <Grid>
      <Header
        titles={[
          "Non Digipost",
          "Recieveable Revenue Tools",
          "Generate No Invoice & PDF File",
        ]}
      />
      <Grid mt={3}>
        <GenerateNoInvoiceTable />
      </Grid>
    </Grid>
  );
}
