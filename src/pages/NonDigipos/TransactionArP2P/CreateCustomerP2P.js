import React, { useState, useEffect } from "react";
import { Grid, Modal, Button } from "@mui/material";

import Header from "../../../components/Header";
import TableCreateCustomerP2P from "../../../components/NonDigiposComponents/TransactionARP2P/TableCreateCustomerP2P";
import CustomButon from "../../../components/CustomButon";
import CustomInput from "../../../components/InputComponent";
import CustomTab from "../../../components/CustomTab";
import CustomInputTab from "../../../components/InputComponentTab";
import CustomInputText from "../../../components/InputComponentText";
import CustomSelect from "../../../components/SelectComponent";
import CustomModal from "../../../components/CustomModal";
import { PlusButtonIcon } from "../../../assets/icons/customIcon";
import { Link } from "react-router-dom";

export default function CreateCustomerP2P() {
    const [open, setOpen] = useState();
    const handleClose = () => setOpen(false);
   
    return (
      <Grid>
      <Header titles={['Non Digipost', 'Transaksi Tax','Create Customer P2P']}/>
        <Grid mt="36px" sx={{ "& a": { textDecoration: "none" } }}>
              <CustomButon.Primary
                onClick={(e) => setOpen(true)}
                sx={{ "& span": { marginLeft: "8px" } }}
              >
                <PlusButtonIcon />
                <span>Create Customer P2P</span>
              </CustomButon.Primary>
            </Grid>
        <Grid mt="38px">
          <TableCreateCustomerP2P />
        </Grid>
        {open && (
        <CustomModal width="800px" open={open} onClose={handleClose} title="Form Create Transaksi P2P">
          <Grid container spacing={10} rowSpacing={12}>
          <Grid item xs={6} rowSpacing="12px">
            <CustomInputTab.InputLabel label="ID :" labelWidth="110px"/>
            <CustomInputTab.InputLabel label="Name :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="NPWP :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Account Desc :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Legal Name :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Customer Class :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Customer Source :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Prefix :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Site Name :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Address NPWP :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Address :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="City :" labelWidth="110px" />
            </Grid>
            <Grid item xs={6} rowSpacing="12px">
            <CustomInputTab.InputLabel label="Province :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Postal Code :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Country :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Business :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Bill To :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="First Name :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Phone :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Phone Area :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Mobile Phone Code :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Mobile Phone Number :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Email :" labelWidth="110px" />
            <CustomInputTab.InputLabel label="Interface Status :" labelWidth="110px" />
            </Grid>
            
            <Grid item justifyContent="flex-end" container gap="24px">
              <CustomButon.Primary
                size="small"
                onClick={handleClose}
                sx={{ "& span": { marginLeft: "8px" }, backgroundColor: "red" }}
              >
                Cancel{" "}
              </CustomButon.Primary>
              <CustomButon.Primary
                size="small"
                onClick={(e) => setOpen(true)}
                sx={{ "& span": { marginLeft: "8px" } }}
              >
                Save{" "}
              </CustomButon.Primary>
            </Grid>
          </Grid>
        </CustomModal>
      )}
      </Grid>
  );
}
