import React, { useState, useEffect } from "react";
import { Grid, Modal, Button } from "@mui/material";

import Header from "../../../components/Header";
import TableCreateTransaksiP2P from "../../../components/NonDigiposComponents/TransactionARP2P/TableCreateTransaksiP2P";
import CustomButon from "../../../components/CustomButon";
import CustomInput from "../../../components/InputComponent";
import CustomTab from "../../../components/CustomTab";
import CustomInputTab from "../../../components/InputComponentTab";
import CustomInputText from "../../../components/InputComponentText";
import CustomSelect from "../../../components/SelectComponent";
import CustomModal from "../../../components/CustomModal";
import { PlusButtonIcon } from "../../../assets/icons/customIcon";
import { Link } from "react-router-dom";

export default function CreateTransaksiP2P() {
    const [open, setOpen] = useState();
    const handleClose = () => setOpen(false);
   
    return (
      <Grid>
      <Header titles={['Non Digipost', 'Transaksi Tax','Create Transaksi P2P']}/>
        <Grid mt="36px" sx={{ "& a": { textDecoration: "none" } }} container gap="24px">
              <CustomButon.Primary
                onClick={(e) => setOpen(true)}
                sx={{ "& span": { marginLeft: "8px" } }}
              >
                <PlusButtonIcon />
                <span>Create Transaksi P2P</span>
              </CustomButon.Primary>
              <CustomButon.Primary
                // onClick={(e) => setOpen(true)}
                sx={{ "& span": { marginLeft: "8px" } }}
              >
                <PlusButtonIcon />
                <span>Import Excel</span>
              </CustomButon.Primary>
              <CustomButon.Primary
                // onClick={(e) => setOpen(true)}
                sx={{ "& span": { marginLeft: "8px" } }}
              >
                <PlusButtonIcon />
                <span>Export Excel</span>
              </CustomButon.Primary>
              <CustomButon.Primary
                // onClick={(e) => setOpen(true)}
                sx={{ "& span": { marginLeft: "8px" } }}
              >
                <PlusButtonIcon />
                <span>Validasi</span>
              </CustomButon.Primary>
            </Grid>
        <Grid mt="38px">
          <TableCreateTransaksiP2P />
        </Grid>
        {open && (
        <CustomModal width="500px" open={open} onClose={handleClose} title="Form Create Transaksi P2P">
          <Grid container spacing={10} rowSpacing={10}>
          <Grid item xs={12} rowSpacing="10px">
            <CustomInputTab.InputLabel label="Transaction Source :" labelWidth="200px" />
            <CustomInputTab.InputLabel label="Transaction Type :" labelWidth="200px" />
            <CustomInputTab.InputLabel label="Cust Name :" labelWidth="200px" />
            <CustomInputTab.InputLabel label="Inv Number :" labelWidth="200px" />
            <CustomInputTab.InputLabel label="Month :" labelWidth="200px" />
            <CustomInputTab.InputLabel label="Tenor :" labelWidth="200px" />
            <CustomInputTab.InputLabel label="Tanggal Pendanaan :" labelWidth="200px" />
            <CustomInputTab.InputLabel label="Due Date :" labelWidth="200px" />
            <CustomInputTab.InputLabel label="Disburstment Amount :" labelWidth="200px" />
            <CustomInputTab.InputLabel label="Description :" labelWidth="200px" />
            <CustomInputTab.InputLabel label="Contract Number :" labelWidth="200px" />
            <CustomInputTab.InputLabel label="Load Type :" labelWidth="200px" />
        </Grid>
            
            <Grid item justifyContent="flex-end" container gap="24px">
              <CustomButon.Primary
                size="small"
                onClick={handleClose}
                sx={{ "& span": { marginLeft: "8px" }, backgroundColor: "red" }}
              >
                Cancel{" "}
              </CustomButon.Primary>
              <CustomButon.Primary
                size="small"
                onClick={(e) => setOpen(true)}
                sx={{ "& span": { marginLeft: "8px" } }}
              >
                Save{" "}
              </CustomButon.Primary>
            </Grid>
          </Grid>
        </CustomModal>
      )}
      </Grid>
  );
}
