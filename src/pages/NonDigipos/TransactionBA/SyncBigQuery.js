import React from "react";
import Header from "../../../components/Header";
import { Grid } from "@mui/material";
import TableSyncBigQuery from "../../../components/NonDigiposComponents/TransationBAComponents/TableSyncBigQuery";

export default function SyncBigQuery() {
  return (
    <Grid>
      <Header titles={['Non Digipost', 'Transaction BA','Sync Big Query']}/>
      <Grid mt="36px">
        <TableSyncBigQuery />
      </Grid>
    </Grid>
  );
}
