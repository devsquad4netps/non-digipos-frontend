import React from "react";
import { Grid } from "@mui/material";

import Header from "../../../components/Header";
import CustomButon from "../../../components/CustomButon";
import ReviewSummaryMerchantTable from "../../../components/NonDigiposComponents/TransationBAComponents/ReviewSummaryMerchantTable";

export default function ReviewSummaryMerchant() {
  return (
    <Grid>
      <Header
        titles={["Non Digipost", "Transaction BA", "Review Summary Merchant"]}
      />
      <Grid mt="36px">
        <ReviewSummaryMerchantTable />
      </Grid>
    </Grid>
  );
}
