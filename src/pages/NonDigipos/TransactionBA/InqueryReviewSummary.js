import React from "react";
import { Grid } from "@mui/material";

import Header from "../../../components/Header";
import CustomButon from "../../../components/CustomButon";
import TableInqueryReviewSummary from "../../../components/NonDigiposComponents/TransationBAComponents/TableInqueryReviewSummary";

export default function InqueryReviewSummary() {
  return (
    <Grid>
      <Header
        titles={["Non Digipost", "Transaction BA", "Inquiry/ Review Summary"]}
      />
      <TableInqueryReviewSummary />
    </Grid>
  );
}
