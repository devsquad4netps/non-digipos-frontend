import React from "react";
import { Grid } from "@mui/material";

import Header from "../../../components/Header";
import CustomButon from "../../../components/CustomButon";
import DistribusiBarTable from "../../../components/NonDigiposComponents/TransationBAComponents/DistribusiBarTable";

export default function GenerateBARNumber() {
  return (
    <Grid>
      <Header
        idSearch="distribusi-bar"
        titles={["Non Digipost", "Transaction BA", "Distribusi BAR"]}
      />
      <DistribusiBarTable />
    </Grid>
  );
}
