import React from "react";
import { Grid } from "@mui/material";

import Header from "../../../components/Header";
import CustomButon from "../../../components/CustomButon";
import TableMigrasiDataCsv from "../../../components/NonDigiposComponents/TransationBAComponents/TableMigrasiDataCsv";

export default function MigrasiDataCsv() {
  return (
    <Grid>
      <Header titles={["Non Digipost", "Transaction BA", "Migrasi Data CSV"]} />
      <TableMigrasiDataCsv />
    </Grid>
  );
}
