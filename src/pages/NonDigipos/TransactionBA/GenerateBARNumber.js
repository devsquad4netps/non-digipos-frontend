import React from "react";
import { Grid } from "@mui/material";

import Header from "../../../components/Header";
import CustomButon from "../../../components/CustomButon";
import GenerateBarNumberTable from "../../../components/NonDigiposComponents/TransationBAComponents/GenerateBarNumberTable";

export default function GenerateBARNumber() {
  const generateBarNumber = async () => {};
  const regenerateBarNumber = async () => {};
  const validateBarNumber = async () => {};
  return (
    <Grid>
      <Header
        titles={["Non Digipost", "Transaction BA", "Generate BAR Number"]}
      />

      <GenerateBarNumberTable />
    </Grid>
  );
}
