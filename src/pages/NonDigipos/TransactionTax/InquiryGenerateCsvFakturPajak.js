import React, { useState, useEffect } from "react";
import { Grid, Modal, Button, Checkbox, Menu, MenuItem } from "@mui/material";

import Header from "../../../components/Header";
import CustomButon from "../../../components/CustomButon";
import {
  PlusButtonIcon,
  CrossIcon,
  CheckIcon,
} from "../../../assets/icons/customIcon";
import CustomChip from "../../../components/CustomChip";
import TableComponent from "../../../components/TableComponent";
import Swal from "sweetalert2";
import master from "../../../helper/services/master";

import moment from "moment";
import { CSVLink } from "react-csv";

import { Axios } from "../../../helper/http";
import { VIEW_FILE_BASE64 } from "../../../helper/globalFuncion";

export default function InquiryGenerateCsvFakturPajak() {
  const [dataTable, setDataTable] = useState([]);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const [selected, setSelected] = useState([]);
  const [periode, setPeriode] = useState();
  const headers = [
    [
      { label: "Number BAR", key: "MPM_NO_BAR" },
      { label: "Invoice Number", key: "MPM_NO_INVOICE" },
      { label: "No Faktur Pajak", key: "MPM_NO_FAKTUR" },
      { label: "Nama Merchant", key: "M_NAME" },
      { label: "Periode", key: "PERIODE" },
      { label: "MDR", key: "MPM_MDR" },
      { label: "TRX", key: "MPM_COUNT_TRX" },
      { label: "Total Amount", key: "MPM_AMOUNT" },
      { label: "Total MDR", key: "MPM_MDR" },
      { label: "Tanggal Generated", key: "AT_CREATE" },
    ],
    [
      { label: "Number BAR", key: "MPM_NO_BAR" },
      { label: "Invoice Number", key: "MPM_NO_INVOICE" },
      { label: "No Faktur Pajak", key: "MPM_NO_FAKTUR" },
      { label: "Nama Merchant", key: "M_NAME" },
      { label: "Periode", key: "PERIODE" },
      { label: "MDR", key: "MPM_MDR" },
      { label: "TRX", key: "MPM_COUNT_TRX" },
      { label: "Total Amount", key: "MPM_AMOUNT" },
      { label: "Total MDR", key: "MPM_MDR" },
      { label: "Tanggal Generated", key: "AT_CREATE" },
    ],
  ];
  const [csvReport, setCsvReport] = useState({
    data: [],
    headers: [],
    filename: ``,
  });

  const isSelected = async (event, done) => {
    Axios.get("/faktur-pajak/transaksi/views").then((res) => {
      console.log(res);
    });
    // setCsvReport({
    //   data: dataTable,
    //   filename: `INQUIRY-GENERATE-CSV-FP-${periode}.csv`,
    // }).then(() => {
    //   done(true);
    // });
  };
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleViewFile = (MPDOC_NAME, id) => {
    handleClose();
    Axios.post(`/document-view/file/bar/${id}/`, {
      names: MPDOC_NAME,
    }).then(({ data }) => {
      if (data.success) {
        VIEW_FILE_BASE64(data.data);
      } else {
        Swal.fire({
          icon: data.success ? "success" : "error",
          title: data.success ? "Success" : "Error",
          text: data.message,
        });
      }
    });
  };

  const selectAll = (event) => {
    if (event.target.checked) {
      //event.target.checked == true
      dataTable.map((row) => {
        setSelected([...selected, row.MPM_ID]);
      });
    } else {
      //event.target.checked == false
      dataTable.map((row) => {
        let data = [...selected];
        setSelected(data.filter((val) => val !== row.MPM_ID));
      });
    }
  };

  const getTableData = async () => {
    try {
      const response = await master.getMasterTransaction();
      if (response.data.success) {
        setDataTable(response.data.datas);
      } else {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: response?.data?.message?.sqlMessage,
        });
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Error",
        text: error,
      });
    }
  };

  useEffect(() => {
    getTableData();
  }, []);

  const TableInquiryGenerateCsvFakturPajak = (props) => {
    const columns = [
      [
        {
          name: (
            <Checkbox
              onChange={selectAll}
              sx={{
                color: "white",
                "&.Mui-checked": {
                  color: "white",
                },
              }}
            />
          ),
          rowSpan: 2,
          renderCell: ({ MPM_ID, PERIODE }, i) => (
            <Checkbox
              checked={selected.includes(MPM_ID)}
              onClick={({ target }) => {
                if (target.checked) {
                  setSelected([...selected, MPM_ID]);
                  setPeriode(PERIODE);
                } else {
                  let data = [...selected];
                  setSelected(data.filter((val) => val !== MPM_ID));
                  setPeriode("0000-00");
                }
              }}
            />
          ),
          width: 10,
        },
        {
          name: "Number BAR",
          rowSpan: 2,
          renderCell: ({ MPM_NO_BAR }) => MPM_NO_BAR,
        },
        {
          name: "Invoice Number",
          rowSpan: 2,
          renderCell: ({ MPM_NO_INVOICE }) =>
            MPM_NO_INVOICE ? MPM_NO_INVOICE : "No Data",
        },
        {
          name: "No Faktur Pajak",
          rowSpan: 2,
          renderCell: ({ MPM_NO_FAKTUR }) =>
            MPM_NO_FAKTUR ? MPM_NO_FAKTUR : "No Data",
        },
        {
          name: "Nama Merchant",
          rowSpan: 2,
          renderCell: ({ M_NAME }) => M_NAME,
        },
        {
          name: "Transaksi Merchant",
          colSpan: 9,
          heading: true,
          renderCell: ({ transaksiMerchant }) => transaksiMerchant,
        },
      ],
      [
        {
          name: "Tahun",
          renderCell: ({ PERIODE }) =>
            PERIODE ? PERIODE.split("-")[0] : "No Data",
        },
        {
          name: "Bulan",
          renderCell: ({ PERIODE }) =>
            PERIODE ? PERIODE.split("-")[1] : "No Data",
        },
        {
          name: "MDR",
          renderCell: ({ MPM_MDR }) => MPM_MDR,
        },
        {
          name: "TRX",
          renderCell: ({ MPM_COUNT_TRX }) => MPM_COUNT_TRX,
        },
        {
          name: "Total Amount",
          renderCell: ({ MPM_AMOUNT }) => MPM_AMOUNT,
        },
        {
          name: "Total MDR",
          renderCell: ({ MPM_TOTAL }) => MPM_TOTAL,
        },
        {
          name: "Tanggal Generated",
          renderCell: ({ AT_CREATE }) =>
            AT_CREATE
              ? moment(Number(AT_CREATE)).format("YYYY-MM-DD")
              : "undefined",
        },
        {
          name: "Status",
          renderCell: ({ AT_FLAG }) =>
            AT_FLAG ? <CheckIcon /> : <CrossIcon />,
        },
        {
          name: "View File PDF",
          renderCell: ({ DOCUMENT, MPM_ID }) => (
            <Grid>
              <CustomChip
                MPM_ID={DOCUMENT}
                onClick={handleClick}
                label="View"
              />
              <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                  "aria-labelledby": "basic-button",
                }}
              >
                {DOCUMENT.map((val) => {
                  return (
                    <MenuItem
                      onClick={() => handleViewFile(val?.MPDOC_NAME, MPM_ID)}
                    >
                      {val?.MPDOC_NAME}
                    </MenuItem>
                  );
                })}
              </Menu>
            </Grid>
          ),
        },
      ],
    ];
    return <TableComponent columns={columns} rows={dataTable} />;
  };

  return (
    <Grid>
      <Header
        titles={[
          "Non Digipost",
          "Transaksi Tax",
          "Inquiry & Generate CSV Faktur Pajak",
        ]}
      />
      <Grid
        mt="36px"
        sx={{ "& a": { textDecoration: "none" } }}
        container
        gap="24px"
      >
        <CSVLink {...csvReport} asyncOnClick={true} onClick={isSelected}>
          <CustomButon.Primary
            sx={{ "& svg": { marginRight: "8px", color: "white" } }}
          >
            <PlusButtonIcon />
            Generate CSV
          </CustomButon.Primary>
        </CSVLink>
        <CustomButon.Primary sx={{ "& span": { marginLeft: "8px" } }}>
          <PlusButtonIcon />
          <span>Export To Excel</span>
        </CustomButon.Primary>
      </Grid>
      <Grid mt="38px">{TableInquiryGenerateCsvFakturPajak()}</Grid>
    </Grid>
  );
}
