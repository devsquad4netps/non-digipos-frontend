import React, { useState, useEffect } from "react";
import { Grid, Input, Paper, IconButton, Checkbox, FormControlLabel } from "@mui/material";
import { useForm } from "react-hook-form";
import Header from "../../../components/Header";
import TableComponent from "../../../components/TableComponent";
import CustomButon from "../../../components/CustomButon";
import CustomModal from "../../../components/CustomModal";
import CustomInput from "../../../components/InputComponent";
import CustomTheme from "../../../assets/styles/customTheme";
import CustomSelect from "../../../components/SelectComponent";
import { TrashIcon } from "../../../assets/icons/customIcon";
import moment from "moment";

import Swal from "sweetalert2";
import master from "../../../helper/services/master"

export default function RegisterTaxNumber() {
    let error = {}
    let id = ''
    const options = [{label: '2022', value:'2022'},{label: '2023', value:'2023'}]
    const [periode, setPeriode] = useState('');
    const [startNumber, setStartNumber] = useState(0);
    const handleStartNumber = (startNum) => {
      if(startNum < 0 ){
        Swal.fire({
          icon: "error",
          title: "Error",
          text: 'Nomor Pertama harus lebih besar dari 0',
        });
        handleSumQuota(0 , endNumber)
        setStartNumber(0)
      }
      let start = startNum
      !start ? setStartNumber(0) : setStartNumber(start)
      !endNumber ? setEndNumber(0) : setEndNumber(endNumber)
      setStartNumber(start)
      handleSumQuota(start , endNumber)
    }
    const [endNumber, setEndNumber] = useState(0);
    const handleEndNumber =(endNum) => {
      if(endNum < 0 ){
        Swal.fire({
          icon: "error",
          title: "Error",
          text: 'Nomor Terakhir harus lebih besar dari 0',
        });
        handleSumQuota(startNumber, 0)
        setEndNumber(0)
      }
      let end = endNum
      !end ? setEndNumber(0) : setEndNumber(end)
      !startNumber ? setStartNumber(0) : setStartNumber(startNumber)
      setEndNumber(end)
      handleSumQuota(startNumber, end)
    }
    const [sumQuota, setSumQuota] = useState(0);
    const handleSumQuota = (startNum, endNum) =>{
      let end = endNum 
      let start = startNum
      let quota
      if(start < 0 || end < 0){
        quota = 0
      }else{
        quota = endNum - startNum + 1
        if(quota < 0){
          quota = 0
        }
      }
      setSumQuota(quota)
    }
    const isError = error[id] ? true : false;
    const [checked, setChecked] = React.useState(false);
    const handleChange3 = (event) => {
      setChecked( event.target.checked );
    };

    const {
      register,
      handleSubmit,
      formState: { errors },
      watch,
    } = useForm({ mode: "onChange"})
    
    const doRegister = async (data) => {
      let payload = {
        QUOTE_ALLOCATION : data.QUOTE_ALLOCATION,
        START_NUMBER : ~~startNumber,
        END_NUMBER : ~~endNumber,
        REMARKS : data.REMARKS,
        PERIODE : periode,
        FP_BUFFER : checked
      }
      try {
        const response = await master.registerNoFakturPajak(payload);
        if (response.data.success) {
          getSummaryRequestTableData()
          getSummaryTotalTableData()
          Swal.fire("Tax Request Created!", response?.data?.message, "success")
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message,
          });
        }
      } catch (error) {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
      }
    }
    const [dataSummaryRequest, setDataSummaryRequest] = useState([])
    const [dataSummaryTotal, setDataSummaryTotal] = useState([])
    const [open1, setOpen1] = useState();
    const handleClose1 = () => setOpen1(false);
    const [checkedModal, setCheckedModal] = React.useState(false);
    const handleChangeModal = (event) => {
      setCheckedModal( event.target.checked );
    };
    const [selectedData, setSelectedData] = useState()
    
    
    const getSummaryRequestTableData = async () => {
      try {
        const response = await master.getFakturPajakSummaryRequest();
        if (response.data.success) {
          let arr = []
          for (const key in response.data.datas) {
            if (Object.hasOwnProperty.call(response.data.datas, key)) {
              const row = response.data.datas[key];
              arr.push(row)
            }
          }
          setDataSummaryRequest(arr)
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message?.sqlMessage,
          });
        }
      } catch (error) {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
      }
    }
    
    const getSummaryTotalTableData = async () => {
      try {
        // console.log(data)
        const response = await master.getFakturPajakSummaryTotal();
        if (response.data.success) {
          let arr = []
          if(response.data.datas[Object.keys(response.data.datas)[0]]){
            let tempObj = { PERIODE: Object.keys(response.data.datas)}
            let result = Object.assign(tempObj, response.data.datas[Object.keys(response.data.datas)[0]])
            arr.push(result)
          }
          setDataSummaryTotal(arr)
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message?.sqlMessage,
          });
        }
      } catch (error) {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
      }
    }
  
    const openModal = async (ID) =>{
      setOpen1(true)
      setSelectedData(ID)
    }

    useEffect(() => {
      getSummaryTotalTableData();
      getSummaryRequestTableData();
    }, []);
    
    const deleteProductGroup = async (val) => {
      setOpen1(false)
      Swal.fire({
        title: "Apakah anda yakin?",
        text: "Akan menghapus data ini?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Confirm",
      }).then(async (result) => {
        if (result.isConfirmed) {
          val = {
            ID : selectedData,
            COUNT_CANCEL: val.COUNT_CANCEL,
            FP_BUFFER : checkedModal
          }
          const response = await master.cancelFakturPajakSummaryResult(val);
          if (response.data.success) {
            getSummaryRequestTableData()
            getSummaryTotalTableData()
            Swal.fire("Tax Request Cancelled!", response?.data?.message, "success")
          } else{
            Swal.fire({
              icon: "error",
              title: "Error",
              text: response?.data?.message,
            });
          }
        }
      });
    }; 
    
    
    const TotalTable = () => {
      const columns = [
        [
          {
            name: "Periode",
            renderCell: ({ PERIODE }) => PERIODE,
          },
          {
            name: "No Terakhir",
            renderCell: ({ NO_TERAKHIR }) => ((NO_TERAKHIR || NO_TERAKHIR === 0) ? NO_TERAKHIR : "No Data"),
          },
          {
            name: "Qty Digunakan",
            renderCell: ({ QTY_USED }) => ((QTY_USED || QTY_USED === 0) ? QTY_USED : "No Data"),
          },
          {
            name: "Qty Buffer",
            renderCell: ({ QTY_BUFFER }) => ((QTY_BUFFER || QTY_BUFFER === 0) ? QTY_BUFFER : "No Data"),
          },
          {
            name: "Qty Belum Digunakan",
            renderCell: ({ QTY_NOT_USED }) => ((QTY_NOT_USED || QTY_NOT_USED === 0) ? QTY_NOT_USED : "No Data"),
          },
            {
            name: "Total No Tersedia",
            renderCell: ({ TOTAL }) => ((TOTAL || TOTAL === 0) ? TOTAL : "No Data"),
          },
        ],
      ];
    
      
      return <TableComponent columns={columns} rows={dataSummaryTotal} noPaging={true} />;
    }
      
    const RequestTable = () => {   
      const columns = [
        [
          {
            name: "No",
            renderCell: ({}, index) => index + 1,
          },
          {
            name: "Periode",
            renderCell: ({ PERIODE }) => PERIODE,
          },
          {
            name: "Buffer Req",
            renderCell: ({ QTY_BUFFER }) => ((QTY_BUFFER || QTY_BUFFER === 0) ? QTY_BUFFER : "No Data"),
          },
          {
            name: "Not Buffer Req",
            renderCell: ({ QTY_NOT_BUFFER }) => ((QTY_NOT_BUFFER || QTY_NOT_BUFFER === 0) ? QTY_NOT_BUFFER : "No Data"),
          },
          {
            name: "Jumlah Req",
            renderCell: ({ TOTAL }) => ((TOTAL || TOTAL === 0) ? TOTAL : "No Data"),
          },
          {
            name: "Tanggal Buat",
            renderCell: ({ TANGGAL }) =>
              TANGGAL  
            ? moment(Number(TANGGAL)).format("YYYY-MM-DD")
                : "undefined",
          },
          {
            name: "Action",
            renderCell: ({ ID }) => (
              <Grid>
                {/* <IconButton>
                  <EditIcon />
                </IconButton> */}
                <IconButton onClick={()=>{openModal(ID)}}>
                  <TrashIcon />
                </IconButton>
              </Grid>
            ),
          },
        ],
      ];
    
      return (
        <>
          <TableComponent columns={columns} rows={dataSummaryRequest} noPaging={true} />;
            
          {open1 && (
            <CustomModal open={open1} onClose={handleClose1} title="Form Master Product Group" >
                    
              <form onSubmit={handleSubmit(deleteProductGroup)} id="add-form">
                <Grid container rowSpacing="12px">
                  <CustomInput.InputLabel
                  item
                  label="Count Faktur Cancel" labelWidth="110px"
                  required
                  type="number"
                  register={{  ...register("COUNT_CANCEL" , { valueAsNumber: true }, { required: true }, { value: watch("MPG_MDR_AMOUNT")}, ) }}
                  errors={errors}
    
                  />
                  <Grid item md={12} sx={{ marginBottom:"20px"}}>
                    <Grid>
                      <FormControlLabel
                        label="Buffer"
                        control={
                        <Checkbox 
                          checked={checkedModal} 
                          onChange={handleChangeModal}
                          sx={{
                            '& .MuiSvgIcon-root': { fontSize: 32 },
                            color: CustomTheme.customTheme.color.primary,
                            "&.Mui-checked": { color: CustomTheme.customTheme.color.primary }, 
                          }}
                        />
                      }
                      />
                    </Grid>
                  </Grid>
                  <Grid item container direction="row" justifyContent="flex-end" gap="24px">
                    <CustomButon.Primary
                      item
                      size="small"
                      onClick={handleClose1}
                      sx={{ backgroundColor: "red" }}
                    >
                      <span>Cancel</span>
                    </CustomButon.Primary>
                    <CustomButon.Primary
                      id="add-form"
                      item
                      size="small"
                      type="submit"
                    >
                      <span>Save</span>
                    </CustomButon.Primary>
                  </Grid>
                </Grid>
              </form>
                
            </CustomModal>
          )}
        </>
      )
        
    
    }
      
    const  TableRegisterTaxNumber = (props) => {
      return (
        <>
          {
             TotalTable()
          }
          {
             RequestTable()
          }
        </>
      )
    }

  
    return (
      <Grid>
        <Header titles={['Non Digipost', 'Transaksi Tax','Register Tax Number']}/>
        <Grid mt="36px" sx={{ "& a": { textDecoration: "none" } }}>
              {/* <CustomButon.Primary
                onClick={(e) => setOpen(true)}
                sx={{ "& span": { marginLeft: "8px" } }}
              >
                <PlusButtonIcon />
                <span>Distribusi Bukti Potong</span>
              </CustomButon.Primary> */}
            </Grid>
        <Grid mt="38px" container columnSpacing={2}>
          <Grid item md={4}>
           <Paper elevation={2} 
            sx={{padding:"20px"}}
            children={
              <>
                <form onSubmit={handleSubmit(doRegister)} id="edit-form">
                  <Grid container
                  justifyContent="space-around"
                  alignItems="stretch"
                  >
                    <Grid item md={12} sx={{ marginBottom:"20px"}}>

                      <Grid 
                        sx={{
                          marginBottom: "7px"
                        }}
                      >
                        <label htmlFor="">
                          Kode Perusahaan
                        </label>
                      </Grid>
                      <Input
                        md={12}
                        id="1"
                        disableUnderline
                        size="small"
                        fullWidth
                        variant="contained"
                        inputProps={{
                          ...register("QUOTE_ALLOCATION" ),
                          sx: {
                            padding: 0,
                            paddingLeft: "10px",
                            paddingRight: "10px",
                            backgroundColor: "white",
                            borderRadius: "10px",
                            minHeight: "25px",
                            height: "25px",
                            fontSize: "12px",
                            border: isError ? "1px solid red" : "1px solid #000000",
                          },
                        }}
                      />
                    </Grid>
                    <Grid item md={12} sx={{
                      marginBottom: "20px"
                    }}>
                      <Grid container
                        spacing={2}
                        direction="row"
                        justifyContent="space-between"
                        alignItems="stretch"
                      >
                        <Grid item md={4}>
                          <Grid 
                            sx={{
                              marginBottom: "7px"
                            }}
                            >
                              <label htmlFor="">
                                Nomor Pertama
                              </label>
                          </Grid>
                          <Input
                            md={8}
                            type="number"
                            disableUnderline
                            size="small"
                            fullWidth
                            variant="contained"
                            value={startNumber}
                            onChange={(e)=>handleStartNumber(e.target.value)}
                            inputProps={{
                              min: 0,
                              sx: {
                                padding: 0,
                                paddingLeft: "10px",
                                paddingRight: "10px",
                                backgroundColor: "white",
                                borderRadius: "10px",
                                minHeight: "25px",
                                height: "25px",
                                fontSize: "12px",
                                border: isError ? "1px solid red" : "1px solid #000000",
                              },
                            }}/>
                        </Grid>
                        <Grid item md={4}>
                          <Grid 
                            sx={{
                              marginBottom: "7px"
                            }}
                            >
                              <label htmlFor="">
                                Nomor Terakhir
                              </label>
                          </Grid>
                          <Input
                            md={8}
                            type="number"
                            disableUnderline
                            size="small"
                            fullWidth
                            variant="contained"
                            value={endNumber}
                            onChange={(e)=>handleEndNumber(e.target.value)}
                            inputProps={{
                              min: 0,
                              sx: {
                                padding: 0,
                                paddingLeft: "10px",
                                paddingRight: "10px",
                                backgroundColor: "white",
                                borderRadius: "10px",
                                minHeight: "25px",
                                height: "25px",
                                fontSize: "12px",
                                border: isError ? "1px solid red" : "1px solid #000000",
                              },
                            }}/>
                        </Grid>
                        <Grid item md={4}>
                          <Grid 
                            sx={{
                              marginBottom: "7px"
                            }}
                            >
                              <label htmlFor="">
                                Jumlah Kuota
                              </label>
                          </Grid>
                          <Input
                            disabled 
                            type="number"
                            md={8}
                            disableUnderline
                            size="small"
                            fullWidth
                            variant="contained"
                            value={sumQuota}
                            inputProps={{
                              sx: {
                                padding: 0,
                                paddingLeft: "10px",
                                paddingRight: "10px",
                                backgroundColor: "white",
                                borderRadius: "10px",
                                minHeight: "25px",
                                height: "25px",
                                fontSize: "12px",
                                border: isError ? "1px solid red" : "1px solid #000000",
                              },
                            }}/>
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item md={12} sx={{ marginBottom:"20px"}}>
                      <Grid 
                          sx={{
                            marginBottom: "7px"
                          }}
                        >
                          <label htmlFor="">
                            Catatan
                          </label>
                        </Grid>
                        <Input
                        md={12}
                        id="1"
                        multiline={true}
                        minRows={3}
                        disableUnderline
                        size="large"
                        fullWidth
                        variant="contained"
                        inputProps={{
                          ...register("REMARKS" ),

                          sx: {
                            padding: 0,
                            paddingLeft: "10px",
                            paddingRight: "10px",
                            backgroundColor: "white",
                            borderRadius: "10px",
                            minHeight: "25px",
                            height: "25px",
                            fontSize: "12px",
                            border: isError ? "1px solid red" : "1px solid #000000",
                          },
                        }}
                      />
                    </Grid>
                    <Grid item md={12} sx={{ marginBottom:"20px"}}>
                      <Grid container
                        spacing={2}
                        direction="row"
                        justifyContent="space-between"
                        alignItems="stretch"
                        >
                        {/* <Grid item md={6}>
                          <Grid 
                          sx={{
                            marginBottom: "7px"
                          }}>
                            <label htmlFor="">
                              Periode
                            </label>
                          </Grid>
                          <Select
                            disableUnderline
                            variant="standard"
                            fullWidth
                            onChange={(e)=>setPeriode(e.target.value)}
                            inputProps={{
                              sx: {
                                padding: 0,
                                paddingLeft: "10px",
                                paddingRight: "10px",
                                backgroundColor: "white",
                                borderRadius: "10px",
                                minHeight: "25px",
                                height: "25px",
                                fontSize: "12px",
                                border: "1px solid #000000",
                              },
                          }}>
                            {options.map((option)=> {
                              return (<MenuItem key={option.value} value={option.value}>{option.label}</MenuItem>)
                            })}
                          </Select>
                        </Grid> */}
                          <CustomSelect.SelectLabel
                            item
                            label="Periode" labelWidth="110px"
                            required
                            value={(periode)}
                            options={options}
                            onChange={(e)=>setPeriode(e.target.value)}
                          />
                      </Grid>
                    </Grid>
                    <Grid item md={12} sx={{ marginBottom:"20px"}}>
                      <Grid>
                        <FormControlLabel
                          label="Buffer"
                          control={
                          <Checkbox 
                            checked={checked} 
                            onChange={handleChange3}
                            sx={{
                              '& .MuiSvgIcon-root': { fontSize: 32 },
                              color: CustomTheme.customTheme.color.primary,
                              "&.Mui-checked": { color: CustomTheme.customTheme.color.primary }, 
                            }}
                          />
                        }
                        />
                      </Grid>
                    </Grid>
                    <Grid item md={12} sx={{ marginBottom:"20px"}}>
                      <CustomButon.Primary
                        type="submit"
                        id="register-form"
                      >
                        <span>REGISTER</span>
                      </CustomButon.Primary>
                    </Grid>
                  </Grid>
                </form>
              </>
          
           }/>
          </Grid>
          <Grid item md={8}>
            <Grid sx={{marginBottom:"40px"}}>
             {
              TableRegisterTaxNumber()
             }
            </Grid>
          </Grid>
        </Grid>
      </Grid>
  );
}
