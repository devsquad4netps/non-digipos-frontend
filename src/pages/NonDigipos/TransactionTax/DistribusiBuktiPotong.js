import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { Grid, Modal, Button, IconButton } from "@mui/material";
import Header from "../../../components/Header";
import CustomButon from "../../../components/CustomButon";
import CustomModal from "../../../components/CustomModal";
import CustomButonFile from "../../../components/CustomButtonFile";
import { PlusButtonIcon, TrashIcon } from "../../../assets/icons/customIcon";
import TableDistribusiBuktiPotong from "../../../components/NonDigiposComponents/TransactionTaxComponents/TableDistribusiBuktiPotong";

export default function DistribusiFakturPajak() {
  return (
    <Grid>
      <Header
        titles={[
          "Non Digipost",
          "Transaksi Tax",
          "Upload & Distribusi Faktur Pajak",
        ]}
      />
      <Grid mt={3}>
        <TableDistribusiBuktiPotong /> 
      </Grid>
    </Grid>
  );
}
