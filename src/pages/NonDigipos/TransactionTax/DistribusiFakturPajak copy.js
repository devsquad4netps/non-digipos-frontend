import React, { useState, useEffect } from "react";
import {
  Modal,
  Button,
  IconButton,
  Checkbox,
  Grid,
  RadioGroup,
  FormControlLabel,
  Radio,
  Menu,
  MenuItem } from "@mui/material";
import { Axios } from "../../../helper/http";
import { useForm } from "react-hook-form";
import Header from "../../../components/Header";
import TableDistribusiFakturPajak from "../../../components/NonDigiposComponents/TransactionTaxComponents/TableDistribusiFakturPajak";
import CustomButon from "../../../components/CustomButon";
import CustomInput from "../../../components/InputComponent";
import CustomButonFile from "../../../components/CustomButtonFile";
import CustomInputText from "../../../components/InputComponentText";
import CustomSelect from "../../../components/SelectComponent";
import CustomModal from "../../../components/CustomModal";
import { PlusButtonIcon, TrashIcon } from "../../../assets/icons/customIcon";
import { Link } from "react-router-dom";
import master from "../../../helper/services/master";
import Swal from "sweetalert2";

export default function DistribusiFakturPajak() {
  const handleClose = () => setState({ ...state, loading: false });

  const [state, setState] = useState({
    open: false,
    open1: false,
    loading: false,
    refresh: false,
    files: null,
    selected: null
  });

  const {open, open1, selectedId, selected, loading, refresh, files, attentionChecked, validateChecked} = state;

  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
    control,
    setValue,
  } = useForm();

  const onSelectedId = (e, item) => {
    setState({...state, selectedId: e.target.checked ? item : null})
  }

  const setLoading = () => setState({ ...state, loading: true });
  const setSelected = () => setState({ ...state, selected: true });
  const setOpen = () => setState({ ...state, open: !open });
  const setOpen1 = () => setState({ ...state, open1: !open1 });
  
  const onFileChange = (e) => {
    const _file = e.target.files;
    setState({...state, files: _file});
  }

const onRemoveImage = () => {
    setState({...state, files: null});
}

const onSubmit = async (data) => {
  const formData = new FormData();

  formData.append("file")
  setLoading();
  console.log(selectedId);
  try {
    const request = selectedId ? await master.masterAttentionUpdate(selectedId, formData): await master.masterAttentionCreate(formData);
    console.log(request);
    if (request.data.success) {
      setState({ ...state, loading: false, open: false, selectedId: null});
      Swal.fire({
        icon: "success",
        title: "Success",
        text: "Create data success",
      });
    } else {
      setState({...state, loading: false, open: false, selectedId: null});
      Swal.fire({
        icon: "error",
        title: "Error",
        text: "Error on submit"
      });
    }
  } catch ({ response: { data } }) {
    console.log(data);
    setState({ ...state, loading: false, open: false, selectedId: null });
    Swal.fire({
      icon: "error",
      title: "Error",
      text: data.error.message,
    });
  }
};

const [datas, setDatas] = useState({
  AVALUABLE_ID: {},
});
const [dataDistribute, setDataDistribute] = useState({
  DISTRIBUTE_MERCHANT: 0,
  FEEDBACK_MERCHANT: 0,
});
const [body, setBody] = useState({
  MPM_ID: selected,
  DOC_DISTRIBUTE: [0, 0, 0, 0, 0, 0, 0],
  NOTE: " ",
  SEND_CC: "",
});

const checkDistribute = () => {
  Axios.post("/merchant-transaksi-master-action/distribusi/checking", {
    ...dataDistribute,
    MPM_ID: selected,
  }).then((res) => {
    setDatas(res.data.datas);
    const { AVALIABLE_DOC } = res.data.datas;
    const clone = [0, 0, 0, 0, 0, 0, 0];
    Object.keys(AVALIABLE_DOC).map((val) => {
      return (
        AVALIABLE_DOC[val] === 1 &&
        (val === "BAR"
          ? (clone[0] = 1)
          : val === "BAR_SIGN"
          ? (clone[1] = 1)
          : val === "INVOICE"
          ? (clone[2] = 1)
          : val === "INVOICE_SIGN"
          ? (clone[3] = 1)
          : val === "FAKTUR_PAJAK"
          ? (clone[4] = 1)
          : val === "BUKPOT"
          ? (clone[5] = 1)
          : val === "OTHERS"
          ? (clone[6] = 1)
          : "")
      );
    });
    setBody({ ...body, DOC_DISTRIBUTE: clone });
    // datas["BAR"] === 1 ? setBody({...body, DOC_DISTRIBUTE :})
  });
};

useEffect(() => {
  checkDistribute();
}, [dataDistribute]);

const handleCheck = (index) => {
  let clone = [...body?.DOC_DISTRIBUTE];
  clone[index] === 1 ? (clone[index] = 0) : (clone[index] = 1);
  setBody({ ...body, DOC_DISTRIBUTE: clone });
};

var isDisable = true;
Object.keys(datas?.AVALUABLE_ID).map((val, i) => {
  if (datas?.AVALUABLE_ID[val].length > 0) {
    if (
      body?.DOC_DISTRIBUTE[i] === 1 &&
      dataDistribute.DISTRIBUTE_MERCHANT === 1
    ) {
      isDisable = false;
    }
  }
});

   
    return (
      <Grid>
        <Header titles={['Non Digipost', 'Transaksi Tax','Upload & Distribusi Faktur Pajak']}/>
        <Grid mt="36px" sx={{ "& a": { textDecoration: "none" } }} container gap="24px">
              <CustomButon.Primary
                onClick={(e) => setOpen1()}
                sx={{ "& span": { marginLeft: "8px" } }}
              >
                <span>Distribusi</span>
              </CustomButon.Primary>
              <CustomButon.Primary
                onClick={(e) => setOpen(true)}
                sx={{ "& span": { marginLeft: "8px" } }}
                disabled={!selectedId}
              >
                <span>Upload Bulk</span>
              </CustomButon.Primary>
              <CustomButon.Primary
                // onClick={(e) => setOpen(true)}
                // sx={{ "& span": { marginLeft: "8px" } }}
                // disabled={!selectedId}
              >
                <span>Export Excel</span>
              </CustomButon.Primary>
            </Grid>
        <Grid mt="38px">
          <TableDistribusiFakturPajak onSelect={onSelectedId}/>
        </Grid>
        {open && (
          <CustomModal open={open} onClose={handleClose} title="">
            <Grid container item xs={12} rowSpacing="12px">
            <div style={{display: 'flex', flexDirection: "row", justifyContent: "flex-start", alignItems: "center", marginBottom: 10}}>
                  {/* <div style={{marginRight: 10}}>
                    <span>Preview Doc: </span>
                  </div> */}
                  { files 
                    ? 
                    <div style={{position: "relative"}}>
                      <>{files[0].name}</> 
                      <IconButton onClick={onRemoveImage}>
                            <TrashIcon />
                        </IconButton>
                    </div>
                    :
                    // <div style={{height: 100, width: 100, border: "1px solid #ccc", borderRadius: 12, display: 'flex', justifyContent: "center", alignItems: "center"}}>
                    //   <p style={{color: "#ccc", fontSize:10}}>Select Image</p>
                    // </div>
                    <></>
                  }
              </div>
              <div>
                <input id="file-image" style={{display: 'none'}} type="file" accept="application/pdf" onChange={onFileChange} multiple={true} />
                {!files && <CustomButonFile.Primary label="Upload Sign :" onClick={() => document.getElementById("file-image").click()}>Upload Doc</CustomButonFile.Primary>}
              </div>
  
              <Grid item justifyContent="flex-end" container gap="24px">
                <CustomButon.Primary
                  size="small"
                  onClick={setOpen}
                  sx={{ "& span": { marginLeft: "8px" }, backgroundColor: "red" }}
                >
                  Cancel{" "}
                </CustomButon.Primary>
                <CustomButon.Primary
                  size="small"
                  onClick={(e) => setOpen(true)}
                  sx={{ "& span": { marginLeft: "8px" } }}
                >
                  Save{" "}
                </CustomButon.Primary>
              </Grid>
            </Grid>
          </CustomModal>
        )}
        {open1 && (
          <CustomModal open={open1} onClose={handleClose} title="" width={800}>
            <h3 className="title">Form Distribusi Document</h3>
            <Grid container alignItems="center" className="items" spacing="24px">
            <Grid item md={5}>
              <label for="">
                Distribute to Merchant <span>:</span>{" "}
              </label>
            </Grid>
            <Grid item md={7}>
              <RadioGroup
                row
                aria-labelledby="demo-row-radio-buttons-group-label"
                name="row-radio-buttons-group"
                // onChange={(e, v) => {
                //   setBody({ ...body, DOC_DISTRIBUTE: [0, 0, 0, 0, 0, 0, 0] });
                //   setDataDistribute({
                //     ...dataDistribute,
                //     DISTRIBUTE_MERCHANT: Number(v),
                //   });
                // }}
                // value={dataDistribute?.DISTRIBUTE_MERCHANT}
              >
                <FormControlLabel value={1} control={<Radio />} label="Yes" />
                <FormControlLabel value={0} control={<Radio />} label="No" />
              </RadioGroup>
            </Grid>
            </Grid>
            <Grid container alignItems="center" className="items" spacing="24px">
                <Grid item md={5}>
                  <label for="">
                    Feedback Merchant <span>:</span>{" "}
                  </label>
                </Grid>
                <Grid item md={7}>
                  <RadioGroup
                    row
                    aria-labelledby="demo-row-radio-buttons-group-label"
                    name="row-radio-buttons-group"
                    // onChange={(e, v) => {
                    //   setBody({ ...body, DOC_DISTRIBUTE: [0, 0, 0, 0, 0, 0, 0] });
                    //   setDataDistribute({
                    //     ...dataDistribute,
                    //     FEEDBACK_MERCHANT: Number(v),
                    //   });
                    // }}
                    // value={dataDistribute?.FEEDBACK_MERCHANT}
                  >
                    <FormControlLabel value={1} control={<Radio />} label="Yes" />
                    <FormControlLabel value={0} control={<Radio />} label="No" />
                  </RadioGroup>
                </Grid>
              </Grid>
              <Grid container className="items" spacing="24px">
                <Grid item md={5}>
                  <label for="">
                    Document Distribusi <span>:</span>{" "}
                  </label>
                </Grid>
              <Grid item md={7} container>
                <Grid item md={7}>
                  <FormControlLabel
                    sx={{ width: "fit-content" }}
                    label={`Berita Acara Recon `}
                    control={
                      <Checkbox
                        // checked={body.DOC_DISTRIBUTE[0] === 1}
                        // onClick={() => handleCheck(0)}
                        // disabled={datas?.AVALIABLE_DOC?.BAR === 0}
                      />
                    }
                  />
               </Grid>
               <Grid item md={5}>
                <FormControlLabel
                  sx={{ width: "fit-content" }}
                  label={`Faktur Pajak `}
                  control={
                    <Checkbox
                      // checked={body.DOC_DISTRIBUTE[4] === 1}
                      // onClick={() => handleCheck(4)}
                      // disabled={datas?.AVALIABLE_DOC?.FAKTUR_PAJAK === 0}
                    />
                  }
                />
             </Grid>
             <Grid item md={7}>
              <FormControlLabel
                sx={{ width: "fit-content" }}
                label={`Berita Acara Sign `}
                control={
                  <Checkbox
                    // checked={body.DOC_DISTRIBUTE[1] === 1}
                    // onClick={() => handleCheck(1)}
                    // disabled={datas?.AVALIABLE_DOC?.BAR_SIGN === 0}
                  />
                }
              />
            </Grid>
            <Grid item md={5}>
            <FormControlLabel
              sx={{ width: "fit-content" }}
              label={`Bukti Potong (${datas?.AVALIABLE_DOC?.BUKPOT})`}
              control={
                <Checkbox
                  checked={body.DOC_DISTRIBUTE[5] === 1}
                  onClick={() => handleCheck(5)}
                  disabled={datas?.AVALIABLE_DOC?.BUKPOT === 0}
                />
              }
            />
          </Grid>
          <Grid item md={7}>
            <FormControlLabel
              sx={{ width: "fit-content" }}
              label={`Invoice `}
              control={
                <Checkbox
                  // checked={body.DOC_DISTRIBUTE[2] === 1}
                  // onClick={() => handleCheck(2)}
                  // disabled={datas?.AVALIABLE_DOC?.INVOICE === 0}
                />
              }
            />
          </Grid>
          <Grid item md={5}>
            <FormControlLabel
              sx={{ width: "fit-content" }}
              label={`Others `}
              control={
                <Checkbox
                  // checked={body.DOC_DISTRIBUTE[6] === 1}
                  // onClick={() => handleCheck(6)}
                  // disabled={datas?.AVALIABLE_DOC?.OTHERS === 0}
                />
              }
            />
          </Grid>
          <Grid item md={7}>
            <FormControlLabel
              sx={{ width: "fit-content" }}
              label={`Invoice Sign `}
              control={
                <Checkbox
                  // checked={body.DOC_DISTRIBUTE[3] === 1}
                  // onClick={() => handleCheck(3)}
                  // disabled={datas?.AVALIABLE_DOC?.INVOICE_SIGN === 0}
                />
              }
            />
          </Grid>
        </Grid>    
      </Grid>
      <form onSubmit={handleSubmit(onSubmit)} id="submit-form">
        <Grid
          container
          className="items"
          spacing="24px"
          sx={{
            "& label": {
              color: errors["SEND_CC"] && "red",
            },
            "& .required": {
              flex: 1,
              color: "red",
              marginLeft: "4px",
            },
          }}
        >
          <Grid item md={5}>
            <label for="">
              Send CC <span className="required">*</span> <span>:</span>{" "}
            </label>
          </Grid>
          <Grid item md={7} container>
            <CustomInput
              id="SEND_CC"
              errors={errors}
              register={{
                ...register("SEND_CC", {
                  required: true,
                }),
              }}
            />
          </Grid>
        </Grid>
        <Grid
          container
          className="items"
          spacing="24px"
          mt="-16px"
          sx={{
            "& label": {
              color: errors["SEND_CC"] && "red",
            },
            "& .required": {
              flex: 1,
              color: "red",
              marginLeft: "4px",
            },
          }}
        >
          <Grid item md={5}>
            <label for="">
              Note <span className="required">*</span> <span>:</span>{" "}
            </label>
          </Grid>
          <Grid item md={7} container>
            <CustomInput
              errors={errors}
              id="NOTE"
              register={{ ...register("NOTE", { required: true }) }}
              multiline
              minRows={3}
            />
          </Grid>
        </Grid>
        <Grid container className="items" spacing="24px" mt="-16px">
          <Grid item md={5}>
            <label for="">
              Transaksi Selected Distribusi <span>:</span>{" "}
            </label>
          </Grid>
          <Grid item md={7} container>
            <strong>
              {/* {selected.length} */}
              </strong>
          </Grid>
        </Grid>
        <Grid container className="items" spacing="24px" mt="-16px">
          <Grid item md={5}>
            <label for="">
              Transaksi Available Distribusi <span>:</span>{" "}
            </label>
          </Grid>
          <Grid item md={7} container>
            <strong>
              {/* {Object.keys(datas?.AVALUABLE_ID).length > 0 &&
                Object.values(datas?.AVALUABLE_ID).reduce((a, b) =>
                  a.length > b.length ? a : b
                ).length}{" "}
              (Show) */}
            </strong>
          </Grid>
        </Grid>
        <Grid container className="items" mt="10px" justifyContent="flex-end">
          <CustomButon.Primary
                  size="small"
                  onClick={setOpen1}
                  sx={{ "& span": { marginLeft: "8px" }, backgroundColor: "red" }}
                >
                  Cancel{" "}
          </CustomButon.Primary>
          <CustomButon.Primary
            sx={{ marginLeft: "32px" }}
            type="submit"
            id="submit-form"
            // disabled={isDisable}
          >
            Submit
          </CustomButon.Primary>
        </Grid>
      </form>
          </CustomModal>
        )}
      </Grid>
  );
}
