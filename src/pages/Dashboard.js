import React from "react";
import { Grid } from "@mui/material";
import Header from "../components/Header";
import CustomButon from "../components/CustomButon";
import InqueryReviewSummaryTable from "../components/NonDigiposComponents/TransationBAComponents/InqueryReviewSummaryTable";

export default function Dashboard() {
  return (
    <Grid>
      <Header titles={['Dashboard']}/>
      <Grid mt={3}>
        <InqueryReviewSummaryTable />
      </Grid>
      {/* <CustomButon.Primary></CustomButon.Primary> */}
    </Grid>
  );
}
