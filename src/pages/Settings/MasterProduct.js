import React, { useState, useEffect } from "react";
import { Grid, Modal, Button } from "@mui/material";
import { useForm } from "react-hook-form";
import MenuItem from "@mui/material/MenuItem";
import Select from '@mui/material/Select';
import Header from "../../components/Header";
import TableMasterProduct from "../../components/SettingsComponents/TableMasterProduct";
import CustomButon from "../../components/CustomButon";
import CustomInput from "../../components/InputComponent";
import CustomInputText from "../../components/InputComponentText";
import CustomSelect from "../../components/SelectComponent";
import CustomSelectInv from "../../components/SelectComponentInv";
import CustomSelectBrand from "../../components/SelectComponentBrand";
import CustomSelectTribe from "../../components/SelectComponentTribe";
import CustomSelectMr from "../../components/SelectComponentMr";
import CustomSelectSkema from "../../components/SelectComponentSkema";
import CustomModal from "../../components/CustomModal";
import { PlusButtonIcon } from "../../assets/icons/customIcon";
import { Link } from "react-router-dom";
import master from "../../helper/services/master";
import Swal from "sweetalert2";

 
export default function MasterProduct() {
  // const [open, setOpen] = useState();
  // const [loading, setLoading] = useState(true);
  // const [dataTribe, setDataTribe] = useState([]);
  // const [refresh, setRefresh] = useState(false);

  const handleClose = () => setState({...state, loading: false, open: false});
  const [MP_TYPE_INVOICE, setMP_TYPE_INVOICE] = useState();
  const [M_ID, setM_ID] = useState();
  const [MPT_ID, setMPT_ID] = useState();
  const [MPS_ID, setMPS_ID] = useState();
  const [dataTribe, setDataTribe] = useState([]);
  const [dataMerchant, setDataMerchant] = useState([]);
  const [dataSchema, setDataSchema] = useState([]);
  

  const [state, setState] = useState({
    open: false,
    loading: false,
    selectedId: null,
    refresh: false,
  });

  const {open, selectedId, loading, refresh, } = state;

  const {
    register,
    resetField,
    handleSubmit,
    formState: { errors },
    watch,
    control,
    setValue,
  } = useForm();

  const setLoading = () => setState({...state, loading: true});
  const setOpen = () => setState({...state, open: !open});

  const [dataProduct, setDataProduct] = useState([]);

  const getDataProduct = async () => {
      setLoading(true);
      try {
        const response = await master.merchantProductList();
        if (response.data.success) {
          setDataProduct(response.data.datas);
          
        }
      } catch (error) {
        console.log(error);
      }
    };

  useEffect(() => {
    // const reqTribe = master.merchantProductTribeList().then(res => res.data.datas);
    // const reqMerchant = master.merchantList().then(res => res.data.datas);
    // const reqSchema = master.merchantProductSkemaList().then(res => res.data.datas);
    // const reqData = master.merchantProductList().then(res=> res.data.datas)
      getDataTribe();
      getDataMerchant();
      getDataSchema();
      getDataProduct();
  }, []);

  const getDataTribe = async () => {
    try {
      const request = await master.merchantProductTribeList()
      if (request.data.success) {
        // console.log(request.data.datas)
        setDataTribe(request.data.datas)
      }
    } catch ({response: {data}}) {
      setState({...state, loading: false, open: false, selectedId: null});
      Swal.fire({
        icon: "error",
        title: "Error",
        text: data.error.message,
      });
    }
  }

  const getDataMerchant = async () => {
    try {
      const request = await master.merchantList()
      if (request.data.success) {
        // console.log(request.data.datas)
        setDataMerchant(request.data.datas)
      }
    } catch ({response: {data}}) {
      setState({...state, loading: false, open: false, selectedId: null});
      Swal.fire({
        icon: "error",
        title: "Error",
        text: data.error.message,
      });
    }
  }

  const getDataSchema = async () => {
    try {
      const request = await master.merchantProductSkemaList()
      if (request.data.success) {
        // console.log(request.data.datas)
        setDataSchema(request.data.datas)
      }
    } catch ({response: {data}}) {
      setState({...state, loading: false, open: false, selectedId: null});
      Swal.fire({
        icon: "error",
        title: "Error",
        text: data.error.message,
      });
    }
  }
    

  const onSubmit = async (data) => {
    data = {
      MP_LABEL: data.MP_BRAND,
      MP_BRAND: data.MP_BRAND,
      MP_DESCRIPTION : data.MP_DESCRIPTION,
      MP_TYPE_INVOICE: MP_TYPE_INVOICE,
      MP_TYPE_SKEMA: MPS_ID,
      M_ID: M_ID,
      MPT_ID : MPT_ID,
      AT_FLAG: 1
    }

    setLoading();
    try {
      const request = selectedId ? await master.merchantProductUpdate(selectedId, data) : await master.merchantProductCreate(data);
      if (request.data.success) {
        const response = await master.merchantProductList();
        if(response.data.success){
          Swal.fire({
            icon: "success",
            title: "Success",
            text: selectedId ? "Update data success" :  "Create data success",
          });
          resetForm()
          setState({
            open: false,
            loading: false,
            selectedId: null,
            refresh: false,
          });
        }
      } else {
        setState({
          open: false,
          loading: false,
          selectedId: null,
          refresh: false,
        });
        Swal.fire({
          icon: "error",
          title: "Error",
          text: selectedId ? "Update data success" :  "Create data success"
        });
      }
    } catch ({response: {data}}) {
      setState({
        open: false,
        loading: false,
        selectedId: null,
        refresh: false,
      });
      Swal.fire({
        icon: "error",
        title: "Error",
        text: data.error.message,
      });
    }
  };

  const onUpdate = (id) => {
    setState({...state, selectedId: id});
    setTimeout(() => {
      getEditProduct(id);
    }, 150);
  }

  const getEditProduct = (id) => {
      setState({...state, open: true, selectedId: id});
      master.merchantProductGet(id).then(res => {
        setValue("MP_BRAND", res.data.datas.MP_BRAND, true)
        setValue("MP_DESCRIPTION", res.data.datas.MP_DESCRIPTION, true)
        setValue("MPT_ID", res.data.datas.MPT_ID, true)
        setValue("M_ID", res.data.datas.M_ID, true)
        setValue("MP_TYPE_SKEMA", res.data.datas.MP_TYPE_SKEMA, true)
        setMP_TYPE_INVOICE(res.data.datas.MP_TYPE_INVOICE)
        setMPT_ID(res.data.datas.MPT_ID)
        setM_ID(res.data.datas.M_ID)
        setMPS_ID(res.data.datas.MP_TYPE_SKEMA)
        
      }).catch(({message}) => {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: message,
        });
      });
  }

  const resetForm = () => {
    resetField("MP_BRAND")
    resetField("MP_DESCRIPTION")
    resetField("MPT_ID")
    resetField("M_ID")
    resetField("MP_TYPE_SKEMA")
    setMP_TYPE_INVOICE()
    setMPT_ID()
    setM_ID()
    setMPS_ID()
  }

  const deleteProduct = async (val) => {
    Swal.fire({
      title: "Apakah anda yakin?",
      text: "Akan menghapus data ini?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Confirm",
    }).then(async (result) => {
      if (result.isConfirmed) {
        const response = await master.merchantProductDelete(val);
        if (response.status === 200) {
          Swal.fire("Deleted!", "Your data has been deleted.", "success")
          getDataProduct()
          handleClose()
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message,
          });
          handleClose()
        }
      }
    });
  };


  return (
    <Grid>
      <Header titles={['Settings', 'Master Product']}/>
      <Grid mt="36px" sx={{ "& a": { textDecoration: "none" } }}>
            <CustomButon.Primary
              onClick={(e) => {
                resetForm()
                setOpen(true)
              }}
              sx={{ "& span": { marginLeft: "8px" } }}
            >
              <PlusButtonIcon />
              <span>Add Product</span>
            </CustomButon.Primary>
          </Grid>
      <Grid mt="38px">
        <TableMasterProduct data={dataProduct} editFunct={(data) => {onUpdate(data)}} deleteFunct={(data)=> {deleteProduct(data)}}/>
      </Grid>
      {open && (
        <CustomModal open={open} onClose={handleClose} title="Form Add Product">
          <form onSubmit={handleSubmit(onSubmit)} id="submit-product">
          <Grid container item xs={12} rowSpacing="12px">
            {/* <CustomSelectBrand.SelectLabel values={watch("MP_CODE")}
                    labelWidth="110px"
                    errors={errors}
                    id="MP_CODE"
                    label="Code"
                    register={{
                      ...register("MP_CODE", { required: true }),
                    }} /> */}
            <CustomInput.InputLabel label="Brand"
                  labelWidth="110px"
                  register={{ ...register("MP_BRAND", { required: true }) }}
                  id="MP_BRAND"
                  errors={errors}
                  required />
            <CustomInputText.InputLabel label="Description :" labelWidth="110px"
                  register={{ ...register("MP_DESCRIPTION", { required: true }) }}
                  id="MP_DESCRIPTION"
                  errors={errors}
                  required />
            {/* <CustomSelectInv.SelectLabel values={watch("MP_TYPE_INVOICE")}
                    labelWidth="110px"
                    errors={errors}
                    id="MP_TYPE_INVOICE"
                    label="Invoice Type"
                    value={(watch(MP_TYPE_INVOICE))}
                    register={{
                      ...register("MP_TYPE_INVOICE", { required: true }),
                    }} /> */}
            <CustomSelect.SelectLabel
                    item
                    label="Type MDR" labelWidth="110px"
                    required
                    value={(MP_TYPE_INVOICE)}
                    options={[{label: 'PAID INVOICE', value:1},{label: 'REGULER INVOICE', value:2},{label: 'REIMBURSEMENT', value:3}]}
                    onChange={(e)=>setMP_TYPE_INVOICE(e.target.value)}
                    />
            <CustomSelectTribe.SelectLabel
                  item
                  label="Tribe" labelWidth="110px"
                  required
                  value={MPT_ID}
                  options={dataTribe}
                  onChange={(e)=>setMPT_ID(e.target.value)}
                  />
            <CustomSelectMr.SelectLabel
                  item
                  label="Merchant" labelWidth="110px"
                  required
                  value={M_ID}
                  options={dataMerchant}
                  onChange={(e)=>setM_ID(e.target.value)}
                  />
            <CustomSelectSkema.SelectLabel
                  item
                  label="Skema" labelWidth="110px"
                  required
                  value={MPS_ID}
                  options={dataSchema}
                  onChange={(e)=>setMPS_ID(e.target.value)}
                  />

            <Grid item justifyContent="flex-end" container gap="24px">
              <CustomButon.Primary
                size="small"
                onClick={setOpen}
                sx={{ "& span": { marginLeft: "8px" }, backgroundColor: "red" }}
              >
                Cancel{" "}
              </CustomButon.Primary>
              <CustomButon.Primary
                id="submit-product"
                type="submit"
                size="small"
                sx={{ "& span": { marginLeft: "8px" } }}
              >
                Save
              </CustomButon.Primary>
            </Grid>
          </Grid>
          </form>
        </CustomModal>
      )}
    </Grid>
  );
}
