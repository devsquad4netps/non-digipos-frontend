import React, { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { Grid, Input, Paper, MenuItem, Select, Radio, RadioGroup, FormControl, FormLabel, FormControlLabel } from "@mui/material";
import { TreeView, TreeItem } from "@mui/lab";
import { Axios } from "../../helper/http";


import Header from "../../components/Header";
import CustomModal from "../../components/CustomModal";
import CustomButon from "../../components/CustomButon";
import CustomInput from "../../components/InputComponent";
import CustomTheme from "../../assets/styles/customTheme";
import CustomSelect from "../../components/SelectComponent";
import { 
  PlusButtonIcon,
  ChevronRight,
  EditIcon,
  TrashIcon, 
} from "../../assets/icons/customIcon";
import TableMasterProductGroup from "../../components/SettingsComponents/TableMasterProductGroup";
import TableMasterField from "../../components/SettingsComponents/TableMasterField";

import master from "../../helper/services/master"
import Swal from "sweetalert2";
import EmptyTable from "../../assets/images/empty-table.png";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme) => {
  const { customTheme } = theme;
  return {
    root: {
      "& .header": {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        "& h3": {
          display: "flex",
          width: "225px",
          margin: 0,
        },
        "& div": {
          width: "100%",
          height: "2px",
          backgroundColor: "#D9D9D9",
        },
      },
      "& .status": {
        marginTop: "32px",
        "& .status-field": {
          padding: "4px",
          display: "flex",
          alignItems: "center",
          gap: "16px",
          "& h3": {
            margin: 0,
            wdth: "100%",
            display: "inline-block",
          },
          "& .avatar": {
            minWidth: "36px",
            minHeight: "36px",
            borderRadius: "50%",
            border: "2px solid black",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            fontWeight: "800",
            fontSize: "16px",
          },
          "& .line": {
            border: "1px solid",
            width: "auto",
            flex: 1,
          },
        },
      },
    },
    treeRoot: {},
  };
});

export default function MasterProductGroup() {

    const classes = useStyles();
    const [active, setActive] = useState(0);
    const data = [{ID:1, LABEL:"Config Field"}, {ID:2, LABEL:"Config Query"}, {ID:3, LABEL:"Config Product Group"}
    // , {ID:4, LABEL:"Config Reminder"}
  ]


    const handleNext = () => {
      if(active === 2){
        updateMerchantQuery()
      }else  if(active === 3 && !MPG.MPG_ID){
        Swal.fire({
          icon: "warning",
          title: "tidak ada Product Group yang dipilih",
          text: "mohon pilih Product Group terlebih dahulu!",
        });
      }else{
        setActive(active + 1);
      }
    };

    const handleBack = () => {
      if (active > 0) {
        setActive(active - 1);
      }
    };

    const [open, setOpen] = useState();
    const handleClose = () => setOpen(false);
    const [openCondition, setOpenCondition] = useState();
    const handleCloseCondition = () => setOpenCondition(false);
    const [open1, setOpen1] = useState();
    const handleClose1 = () => setOpen1(false);
    const [open2, setOpen2] = useState();
    const handleClose2 = () => setOpen2(false);
    const [checked, setChecked] = React.useState([true, false]);
    const handleChange3 = (event) => {
      setChecked([checked[0], event.target.checked]);
    };
    const [isEdit, setIsEdit] = useState(false)
    const [isEditConfigField, setIsEditConfigField] = useState(false)
    const [isEditCondition, setIsEditCondition] = useState(false)


    const doEditCondition = (id) => {
      setOpenCondition(true)
      resetField('MPG_LABEL')
      resetField('MPG_MDR_AMOUNT')
      resetField('MPG_DPP')
      resetField('MPG_PPN')
      resetField('MPG_PPH')
      getEditMPC(id)
    }

    const doEdit = (id) => {
      setOpen1(true)
      resetField('MPG_LABEL')
      resetField('MPG_MDR_AMOUNT')
      resetField('MPG_DPP')
      resetField('MPG_PPN')
      resetField('MPG_PPH')
      getEditMPG(id)
    }

    const doMPVEdit = (id) => {
      getDdlMPV_TYPE()
      getDdlMPV_TYPE_KEY()
      setOpen2(true)
      resetField('MPV_KEY')
      resetField('MPV_LABEL')
      getEditMPV(id)
    }

    const [loading, setLoading] = useState(true);
    const [dataProduct, setDataProduct] = useState([]);
    const [dataProductGroup, setDataProductGroup] = useState([]);
    const [dataMasterField, setMasterField] = useState([]);
    const [refresh, setRefresh] = useState(false);
    const [MPG_ID, setMPG_ID] = useState();
    const [M_IDQuery, setM_IDQuery] = useState();
    const [MP_IDQuery, setMP_IDQuery] = useState();
    const [MPG, setMPG] = useState();
    const [MP, setMP] = useState();
    const [invoiceTypeList, setInvoiceTypeList] = useState([{label: 'PAID INVOICE', value:1},{label: 'REGULER INVOICE', value:2},{label: 'REIMBURSEMENT', value:3}])
    const [invoiceType, setInvoiceType] = useState('');
    const [skema, setSkema] = useState('');
    const [tribe, setTribe] = useState('');
    const [MPG_MDR_TYPE, setMPG_MDR_TYPE] = useState();
    const [MPV_ID, setMPV_ID] = useState();
    const [MPV_TYPE, setMPV_TYPE] = useState();
    const [MPV_TYPE_KEY, setMPV_TYPE_KEY] = useState();
    const [ddlMPV_TYPE, setDdlMPV_TYPE] = useState()
    const [ddlMPV_TYPE_KEY, setDdlMPV_TYPE_KEY] = useState()

    const [dataCondition, setDataCodition] = useState([]);
    const [choseId, setChoseId] = useState(null);
    const [ddlOperator, setDdlOperator] = useState([{label: '=', value:'='},{label: '<=', value:'<='},{label: '>=', value:'>='},{label: '<', value:'<'},{label: '>', value:'>'},{label: '<>', value:'<>'},{label: 'LIKE', value:'LIKE'},{label: 'NOT IN', value:'NOT IN'},{label: 'IN', value:'IN'}])
    const [operator, setOperator] = useState()
    const [ddlKeyField, setDdlKeyField] = useState([])
    const [keyField, setKeyField] = useState()
    const [MPC_GROUP, setMPCGroup] = useState()
    const [MPC_VALUE_TYPE, setMPC_VALUE_TYPE] = useState()



    const [error, setError] = useState(null);
    const {
      register,
      handleSubmit,
      resetField,
      trigger,
      getValues,
      unregister,
      formState: { errors },
      watch,
      setValue,
    } = useForm({ mode: "onChange"})

    const getProducts = async () => {
      setLoading(true);
      try {
        const response = await master.merchantProductList();
        if (response.data.success) {
          setDataProduct(response.data.datas);
          setTimeout(() => {
            setLoading(false);
          }, 1000);
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message?.sqlMessage,
          });
        }
      } catch (error) {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
      }
    };

    const getTableMasterProductGroup = async (product,MP_ID) => {
      setLoading(true);
      setOpen(false)
      setActive(0)
      setMPG({})
      try {
        const response = await master.merchantProductGroupList(MP_ID);
        if (response.data.success) {
          setDataProductGroup(response.data.datas);
          setMP(product)
          getTableMasterFieldData(MP_ID)
          getMerchantQuery(MP_ID)


          // SET INVOICE TYPE ON DETAIL //
          invoiceTypeList.map((invoiceType)=> {
            if(invoiceType.value === product.MP_TYPE_INVOICE ) {
              setInvoiceType(invoiceType.label)
            }
          })
          
          
          const responseSkema = await master.merchantProductSkemaList()
          // SET SKEMA ON DETAIL //
          if(responseSkema.data.success) {
            responseSkema.data.datas.map((skema)=> {
              if(skema.MPS_ID === product.MP_TYPE_SKEMA ) {
                setSkema(skema.MPS_LABEL)
              }
            })

            // SET TRIBE ON DETAIL //
            const responseTribe = await master.merchantProductTribeList()
            if(responseTribe.data.success) {
              responseTribe.data.datas.map((tribe)=> {
                if(tribe.MPT_ID === product.MPT_ID ) {
                  setTribe(tribe.MPT_LABEL)
                }
              })
            
            
            }

          }

          
          setTimeout(() => {
            setLoading(false);
          }, 1000);
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message?.sqlMessage,
          });
        }
      } catch (error) {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
      }
    };

    const renderTree = (nodes) => {
      return (
        <TreeItem
          key={nodes.id}
          value={nodes}
          nodeId={nodes.id}
          label={
            <Grid
              sx={{
                "& svg": {
                  marginRight: "8px",
                },
                "& span": {
                  marginRight: "8px",
                },
              }}
            >
              {" "}
              <span></span>
              {/* {ListMenuSidebar.filter((val) => val.id === nodes.M_Icon)[0]?.icon} */}
              {nodes.M_Name}
            </Grid>
          }
        >
          {Array.isArray(nodes.child)
            ? nodes.child.map((node) => renderTree(node))
            : null}
        </TreeItem>
      );
    };

    const getMerchantProductCondition = async (MPG_ID) => {
      try {
        const response = await master.merchantProductConditionGet(MPG_ID);
        if(response.data.success){
          const data = response.data.datas
          let resultArr = []
          for (const group in data) {
            let tempObj = {}
            tempObj.id = 'AND'+group
            tempObj.M_Name = 'AND'
            if (Object.hasOwnProperty.call(data, group)) {
              const conditionArr = data[group]; // condition Array of each group
              tempObj.child = conditionArr
              conditionArr.forEach((singleCondition, i) => {
                ddlKeyField.forEach((singleKeyField)=> {
                  // console.log(singleKeyField.ID, singleCondition.MPV_ID)
                  if(singleKeyField.ID === singleCondition.MPV_ID){
                    if(i === conditionArr.length - 1) {
                      Object.assign(singleCondition,{M_Name: singleKeyField.LABEL + '  ' + singleCondition.MPC_OPERATOR + '  ' + singleCondition.MPC_VALUE, id: 'OR'+singleCondition.MPC_ID})
                    }else {
                      Object.assign(singleCondition,{M_Name: singleKeyField.LABEL + '  ' + singleCondition.MPC_OPERATOR + '  ' + singleCondition.MPC_VALUE + '  (OR)', id: 'OR'+singleCondition.MPC_ID})
                    }
                  }

                })
              })
              resultArr.push(tempObj)
            }
          }          
          setDataCodition({
            id: "root",
            M_Name: "Parent",
            child: resultArr,
          });
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message
          });
        }
      } catch (error) {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
      }
    }

    const getEditMPC = async (MPC_ID) => {
      setIsEditCondition(true)
      try {
        const response = await master.merchantProductConditionGetById(MPC_ID);
        if (response.data.success) {
          setKeyField(response.data.datas.MPV_ID)
          setOperator(response.data.datas.MPC_OPERATOR)
          setValue("value", response.data.datas.MPC_VALUE, true)
          setMPCGroup(response.data.datas.MPC_GROUP)
          trigger()
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message,
          });
        }
      } catch (error) {
        setError(error.response.data.error);
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
        handleCloseCondition()
      }
    }

    const editMPC = async (editForm) => {
      editForm = {
        MPV_ID : keyField,
        MPC_OPERATOR : operator,
        MPC_VALUE : editForm.value,
        MPC_GROUP : MPC_GROUP,
        M_ID : MPG.M_ID,
        MP_ID : MPG.MP_ID,
        MPG_ID :MPG.MPG_ID,
        AT_FLAG : 1
    }

      // const formData = new FormData();

      // Object.keys(data).forEach((key) => {
      //   formData.append(key, data[key]);
      // });

      try {
        const response = await master.merchantProductConditionUpdate(+choseId.toString().slice(2),editForm);
        if (response.status === 200) {
          // getTableMasterProductGroup(MP,MP.MP_ID)
          getMerchantProductCondition(MPG.MPG_ID)
          Swal.fire({
            icon: "success",
            title: "Success",
            text: response?.data?.message,
          });
          handleCloseCondition()
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message?.sqlMessage,
          });
        }
      } catch (error) {
        console.log(error)
        setError(error.response.data.error);
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
        handleCloseCondition()
      }
    }

    const getDdlMPV_ID = async (MP_ID) => {
      try {
        const response = await master.merchantProductVariableList(MP_ID);
        if (response.data.success) {
            let target = response.data.datas.MERCHANT_PRODUCT_VARIABLE
            let result = []
            target.map((singleRow)=> {
              result.push({LABEL: singleRow.MPV_KEY, ID:singleRow.MPV_ID, TYPE: singleRow.MPV_TYPE.MPV_TYPE_LABEL})
            })
            setDdlKeyField(result)
          // setMasterField(response.data.datas.MERCHANT_PRODUCT_VARIABLE);
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message
          });
        }
      } catch (error) {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
      }
    }

    const addMPC = async (addForm) => {
      if(choseId.toString().includes('AND')){
        addForm =
        {
          MPV_ID : keyField,
          MPC_OPERATOR : operator,
          MPC_VALUE : addForm.value,
          MPC_GROUP : +choseId.toString().slice(3),
          M_ID : MPG.M_ID,
          MP_ID : MPG.MP_ID,
          MPG_ID :MPG.MPG_ID,
          AT_FLAG : 1
       }
      }else if(choseId.toString().includes('root')){
        addForm =
        {
          MPV_ID : keyField,
          MPC_OPERATOR : operator,
          MPC_VALUE : addForm.value,
          MPC_GROUP : dataCondition.child.length+1,
          M_ID : MPG.M_ID,
          MP_ID : MPG.MP_ID,
          MPG_ID :MPG.MPG_ID,
          AT_FLAG : 1
       }
      }
      // const formData = new FormData();

      // Object.keys(data).forEach((key) => {
      //   formData.append(key, data[key]);
      // });
      trigger()
      try {
        const response = await master.merchantProductConditionCreate(addForm);
        if (response.data.success) {
          getMerchantProductCondition(MPG.MPG_ID)
          Swal.fire({
            icon: "success",
            title: "Success",
            text: response?.data?.message,
          });
          handleCloseCondition()
        }else{
          handleCloseCondition()
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message,
          });
        }
      } catch (error) {
        console.log(error)
        setError(error.response.data.error);
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
        handleCloseCondition()
      }
    }

    const handleAddCondition = (edit) => {
      
      if (choseId) {
        if (edit === "edit") {
          if (choseId === "root") {
            Swal.fire({
              icon: "warning",
              title: "Parent tidak bisa di edit",
              text: `mohon pilih Condition yang ada dalam Group 'AND'!`,
            });
          } else if (choseId.toString().includes("AND")) {
            Swal.fire({
              icon: "warning",
              title: "Group tidak bisa di edit",
              text: `mohon pilih Condition yang ada dalam Group 'AND'!`,
            });
          }else {
            doEditCondition(choseId.toString().slice(3));
          }
        }else if(choseId.toString().includes('OR')){
          Swal.fire({
            icon: "warning",
            title: "Wrong Target",
            text: `mohon pilih Group 'AND' untuk menambahkan Condition 'OR'`,
          });
        } else {
          setOpenCondition(true);
        }
      }else {
        setOpenCondition(true);
        // Swal.fire({
        //   icon: "warning",
        //   title: "tidak ada Condition yang dipilih",
        //   text: "mohon pilih Condition terlebih dahulu!",
        // });
      }
    }

    const handleDelete = async () => {
      if (!choseId) {
        Swal.fire({
          icon: "warning",
          title: "tidak ada Condition yang dipilih",
          text: "mohon pilih Condition terlebih dahulu!",
        });
      } else if (choseId === "root") {
        Swal.fire({
          icon: "warning",
          title: "Parent tidak bisa di hapus",
          text: `mohon pilih Condition yang ada dalam Group 'AND'!`,
        });
      } else if (choseId.toString().includes("AND")){
        Swal.fire({
          icon: "warning",
          title: "Group tidak bisa di hapus",
          text: `mohon pilih Condition yang ada dalam Group 'AND'!`,
        });    
      }else {
        Swal.fire({
          title: "Apakah anda yakin?",
          text: "Anda akan menghapus Condition ini!?",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Confirm",
        }).then((result) => {
          if (result.isConfirmed) {
            Axios.post(`/merchant-condition-action/delete/${choseId.toString().slice(2)}`).then((res) => {
              if(res.data.success){
                setChoseId()
                Swal.fire({
                  icon: "success",
                  title: "Success",
                  text: "Berhasil menghapus Condition",
                }).then(() => getMerchantProductCondition(MPG.MPG_ID));
              }else {
                setChoseId()
                Swal.fire({
                  icon: "error",
                  title: "Error",
                  text: res?.data?.message,
                });
              }
            });
          }
        });
      }
    }

    const getMerchantQuery = async (MP_ID) => {
      try {
        const response = await master.merchantProductQueryGet(MP_ID);
        if(response.data.success){
          setValue("MPQ_QUERY", response.data.datas.MPQ_QUERY, true)
          setValue("MPQ_GROUP_SUMMARY", response.data.datas.MPQ_GROUP_SUMMARY, true)
          setM_IDQuery(response.data.datas.M_ID)
          setMP_IDQuery(response.data.datas.MP_ID)
        }
      } catch (error) {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
      }
    }

    const updateMerchantQuery = async () => {
      let data = {
        MPQ_QUERY: getValues("MPQ_QUERY"),
        MPQ_GROUP_SUMMARY: getValues("MPQ_GROUP_SUMMARY"),
        M_ID: M_IDQuery, 
        MP_ID: MP_IDQuery,
      }
      try {
        const response = await master.merchantProductQueryUpdate(M_IDQuery, data);
        if(response.data.success){
          setActive(active + 1);
        }else {
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message,
          });
        }
      } catch (error) {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
      }
    }

    const SelectOneData = async (data, dataColumnIndex) => {
      dataProductGroup.map((row, i)=>{
        let comparator = Object.getOwnPropertyNames(row)
        if(data === row[comparator[dataColumnIndex]]){
          getDdlMPV_ID(row['MP_ID'])
          setMPG_ID(row['MPG_ID'])
          setMPG(row)
          getMerchantProductCondition(row['MPG_ID'])
        }
      })
    }

    const getTableMasterFieldData = async (MP_ID) => {
      setLoading(true);
      try {
        const response = await master.merchantProductVariableList(MP_ID);
        if (response.data.success) {
          setMasterField(response.data.datas.MERCHANT_PRODUCT_VARIABLE);
          setTimeout(() => {
            setLoading(false);
          }, 1000);
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message?.sqlMessage,
          });
        }
      } catch (error) {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
      }
    }

    // const onSubmit = async (queryData) => {
    //   queryData = {
    //     MPG_QUERY: queryData.MPG_QUERY,
    //     MPG_ID: MPG_ID,
    //   };

    //   // const formData = new FormData();

    //   // Object.keys(data).forEach((key) => {
    //   //   formData.append(key, data[key]);
    //   // });

    //   try {
    //     const response = await master.merchantProductGroupRunQuery(queryData);
    //     if (response.status === 200) {
    //     }else{
    //       Swal.fire({
    //         icon: "error",
    //         title: "Error",
    //         text: response?.data?.message?.sqlMessage,
    //       });
    //     }
    //   } catch (error) {
    //     setError(error.response.data.error);
    //     Swal.fire({
    //       icon: "error",
    //       title: "Error",
    //       text: error
    //     });
    //   }
    // }

    const addMPG = async (addForm) => {
      addForm = {
        MPG_LABEL: addForm.MPG_LABEL,
        MPG_MDR_TYPE: MPG_MDR_TYPE,
        MPG_MDR_AMOUNT: addForm.MPG_MDR_AMOUNT,
        MPG_DPP: addForm.MPG_DPP,
        MPG_PPN: addForm.MPG_PPN,
        MPG_PPH: addForm.MPG_PPH,
        M_ID: MP.MERCHANT.M_ID,
        MP_ID: MP.MP_ID,
        AT_FLAG: MP.AT_FLAG
      };

      // const formData = new FormData();

      // Object.keys(data).forEach((key) => {
      //   formData.append(key, data[key]);
      // });
      trigger()
      try {
        const response = await master.merchantProductGroupCreate(addForm);
        if (response.status === 200) {
          getTableMasterProductGroup(MP,MP.MP_ID)
          Swal.fire({
            icon: "success",
            title: "Success",
            text: response?.data?.message,
          });
          handleClose1()
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message?.sqlMessage,
          });
        }
      } catch (error) {
        console.log(error)
        setError(error.response.data.error);
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
        handleClose1()
      }
    }

    const getEditMPG = async (id) => {
      setLoading(true);
      setIsEdit(true)
      try {
        const response = await master.merchantProductGroupGet(id);
        if (response.data.success) {
          setMPG_ID(id)
          setValue("MPG_LABEL", response.data.datas.MPG_LABEL, true)
          setMPG_MDR_TYPE(response.data.datas.MPG_MDR_TYPE)
          setValue("MPG_MDR_AMOUNT", response.data.datas.MPG_MDR_AMOUNT, true)
          setValue("MPG_DPP", response.data.datas.MPG_DPP, true)
          setValue("MPG_PPN", response.data.datas.MPG_PPN, true)
          setValue("MPG_PPH", response.data.datas.MPG_PPH, true)
          trigger()

          setTimeout(() => {
            setLoading(false);
          }, 1000);
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message?.sqlMessage,
          });
        }
      } catch (error) {
        setError(error.response.data.error);
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
        handleClose1()
      }
    }

    const editMPG = async (editForm) => {
      editForm = {
        MPG_LABEL: editForm.MPG_LABEL,
        MPG_MDR_TYPE: MPG_MDR_TYPE,
        MPG_MDR_AMOUNT: editForm.MPG_MDR_AMOUNT,
        MPG_DPP: editForm.MPG_DPP,
        MPG_PPN: editForm.MPG_PPN,
        MPG_PPH: editForm.MPG_PPH,
        M_ID: MP.MERCHANT.M_ID,
        MP_ID: MP.MP_ID,
        AT_FLAG: MP.AT_FLAG
      };

      // const formData = new FormData();

      // Object.keys(data).forEach((key) => {
      //   formData.append(key, data[key]);
      // });

      try {
        const response = await master.merchantProductGroupUpdate(MPG_ID,editForm);
        if (response.status === 200) {
          getTableMasterProductGroup(MP,MP.MP_ID)
          Swal.fire({
            icon: "success",
            title: "Success",
            text: response?.data?.message,
          });
          handleClose1()
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message?.sqlMessage,
          });
        }
      } catch (error) {
        console.log(error)
        setError(error.response.data.error);
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
        handleClose1()
      }
    }

    const deleteProductGroup = async (val) => {
      Swal.fire({
        title: "Apakah anda yakin?",
        text: "Akan menghapus data ini?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Confirm",
      }).then(async (result) => {
        if (result.isConfirmed) {
          const response = await master.merchantProductGroupDelete(val);
          if (response.data.success) {
            resetField('MPG_QUERY')
            getTableMasterProductGroup(MP,MP.MP_ID)
            Swal.fire("Deleted!", "Your data has been deleted.", "success").then(
              () => setRefresh(true)
            );
          } else{
            Swal.fire({
              icon: "error",
              title: "Error",
              text: response?.data?.message,
            });
          }
        }
      });
    };

    const getDdlMPV_TYPE = async () => {
      try {
        const response = await master.ddlMPV_TYPE();
        if (response.data.success) {
          setDdlMPV_TYPE(response.data.datas)
          setTimeout(() => {
            setLoading(false);
          }, 1000);
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message?.sqlMessage,
          });
        }
      } catch (error) {
        setError(error.response.data.error);
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
      }
    }

    const getDdlMPV_TYPE_KEY = async (val) => {
      try {
        const response = await master.ddlMPV_TYPE_KEY();
        if (response.data.success) {
          setDdlMPV_TYPE_KEY(response.data.datas)
          setTimeout(() => {
            setLoading(false);
          }, 1000);
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message?.sqlMessage,
          });
        }
      } catch (error) {
        setError(error.response.data.error);
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
      }
    }

    const addMPV = async (addForm) => {
      addForm = {
        MPV_KEY: addForm.MPV_KEY,
        MPV_LABEL: addForm.MPV_LABEL,
        MPV_TYPE_ID: MPV_TYPE,
        MPV_TYPE_KEY_ID: MPV_TYPE_KEY,
        M_ID: MP.MERCHANT.M_ID,
        MP_ID: MP.MP_ID,
        MPG_ID: MPG_ID,
        AT_FLAG: MP.AT_FLAG
      };

      // const formData = new FormData();

      // Object.keys(data).forEach((key) => {
      //   formData.append(key, data[key]);
      // });
      trigger()
      try {
        const response = await master.merchantProductVariableCreate(addForm);
        if (response.status === 200) {
          getTableMasterFieldData(MP.MP_ID)
          Swal.fire({
            icon: "success",
            title: "Success",
            text: response?.data?.message,
          });
          handleClose2()
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message?.sqlMessage,
          });
        }
      } catch (error) {
        console.log(error)
        setError(error.response.data.error);
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
        handleClose2()
      }
    }

    const getEditMPV = async (id) => {
      setLoading(true);
      setIsEditConfigField(true)
      try {
        const response = await master.merchantProductVariableGet(id);
        if (response.data.success) {
          setMPV_ID(id)
          setValue("MPV_KEY", response.data.datas.MPV_KEY, true)
          setValue("MPV_LABEL", response.data.datas.MPV_LABEL, true)
          setMPV_TYPE(response.data.datas.MPV_TYPE_ID)
          setMPV_TYPE_KEY(response.data.datas.MPV_TYPE_KEY_ID)
          trigger()

          setTimeout(() => {
            setLoading(false);
          }, 1000);
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message?.sqlMessage,
          });
        }
      } catch (error) {
        setError(error.response.data.error);
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
        handleClose2()
      }
    }

    const editMPV = async (editForm) => {
      editForm = {
        MPV_KEY: editForm.MPV_KEY,
        MPV_LABEL: editForm.MPV_LABEL,
        MPV_TYPE_ID: MPV_TYPE,
        MPV_TYPE_KEY_ID: MPV_TYPE_KEY,
        M_ID: MP.MERCHANT.M_ID,
        MP_ID: MP.MP_ID,
        MPG_ID: MPG_ID,
        AT_FLAG: MP.AT_FLAG
      };

      // const formData = new FormData();

      // Object.keys(data).forEach((key) => {
      //   formData.append(key, data[key]);
      // });

      try {
        const response = await master.merchantProductVariableUpdate(MPV_ID,editForm);
        if (response.status === 200) {
          getTableMasterFieldData(MPG_ID)
          Swal.fire({
            icon: "success",
            title: "Success",
            text: response?.data?.message,
          });
          handleClose2()
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message?.sqlMessage,
          });
        }
      } catch (error) {
        console.log(error)
        setError(error.response.data.error);
        Swal.fire({
          icon: "error",
          title: "Error",
          text: error
        });
        handleClose2()
      }
    }

    const deleteMPV = async (val) => {
      console.log(val);
      Swal.fire({
        title: "Apakah anda yakin?",
        text: "Akan menghapus data ini?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Confirm",
      }).then(async (result) => {
        if (result.isConfirmed) {
          const response = await master.merchantProductVariableDelete(val);
          console.log(response);
          if (response.data.success) {
            getTableMasterFieldData(MP.MP_ID)
            Swal.fire("Deleted!", response.data.message, "success").then(
              () => setRefresh(true)
            );
          }else {
            Swal.fire({
              icon: "error",
              title: "Error",
              text: response?.data?.message
            });
          }
        }
      });
    };

    useEffect(() => {
      getProducts();
    }, []);

    return(
      <>
        <Header titles={['Settings', 'Master Product Group']}/>

        <Grid mt="38px" container columnSpacing={2}>
            <Grid item md={3}>
                <Paper elevation={2}
                    sx={{padding:"20px"}}
                    children={
                    <>
                        <Grid>
                            <CustomInput.InputLabel label="Product" labelWidth="110px" rowSpacing={4} />
                        </Grid>
                        <Grid mt="38px">
                          <FormControl>
                            <FormLabel id="demo-radio-buttons-group-label">Product</FormLabel>
                            <RadioGroup
                              aria-labelledby="demo-radio-buttons-group-label"
                              defaultValue="female"
                              name="radio-buttons-group"
                            >
                          {dataProduct.map((product) => {
                              return <FormControlLabel key={product.MP_ID} value={product.MP_ID} control={<Radio onChange={()=> {getTableMasterProductGroup(product,product.MP_ID)}}/>} label={`${product.MP_CODE} - ${product.MP_BRAND}`} />
                          })}
                            </RadioGroup>
                          </FormControl>
                        </Grid>
                    </>
                }/>
            </Grid>
            <Grid item md={9}>
              { MP ?
              <>
                <Paper elevation={2}
                sx={{padding:"20px"}}
                children={
                <>
                    {/*/ INI LAYOUT LAMA JANGAN DIHAPUS /*/}
                    {/* <CustomButon.Primary
                        onClick={(e) => {
                          setOpen1(true)
                          setIsEdit(false)
                          resetField('MPG_LABEL')
                          resetField('MPG_MDR_AMOUNT')
                          resetField('MPG_DPP')
                          resetField('MPG_PPN')
                          resetField('MPG_PPH')
                        }}
                        sx={{ "& span": { marginLeft: "8px"} }}
                        >
                        <PlusButtonIcon />
                        <span>Add Group</span>
                    </CustomButon.Primary> */}
                    <Grid  
                    container
                    direction="row"
                    justifyContent="start-end"
                    alignItems="center"
                    spacing={4}
                    sx={{marginTop: '8px'}}>
                      <Grid item><h3 item>{!MP ? '' : `${MP.MP_CODE} - ${MP.MP_BRAND}`}</h3></Grid>
                    </Grid>
                    <Grid
                    sx={{marginTop: '48px'}}>                        
                          <p>{!MP ? '' : `Code : ${MP.MP_CODE}`}</p>

                    </Grid>
                    <Grid>                        
                          <p>{!MP ? '' : `Brand : ${MP.MP_BRAND}`}</p>

                    </Grid>
                    <Grid>                        
                          <p>{!MP ? '' : `Brand Description : ${MP.MP_DESCRIPTION}`}</p>

                    </Grid>
                    <Grid>                        
                          <p>{!MP ? '' : `Invoice Type : ${invoiceType}`}</p>

                    </Grid>
                    <Grid>                        
                          <p>{!MP ? '' : `Skema : ${skema}`}</p>

                    </Grid>
                    <Grid>                        
                          <p>{!MP ? '' : `Tribe : ${tribe}`}</p>

                    </Grid>
                    <Grid>                        
                          <p>{!MP ? '' : `Merchant : ${MP.MERCHANT.M_LEGAL_NAME}`}</p>

                    </Grid>

                    {!open && (
                      <Grid
                        container
                        direction="row"
                        justifyContent="flex-end"
                        alignItems="center"
                      >
                        <CustomButon.Primary
                            onClick={(e) => {
                              setOpen(true)
                              setActive(active +1)
                            }}
                            >
                            <span>Next</span>
                        </CustomButon.Primary>
                      </Grid>
                    )}

                    {/* <Grid mt="38px">
                      <TableMasterProductGroup onClick={(e) => {SelectOneData(e.target.innerHTML, e.target.cellIndex)}} data={dataProductGroup} editFunct={doEdit} deleteFunct={deleteProductGroup}/>
                    </Grid> */}
                  {/* {MPG_ID ?
                  <>
                    <Grid  
                    container
                    direction="row"
                    justifyContent="start-end"
                    alignItems="center"
                    spacing={2}
                    sx={{marginBottom: '8px'}}>
                      <Grid item><h3 item sx={{ fontWeight: 'bold' }}>CONFIG FIELD TRANSACTION : </h3></Grid>
                      <Grid item><h3 item>{!MPG ? '' : MPG.MPG_LABEL}</h3></Grid>
                    </Grid>
                    <CustomButon.Primary
                        onClick={(e) => {
                          getDdlMPV_TYPE()
                          getDdlMPV_TYPE_KEY()
                          setOpen2(true)
                        }}
                        sx={{ "& span": { marginLeft: "8px" } }}
                        >
                        <PlusButtonIcon />
                        <span>Add Field</span>
                    </CustomButon.Primary>

                    {open2 && (
                    <CustomModal height={260} width={600} open={open2} onClose={handleClose2} title="Form Config Field">
                      <Grid container item xs={12} rowSpacing="12px">
                      { isEditConfigField ?
                        <form onSubmit={handleSubmit(editMPV)} id="edit-form">
                           <Grid container rowSpacing="12px">
                             <CustomInput.InputLabel
                             item
                             label="Key Field" labelWidth="110px"
                             required
                             register={{  ...register("MPV_KEY" , { required: true }, { value: watch("MPV_KEY")}) }}
                             errors={errors}

                             />
                             <CustomInput.InputLabel
                             item
                             label="Label Field" labelWidth="110px"
                             required
                             register={{  ...register("MPV_LABEL" , { required: true }, { value: watch("MPV_LABEL")}, ) }}
                             errors={errors}

                             />
                             <CustomSelect.SelectLabelAPIGet
                             item
                             label="Type Field" labelWidth="110px"
                             required
                             value={(MPV_TYPE)}
                             options={ddlMPV_TYPE}
                             onChange={(e)=>setMPV_TYPE(e.target.value)}
                             />
                              <CustomSelect.SelectLabelAPIGet
                             item
                             label="Category Field" labelWidth="110px"
                             required
                             value={(MPV_TYPE_KEY)}
                             options={ddlMPV_TYPE_KEY}
                             onChange={(e)=>setMPV_TYPE_KEY(e.target.value)}
                             />


                             <Grid item container direction="row" justifyContent="flex-end" gap="24px">
                               <CustomButon.Primary
                                 item
                                 size="small"
                                 onClick={handleClose2}
                                 sx={{ backgroundColor: "red" }}
                               >
                                 <span>Cancel</span>
                               </CustomButon.Primary>
                               <CustomButon.Primary
                                 id="edit-form"
                                 item
                                 size="small"
                                 type="submit"
                               >
                                 <span>Save</span>
                               </CustomButon.Primary>
                             </Grid>
                           </Grid>
                         </form>
                         
                         : 
                         
                         <form onSubmit={handleSubmit(addMPV)} id="add-form">
                          <Grid container rowSpacing="12px">
                            <CustomInput.InputLabel
                            item
                            label="Key Field" labelWidth="110px"
                            required
                            register={{  ...register("MPV_KEY" , { required: true }, { value: watch("MPV_KEY")}) }}
                            errors={errors}

                            />
                            <CustomInput.InputLabel
                            item
                            label="Label Field" labelWidth="110px"
                            required
                            register={{  ...register("MPV_LABEL" , { required: true }, { value: watch("MPV_LABEL")}, ) }}
                            errors={errors}

                            />
                            <CustomSelect.SelectLabelAPIGet
                            item
                            label="Type Field" labelWidth="110px"
                            required
                            value={(MPV_TYPE)}
                            options={ddlMPV_TYPE}
                            onChange={(e)=>setMPV_TYPE(e.target.value)}
                            />
                            <CustomSelect.SelectLabelAPIGet
                            item
                            label="Category Field" labelWidth="110px"
                            required
                            value={(MPV_TYPE_KEY)}
                            options={ddlMPV_TYPE_KEY}
                            onChange={(e)=>setMPV_TYPE_KEY(e.target.value)}
                            />


                            <Grid item container direction="row" justifyContent="flex-end" gap="24px">
                              <CustomButon.Primary
                                item
                                size="small"
                                onClick={handleClose2}
                                sx={{ backgroundColor: "red" }}
                              >
                                <span>Cancel</span>
                              </CustomButon.Primary>
                              <CustomButon.Primary
                                id="add-form"
                                item
                                size="small"
                                type="submit"
                              >
                                <span>Save</span>
                              </CustomButon.Primary>
                            </Grid>
                          </Grid>
                        </form>
                        }
                        
                      </Grid>
                    
                    </CustomModal>
                    )}

                    <Grid mt="38px">
                      <TableMasterField data={dataMasterField} editFunct={doMPVEdit} deleteFunct={deleteMPV}/>
                    </Grid>

                    <form onSubmit={handleSubmit(onSubmit)} id="submit-form">
                      <Grid mt="38px">
                        <CustomInput.InputLabel
                        label="QUERY"
                        labelWidth="110px"
                        multiline
                        minRows={3}
                        rowSpacing={2}
                        required
                        register={{  ...register("MPG_QUERY") }}
                        errors={errors}

                        value={watch("MPG_QUERY")}
                        />
                        <Grid mt="38px"
                          container
                          direction="row"
                          justifyContent="flex-end"
                          alignItems="center"
                          >
                          <CustomButon.Primary
                              id="submit-form"
                              item
                              type="submit">
                              <span>Run</span>
                          </CustomButon.Primary>
                        </Grid>
                      </Grid>
                    </form>
                  </>
                  :
                  <Grid
                  container
                  direction="column"
                  justifyContent="flex-end"
                  alignItems="center"
                 >
                   <img src={EmptyTable} alt="" />
                   <h2>No Data Availabe</h2>
                   <h5>Please select a Master Product Group!</h5>

                 </Grid>} */}


                </>
                }/>
              {open && (
                <Paper elevation={2}
                sx={{padding:"20px", marginTop:"20px"}}
                children={
                  <>
                    <Grid className={classes.root}>
                      <Grid className="status">
                        <Grid className="status-field">
                        {active > 0 &&
                          data.map((val, i) => {
                            return (
                              <React.Fragment key={1}>
                                <Grid
                                  className="avatar"
                                  sx={{
                                    backgroundColor: active === i + 1 && "#9747FF",
                                    color: active === i + 1 && "white",
                                  }}
                                >
                                  {i + 1}
                                </Grid>
                                <h3>{val.LABEL}</h3>
                                {i !== data.length - 1 && (
                                  <div className="line" />
                                )}
                              </React.Fragment>
                            );
                          })}
                        </Grid>
                        <div className="line" />
                      </Grid>
                    </Grid>
                    {active === 1 ?
                    (<>
                      <Grid  
                      container
                      direction="row"
                      justifyContent="start-end"
                      alignItems="center"
                      spacing={2}
                      sx={{marginBottom: '8px', marginTop: '38px'}}>
                        <Grid item><h3 item sx={{ fontWeight: 'bold' }}>CONFIG FIELD TRANSACTION</h3></Grid>
                      </Grid>
                      <Grid mt="38px">
                        <CustomButon.Primary
                            onClick={(e) => {
                              getDdlMPV_TYPE()
                              getDdlMPV_TYPE_KEY()
                              setOpen2(true)
                            }}
                            sx={{ "& span": { marginLeft: "8px",} }}
                            >
                            <PlusButtonIcon />
                            <span>Add Field</span>
                        </CustomButon.Primary>
                      </Grid>

                      <Grid mt="38px">
                        <TableMasterField data={dataMasterField} editFunct={doMPVEdit} deleteFunct={deleteMPV}/>
                      </Grid>
                      {open2 && (
                        <CustomModal height={260} width={600} open={open2} onClose={handleClose2} title="Form Config Field">
                          <Grid container item xs={12} rowSpacing="12px">
                          { isEditConfigField ?
                            <form onSubmit={handleSubmit(editMPV)} id="edit-form">
                              <Grid container rowSpacing="12px">
                                <CustomInput.InputLabel
                                item
                                label="Key Field" labelWidth="110px"
                                required
                                register={{  ...register("MPV_KEY" , { required: true }, { value: watch("MPV_KEY")}) }}
                                errors={errors}

                                />
                                <CustomInput.InputLabel
                                item
                                label="Label Field" labelWidth="110px"
                                required
                                register={{  ...register("MPV_LABEL" , { required: true }, { value: watch("MPV_LABEL")}, ) }}
                                errors={errors}

                                />
                                <CustomSelect.SelectLabelAPIGet
                                item
                                label="Type Field" labelWidth="110px"
                                required
                                value={(MPV_TYPE)}
                                options={ddlMPV_TYPE}
                                onChange={(e)=>setMPV_TYPE(e.target.value)}
                                />
                                  <CustomSelect.SelectLabelAPIGet
                                item
                                label="Category Field" labelWidth="110px"
                                required
                                value={(MPV_TYPE_KEY)}
                                options={ddlMPV_TYPE_KEY}
                                onChange={(e)=>setMPV_TYPE_KEY(e.target.value)}
                                />


                                <Grid item container direction="row" justifyContent="flex-end" gap="24px">
                                  <CustomButon.Primary
                                    item
                                    size="small"
                                    onClick={handleClose2}
                                    sx={{ backgroundColor: "red" }}
                                  >
                                    <span>Cancel</span>
                                  </CustomButon.Primary>
                                  <CustomButon.Primary
                                    id="edit-form"
                                    item
                                    size="small"
                                    type="submit"
                                  >
                                    <span>Save</span>
                                  </CustomButon.Primary>
                                </Grid>
                              </Grid>
                            </form>
                            
                            : 
                            
                            <form onSubmit={handleSubmit(addMPV)} id="add-form">
                              <Grid container rowSpacing="12px">
                                <CustomInput.InputLabel
                                item
                                label="Key Field" labelWidth="110px"
                                required
                                register={{  ...register("MPV_KEY" , { required: true }, { value: watch("MPV_KEY")}) }}
                                errors={errors}

                                />
                                <CustomInput.InputLabel
                                item
                                label="Label Field" labelWidth="110px"
                                required
                                register={{  ...register("MPV_LABEL" , { required: true }, { value: watch("MPV_LABEL")}, ) }}
                                errors={errors}

                                />
                                <CustomSelect.SelectLabelAPIGet
                                item
                                label="Type Field" labelWidth="110px"
                                required
                                value={(MPV_TYPE)}
                                options={ddlMPV_TYPE}
                                onChange={(e)=>setMPV_TYPE(e.target.value)}
                                />
                                <CustomSelect.SelectLabelAPIGet
                                item
                                label="Category Field" labelWidth="110px"
                                required
                                value={(MPV_TYPE_KEY)}
                                options={ddlMPV_TYPE_KEY}
                                onChange={(e)=>setMPV_TYPE_KEY(e.target.value)}
                                />


                                <Grid item container direction="row" justifyContent="flex-end" gap="24px">
                                  <CustomButon.Primary
                                    item
                                    size="small"
                                    onClick={handleClose2}
                                    sx={{ backgroundColor: "red" }}
                                  >
                                    <span>Cancel</span>
                                  </CustomButon.Primary>
                                  <CustomButon.Primary
                                    id="add-form"
                                    item
                                    size="small"
                                    type="submit"
                                  >
                                    <span>Save</span>
                                  </CustomButon.Primary>
                                </Grid>
                              </Grid>
                            </form>
                            }
                            
                          </Grid>
                        
                        </CustomModal>
                      )}
                    </>):
                      active === 2 ?
                      (<>
                          <Grid mt="38px">
                            <CustomInput.InputLabel
                            label="QUERY"
                            labelWidth="110px"
                            multiline
                            minRows={3}
                            rowSpacing={2}
                            required
                            register={{  ...register("MPQ_QUERY") }}
                            errors={errors}

                            value={watch("MPQ_QUERY")}
                            />
                          </Grid>

                          <Grid mt="38px">
                            <CustomInput.InputLabel
                            label="QUERY BY GROUP"
                            labelWidth="110px"
                            multiline
                            minRows={3}
                            rowSpacing={2}
                            required
                            register={{  ...register("MPQ_GROUP_SUMMARY") }}
                            errors={errors}

                            value={watch("MPQ_GROUP_SUMMARY")}
                            />
                          </Grid>
                      </>) :
                        active === 3 ?
                        (<>
                          <Grid  
                          container
                          direction="row"
                          justifyContent="start-end"
                          alignItems="center"
                          spacing={2}
                          sx={{marginBottom: '8px', marginTop: '38px'}}>
                            <Grid item><h3 item sx={{ fontWeight: 'bold' }}>CONFIG PRODUCT GROUP :</h3></Grid>
                            <Grid item><h3 item>{!MPG ? '' : MPG.MPG_LABEL}</h3></Grid>
                          </Grid>
                          <CustomButon.Primary
                              onClick={(e) => {
                                setOpen1(true)
                                setIsEdit(false)
                                resetField('MPG_LABEL')
                                resetField('MPG_MDR_AMOUNT')
                                resetField('MPG_DPP')
                                resetField('MPG_PPN')
                                resetField('MPG_PPH')
                              }}
                              sx={{ "& span": { marginLeft: "8px"} }}
                              >
                              <PlusButtonIcon />
                              <span>Add Group</span>
                          </CustomButon.Primary>
                          {open1 && (
                          <CustomModal height={400} width={600} open={open1} onClose={handleClose1} title="Form Master Product Group" >
                            { isEdit ?
                             <form onSubmit={handleSubmit(editMPG)} id="edit-form">
                                <Grid container rowSpacing="16px">
                                  <CustomInput.InputLabel
                                  item
                                  label="Label" labelWidth="110px"
                                  required
                                  register={{  ...register("MPG_LABEL" , { required: true }, { value: watch("MPG_LABEL")}) }}
                                  errors={errors}

                                  />
                                  <CustomSelect.SelectLabel
                                  item
                                  label="Type MDR" labelWidth="110px"
                                  required
                                  value={(MPG_MDR_TYPE)}
                                  options={[{label:'Percent', value:1}, {label:'Ammunt', value:2}]}
                                  onChange={(e)=>setMPG_MDR_TYPE(e.target.value)}
                                  />
                                  <CustomInput.InputLabel
                                  item
                                  label="Amount MDR" labelWidth="110px"
                                  required
                                  type="number"
                                  register={{  ...register("MPG_MDR_AMOUNT" , { valueAsNumber: true }, { required: true }, { value: watch("MPG_MDR_AMOUNT")}, ) }}
                                  errors={errors}

                                  />
                                  <CustomInput.InputLabel
                                  item
                                  label="Bobot DPP" labelWidth="110px"
                                  required
                                  type="number"
                                  register={{  ...register("MPG_DPP" , { valueAsNumber: true }, { required: true }, { value: watch("MPG_DPP")}) }}
                                  errors={errors}

                                  />
                                  <CustomInput.InputLabel
                                  item
                                  label="BOBOT PPN" labelWidth="110px"
                                  required
                                  type="number"
                                  register={{  ...register("MPG_PPN" , { valueAsNumber: true }, { required: true }, { value: watch("MPG_PPN")}) }}
                                  errors={errors}

                                  />
                                  <CustomInput.InputLabel
                                  item
                                  label="BOBOT PPH" labelWidth="110px"
                                  required
                                  type="number"
                                  register={{  ...register("MPG_PPH" , { valueAsNumber: true }, { required: true }, { value: watch("MPG_PPH")}) }}
                                  errors={errors}

                                  />

                                  <Grid item container direction="row" justifyContent="flex-end" gap="24px">
                                    <CustomButon.Primary
                                      item
                                      size="small"
                                      onClick={handleClose1}
                                      sx={{ backgroundColor: "red" }}
                                    >
                                      <span>Cancel</span>
                                    </CustomButon.Primary>
                                    <CustomButon.Primary
                                      id="edit-form"
                                      item
                                      size="small"
                                      type="submit"
                                    >
                                      <span>Save</span>
                                    </CustomButon.Primary>
                                  </Grid>
                                </Grid>
                              </form>
                              
                              : 
                              
                              <form onSubmit={handleSubmit(addMPG)} id="add-form">
                                <Grid container rowSpacing="12px">
                                  <CustomInput.InputLabel
                                  item
                                  label="Label" labelWidth="110px"
                                  required
                                  register={{  ...register("MPG_LABEL" , { required: true }, { value: watch("MPG_LABEL")}) }}
                                  errors={errors}

                                  />
                                  <CustomSelect.SelectLabel
                                  item
                                  label="Type MDR" labelWidth="110px"
                                  required
                                  value={(MPG_MDR_TYPE)}
                                  options={[{label:'Percent', value:1}, {label:'Amount', value:2}]}
                                  onChange={(e)=>setMPG_MDR_TYPE(e.target.value)}
                                  />
                                  <CustomInput.InputLabel
                                  item
                                  label="Amount MDR" labelWidth="110px"
                                  required
                                  type="number"
                                  register={{  ...register("MPG_MDR_AMOUNT" , { valueAsNumber: true }, { required: true }, { value: watch("MPG_MDR_AMOUNT")}, ) }}
                                  errors={errors}

                                  />
                                  <CustomInput.InputLabel
                                  item
                                  label="Bobot DPP" labelWidth="110px"
                                  required
                                  type="number"
                                  register={{  ...register("MPG_DPP" , { valueAsNumber: true }, { required: true }, { value: watch("MPG_DPP")}) }}
                                  errors={errors}

                                  />
                                  <CustomInput.InputLabel
                                  item
                                  label="BOBOT PPN" labelWidth="110px"
                                  required
                                  type="number"
                                  register={{  ...register("MPG_PPN" , { valueAsNumber: true }, { required: true }, { value: watch("MPG_PPN")}) }}
                                  errors={errors}

                                  />
                                  <CustomInput.InputLabel
                                  item
                                  label="BOBOT PPH" labelWidth="110px"
                                  required
                                  type="number"
                                  register={{  ...register("MPG_PPH" , { valueAsNumber: true }, { required: true }, { value: watch("MPG_PPH")}) }}
                                  errors={errors}

                                  />

                                  <Grid item container direction="row" justifyContent="flex-end" gap="24px">
                                    <CustomButon.Primary
                                      item
                                      size="small"
                                      onClick={handleClose1}
                                      sx={{ backgroundColor: "red" }}
                                    >
                                      <span>Cancel</span>
                                    </CustomButon.Primary>
                                    <CustomButon.Primary
                                      id="add-form"
                                      item
                                      size="small"
                                      type="submit"
                                    >
                                      <span>Save</span>
                                    </CustomButon.Primary>
                                  </Grid>
                                </Grid>
                              </form>} 
                             
                             
                             
                          </CustomModal>
                          )}
                          <Grid mt="38px">
                            <TableMasterProductGroup onClick={(e) => {SelectOneData(e.target.innerHTML, e.target.cellIndex)}} data={dataProductGroup} editFunct={doEdit} deleteFunct={deleteProductGroup}/>
                          </Grid>
                          <Grid  
                          container
                          direction="row"
                          justifyContent="start-end"
                          alignItems="center"
                          spacing={2}
                          sx={{marginBottom: '8px', marginTop: '38px'}}>
                            <Grid item><h3 item sx={{ fontWeight: 'bold' }}>CONDITION MDR</h3></Grid>
                          </Grid>
                          <CustomButon.Primary
                              onClick={(e) => {
                                setOpen1(true)
                                setIsEdit(false)
                                resetField('MPG_LABEL')
                                resetField('MPG_MDR_AMOUNT')
                                resetField('MPG_DPP')
                                resetField('MPG_PPN')
                                resetField('MPG_PPH')
                              }}
                              sx={{ "& span": { marginLeft: "8px"} }}
                              >
                              <PlusButtonIcon />
                              <span>Add MDR</span>
                          </CustomButon.Primary>
                          <Grid mt="38px">
                            <TableMasterProductGroup onClick={(e) => {SelectOneData(e.target.innerHTML, e.target.cellIndex)}} data={dataProductGroup} editFunct={doEdit} deleteFunct={deleteProductGroup}/>
                          </Grid>
                          <Grid  
                          container
                          direction="row"
                          justifyContent="start-end"
                          alignItems="center"
                          spacing={2}
                          sx={{marginBottom: '8px', marginTop: '38px'}}>
                            <Grid item><h3 item sx={{ fontWeight: 'bold' }}>CONDITION DATA SOURCE</h3></Grid>
                          </Grid>
                          <Grid
                            mt="24px"
                            sx={{
                              "& svg": {
                                height: "12px",
                              },
                              "& .active": {
                                transform: "rotate(90deg)",
                              },
                            }}
                          >
                             {!MPG.MPG_ID ? 
                               ( <>
                                </>)
                              : 
                              ( <>
                                <CustomButon.Primary
                                   onClick={()=>{
                                     resetField('value')
                                     setOperator()
                                     setKeyField()
                                     handleAddCondition()
                                   }
                                  }
                                   sx={{ "& span": { marginLeft: "8px" } }}
                                 >
                                   <PlusButtonIcon />
                                   <span>Add Condition</span>
                                 </CustomButon.Primary>
                                 <CustomButon.Primary
                                   onClick={() => handleAddCondition("edit")}
                                   sx={{
                                     "& span": {
                                       marginLeft: "8px",
                                     },
                                     "& svg": {
                                       height: "18px",
                                     },
                                     marginLeft: "12px",
                                     marginRight: "12px",
                                   }}
                                 >
                                   <EditIcon />
                                   <span>Edit Condition</span>
                                 </CustomButon.Primary>
                                 <CustomButon.Primary
                                   onClick={handleDelete}
                                   sx={{
                                     "& span": {
                                       marginLeft: "8px",
                                     },
                                     "& svg": {
                                       height: "18px",
                                     },
                                   }}
                                 >
                                   <TrashIcon fill={"currentColor"} />
                                   <span>Delete Condition</span>
                                 </CustomButon.Primary> 
                                 <TreeView
                                    aria-label="rich object"
                                    defaultCollapseIcon={<ChevronRight className="active" />}
                                    defaultExpanded={["root"]}
                                    defaultExpandIcon={<ChevronRight />}
                                    onNodeSelect={(a, b) => {
                                      setChoseId(b)
                                    }}
                                    sx={{
                                      height: "100%",
                                      flexGrow: 1,
                                    }}
                                  >
                                    {renderTree(dataCondition)}
                                  </TreeView>
                                  {openCondition && (
                                  <CustomModal height={260} width={600} open={openCondition} onClose={handleCloseCondition} title="Form Condition">
                                    <Grid container item xs={12} rowSpacing="12px">
                                    { isEditCondition ?
                                      <form onSubmit={handleSubmit(editMPC)} id="edit-form">
                                        <Grid container rowSpacing="12px">
                                        <CustomSelect.SelectLabelAPIGet
                                          item
                                          label="Key Field" labelWidth="110px"
                                          required
                                          value={(keyField)}
                                          options={ddlKeyField}
                                          onChange={(e)=>{
                                            setKeyField(e.target.value)
                                            ddlKeyField.forEach(singleKeyField => {
                                              if(singleKeyField.ID === e.target.value){
                                                setMPC_VALUE_TYPE(singleKeyField.TYPE.toLowerCase())
                                              }
                                            })
                                          }}
                                          />
                                          <CustomSelect.SelectLabel
                                          item
                                          label="Operator" labelWidth="110px"
                                          required
                                          value={(operator)}
                                          options={ddlOperator}
                                          onChange={(e)=>setOperator(e.target.value)}
                                          />
                                          <CustomInput.InputLabel
                                          item
                                          type={MPC_VALUE_TYPE}
                                          label="Value" labelWidth="110px"
                                          required
                                          register={{  ...register("value" , { required: true }, { value: watch("value")}) }}
                                          errors={errors}

                                          />


                                          <Grid item container direction="row" justifyContent="flex-end" gap="24px">
                                            <CustomButon.Primary
                                              item
                                              size="small"
                                              onClick={handleCloseCondition}
                                              sx={{ backgroundColor: "red" }}
                                            >
                                              <span>Cancel</span>
                                            </CustomButon.Primary>
                                            <CustomButon.Primary
                                              id="edit-form"
                                              item
                                              size="small"
                                              type="submit"
                                            >
                                              <span>Save</span>
                                            </CustomButon.Primary>
                                          </Grid>
                                        </Grid>
                                      </form>
                                      
                                      : 
                                      
                                      <form onSubmit={handleSubmit(addMPC)} id="add-form">
                                        <Grid container rowSpacing="12px">
                                        <CustomSelect.SelectLabelAPIGet
                                          item
                                          label="Key Field" labelWidth="110px"
                                          required
                                          value={(keyField)}
                                          options={ddlKeyField}
                                          onChange={(e)=>{
                                            setKeyField(e.target.value)
                                            ddlKeyField.forEach(singleKeyField => {
                                              if(singleKeyField.ID === e.target.value){
                                                setMPC_VALUE_TYPE(singleKeyField.TYPE.toLowerCase())
                                              }
                                            })
                                          }}
                                          />
                                          <CustomSelect.SelectLabel
                                          item
                                          label="Operator" labelWidth="110px"
                                          required
                                          value={(operator)}
                                          options={ddlOperator}
                                          onChange={(e)=>setOperator(e.target.value)}
                                          />
                                          <CustomInput.InputLabel
                                          item
                                          type={MPC_VALUE_TYPE}
                                          label="Value" labelWidth="110px"
                                          required
                                          register={{  ...register("value" , { required: true }, { value: watch("value")}) }}
                                          errors={errors}

                                          />


                                          <Grid item container direction="row" justifyContent="flex-end" gap="24px">
                                            <CustomButon.Primary
                                              item
                                              size="small"
                                              onClick={handleCloseCondition}
                                              sx={{ backgroundColor: "red" }}
                                            >
                                              <span>Cancel</span>
                                            </CustomButon.Primary>
                                            <CustomButon.Primary
                                              id="add-form"
                                              item
                                              size="small"
                                              type="submit"
                                            >
                                              <span>Save</span>
                                            </CustomButon.Primary>
                                          </Grid>
                                        </Grid>
                                      </form>
                                      }
                                      
                                    </Grid>
                                  
                                  </CustomModal>
                                  )}
                               </>)          
                              }
                           
                          </Grid>
                          </>) :
                          active === 4 ?
                          (<>
                            {/* <h3>4</h3> */}
                            </>) :
                            (<>
                              {/* <h3>5</h3> */}
                            </>) 
                    }
                    <Grid container justifyContent="flex-end" mt="32px" gap="24px">
                      <CustomButon.Primary disabled={active === 1} onClick={handleBack}>
                        Back
                      </CustomButon.Primary>
                      <CustomButon.Primary disabled={active === 4} onClick={handleNext}>
                       Next
                      </CustomButon.Primary>
                    </Grid>

                  </>
                }
                />
              )}

              </> :
              <>
                <Paper elevation={2}
                sx={{padding:"20px"}}
                children={
                  <>
                    {/* <CustomButon.Primary
                        onClick={(e) => {
                          setOpen1(true)
                          setIsEdit(false)
                        }}
                        sx={{ "& span": { marginLeft: "8px" } }}
                        >
                        <PlusButtonIcon />
                        <span>Add Group</span>
                    </CustomButon.Primary> */}
                    <Grid
                    container
                    direction="column"
                    justifyContent="center"
                    alignItems="center" >
                    
                      <img src={EmptyTable} alt=""/>
                      <h2>No Data Availabe</h2>
                      <h5>Please select a Product!</h5>

                    </Grid>
                  </>
              
              }
                />
              </>}
            </Grid>

        </Grid>
      </>
    )
}
