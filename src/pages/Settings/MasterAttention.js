import React, { useState, useEffect } from "react";
import { Grid, Modal, Button, InputLabel, Switch, IconButton } from "@mui/material";

import { useForm } from "react-hook-form";
import Header from "../../components/Header";
import TableMasterAttention from "../../components/SettingsComponents/TableMasterAttention";
import CustomButon from "../../components/CustomButon";
import CustomButonFile from "../../components/CustomButtonFile";
import CustomTab from "../../components/CustomTab";
import CustomInputTab from "../../components//InputComponentTab";
import CustomInputCek from "../../components/InputComponentCek";
import CustomInputBrows from "../../components/InputComponentBrows";
import CustomModal from "../../components/CustomModal";
import { PlusButtonIcon, TrashIcon } from "../../assets/icons/customIcon";
import { Link } from "react-router-dom";
import master from "../../helper/services/master";
import Swal from "sweetalert2";
import CustomInput from "../../components/InputComponent";

export default function MasterAttention() {
  const handleClose = () => setState({ ...state, loading: false, open:false });

  const [state, setState] = useState({
    open: false,
    selectedId: null,
    loading: false,
    refresh: false,
    files: null,
    attentionChecked: false,
    validateChecked: false
  });

  const onFileChange = (e) => {
    const _file = e.target.files;
    setState({...state, files: _file, attentionChecked: true, validateChecked: true});
  }

  const onRemoveImage = () => {
    setState({...state, files: null, attentionChecked: false, validateChecked: false});
  }

  const onATFlagChange = (e) => {
    const _check = e.target.name;
    setState({...state, [_check]: !state[_check]});
  }

  const {open, selectedId, loading, refresh, files, attentionChecked, validateChecked} = state;

  const {
    register,
    handleSubmit,
    resetField,
    formState: { errors },
    watch,
    control,
    setValue,
  } = useForm();

  const setLoading = () => setState({ ...state, loading: true });
  const setOpen = () => setState({ ...state, open: !open });

  const [dataBeritaAcara, setDataBeritaAcara] = useState([]);

  const getDataBeritaAcara = async () => {
    try {
      const response = await master.masterAttentionList('1');
      if (response.data.success) {
        setDataBeritaAcara(response.data.datas);
       
      }
    } catch (error) {
      console.log(error);
    }
  };

  const onSubmit = async (data) => {
    const formData = new FormData();
    formData.append("AT_FLAG", validateChecked ? 1 : 0);

    for(let key in data) {
      if(key != 'AT_FLAG' && key != 'AT_FLAG_ATTENTION') {
        formData.append(key, data[key]);
      }
    }

    if(files) {
      formData.append("PIC_SIGN", files[0]);
    }else{
      formData.append("PIC_SIGN", null);
    }

    if(!selectedId){
    formData.append("PIC_TYPE", 1);
    }

    //cek formData
    // for (const value of formData.values()) {
    //   console.log(value);
    // }

    setLoading();
    console.log(selectedId);
    try {
      const request = selectedId ? await master.masterAttentionUpdate(selectedId, formData): await master.masterAttentionCreate(formData);
      console.log(request);
      if (request.data.success) {
        getDataBeritaAcara()
        resetForm()
        setState({ ...state, loading: false, open: false, selectedId: null});
        Swal.fire({
          icon: "success",
          title: "Success",
          text: "Create data success",
        });
      } else {
        setState({...state, loading: false, open: false, selectedId: null});
        Swal.fire({
          icon: "error",
          title: "Error",
          text: "Error on submit"
        });
      }
    } catch ({ response: { data } }) {
      console.log(data);
      setState({ ...state, loading: false, open: false, selectedId: null });
      Swal.fire({
        icon: "error",
        title: "Error",
        text: data.error.message,
      });
    }
  };

  const onUpdate = (id) => {
    setTimeout(() => {
      getEditBa(id);
    }, 150);
  }

  const resetForm = () => {
    resetField("PIC_NAME")
    resetField("PIC_JABATAN")
    resetField("PIC_ALAMAT")
    resetField("PIC_EMAIL")
    resetField("PIC_NOTELP")
    resetField("PT_NAME")
    resetField("PT_ALAMAT")
    resetField("PT_NOTELP")
    resetField("PT_NPWP")
    resetField("PT_ACOUNT_NAME")
    resetField("PT_ACOUNT_NUMBER")
    resetField("PT_BANK_NAME")
    resetField("PT_BRANCH")
    resetField("PIC_TYPE")
    resetField("AT_FLAG")
    resetField("AT_FLAG_ATTENTION")
  }

  const getEditBa = (id) => {
    setState({...state, open: true});
    master.masterAttentionGet(id).then(res => {
       setTimeout(() => {
          setValue("PIC_NAME", res.data.datas.PIC_NAME, true)
          setValue("PIC_JABATAN", res.data.datas.PIC_JABATAN, true)
          setValue("PIC_ALAMAT", res.data.datas.PIC_ALAMAT, true)
          setValue("PIC_EMAIL", res.data.datas.PIC_EMAIL, true)
          setValue("PIC_NOTELP", res.data.datas.PIC_NOTELP, true)
          setValue("PT_NAME", res.data.datas.PT_NAME, true)
          setValue("PT_ALAMAT", res.data.datas.PT_ALAMAT, true)
          setValue("PT_NOTELP", res.data.datas.PT_NOTELP, true)
          setValue("PT_NPWP", res.data.datas.PT_NPWP, true)
          setValue("PT_ACOUNT_NAME", res.data.datas.PT_ACOUNT_NAME, true)
          setValue("PT_ACOUNT_NUMBER", res.data.datas.PT_ACOUNT_NUMBER, true)
          setValue("PT_BANK_NAME", res.data.datas.PT_BANK_NAME, true)
          setValue("PT_BRANCH", res.data.datas.PT_BRANCH, true)
          setValue("PIC_TYPE", res.data.datas.PIC_TYPE, true)
          setValue("AT_FLAG", res.data.datas.AT_FLAG, true)
          setValue("AT_FLAG_ATTENTION", res.data.datas.AT_FLAG, true)
        }, 150);
        setState({...state, open: true, attentionChecked: true, validateChecked: true, selectedId: id});
    }).catch(({message}) => {
      Swal.fire({
        icon: "error",
        title: "Error",
        text: message,
      });
    });
}

const deleteBeritaAcara = async (val) => {
  Swal.fire({
    title: "Apakah anda yakin?",
    text: "Akan menghapus data ini?",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Confirm",
  }).then(async (result) => {
    if (result.isConfirmed) {
      const response = await master.masterAttentionDelete(val);
      if (response.data.success) {
        Swal.fire("Deleted!", "Your data has been deleted.", "success")
        getDataBeritaAcara()
      }else{
        Swal.fire({
          icon: "error",
          title: "Error",
          text: response?.data?.message,
        });
        handleClose()
      }
    }
  });
};

useEffect(() => {
  getDataBeritaAcara();
}, []);

  return (
    <Grid>
      <Header titles={["Settings", "Master Attention"]} />
      
      <Grid mt="36px" sx={{ "& a": { textDecoration: "none" } }}>
        <CustomButon.Primary
          onClick={(e) => {
            resetForm()
            setOpen(true)
          }}
          sx={{ "& span": { marginLeft: "8px" } }}
        >
          <PlusButtonIcon />
          <span>Add Berita Acara</span>
        </CustomButon.Primary>
      </Grid>
      <Grid 
      container 
      direction="row"
      justifyContent="center"
      alignItems="stretch"
      md={12}
      sx={{
        width:'100%',
        marginTop: '38px',
      }}
      >
        <Grid item lg md sm>
          <Link to="/settings/master-attention">
            <CustomButon.Primary sx={{ height:"50px", width:'100%'}}>
              Attention Berita Acara
            </CustomButon.Primary>
          </Link>
        </Grid>

        <Grid item lg={6} md={6} sm={6}>
          <Link to="/settings/master-invoice">
            <CustomButon.Primary sx={{ height:"50px", width:'100%'}}>
              Attention Invoice
            </CustomButon.Primary>
          </Link>
        </Grid>

      </Grid>
     


      <Grid mt="38px">
        <TableMasterAttention data={dataBeritaAcara} editFunct={(data) => {onUpdate(data)}} deleteFunct={(data) => {deleteBeritaAcara(data)}}/>
      </Grid>
        <CustomModal width="800px" open={open} onClose={handleClose} title="Form Add Berita Acara">
          <form onSubmit={handleSubmit(onSubmit)} id="onSubmit">
          <Grid container spacing={10} rowSpacing={12}>
            <Grid item xs={6} rowSpacing="12px">
              <CustomInputTab.InputLabel 
                label="Name"
                labelWidth="110px"
                register={{ ...register("PIC_NAME", { required: true }) }}
                id="PIC_NAME"
                errors={errors}
                required
                rowSpacing={12}
              />
              <CustomInput.InputLabel
                label="Position :"
                labelWidth="110px"
                register={{ ...register("PIC_JABATAN", { required: true }) }}
                id="PIC_JABATAN"
                errors={errors} />
              <CustomInputTab.InputLabel label="Address :" labelWidth="110px"
                register={{ ...register("PIC_ALAMAT", { required: true }) }}
                id="PIC_ALAMAT" />
              <CustomInputTab.InputLabel label="Phone Number :" labelWidth="110px"
                register={{ ...register("PIC_NOTELP", { required: true }) }}
                id="PIC_NOTELP"
                errors={errors}
              />
              <CustomInput.InputLabel
                label="Email :"
                labelWidth="110px"
                register={{ ...register("PIC_EMAIL", { required: true }) }}
                id="PIC_EMAIL"
              />
              <CustomInput.InputLabel
                label="Company :"
                labelWidth="110px"
                register={{ ...register("PT_NAME", { required: true }) }}
                id="PT_NAME" />
              <CustomInputTab.InputLabel label="Address :" labelWidth="110px"
                register={{ ...register("PT_ALAMAT", { required: true }) }}
                id="PT_ALAMAT" />
              <CustomInputTab.InputLabel label="No. Telp :" labelWidth="110px"
                register={{ ...register("PT_NOTELP", { required: true }) }}
                id="PT_NOTELP" />
              <div>
                <input
                 register={{ ...register("AT_FLAG_ATTENTION") }}
                 type="checkbox" onChange={onATFlagChange} name="attentionChecked" checked={attentionChecked} />
                <span>Set Attention</span>
              </div>
            </Grid>
            <Grid item xs={6} rowSpacing="12px">
              <div style={{display: 'flex', flexDirection: "row", justifyContent: "flex-start", alignItems: "center", marginBottom: 10}}>
                  <div style={{marginRight: 10}}>
                    <span>Preview Sign: </span>
                  </div>
                  { files 
                    ? 
                    <div style={{width: 100, height: 100, position: "relative"}}>
                        <IconButton style={{position: "absolute", top: -15, right: -15, color: 'red'}} onClick={onRemoveImage}>
                            <TrashIcon />
                        </IconButton>
                      <img src={URL.createObjectURL(files[0])} style={{width: 100, height: 100}} /> 
                    </div>
                    :
                    <div style={{height: 100, width: 100, border: "1px solid #ccc", borderRadius: 12, display: 'flex', justifyContent: "center", alignItems: "center"}}>
                      <p style={{color: "#ccc", fontSize:10}}>Select Image</p>
                    </div>
                  }
              </div>
              <input id="file-image" style={{display: 'none'}} type="file" accept="image/png, image/jpg, image/jpeg" onChange={onFileChange} multiple={true} />
              
              {!files && <CustomButonFile.Primary label="Upload Sign :" onClick={() => document.getElementById("file-image").click()}>Upload Sign</CustomButonFile.Primary>}
                <div style={{marginTop: 10}}>
                  <input type="checkbox" {...register("AT_FLAG", { required: false })} onChange={onATFlagChange} name="validateChecked" checked={validateChecked} />
                  <span>Validasi Sign</span>
                </div>
          
              <CustomInputTab.InputLabel label="NPWP :" labelWidth="110px"
                    register={{ ...register("PT_NPWP", { required: true }) }}
                    id="PT_NPWP" />
              <CustomInputTab.InputLabel label="Account Name :" labelWidth="110px"
                    register={{ ...register("PT_ACOUNT_NAME", { required: true }) }}
                    id="PT_ACOUNT_NAME"/>
              <CustomInputTab.InputLabel label="Account Number :" labelWidth="110px"
                    register={{ ...register("PT_ACOUNT_NUMBER", { required: true }) }}
                    id="PT_ACOUNT_NUMBER" />
              <CustomInputTab.InputLabel label="Bank Name :" labelWidth="110px"
                    register={{ ...register("PT_BANK_NAME", { required: true }) }}
                    id="PT_BANK_NAME"
                  />
                  <CustomInput.InputLabel
                    label="Branch :"
                    labelWidth="110px"
                    register={{ ...register("PT_BRANCH", { required: true }) }}
                    id="PT_BRANCH"
                  />
            </Grid>
            <Grid item justifyContent="flex-end" container gap="24px">
              <CustomButon.Primary
                size="small"
                onClick={setOpen}
                sx={{ "& span": { marginLeft: "8px" }, backgroundColor: "red" }}
              >
                Cancel{" "}
              </CustomButon.Primary>
              <CustomButon.Primary
                id="submit-ba"
                type="submit"
                size="small"
                // onClick={(e) => setOpen(true)}
                sx={{ "& span": { marginLeft: "8px" } }}
              >
                Save
              </CustomButon.Primary>
            </Grid>
          </Grid>
          </form>
        </CustomModal>
      
    </Grid>
  );
}
