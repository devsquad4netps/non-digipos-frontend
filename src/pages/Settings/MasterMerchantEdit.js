import React, { useEffect, useState } from "react";
import { Grid } from "@mui/material";

import Header from "../../components/Header";
import FormEditMerchant from "../../components/SettingsComponents/FormEditMerchant";
import master from "../../helper/services/master";
import { useParams } from "react-router-dom";
import EmptyData from "../../components/EmptyData";

export default function MasterMerchantEdit() {
  const { id } = useParams();
  const [initialValues, setInitialValues] = useState();
  const [loading, setLoading] = useState(false);

  const getMerchantDetail = async () => {
    setLoading(true);
    try {
      const response = await master.merchantGet(id);
      setInitialValues(response.data.datas);
      console.log(response);
    } catch (error) {
      console.log(error);
    }
    setTimeout(() => {
      setLoading(false);
    }, 500);
  };

  useEffect(() => {
    getMerchantDetail();
  }, []);
  return (
    <Grid>
      <Header titles={['Settings', 'Master Merchant', 'Merchant Edit']}/>
      <Grid mt="60px">
        {loading ? (
          <EmptyData loading />
        ) : initialValues ? (
          <FormEditMerchant initialValues={initialValues} />
        ) : (
          <EmptyData />
        )}
      </Grid>
      {/* <Grid item md={12}>
        <h3>Rekening Information</h3>
        <Grid mt="36px" sx={{ "& a": { textDecoration: "none" } }}>
          <CustomButon.Primary
            title={"Add Merchant"}
            sx={{ "& span": { marginLeft: "8px" } }}
          >
            <PlusButtonIcon />
            <span>Add</span>
          </CustomButon.Primary>
        </Grid>
        <Grid mt="16px">
          <TableAddMasterMerchant />
        </Grid>
      </Grid> */}
    </Grid>
  );
}
