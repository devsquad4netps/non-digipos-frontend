import React, { useEffect, useState, useMemo } from "react";
import Header from "../../components/Header";
import {
  Grid,
  Paper,
  ListItemButton,
  Collapse,
  MenuItem,
  TextField,
  Autocomplete,
} from "@mui/material";
import { useSelector } from "react-redux";
import CustomButon from "../../components/CustomButon";
import { makeStyles } from "@mui/styles";
import { Axios } from "../../helper/http";

import {
  SolidArrow,
  PlusButtonIcon,
  ChevronRight,
  EditIcon,
  TrashIcon,
} from "../../assets/icons/customIcon";
import ListMenuCopy from "../../components/ListMenuCopy";
import CustomModal from "../../components/CustomModal";
import CustomInput from "../../components/InputComponent";
import { ListMenuSidebar } from "../../assets/icons/customIcon";
import { TreeView, TreeItem } from "@mui/lab";
import Swal from "sweetalert2";
import { getUserId } from "../../helper/cookies";
import { useForm } from "react-hook-form";

const useStyles = makeStyles((theme) => {
  const { customTheme } = theme;
  return {
    root: {
      "& h3": {
        margin: 0,
      },
    },
    leftContent: {
      "& .listButton": {
        border: "1px solid",
        padding: "0px",
      },
    },
  };
});

function list_to_tree(list) {
  var map = {},
    node,
    roots = [],
    i;

  for (i = 0; i < list.length; i += 1) {
    map[list[i].id] = i; // initialize the map
    list[i].child = []; // initialize the children
  }

  for (i = 0; i < list.length; i += 1) {
    node = list[i];
    if (node.M_Parent !== 0) {
      // if you have dangling branches check that map[node.parentId] exists
      list[map[node.M_Parent]].child.push(node);
    } else {
      roots.push(node);
    }
  }
  return roots;
}

export default function MasterMenu() {
  const classes = useStyles();
  const [open, setOpen] = useState();
  const [dataMenu, setDataMenu] = useState([]);
  const [choseId, setChoseId] = useState(null);
  const [listMenu, setListMenu] = useState([]);
  const [dataEdit, setDataEdit] = useState({});

  useEffect(() => {
    getDataMenu();
  }, []);

  const getDataMenu = () => {
    Axios.get(`/users/${getUserId()}`).then((res) => {
      Axios.post("/Menu/getMenuRole", {
        role: res.data.id,
      }).then((res) => {
        const data = res.data.info;
        setListMenu(data);
        const newArray = [];
        data.map((val) => newArray.push({ ...val, child: [] }));
        console.log(newArray);
        setDataMenu({
          id: "root",
          M_Name: "Parent",
          child: list_to_tree(newArray.sort((a, b) => a.M_Index - b.M_Index)),
        });
      });
    });
  };

  const handleAddMenu = (edit) => {
    if (choseId) {
      if (edit === "edit") {
        if (choseId === "root") {
          Swal.fire({
            icon: "warning",
            title: "Root menu tidak bisa di edit",
            text: "mohon pilih menu yang ada dalam root!",
          });
        } else {
          let menu = listMenu.filter((val) => val.id === Number(choseId))[0];
          setDataEdit(menu);
          // console.log(listMenu)
          setOpen("edit");
        }
      } else {
        setOpen(true);
      }
    } else {
      Swal.fire({
        icon: "warning",
        title: "tidak ada menu yang dipilih",
        text: "mohon pilih menu terlebih dahulu!",
      });
    }
  };

  const handleDelete = () => {
    if (!choseId) {
      Swal.fire({
        icon: "warning",
        title: "tidak ada menu yang dipilih",
        text: "mohon pilih menu terlebih dahulu!",
      });
    } else if (choseId === "root") {
      Swal.fire({
        icon: "warning",
        title: "Root menu tidak bisa di hapus",
        text: "mohon pilih menu yang ada dalam root!",
      });
    } else {
      Swal.fire({
        title: "Apakah anda yakin?",
        text: "Anda akan menghapus menu ini!?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Confirm",
      }).then((result) => {
        if (result.isConfirmed) {
          Axios.delete(`/Menu/${choseId}`).then((res) => {
            Swal.fire({
              icon: "success",
              title: "Success",
              text: "Berhasil menghapus menu",
            }).then(() => getDataMenu());
          });
        }
      });
    }
  };

  const renderTree = (nodes) => {
    return (
      <TreeItem
        key={nodes.id}
        value={nodes}
        nodeId={nodes.id}
        label={<Grid>{nodes.M_Name}</Grid>}
      >
        {Array.isArray(nodes.child)
          ? nodes.child.map((node) => renderTree(node))
          : null}
      </TreeItem>
    );
  };

  return (
    <Grid className={classes.root}>
      <Header />
      <Grid container spacing={"24px"} mt="12px">
        <Grid item xs={7}>
          <Paper sx={{ padding: "18px" }} className={classes.leftContent}>
            <CustomButon.Primary
              onClick={handleAddMenu}
              sx={{ "& span": { marginLeft: "8px" } }}
            >
              <PlusButtonIcon />
              <span>Add Menu</span>
            </CustomButon.Primary>
            <CustomButon.Primary
              onClick={() => handleAddMenu("edit")}
              sx={{
                "& span": {
                  marginLeft: "8px",
                },
                "& svg": {
                  height: "18px",
                },
                marginLeft: "12px",
                marginRight: "12px",
              }}
            >
              <EditIcon />
              <span>Edit Menu</span>
            </CustomButon.Primary>
            <CustomButon.Primary
              onClick={handleDelete}
              sx={{
                "& span": {
                  marginLeft: "8px",
                },
                "& svg": {
                  height: "18px",
                },
              }}
            >
              <TrashIcon fill={"currentColor"} />
              <span>Delete Menu</span>
            </CustomButon.Primary>
            <Grid
              mt="24px"
              sx={{
                "& svg": {
                  height: "12px",
                },
                "& .active": {
                  transform: "rotate(90deg)",
                },
              }}
            >
              <TreeView
                aria-label="rich object"
                defaultCollapseIcon={<ChevronRight className="active" />}
                defaultExpanded={["root"]}
                defaultExpandIcon={<ChevronRight />}
                onNodeSelect={(a, b) => setChoseId(b)}
                sx={{
                  height: "100%",
                  flexGrow: 1,
                }}
              >
                {renderTree(dataMenu)}
              </TreeView>
            </Grid>
            {open && (
              <ModalMenus
                open={open}
                onClose={() => {
                  setDataEdit({});
                  setOpen(false);
                }}
                choseId={choseId}
                getDataMenu={() => getDataMenu()}
                listMenu={listMenu}
                dataEdit={dataEdit}
              />
            )}
          </Paper>
        </Grid>
        <Grid item xs={5}>
          <Paper>
            <MenuRight />
          </Paper>
        </Grid>
      </Grid>
    </Grid>
  );
}

const MenuRight = () => {
  const [dataRows, setDataRows] = useState([]);
  const [dataSelect, setDataSelect] = useState();
  const [dataChange, setDataChange] = useState("");
  const [dataMenu, setDataMenu] = useState([]);

  useEffect(() => {
    getDataRoles();
  }, []);

  useEffect(() => {
    if (dataSelect) {
      Axios.post("/Menu/getMenuRoleById", {
        role: `${dataSelect.id}`,
      }).then((res) => {
        const data = res.data.info;
        // setListMenu(data);
        const newArray = [];
        data.map((val) => newArray.push({ ...val, child: [] }));
        setDataMenu({
          id: "root",
          M_Name: "Parent",
          child: list_to_tree(newArray),
        });
      });
    } else {
      setDataMenu([]);
    }
  }, [dataSelect]);

  const getDataRoles = () => {
    Axios.get("/Roles").then((res) => {
      setDataRows(res.data);
    });
  };

  const renderTree = (nodes) => {
    return (
      <TreeItem
        key={nodes.id}
        value={nodes}
        nodeId={nodes}
        label={<Grid>{nodes.M_Name}</Grid>}
      >
        {Array.isArray(nodes.child)
          ? nodes.child.map((node) => renderTree(node))
          : null}
      </TreeItem>
    );
  };

  const handleAddRole = () => {
    Axios.post("/Roles", {
      name: dataChange,
    }).then((res) => {
      Swal.fire({
        icon: "success",
        title: "Success",
        text: "Berhasil menambahkan role baru",
      }).then(() => {
        setDataChange("");
        getDataRoles();
      });
    });
  };

  const handleDelete = () => {
    Swal.fire({
      title: "Apakah anda yakin?",
      text: "Anda akan menghapus roles ini!?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Confirm",
    }).then((result) => {
      if (result.isConfirmed) {
        Axios.delete(`/Roles/${dataSelect.id}`).then((res) => {
          Swal.fire({
            icon: res.data.status ? "success" : "error",
            title: res.data.status ? "Success" : "Error",
            text: res.data.message || "Berhasil menghapus roles",
          }).then(() => getDataRoles());
        });
      }
    });
  };

  return (
    <Grid padding="18px" container>
      <Grid container>
        <Autocomplete
          sx={{
            flex: 1,
          }}
          disablePortal
          id="combo-box-demo"
          options={dataRows}
          getOptionLabel={(option) => option.name}
          onChange={(a, b) => setDataSelect(b)}
          size="small"
          renderInput={(params) => (
            <TextField
              onChange={(e) => setDataChange(e.target.value)}
              {...params}
            />
          )}
        />
        {dataSelect ? (
          <Grid>
            <CustomButon.Primary sx={{ marginLeft: "12px", height: "38px" }}>
              Change Permission
            </CustomButon.Primary>
            <CustomButon.Error
              onClick={handleDelete}
              sx={{ marginLeft: "12px", height: "38px" }}
            >
              <TrashIcon fill="currentColor" />
            </CustomButon.Error>
          </Grid>
        ) : (
          <CustomButon.Primary
            onClick={handleAddRole}
            sx={{
              marginLeft: "12px",
              height: "38px",
              backgroundColor: "green",
            }}
          >
            Add new role
          </CustomButon.Primary>
        )}
      </Grid>
      <Grid
        mt="24px"
        container
        sx={{
          "& svg": {
            height: "12px",
          },
          "& .active": {
            transform: "rotate(90deg)",
          },
        }}
      >
        <TreeView
          aria-label="rich object"
          defaultCollapseIcon={<ChevronRight className="active" />}
          defaultExpanded={["root"]}
          defaultExpandIcon={<ChevronRight />}
          onNodeSelect={(a, b) => console.log(b)}
          sx={{
            height: "100%",
            flexGrow: 1,
          }}
        >
          {renderTree(dataMenu)}
        </TreeView>
      </Grid>
    </Grid>
  );
};

const ModalMenus = ({ open, onClose, choseId, getDataMenu, dataEdit }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
    setValue,
    getValues,
  } = useForm({
    defaultValues: dataEdit.id
      ? dataEdit
      : {
          M_Alt: "",
          M_Class: "",
          M_Icon: "",
          M_Index: 10,
          M_Label: "",
          M_Link: "/",
          M_Name: "",
          M_Parent: choseId === "root" ? 0 : Number(choseId),
          M_Status: "Active",
        },
  });

  console.log(getValues());

  const onSubmit = async (result) => {
    if (open === "edit") {
      Axios.put(`/Menu/${result.id}`, {
        M_Alt: result.M_Alt,
        M_Class: result.M_Class,
        M_Icon: result.M_Icon,
        M_Index: Number(result.M_Index),
        M_Label: result.M_Label,
        M_Link: result.M_Link,
        M_Name: result.M_Name,
        M_Parent: result.M_Parent,
        M_Status: result.M_Status === "Active" ? 1 : 0,
      })
        .then((res) => {
          console.log(res);
          onClose();
          getDataMenu();
        })
        .catch((err) => console.log(err));
    } else {
      Axios.post("/Menu", {
        ...result,
        M_Status: result.M_Status === "Active" ? 1 : 0,
      })
        .then((res) => {
          if (res.status === 200) {
            onClose();
            getDataMenu();
          }
        })
        .catch((err) => console.log(err));
    }
  };

  return (
    <CustomModal
      open={open}
      onClose={onClose}
      title="Form Manage Group"
      width={400}
    >
      <form onSubmit={handleSubmit(onSubmit)}>
        <Grid container spacing="12px">
          <CustomInput.InputLabel
            register={{ ...register("M_Label", { required: true }) }}
            id="M_Label"
            errors={errors}
            labelWidth={100}
            label="Label"
          />
          <CustomInput.InputLabel
            register={{ ...register("M_Name") }}
            id="M_Name"
            errors={errors}
            labelWidth={100}
            label="Name"
          />
          <CustomInput.InputLabel
            register={{ ...register("M_Link", { required: true }) }}
            id="M_Link"
            errors={errors}
            labelWidth={100}
            label="Link"
          />
          <CustomInput.InputLabel
            register={{ ...register("M_Class") }}
            labelWidth={100}
            label="Class"
          />
          <CustomInput.InputLabel
            register={{ ...register("M_Alt") }}
            labelWidth={100}
            label="Alt"
          />
          <CustomInput.InputLabel
            register={{ ...register("M_Index") }}
            labelWidth={100}
            label="Posisi"
          />
          <CustomInput.Select
            labelWidth={100}
            label="Icon"
            required
            id="M_Icon"
            value={watch("M_Icon") ? watch("M_Icon") : ""}
            register={{ ...register("M_Icon") }}
          >
            {ListMenuSidebar.map((val) => {
              return (
                <MenuItem
                  sx={{
                    "& span": {
                      marginLeft: "12px",
                    },
                    display: "flex",
                    alignItems: "center",
                  }}
                  key={val.id}
                  value={val.id}
                >
                  {val.icon} <span>{val.name}</span>
                </MenuItem>
              );
            })}
          </CustomInput.Select>
          <Grid container justifyContent="flex-end" mt="32px">
            <CustomButon.Primary type="submit">Submit</CustomButon.Primary>
          </Grid>
        </Grid>
      </form>
    </CustomModal>
  );
};
