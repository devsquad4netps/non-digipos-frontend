import React, { useState, useEffect } from "react";
import { Grid, Modal, Button } from "@mui/material";
import { useForm } from "react-hook-form";
import MenuItem from "@mui/material/MenuItem";
import Select from '@mui/material/Select';
import Header from "../../components/Header";
import TableMasterBank from "../../components/SettingsComponents/TableMasterBank";
import CustomButon from "../../components/CustomButon";
import CustomInput from "../../components/InputComponent";
import CustomInputText from "../../components/InputComponentText";
import CustomSelect from "../../components/SelectComponent";
import CustomSelectInv from "../../components/SelectComponentInv";
import CustomSelectBrand from "../../components/SelectComponentBrand";
import CustomSelectTribe from "../../components/SelectComponentTribe";
import CustomSelectMr from "../../components/SelectComponentMr";
import CustomSelectSkema from "../../components/SelectComponentSkema";
import CustomModal from "../../components/CustomModal";
import { PlusButtonIcon } from "../../assets/icons/customIcon";
import { Link } from "react-router-dom";
import master from "../../helper/services/master";
import Swal from "sweetalert2";

 
export default function MasterBank() {
  const [selectedId, setSelectedId] = useState();
  const [refresh, setRefresh] = useState(false);
  
  const [open, setOpen] = useState(false);
  const handleClose = () => {
    setOpen(false)
  }
  // const [error, setError] = useState(null);
  // const [isEdit, setIsEdit] = useState(false)

  // const [ID, setID] = useState();
  // const [CODE_BANK, setCODE_BANK] = useState();
  // const [NAME_BANK, setNAME_BANK] = useState();




  const {
    register,
    handleSubmit,
    resetField,
    trigger,
    formState: { errors },
    watch,
    control,
    setValue,
  } = useForm();

//   useEffect(() => {
//     const reqTribe = master.merchantProductTribeList().then(res => res.data.datas);
//     const reqMerchant = master.merchantList().then(res => res.data.datas);
//     const reqSchema = master.merchantProductSkemaList().then(res => res.data.datas);
//     Promise.all([reqTribe, reqMerchant, reqSchema]).then((results) => {
//       const [_tribe, _merchant, _schema] = results;
//       const trible = _tribe.map(item => { 
//         return {label: item.MPT_LABEL, value: item.MPT_ID
//       }});

//       const merchant = _merchant.map(item => { 
//           return {label: item.M_NAME, value: item.M_ID
//       }});

//       const schema = _schema.map(item => { 
//           return {label: item.MPS_LABEL, value: item.MPS_ID
//       }});

//       console.log(trible, merchant, schema);

//       setState({...state, dataTribe: trible, dataMerchant: merchant, dataSchema: schema});
//     });
//   }, []);
  const [dataBank, setDataBank] = useState([]);

  const getDataBank = async () => {
    try {
      const response = await master.masterBankList();
      if (response.data.success) {
        setDataBank(response.data.datas);
       
      }
    } catch (error) {
      console.log(error);
    }
  };


  const onSubmit = async (data) => {
    try {
      const request = selectedId ? await master.masterBankUpdate(selectedId, data) :  await master.masterBankCreate(data);
      if (request.data.success) {
        getDataBank()
        resetForm()
        handleClose()
        Swal.fire({
          icon: "success",
          title: "Success",
          text: selectedId ? "Update data success" :  "Create data success",
        });
      }
    } catch ({response: {data}}) {
      Swal.fire({
        icon: "error",
        title: "Error",
        text: data.error.message,
      });
    }
  };

  const onUpdate = (id) => {
    setTimeout(() => {
      getEditBank(id);
    }, 150);
  }

  const getEditBank = (id) => {
      setOpen(true)
      setSelectedId(id)
      master.masterBankGet(id).then(res => {
         setTimeout(() => {
            setValue("NAME_BANK", res.data.datas.NAME_BANK, true)
            setValue("CODE_BANK", res.data.datas.CODE_BANK, true)
          }, 150);
      }).catch(({message}) => {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: message,
        });
      });
  }

  const resetForm = () => {
    resetField("NAME_BANK")
    resetField("CODE_BANK")
  }

  const deleteBank = async (val) => {
    Swal.fire({
      title: "Apakah anda yakin?",
      text: "Akan menghapus data ini?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Confirm",
    }).then(async (result) => {
      if (result.isConfirmed) {
        const response = await master.masterBankDelete(val);
        if (response.data.success) {
          Swal.fire("Deleted!", "Your data has been deleted.", "success")
          getDataBank()
          handleClose()
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message,
          });
          handleClose()
        }
      }
    });
  };

  useEffect(() => {
    getDataBank();
  }, []);

  return (
    <Grid>
      <Header titles={['Settings', 'Master Product']}/>
      <Grid mt="36px" sx={{ "& a": { textDecoration: "none" } }}>
            <CustomButon.Primary
              onClick={() => {
                resetForm()
                setOpen(true)
              }}
              sx={{ "& span": { marginLeft: "8px" } }}
            >
              <PlusButtonIcon />
              <span>Add Bank</span>
            </CustomButon.Primary>
          </Grid>
      <Grid mt="38px">
        <TableMasterBank data={dataBank} editFunct={(data) => {onUpdate(data)}} deleteFunct={(data) => {deleteBank(data)}}/>
      </Grid>
      {open && (
        <CustomModal open={open} onClose={handleClose} title="Form Add Bank">
          
          <form onSubmit={handleSubmit(onSubmit)} id="submit-bank">
          <Grid container item xs={12} rowSpacing="12px">
            <CustomInput.InputLabel label="Name Bank"
                  labelWidth="110px"
                  register={{ ...register("NAME_BANK", { required: true }) }}
                  id="NAME_BANK"
                  errors={errors}
                  required />
            <CustomInput.InputLabel label="Code Bank"
                  labelWidth="110px"
                  register={{ ...register("CODE_BANK", { required: true }) }}
                  id="CODE_BANK"
                  errors={errors}
                  required />

            <Grid item justifyContent="flex-end" container gap="24px">
              <CustomButon.Primary
                size="small"
                onClick={handleClose}
                sx={{ "& span": { marginLeft: "8px" }, backgroundColor: "red" }}
              >
                Cancel{" "}
              </CustomButon.Primary>
              <CustomButon.Primary
                id="submit-bank"
                type="submit"
                size="small"
                sx={{ "& span": { marginLeft: "8px" } }}
              >
                Save
              </CustomButon.Primary>
            </Grid>
          </Grid>
          </form>
        </CustomModal>
      )}
    </Grid>
  );
}
