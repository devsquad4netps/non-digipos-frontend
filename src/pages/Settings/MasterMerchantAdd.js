import React, { useEffect } from "react";
import { Grid } from "@mui/material";

import Header from "../../components/Header";
import FormAddMerchant from "../../components/SettingsComponents/FormAddMerchant";
import master from "../../helper/services/master";
import { useParams } from "react-router-dom";

export default function MasterMerchantAdd() {
  return (
    <Grid>
      <Header titles={['Settings', 'Master Merchant', 'Merchant Add']}/>
      <Grid mt="60px">
        <FormAddMerchant />
      </Grid>
      {/* <Grid item md={12}>
        <h3>Rekening Information</h3>
        <Grid mt="36px" sx={{ "& a": { textDecoration: "none" } }}>
          <CustomButon.Primary
            title={"Add Merchant"}
            sx={{ "& span": { marginLeft: "8px" } }}
          >
            <PlusButtonIcon />
            <span>Add</span>
          </CustomButon.Primary>
        </Grid>
        <Grid mt="16px">
          <TableAddMasterMerchant />
        </Grid>
      </Grid> */}
    </Grid>
  );
}
