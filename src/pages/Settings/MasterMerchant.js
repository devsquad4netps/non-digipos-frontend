import React, { useEffect, useState } from "react";
import { Grid } from "@mui/material";

import Header from "../../components/Header";
import TableMasterMerchant from "../../components/SettingsComponents/TableMasterMerchant";
import CustomButon from "../../components/CustomButon";
import { PlusButtonIcon } from "../../assets/icons/customIcon";
import { Link } from "react-router-dom";
import master from "../../helper/services/master";

export default function MasterMerchant() {
  const [tableData, setTableData] = useState([]);
  const listMerchant = async () => {
    try {
      let req = await master.merchantList();
      setTableData(req.data.datas);
      console.log(tableData);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    listMerchant();
  }, []);
  return (
    <Grid>
      <Header titles={["Settings", "Master Merchant"]} />
      <Grid mt="36px" sx={{ "& a": { textDecoration: "none" } }}>
        <Link to="/settings/master-merchant/add">
          <CustomButon.Primary sx={{ "& span": { marginLeft: "8px" } }}>
            <PlusButtonIcon />
            <span>Add Merchant</span>
          </CustomButon.Primary>
        </Link>
      </Grid>
      <Grid mt="38px">
        <TableMasterMerchant />
      </Grid>
    </Grid>
  );
}
