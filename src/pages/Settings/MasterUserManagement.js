import React, { useState, useEffect } from "react";
import { Grid } from "@mui/material";
import { useForm } from "react-hook-form";
import Header from "../../components/Header";
import CustomInput from "../../components/InputComponent";
import CustomButon from "../../components/CustomButon";
import CustomSelectRole from "../../components/SelectComponentRole"
import TableMasterUserManagement from "../../components/SettingsComponents/TableMasterUserManagement";
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import master from "../../helper/services/master";
import Swal from "sweetalert2";
import { PlusButtonIcon } from "../../assets/icons/customIcon";
import CustomModal from "../../components/CustomModal";

export default function MasterUserManagement() {
  // const [open, setOpen] = useState();
  // const [loading, setLoading] = useState(true);
  // const [dataTribe, setDataTribe] = useState([]);
  // const [refresh, setRefresh] = useState(false);
  const [ROLE, setROLE] = useState();

  const handleClose = () => setState({...state, loading: false, open: false});

      const [isEdit, setIsEdit] = useState(false)

  const [state, setState] = useState({
    open: false,
    loading: false,
    refresh: false,
    selectedId: null,
  });

  const {open, selectedId, loading, refresh} = state;

  const {
    register,
    resetField,
    handleSubmit,
    formState: { errors },
    watch,
    control,
    setValue,
  } = useForm();

  const setLoading = () => setState({...state, loading: true});
  const setOpen = () => setState({...state, open: !open});

  const [dataUsers, setDataUsers] = useState([]);

  const getDataUser = async () => {
    setLoading(true);
    try {
      const response = await master.masterUserList();
      console.log(response)
      if (response.status == 200) {
        setDataUsers(response.data);
       
      }
    } catch (error) {
      console.log(error);
    }
  };

  const onSubmit = async (data) => {
    selectedId ?  data = { // payload data EDIT
      nik: data.nik,
      fullname: data.fullname,
      jabatan : data.jabatan,
      divisi: data.divisi,
      address: data.address,
      phone: data.phone,
      gender : data.gender,
      email: data.email,
      username: data.username,
      // password: data.password,
      role: ROLE
    }
    : 
    data = { // PAYLOAD DATA CREATE
      nik: data.nik,
      fullname: data.fullname,
      jabatan : data.jabatan,
      divisi: data.divisi,
      address: data.address,
      phone: data.phone,
      gender : data.gender,
      email: data.email,
      username: data.username,
      password: data.password,
      role: ROLE
    }
    

        console.log(data);
    setLoading();
    try {
      const request = selectedId ? await master.masterUserUpdate(selectedId, data) :  await master.masterUserCreate(data);
      if (request.status === 200 || request.data.id) {
        Swal.fire({
          icon: "success",
          title: "Success",
          text: selectedId ? "Update data success" :  "Create data success",
        });
        getDataUser()
        resetForm()
        setState({ open: false,
          loading: false,
          refresh: false,
          selectedId: null,});
      } else {
        setState({ open: false,
          loading: false,
          refresh: false,
          selectedId: null,});
        Swal.fire({
          icon: "error",
          title: "Error",
          text: request?.data?.message?.message
        });
      }
    } catch ({response: {data}}) {
      console.log(data)
      setState({ open: false,
        loading: false,
        refresh: false,
        selectedId: null,});
      Swal.fire({
        icon: "error",
        title: "Error",
        text: data.error.message,
      });
    }
  }; 

  const onUpdate = (id) => {
    setState({...state, selectedId: id});
    setTimeout(() => {
      getEditUser(id);
    }, 150);
  }

  const getEditUser = (id) => {
    console.log(id);
      setState({...state, open: true, selectedId: id});
      master.masterUserGet(id).then(res => {
        //  setTimeout(() => {
            setValue("nik", res.data.nik, true)
            setValue("fullname", res.data.fullname, true)
            setValue("jabatan", res.data.jabatan, true)
            setValue("divisi", res.data.divisi, true)
            setValue("address", res.data.address, true)
            setValue("phone", res.data.phone, true)
            setValue("gender", res.data.gender, true)
            setValue("email", res.data.email, true)
            setValue("username", res.data.username, true)
            setValue("password", res.data.password, true)
            setROLE(res.data.role)
          // }, 150);
      }).catch(({message}) => {
        Swal.fire({
          icon: "error",
          title: "Error",
          text: message,
        });
      });
  }

  const resetForm = () => {
    resetField("nik")
    resetField("fullname")
    resetField("jabatan")
    resetField("divisi")
    resetField("address")
    resetField("phone")
    resetField("gender")
    resetField("email")
    resetField("username")
    resetField("password")
    setROLE()
  }

  const deleteUser = async (val) => {
    Swal.fire({
      title: "Apakah anda yakin?",
      text: "Akan menghapus data ini?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Confirm",
    }).then(async (result) => {
      if (result.isConfirmed) {
        const response = await master.masterUserDelete(val);
        if (response.status === 204) {
          Swal.fire("Deleted!", "Your data has been deleted.", "success")
          getDataUser()
          handleClose()
        }else{
          Swal.fire({
            icon: "error",
            title: "Error",
            text: response?.data?.message,
          });
          handleClose()
        }
      }
    });
  };

  useEffect(() => {
    // console.log(control._formValues);
    getDataUser();
  }, []);

  return (
    <Grid>
      <Header titles={['Settings', 'Master Product']}/>
      <Grid mt="36px" sx={{ "& a": { textDecoration: "none" } }}>
            <CustomButon.Primary
              onClick={(e) => {
                resetForm()
                setOpen(true)
              }}
              sx={{ "& span": { marginLeft: "8px" } }}
            >
              <PlusButtonIcon />
              <span>Add User</span>
            </CustomButon.Primary>
          </Grid>
      <Grid mt="38px">
        <TableMasterUserManagement data={dataUsers} editFunct={(data) => {onUpdate(data)}} deleteFunct={(data) => {deleteUser(data)}}/>
      </Grid>
      {open && (
        <CustomModal width='800px' open={open} onClose={handleClose} title="Form Add User">
          
          <form onSubmit={handleSubmit(onSubmit)} id="submit-user">
          <Grid container spacing={5} rowSpacing={5}>
            <Grid item xs={6} rowSpacing="12px">
            <CustomInput.InputLabel label="NIK"
                  labelWidth="110px"
                  register={{ ...register("nik", { required: true }) }}
                  id="nik"
                  errors={errors}
                  required />
            <CustomInput.InputLabel label="Name"
                  labelWidth="110px"
                  register={{ ...register("fullname", { required: true }) }}
                  id="fullname"
                  errors={errors}
                  required />
            <CustomInput.InputLabel label="Jabatan"
                  labelWidth="110px"
                  register={{ ...register("jabatan", { required: true }) }}
                  id="jabatan"
                  errors={errors}
                  required />
            <CustomInput.InputLabel label="Divisi"
                  labelWidth="110px"
                  register={{ ...register("divisi", { required: true }) }}
                  id="divisi"
                  errors={errors}
                  required />
            <CustomInput.InputLabel label="Address"
                  labelWidth="110px"
                  register={{ ...register("address", { required: true }) }}
                  id="address"
                  errors={errors}
                  multiline
                  minRows={2}
                  required />
            <CustomInput.InputLabel label="Phone"
                  labelWidth="110px"
                  register={{ ...register("phone", { required: true }) }}
                  id="phone"
                  errors={errors}
                  required />
            </Grid>
            <Grid item xs={6} rowSpacing="12px">
            <CustomInput.InputLabel label="Gender"
                  labelWidth="110px"
                  register={{ ...register("gender", { required: true }) }}
                  id="gender"
                  errors={errors}
                  required />
            <CustomInput.InputLabel label="Email"
                  labelWidth="110px"
                  register={{ ...register("email", { required: true }) }}
                  id="email"
                  errors={errors}
                  required />
            {/* <CustomInput.InputLabel label="Realm"
                  labelWidth="110px"
                  register={{ ...register("realm", { required: true }) }}
                  id="realm"
                  errors={errors}
                  required /> */}
            <CustomInput.InputLabel label="Username"
                  labelWidth="110px"
                  register={{ ...register("username", { required: true }) }}
                  id="username"
                  errors={errors}
                  required />
            {(!selectedId ?  <CustomInput.InputLabel label="Password"
                  labelWidth="110px"
                  register={{ ...register("password", { required: true }) }}
                  id="password"
                  errors={errors}
                  required />
                :
                ''
                )}
           
            {/* <CustomSelectRole.SelectLabel values={watch("role")}
                    labelWidth="110px"
                    errors={errors}
                    id="role"
                    label="Role"
                    register={{
                      ...register("role", { required: true }),
                    }} /> */}
            <CustomSelectRole.SelectLabel
                    item
                    label="Role" labelWidth="110px"
                    required
                    value={(ROLE)}
                    options={[{label: 'SUPERADMIN', value:1},{label: 'BUSINESS ASSURANCE', value:2},{label: 'RECEIVABLE', value:3},{label: 'TAX', value:4}]}
                    onChange={(e)=>setROLE(e.target.value)}
                    />
            </Grid> 

            <Grid item justifyContent="flex-end" container gap="24px">
              <CustomButon.Primary
                size="small"
                onClick={setOpen}
                sx={{ "& span": { marginLeft: "8px" }, backgroundColor: "red" }}
              >
                Cancel{" "}
              </CustomButon.Primary>
              <CustomButon.Primary
                id="submit-user"
                type="submit"
                size="small"
                sx={{ "& span": { marginLeft: "8px" } }}
              >
                Save
              </CustomButon.Primary>
            </Grid>
          </Grid>
          </form>
        </CustomModal>
      )}
    </Grid>
  );
}